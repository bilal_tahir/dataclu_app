-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 25, 2020 at 04:11 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tea_dev`
--

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `section` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `image`, `section`, `created_at`, `updated_at`) VALUES
(1, 'g24_1582642465.jpg', 'homeslider', '2020-02-24 16:08:31', '2020-02-25 09:54:25'),
(2, 'slider2_1582642377.jpg', 'homeslider', '2020-02-24 16:08:31', '2020-02-25 09:52:57'),
(3, 'g25_1582642859.jpg', 'homeslider', '2020-02-24 16:10:23', '2020-02-25 10:00:59'),
(4, 'about_part_img_1582642888.png', 'aboutfirst', '2020-02-24 16:10:23', '2020-02-25 10:01:28'),
(5, 'g30_1582642985.png', 'servicefirst', '2020-02-24 16:37:29', '2020-02-25 10:03:05'),
(6, 'g36_1582643084.png', 'aboutsecond', '2020-02-24 16:37:29', '2020-02-25 10:04:44'),
(7, 'g32_1582643099.png', 'servicesecond', '2020-02-24 16:38:58', '2020-02-25 10:04:59'),
(8, 's1_1582643202.png', 'client', '2020-02-24 16:41:40', '2020-02-25 10:06:42'),
(9, 's10_1582643215.png', 'client', '2020-02-24 16:41:40', '2020-02-25 10:06:55'),
(10, 's3_1582643253.png', 'client', '2020-02-24 16:42:27', '2020-02-25 10:07:33'),
(11, 's4_1582643268.png', 'client', '2020-02-24 16:42:27', '2020-02-25 10:07:48'),
(12, 's5_1582643281.png', 'client', '2020-02-24 16:43:34', '2020-02-25 10:08:01'),
(13, 's11_1582643293.png', 'client', '2020-02-24 16:43:34', '2020-02-25 10:08:13'),
(14, 's7_1582643322.png', 'client', '2020-02-24 16:44:17', '2020-02-25 10:08:42'),
(15, 's8_1582643339.png', 'client', '2020-02-24 16:44:17', '2020-02-25 10:08:59'),
(16, 'asset/img/newgallery/g28.jpg', 'serviceslider', '2020-02-24 17:50:46', '2020-02-24 17:50:46'),
(17, 'asset/img/newgallery/g35.png', 'servicethird', '2020-02-24 18:04:23', '2020-02-24 18:04:23'),
(18, 'asset/img/newgallery/g21.jpg', 'aboutslider', '2020-02-24 18:19:39', '2020-02-24 18:19:39'),
(19, 'asset/img/newgallery/g12.jpg', 'gallery', '2020-02-25 05:17:05', '2020-02-25 05:17:05'),
(20, 'asset/img/newgallery/g5.jpg', 'gallery', '2020-02-25 05:17:05', '2020-02-25 05:17:05'),
(21, 'asset/img/newgallery/g23.jpg', 'gallery', '2020-02-25 05:17:53', '2020-02-25 05:17:53'),
(22, 'asset/img/newgallery/g21.jpg', 'gallery', '2020-02-25 05:23:11', '2020-02-25 05:23:11'),
(23, 'asset/img/newgallery/g8.jpg', 'gallery', '2020-02-25 05:17:53', '2020-02-25 05:17:53'),
(24, 'asset/img/newgallery/g9.jpg', 'gallery', '2020-02-25 05:18:41', '2020-02-25 05:18:41'),
(25, 'asset/img/newgallery/g10.jpg', 'gallery', '2020-02-25 05:18:41', '2020-02-25 05:18:41'),
(26, 'asset/img/newgallery/g11.jpg', 'gallery', '2020-02-25 05:19:29', '2020-02-25 05:19:29'),
(27, 'asset/img/newgallery/g22.jpg', 'gallery', '2020-02-25 05:19:29', '2020-02-25 05:19:29'),
(28, 'asset/img/newgallery/g1.jpg', 'gallery', '2020-02-25 05:24:35', '2020-02-25 05:24:35'),
(29, 'asset/img/newgallery/g2.jpg', 'gallery', '2020-02-25 05:24:35', '2020-02-25 05:24:35'),
(30, 'asset/img/newgallery/g3.jpg', 'gallery', '2020-02-25 05:25:30', '2020-02-25 05:25:30'),
(31, 'asset/img/newgallery/g17.jpg', 'gallery', '2020-02-25 05:25:30', '2020-02-25 05:25:30'),
(32, 'asset/img/newgallery/g6.jpg', 'gallery', '2020-02-25 05:26:30', '2020-02-25 05:26:30'),
(33, 'asset/img/newgallery/g10.jpg', 'careerslider', '2020-02-25 05:39:19', '2020-02-25 05:39:19'),
(34, 'asset/img/newgallery/g34.jpg', 'contactslider', '2020-02-25 06:02:34', '2020-02-25 06:02:34'),
(35, 'asset/img/newgallery/team_img1.jpg', 'teamslider', '2020-02-25 06:10:04', '2020-02-25 06:10:04'),
(36, 'asset/img/testimonial_img_1.png', 'teamfirst', '2020-02-25 06:10:04', '2020-02-25 06:10:04'),
(37, 'asset/img/testimonial_img_2.png', 'teamsecond', '2020-02-25 06:10:56', '2020-02-25 06:10:56'),
(38, 'asset/img/testimonial_img_3.png', 'teamthird', '2020-02-25 06:10:56', '2020-02-25 06:10:56'),
(39, 'asset/img/newgallery/g38.jpg', 'projectslider', '2020-02-25 13:12:52', '2020-02-25 13:12:52'),
(40, 'asset/img/newgallery/g39.jpg', 'projectdetailslider', '2020-02-25 13:13:53', '2020-02-25 13:13:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
