<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

//Api For Getting user viewid and searchconsole in JSON Format
Route::get('get/user/consoled-data/{id}', 'APIController@getUserConsoleData');


Route::post('login', 'APIController@login');
Route::post('register', 'APIController@register');
// Route::group(['middleware' => 'auth:api'], function(){
//Api for checking that the token is expired or not
Route::post('validateaccesstoken', 'APIController@validateAccessToken');

//Api For Refreshing Access Token
Route::post('refreshaccesstoken', 'APIController@refreshAccessToken');

//Api For Getting user Data in JSON Format
Route::get('getuserdata/{id}', 'APIController@getUserData');

//Api For Getting All user Data in JSON Format
// Route::get('getusers', 'APIController@getAllUserData');

//Logout API
Route::get('/logout', 'APIController@logout')->name('logout');
// });

//Endpoint for corelation 
Route::post('/analytics_corelate_matrics', 'APIController@corRelationValues');
//Endpoint for analytics and console data
Route::post('/analytics_console_data', 'APIController@AnalyticsConsoleData');

//Endpoint For Active Users
Route::get('/get_active_users', 'APIController@getActiveUsers');
