<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
| Middleware options can be located in `app/Http/Kernel.php`
|
*/
//Open route for share link
Route::get('guest/anomaly/detail/{id}','GuestController@guestpagereq');
//This route is same as above but for guest user 
Route::get('/guest/anomaly/detail/get_save_anomaly_data/{dataResponse}/{anomalyId}','APIController@getSaveAnomalyData');

Route::get('/test/jsonFile','APIController@jsonFile');

// Homepage Route
Route::group(['middleware' => ['web', 'checkblocked']], function () {
    // Route::get('/', 'WelcomeController@welcome')->name('welcome');
    Route::get('/terms', 'TermsController@terms')->name('terms');
});

// Authentication Routes
Auth::routes();
Route::get('/admin/login','Auth\LoginController@adminlogin');

// Public Routes
Route::group(['middleware' => ['web', 'activity', 'checkblocked']], function () {

    // Activation Routes
    Route::get('/activate', ['as' => 'activate', 'uses' => 'Auth\ActivateController@initial']);

    Route::get('/activate/{token}', ['as' => 'authenticated.activate', 'uses' => 'Auth\ActivateController@activate']);
    Route::get('/activation', ['as' => 'authenticated.activation-resend', 'uses' => 'Auth\ActivateController@resend']);
    Route::get('/exceeded', ['as' => 'exceeded', 'uses' => 'Auth\ActivateController@exceeded']);

    // Socialite Register Routes
    Route::get('/social/redirect/{provider}', ['as' => 'social.redirect', 'uses' => 'Auth\SocialController@getSocialRedirect']);
    Route::get('/social/handle/{provider}', ['as' => 'social.handle', 'uses' => 'Auth\SocialController@getSocialHandle']);

    // Non-Socialite Routes Officially Used For Google Login (Not in Package)
    Route::get('/google/login', 'Auth\SocialController@googlecall')->name('google/login');
    Route::get('/oauth2callback','Auth\SocialController@redirectoAuth')->name('oAuth');

    //Route For Guest User
    Route::get('/guestlogin','Auth\SocialController@guestLogin')->name('guestlogin');
    Route::post('/guestlogin','Auth\SocialController@guestLoginPost')->name('guestlogin');

    // Route to for user to reactivate their user deleted account.
    Route::get('/re-activate/{token}', ['as' => 'user.reactivate', 'uses' => 'RestoreUserController@userReActivate']);
});

// Registered and Activated User Routes
Route::group(['middleware' => ['auth', 'activated', 'activity', 'checkblocked']], function () {

    // Activation Routes
    Route::get('/activation-required', ['uses' => 'Auth\ActivateController@activationRequired'])->name('activation-required');
    Route::get('/logout', ['uses' => 'Auth\LoginController@logout'])->name('logout');
});

// Registered and Activated User Routes
Route::group(['middleware' => ['auth', 'activated', 'activity', 'twostep', 'checkblocked']], function () {

    //  Homepage Route - Redirect based on user role is in controller.
    Route::get('/home', ['as' => 'public.home',   'uses' => 'UserController@index']);

    // Show users profile - viewable by other users.
    Route::get('profile/{username}', [
        'as'   => '{username}',
        'uses' => 'ProfilesController@show',
    ]);

    // New Routes
// Route::get('/', 'Auth\SocialController@homecall')->name('/');

// All anomalies in one page route
Route::get('/all/anomalies/view', 'APIController@allAnomaliesData');

// Dashboard Routes
Route::get('/sales/monitoring', function () {
    return view('admin.dashboard.sales_monitoring');
});
Route::get('/website/analytics', function () {
    return view('admin.dashboard.website_analytics');
});

Route::get('/cryptocurrency', function () {
    return view('admin.dashboard.cryptocurrency');
});
Route::get('/helpdesk/management', function () {
    return view('admin.dashboard.helpdesk_management');
});


//App Routes
Route::get('/calender', function () {
    return view('admin.app.calender');
});

Route::get('/chat', function () {
    return view('admin.app.chat');
});
Route::get('/contact', function () {
    return view('admin.app.contact');
});
Route::get('/file/manager', function () {
    return view('admin.app.file_manager');
});
Route::get('/mail', function () {
    return view('admin.app.mail');
});

//Analytics UI
Route::get('/analyticsui', function () {
    return view('analyticsUI');
});

// route for update api
// Route::get('/updateApi/daily','APIController@updateApiDbDaily');
// Route::get('/updateApi/weekly','APIController@updateApiDbWeekly');
// Route::get('/updateApi/data/daily','APIController@updateApiDbDataDaily');
// Route::get('/updateApi/data/weekly','APIController@updateApiDbDataWeekly');

// New Routes
Route::get('update/data/daily','APIController@updateDataDailyNew');
Route::get('update/data/weekly','APIController@updateDataWeeklyNew');

//Shoppify Routes
// Route::get('shopify/authentication','APIController@ShopifyAuthIndex');
// Route::get('shopify/authentication/callback','APIController@ShopifyAuthRedirect');
//Shopify Routes End

// route for fetching api data
Route::get('/user/console/getData/{domain}/{type}/{metric}','APIController@getApiDataDb');
Route::get('/ga/getData/{domain}/{type}/{metric}','APIController@getApiDataDb');

//Route for fetching all anomalies of a user
Route::get('/getData/alluser/{dataType}','APIController@getApiAllAnomalies');

// Route::get('user/console/data','Auth\SocialController@consoleDataDisplay');

Route::get('/anomaly/detail/get_save_anomaly_data/{dataResponse}/{anomalyId}','APIController@getSaveAnomalyData');
// //This route is same as above but for guest user 
// Route::get('/guest/anomaly/detail/get_save_anomaly_data/{dataResponse}/{anomalyId}','APIController@getSaveAnomalyData');

//Route for fetvhing correlation metrics
Route::get('/getData/correlation/metric','APIController@getCorrelatedData');

//route for optimized data
Route::get('/optimized/data/{id}/{domain}','APIController@optimizedData');
// Route::get('/htmlui', function () {
//     return view('htmlui');
// });

//Route For Share Email To Create New User
Route::post('/console/analytics/share/data','UserController@consoleAnalyticsShareData')
->name('consoleAnalytics.shareData');

//Route For saving shopify record
Route::post('save_shopify_Detail', 'UserController@saveshopifyDetail')->name('save.shopifyDetail');

//Route for analytics view home page
// Route::get('/user/analytics/data','AnalyticsViewController@homePage');

Route::get('/anomaly/detail/{id}','UserController@anomalyId');
Route::post('anomaly/detail/delete/share/url', 'SoftDeletesController@deleteshareurl')->name('deleteShareUrl');
Route::post('anomaly/detail/create/share/url', 'SendEmailController@createshareurl')->name('createShareUrl');

Route::post('/enable/disable/services', 'UserController@enableDisable')->name('enableServices.update');

//Route for checking user newly added website on analytics or console
Route::get('/update/analytics/list','Auth\SocialController@updateAnalyticsList')->name('updateUserAnalytics');
Route::get('/update/console/list','Auth\SocialController@updateConsoleList')->name('updateUserConsole');

//Route for anomaly detail page feedback
// Route::post('anomaly/detail/feedback','FeedbackController@submitfeedback');
Route::post('anomaly/detail/feedback/comment','FeedbackController@submitfeedbackcomment');

//Sending Email Route
Route::post('send-mail', 'SendEmailController@sendemail');

//Below routes for ga Page
// Route::get('/ga/reports','APIController@gaIndex');
// Route::get('/ga/reports/updateApidbWeekly','APIController@gaupdateApiDbWeekly');
// Route::get('/ga/reports/updateApidbDaily','APIController@gaupdateApiDbDaily');
// Route::get('/ga/reports/updatedbWeekly','APIController@gaUpdateDbDaily');
// Route::get('/ga/reports/updatedbDaily','APIController@gaUpdateDbWeely');

Route::post('anomaly/detail/send/slack/notification','UserController@slackMessage');
// Notification::route('slack', env('SLACK_HOOK'))
//       ->notify(new SlackNotification());

Route::get('/view', function () {
    return view('viewsecond');
});

});

Route::group(['middleware' => ['auth', 'activated', 'activity', 'role:user','twostep', 'checkblocked']], function () {
    Route::get('/', 'Auth\SocialController@homecall')->name('/'); //77
    Route::get('/ga/reports','APIController@gaIndex'); //181
    Route::get('/user/analytics/data','AnalyticsViewController@homePage'); //161
    Route::get('user/console/data','Auth\SocialController@consoleDataDisplay'); //143
    Route::get('user/profile','UserController@userProfileIndex'); //205
});

// Rpoute for user profile
// Route::get('user/profile','UserController@userProfileIndex');
Route::get('user/console/list','UserController@userAllConsole')->name('console.list');
Route::post('console/default','UserController@consoleDefault')->name('console.update');

Route::post('console/delete','UserController@consoleDelete')->name('console.delete');

// Analytics List Url
Route::get('user/analytics/list','UserController@userAllAnalytics')->name('analytics.list');
Route::post('analytics/default','UserController@analyticsDefault')->name('analytics.update');

Route::post('analytics/delete','UserController@analyticsDelete')->name('analytics.delete');

// Registered, activated, and is current user routes.
Route::group(['middleware' => ['auth', 'activated', 'currentUser', 'activity', 'twostep', 'checkblocked']], function () {

    // User Profile and Account Routes
    Route::resource(
        'profile',
        'ProfilesController',
        [
            'only' => [
                'show',
                'edit',
                'update',
                'create',
            ],
        ]
    );
    Route::put('profile/{username}/updateUserAccount', [
        'as'   => '{username}',
        'uses' => 'ProfilesController@updateUserAccount',
    ]);
    Route::put('profile/{username}/updateUserPassword', [
        'as'   => '{username}',
        'uses' => 'ProfilesController@updateUserPassword',
    ]);
    Route::delete('profile/{username}/deleteUserAccount', [
        'as'   => '{username}',
        'uses' => 'ProfilesController@deleteUserAccount',
    ]);

    // Route to show user avatar
    Route::get('images/profile/{id}/avatar/{image}', [
        'uses' => 'ProfilesController@userProfileAvatar',
    ]);

    // Route to upload user avatar.
    Route::post('avatar/upload', ['as' => 'avatar.upload', 'uses' => 'ProfilesController@upload']);
});

// Registered, activated, and is admin routes.
Route::group(['middleware' => ['auth', 'activated', 'role:admin', 'activity', 'twostep', 'checkblocked']], function () {
    Route::resource('/users/deleted', 'SoftDeletesController', [
        'only' => [
            'index', 'show', 'update', 'destroy',
        ],
    ]);

    Route::resource('users', 'UsersManagementController', [
        'names' => [
            'index'   => 'users',
            'destroy' => 'user.destroy',
        ],
        'except' => [
            'deleted',
        ],
    ]);
    Route::post('search-users', 'UsersManagementController@search')->name('search-users');

    Route::resource('themes', 'ThemesManagementController', [
        'names' => [
            'index'   => 'themes',
            'destroy' => 'themes.destroy',
        ],
    ]);

    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    Route::get('routes', 'AdminDetailsController@listRoutes');
    Route::get('active-users', 'AdminDetailsController@activeUsers');
});

Route::redirect('/php', '/phpinfo', 301);

Route::get('get-curl', 'WelcomeController@getCURL');