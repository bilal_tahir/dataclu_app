<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Profile;
use App\Models\Social;
use App\Models\User;
use App\Models\AnalyticsAccount;
use App\Models\Jobid;
use App\Models\ConsoleList;
use App\Traits\ActivationTrait;
use App\Traits\CaptureIpTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use jeremykenedy\LaravelRoles\Models\Role;
use Laravel\Socialite\Facades\Socialite;
use Google_Client;
use Google_Service_Analytics;
use Google_Service_Oauth2;
use Auth;
use Redirect;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
//code for console
use Google_Service_Webmasters;
use Google_Service_Webmasters_SearchAnalyticsQueryRequest;
use Illuminate\Support\Facades\Hash;
use App\Models\ShareWebsite;

class SocialController extends Controller
{
  use ActivationTrait;

  private $redirectSuccessLogin = 'home';
  /**
   * Non-Socialite Functions. Used to send call for google authentication.
   *
   * @param string $provider The provider
   * @param \Illuminate\Http\Request $request
   *
   * @return \Illuminate\Http\Response
   */


  public function homecall()
  {
    //Starting session for User
    session_start();
    $client = new Google_Client();
    $client->setAuthConfig(storage_path('/analytics/client_secrets.json'));

    //Checking if session has access token
      if(isset($_SESSION['access_token']))
      {
        //Setting user accesstoken using token stored in session
        $client->setAccessToken($_SESSION['access_token']);
        
        //Checking the access token of user is expired or not

        //If access token of the user is expire it will refresh it using refresh token of the user
        if ($client->isAccessTokenExpired()) {
          $userToken = User::where('google_id', $_SESSION['google_id'])->first();
          $rt = $client->refreshToken($userToken->refresh_token);

              // if error exists in refreshing access token
              if(array_key_exists("error",$rt))
              {
                Auth::logout();
                Session::flush();
              }

          // updating access token in session and database 
          $_SESSION['access_token'] = $client->getAccessToken();
          $client->setAccessToken($_SESSION['access_token']);
          $accesToken = ($_SESSION['access_token']);
              if(isset( $accesToken['refresh_token']))
              {
                User::where('access_token', $userToken->access_token)->update([
                  'access_token' => $accesToken['access_token'],
                  'expires_in' => $accesToken['expires_in'],
                  'token_created' => $accesToken['created'],
                  'refresh_token' => $accesToken['refresh_token']
                ]);
              }
              else
              {
                User::where('access_token', $userToken->access_token)->update([
                  'access_token' => $accesToken['access_token'],
                  'expires_in' => $accesToken['expires_in'],
                  'token_created' => $accesToken['created']
                ]);
              }
        
        }

        //getting authenticated user id from db
        $authUserID = Auth::id();
        
        // Getting all the websites of the user linked with google console from db
        $websitesList = ConsoleList::where('user_id',$authUserID)->get();
        $analyticsConsoleListArray = [];
        //rearranging all the console list
            foreach ($websitesList as $consoleListGet)
            {
              $newArray = [];
              $consoleUrlGet = $consoleListGet->siteUrl;
              $explodedConsoleUrl = explode('sc-domain:',$consoleUrlGet);
              $finalConsoleUrlSlash = "www.".$explodedConsoleUrl[1];
              $finalConsoleUrl = str_replace("/","",$finalConsoleUrlSlash);
              $newArray['website'] = $finalConsoleUrl;
              $newArray['display'] = $explodedConsoleUrl[1];
              if($consoleListGet->default_value == 1)
              {
                $newArray['default'] = 1;
              }
              else
              {
                $newArray['default'] = 0;
              }

              array_push($analyticsConsoleListArray,$newArray);
              
            }
        
        // Getting user analytics websites from db
        $websitesListAnalytics = AnalyticsAccount::where('user_id',$authUserID)->get();
        //rearranging all the analytics and previous console lists
            foreach ($websitesListAnalytics as $analyticsListGet)
            {
              $analyticsNewArray = [];
              $ab = 0;
              $def = 0;
              $analyticsUrlGet = $analyticsListGet->Property_Url;
              $explodedAnalyticsUrl = explode('://',$analyticsUrlGet);
              if(strpos($explodedAnalyticsUrl[1], 'www.') !== false )
              {
                $finalAnalyticsUrlSlash = $explodedAnalyticsUrl[1];
              }
              else
              {
              $finalAnalyticsUrlSlash = "www.".$explodedAnalyticsUrl[1];
              }
              $finalAnalyticsUrl = str_replace("/","",$finalAnalyticsUrlSlash);
              
              foreach($analyticsConsoleListArray as $checkweb)
              {
                if($checkweb['website'] == $finalAnalyticsUrl)
                {
                  $ab = 1;
                }
              }
              if($ab != 1)
              {
                $analyticsNewArray['website'] = $finalAnalyticsUrl;
                $analyticsNewArray['display'] = $explodedAnalyticsUrl[1];
                foreach($analyticsConsoleListArray as $checkdef)
                  {
                    if($checkdef['default'] == 1)
                    {
                      $def = 1;
                    }
                  }
                  if($def == 1)
                  {
                    
                    $analyticsNewArray['default'] = 0;
                  }
                  else
                  {
                    if($analyticsListGet->default_value == 1)
                      {
                        $analyticsNewArray['default'] = 1;
                      }
                      else
                      {
                        $analyticsNewArray['default'] = 0;
                      }
                  }
                array_push($analyticsConsoleListArray,$analyticsNewArray);
              }
            
            }


        //First get a single last record of user 
        $userLatestField = Jobid::where('user_id',$authUserID)->latest()->first();
    
        //Getting Job IDs To check the status of data before loading
        $userJobId = Jobid::where('user_id',$authUserID)->get();
        $userj_id = array();
        foreach($userJobId as $jid)
        {
          $userj_id[$jid->propertyUrl] = $jid->updateAnomaly_JobId;
        }
        
        //Returning View With All The Data
        return view('admin.allAnomaliesView')->with([
          'webList' => $websitesList,
          'userSingleId' => $authUserID,
          'userJid' => $userj_id,
          'consoleAnalyticsList' => $analyticsConsoleListArray,
        ]);

    }
    // if session does not have access token the redirect it towards authentication
  else
    {
      return redirect()->route('oAuth');
    }
}
  public function googlecall()
  {
  
    session_start();

    //Checking if token is set in a session else redirect to google login
    if (isset($_SESSION['access_token']) && $_SESSION['access_token'] && isset($_SESSION['google_id'])  && Auth::check()) {
      return redirect()->route('/');
    } 
    // If User Session is not set then redirect to authentication
    else
    {
      return redirect()->route('oAuth');
    }
  }

  function getList($analytics)
  {
    try {
    
      $accounts = $analytics->management_accountSummaries
        ->listManagementAccountSummaries();
    
    } catch (apiServiceException $e) {
      print 'There was an Analytics API service error '
        . $e->getCode() . ':' . $e->getMessage();
    } catch (apiException $e) {
      print 'There was a general API error '
        . $e->getCode() . ':' . $e->getMessage();
    }


    foreach ($accounts->getItems() as $account) {
      $html = <<<HTML
  <pre>
  Account id   = {$account->getId()}
  Account kind = {$account->getKind()}
  Account name = {$account->getName()}
  HTML;

      // Iterate through each Property.
      foreach ($account->getWebProperties() as $property) {
        $html .= <<<HTML
  Property id          = {$property->getId()}
  Property kind        = {$property->getKind()}
  Property name        = {$property->getName()}
  Internal property id = {$property->getInternalWebPropertyId()}
  Property level       = {$property->getLevel()}
  Property URL         = {$property->getWebsiteUrl()}
  HTML;

        // Iterate through each view (profile).
        foreach ($property->getProfiles() as $profile) {
          $html .= <<<HTML
  Profile id   = {$profile->getId()}
  Profile kind = {$profile->getKind()}
  Profile name = {$profile->getName()}
  Profile type = {$profile->getType()}
  HTML;
        }
      }
      $html .= '</pre>';
      print $html;
    }
  }

  function getFirstProfileId($analytics)
  {
    // Get the user's first view (profile) ID.

    // Get the list of accounts for the authorized user.
    $accounts = $analytics->management_accounts->listManagementAccounts();

    if (count($accounts->getItems()) > 0) {
      $items = $accounts->getItems();
      $firstAccountId = $items[0]->getId();

      // Get the list of properties for the authorized user.
      $properties = $analytics->management_webproperties
        ->listManagementWebproperties($firstAccountId);

      if (count($properties->getItems()) > 0) {
        $items = $properties->getItems();
        $firstPropertyId = $items[0]->getId();

        // Get the list of views (profiles) for the authorized user.
        $profiles = $analytics->management_profiles
          ->listManagementProfiles($firstAccountId, $firstPropertyId);

        if (count($profiles->getItems()) > 0) {
          $items = $profiles->getItems();

          // Return the first view (profile) ID.
          return $items[0]->getId();
        } else {
          throw new Exception('No views (profiles) found for this user.');
        }
      } else {
        throw new Exception('No properties found for this user.');
      }
    } else {
      throw new Exception('No accounts found for this user.');
    }
  }

  // Query For Getting Session
  function getSession($analytics, $profileId)
  {
    return $analytics->data_ga->get(
      'ga:' . $profileId,
      '30daysAgo',
      'today',
      'ga:sessions',
      array(
        'dimensions'  => 'ga:date',
        'sort'        => '-ga:sessions',
      )
    );
   
  }

  // Query For Getting PAgeView Against Country
  function getCountry($analytics, $profileId)
  {
    return $analytics->data_ga->get(
      'ga:' . $profileId,
      '30daysAgo',
      'today',
      'ga:pageviews',
      array(
        'dimensions'  => 'ga:date,ga:country',
        'sort'        => '-ga:pageviews',
      )
    );
  }

  //Query For Getting User Per Country 
  function getCountryData($analytics, $profileId)
  {
    return $analytics->data_ga->get(
      'ga:' . $profileId,
      '30daysAgo',
      'today',
      'ga:pageviews',
      array(
        'dimensions'  => 'ga:country', // Here Is the difference
        'sort'        => '-ga:pageviews',
      )
    );
  }

  //Query For Getting Users Record
  function getPage($analytics, $profileId)
  {
    return $analytics->data_ga->get(
      'ga:' . $profileId,
      '30daysAgo',
      'today',
      'ga:users',
      array(
        'dimensions'  => 'ga:date',
        'sort'        => '-ga:users',
      )
    );
  }

  //Query For Getting Bounce RAte Record
  function getBounce($analytics, $profileId)
  {
    return $analytics->data_ga->get(
      'ga:' . $profileId,
      '30daysAgo',
      'today',
      'ga:bounceRate',
      array(
        'dimensions'  => 'ga:date',
        'sort'        => '-ga:bounceRate',
      )
    );
  }

  function printResults($results)
  {
    // Parses the response from the Core Reporting API and prints
    // the profile name and total sessions.
    if (count($results->getRows()) > 0) {

      // dd(count($results->getRows()));

      // Get the profile name.
      $profileName = $results->getProfileInfo()->getProfileName();
      // dd($profileName);

      // Get the entry for the first entry in the first row.
      $rows = $results->getRows();
      // dd($rows);
      $sessions = $rows[0][0];



      return redirect()->route('/');
      // Print the results.
      print "<p>First view (profile) found: $profileName</p>";
      print "<p>Total sessions: $sessions</p>";
    } else {
      print "<p>No results found.</p>";
    }
  }

  /**
   * Non-Socialite Function. Call Back Function From Google
   *
   * @param string $provider The provider
   * @param \Illuminate\Http\Request $request
   *
   * @return \Illuminate\Http\Response
   */
  public function redirectoAuth()
  {

    session_start();
    $client = new Google_Client();
    $client->setAuthConfig(storage_path('/analytics/client_secrets.json'));
    $client->setRedirectUri('https://' . $_SERVER['HTTP_HOST'] . '/oauth2callback');
    // Code For Not Asking User's for Permissions every Time!
    
    $client->setAccessType('offline');
    $client->setApprovalPrompt('auto');
    $client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);
    $client->addScope(Google_Service_Oauth2::USERINFO_EMAIL);
    $client->addScope(Google_Service_Oauth2::USERINFO_PROFILE);
    $client->addScope('https://www.googleapis.com/auth/webmasters');

    //code testing console
    $webmastersService = new Google_Service_Webmasters($client);
  $searchanalytics = $webmastersService->searchanalytics;

    if (!isset($_GET['code'])) {
      $auth_url = $client->createAuthUrl();
      return redirect($auth_url);
    } else {
      $client->authenticate($_GET['code']);
      //Setting access token in session
      $_SESSION['refresh_token'] = $client->getRefreshToken();
      $_SESSION['access_token'] = $client->getAccessToken();
      $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
      
      $_SESSION['token_uri'] = $client->setRedirectUri($redirect_uri);

      
    $client->setAccessToken($_SESSION['access_token']);

      //assinging tokens in variables for ease in use
      $accessToken = ($_SESSION['access_token']);
      $refreshToken = ($_SESSION['refresh_token']);

      //Getting USer Profile Information
      $userProfile = new Google_Service_Oauth2($client);
      $userInfo = $userProfile->userinfo->get();
      //Setting User Google Id in a Session as it would be unique and would be used to refresh access token
      $_SESSION['google_id']  =  $userInfo->id;
      $userFind  = User::where('email', $userInfo->email)->first();
      
      //If User is not existed in database then add it
      if (!$userFind) {
        $profile = new Profile();
        $role = Role::where('slug', '=', 'user')->first();
        $fullname = explode(' ', $userInfo->name);
        if (count($fullname) == 1) {
          $fullname[1] = '';
        }

        $username = str_replace(' ', '', $userInfo->name);

        $user = new User();
        $user->name = $username;
        $user->first_name = $fullname[0];
        $user->last_name = $fullname[1];
        $user->google_id = $userInfo->id;
        $user->email = $userInfo->email;
        $user->avatar = $userInfo->picture;
        $user->activated = true;
        $user->access_token = $accessToken['access_token'];
        $user->expires_in = $accessToken['expires_in'];
        $user->token_created = $accessToken['created'];
        $user->refresh_token = $refreshToken;
        $user->save();

        $user->attachRole($role);
        $user->activated = true;

        $user->profile()->save($profile);
        $user->save();
        $socialUser = $user;

        //Further Code For Saving User Analytics Record
        $userId  = User::where('email',$userInfo->email)->first();
        $analyticsUser = new Google_Service_Analytics($client);
        try
        {
          $accounts = $analyticsUser->management_accountSummaries
                      ->listManagementAccountSummaries();
        }
        catch (apiServiceException $e)
        {
          print 'There was an Analytics API service error '
            . $e->getCode() . ':' . $e->getMessage();
        }
        catch (apiException $e) 
        {
          print 'There was a general API error '
            . $e->getCode() . ':' . $e->getMessage();
        }

        if(count($accounts) > 0)
        {
          $loopCount = 0;
    foreach ($accounts->getItems() as $account) {

      $analyticsRecord = new AnalyticsAccount();
      $analyticsRecord->user_id = $userId->id;
      if ($loopCount == 0)
      {
        $analyticsRecord->default_value = 1;
      }

  
  $analyticsRecord->Account_Id  = $account->getId();
  $analyticsRecord->Account_Kind = $account->getKind();
  $analyticsRecord->Account_Name = $account->getName();

      // Iterate through each Property.
      foreach ($account->getWebProperties() as $property) {
  
  $analyticsRecord->Property_Id         = $property->getId();
  $analyticsRecord->Property_Kind         = $property->getKind();
  $analyticsRecord->Property_Name       = $property->getName();
  $analyticsRecord->Internal_Property_Id = $property->getInternalWebPropertyId();
  $analyticsRecord->Property_Level       = $property->getLevel();
  $analyticsRecord->Property_Url        = $property->getWebsiteUrl();

        // Iterate through each view (profile).
        foreach ($property->getProfiles() as $profile) {
  $analyticsRecord->ViewId  = $profile->getId();
  $analyticsRecord->Profile_Kind = $profile->getKind();
  $analyticsRecord->Profile_Name = $profile->getName();
  $analyticsRecord->Profile_Type = $profile->getType();


        }
      }
      $analyticsRecord->save();
      $loopCount++;
    }
  }

    //code for getting user console urls list
    $service = new Google_Service_Webmasters($client);
    $consoleUrl = $service->sites->listSites()->getSiteEntry();
    $secondloopCount = 0;
    //1
    // Save User Credentials
    $endpoint = 'https://ai.dataclu.com/saveUserCredentials';
    $options = [
        'json' => [
          'user_id' => $userId->id,
              ],
         ];

        $client12 = new \GuzzleHttp\Client();
       $res12 = $client12->post($endpoint, $options);
       $checFirstResponse = json_decode($res12->getBody(),true);
         
         //end
    foreach($consoleUrl as $consoleData)
    {
      // Comment Starts Here

      // $accountURLs = array();
      //         // $c_url = (explode("/",$uA->Property_Url));
      //         // $comp_url = "sc-domain:".$c_url[2];
      //         array_push($accountURLs,$consoleData->siteUrl);
      // // print_r($consoleData->permissionLevel);
      // // print_r($consoleData->siteUrl);

      // Comment Ends Here

      $srConsole = new ConsoleList();
      $srConsole->user_id = $userId->id;
      if(strpos($consoleData->siteUrl, 'www.') !== false )
      {
        $explSiteUrl = explode('www.',$consoleData->siteUrl);
        $finalSiteUrl = 'sc-domain:'.$explSiteUrl[1];
      }
      elseif (strpos($consoleData->siteUrl, 'https://') !== false  ) 
      {
        $explSiteUrl = explode('https://',$consoleData->siteUrl);
        $finalSiteUrl = 'sc-domain:'.$explSiteUrl[1];
      }
      elseif (strpos($consoleData->siteUrl, 'http://') !== false  ) 
      {
        $explSiteUrl = explode('http://',$consoleData->siteUrl);
        $finalSiteUrl = 'sc-domain:'.$explSiteUrl[1];
      }
      else
      {
        $finalSiteUrl = $consoleData->siteUrl;
      }
      if(strpos($finalSiteUrl, '/') !== false)
      {
        $explSiteUrlSecond = explode('/',$finalSiteUrl);
        $finalSiteUrl = $explSiteUrlSecond[0];
      }
      $srConsole->siteUrl = $finalSiteUrl;
      // $srConsole->siteUrl = $consoleData->siteUrl;
      $srConsole->permissionLevel = $consoleData->permissionLevel;
      if ($secondloopCount == 0)
      {
        $srConsole->default_value = 1;
      }
      $srConsole->save();

     
                // Comment Ends hHere



      $secondloopCount++;
    }
    
    //code end
    


    $userAnalytics = AnalyticsAccount::where('user_id',$userId->id)->get();

  
  // New Code For Save Anomalies Call

            $sourceArray = [];
            $allUserAnalytics = AnalyticsAccount::where('user_id',$userId->id)->get();    
            $allUserConsole = ConsoleList::where('user_id',$userId->id)->get(); 
                    // Another New
             foreach ($allUserConsole as $uConsole)
            {
                $consolePresentInAnalytics = 0;
                if (strpos($uConsole->siteUrl, 'sc-domain:') !== false) {
                    $explodedConsoleUrl = explode('sc-domain:',$uConsole->siteUrl);
                    if(strpos($explodedConsoleUrl[1], 'www.') !== false )
                    {
                    $finalConsoleUrlSlash = $explodedConsoleUrl[1];
                    }
                    else
                    {
                    $finalConsoleUrlSlash = "www.".$explodedConsoleUrl[1];
                    }
                    $finalConsoleUrl = str_replace("/","",$finalConsoleUrlSlash);
                }
                if (strpos($uConsole->siteUrl, 'https://') !== false) {
                    $explodedConsoleUrl = explode('https://',$uConsole->siteUrl);
                    if(strpos($explodedConsoleUrl[1], 'www.') !== false )
                    {
                    $finalConsoleUrlSlash = $explodedConsoleUrl[1];
                    }
                    else
                    {
                    $finalConsoleUrlSlash = "www.".$explodedConsoleUrl[1];
                    }
                    $finalConsoleUrl = str_replace("/","",$finalConsoleUrlSlash);
                }
                if (strpos($uConsole->siteUrl, 'http://') !== false) {
                    $explodedConsoleUrl = explode('http://',$uConsole->siteUrl);
                    if(strpos($explodedConsoleUrl[1], 'www.') !== false )
                    {
                    $finalConsoleUrlSlash = $explodedConsoleUrl[1];
                    }
                    else
                    {
                    $finalConsoleUrlSlash = "www.".$explodedConsoleUrl[1];
                    }
                    $finalConsoleUrl = str_replace("/","",$finalConsoleUrlSlash);
                }
                foreach ($allUserAnalytics as $uAnalytic)
                {
                    if (strpos($uAnalytic->Property_Url, 'http://') !== false) {
                    $explodedAnalyticsUrl = explode('http://',$uAnalytic->Property_Url);
                    if(strpos($explodedAnalyticsUrl[1], 'www.') !== false )
                    {
                    $finalAnalyticUrlSlash = $explodedAnalyticsUrl[1];
                    }
                    else
                    {
                    $finalAnalyticUrlSlash = "www.".$explodedAnalyticsUrl[1];
                    }
                    $finalAnalyticUrl = str_replace("/","",$finalAnalyticUrlSlash);
                    }
                    if (strpos($uAnalytic->Property_Url, 'https://') !== false) {
                    $explodedAnalyticsUrl = explode('https://',$uAnalytic->Property_Url);
                    if(strpos($explodedAnalyticsUrl[1], 'www.') !== false )
                    {
                    $finalAnalyticUrlSlash = $explodedAnalyticsUrl[1];
                    }
                    else
                    {
                    $finalAnalyticUrlSlash = "www.".$explodedAnalyticsUrl[1];
                    }
                    $finalAnalyticUrl = str_replace("/","",$finalAnalyticUrlSlash);
                    }
                    if($finalAnalyticUrl == $finalConsoleUrl)
                    {
                        $againExplode = (explode("www.",$finalConsoleUrl));
                        $newObj = new \stdClass();
                        $newObj->siteUrl = "sc-domain:".$againExplode[1];
                        $newObj->relative_id = $uAnalytic->ViewId;
                        $newObj->dimensions = ["query","pagePath"];
                        $newObj->metrics = ["clicks", "impressions","pageviews", "avgPageLoadTime", "bounceRate"];

                        $finalObj = json_encode($newObj);
                        array_push($sourceArray,$finalObj);

                        $consolePresentInAnalytics = 1;
                    }
                    
                }
                if($consolePresentInAnalytics == 0)
                {
                    $againExplode = (explode("www.",$finalConsoleUrl));
                    $newObj = new \stdClass();
                    $newObj->siteUrl = "sc-domain:".$againExplode[1];
                    $newObj->relative_id = "";
                    $newObj->dimensions = ["query"];
                    $newObj->metrics = ["clicks", "impressions"];

                    $finalObj = json_encode($newObj);
                    array_push($sourceArray,$finalObj);
                }

                
            }

            //Now Reversing the loops to check analytics not present in console
            foreach ($allUserAnalytics as $uAnalytic)
                {
                    $analyticsPresentInConsole = 0;
                    if (strpos($uAnalytic->Property_Url, 'http://') !== false) {
                    $explodedAnalyticsUrl = explode('http://',$uAnalytic->Property_Url);
                    if(strpos($explodedAnalyticsUrl[1], 'www.') !== false )
                    {
                    $finalAnalyticUrlSlash = $explodedAnalyticsUrl[1];
                    }
                    else
                    {
                    $finalAnalyticUrlSlash = "www.".$explodedAnalyticsUrl[1];
                    }
                    $finalAnalyticUrl = str_replace("/","",$finalAnalyticUrlSlash);
                    }
                    if (strpos($uAnalytic->Property_Url, 'https://') !== false) {
                    $explodedAnalyticsUrl = explode('https://',$uAnalytic->Property_Url);
                    if(strpos($explodedAnalyticsUrl[1], 'www.') !== false )
                    {
                    $finalAnalyticUrlSlash = $explodedAnalyticsUrl[1];
                    }
                    else
                    {
                    $finalAnalyticUrlSlash = "www.".$explodedAnalyticsUrl[1];
                    }
                    $finalAnalyticUrl = str_replace("/","",$finalAnalyticUrlSlash);
                    }
                    foreach ($allUserConsole as $uConsole)
                        {
                            if (strpos($uConsole->siteUrl, 'sc-domain:') !== false) {
                                $explodedConsoleUrl = explode('sc-domain:',$uConsole->siteUrl);
                                if(strpos($explodedConsoleUrl[1], 'www.') !== false )
                                {
                                $finalConsoleUrlSlash = $explodedConsoleUrl[1];
                                }
                                else
                                {
                                $finalConsoleUrlSlash = "www.".$explodedConsoleUrl[1];
                                }
                                $finalConsoleUrl = str_replace("/","",$finalConsoleUrlSlash);
                            }
                            if (strpos($uConsole->siteUrl, 'https://') !== false) {
                                $explodedConsoleUrl = explode('https://',$uConsole->siteUrl);
                                if(strpos($explodedConsoleUrl[1], 'www.') !== false )
                                {
                                $finalConsoleUrlSlash = $explodedConsoleUrl[1];
                                }
                                else
                                {
                                $finalConsoleUrlSlash = "www.".$explodedConsoleUrl[1];
                                }
                                $finalConsoleUrl = str_replace("/","",$finalConsoleUrlSlash);
                            }
                            if (strpos($uConsole->siteUrl, 'http://') !== false) {
                                $explodedConsoleUrl = explode('http://',$uConsole->siteUrl);
                                if(strpos($explodedConsoleUrl[1], 'www.') !== false )
                                {
                                $finalConsoleUrlSlash = $explodedConsoleUrl[1];
                                }
                                else
                                {
                                $finalConsoleUrlSlash = "www.".$explodedConsoleUrl[1];
                                }
                                $finalConsoleUrl = str_replace("/","",$finalConsoleUrlSlash);
                            }
                            if($finalAnalyticUrl == $finalConsoleUrl)
                                {
                                    $analyticsPresentInConsole = 1;
                                }
                        }
                        if($analyticsPresentInConsole == 0)
                        {
                            $newObj = new \stdClass();
                            $newObj->siteUrl = "";
                            $newObj->relative_id = $uAnalytic->ViewId;
                            $newObj->dimensions = ["pagePath"];
                            $newObj->metrics = ["pageviews", "avgPageLoadTime", "bounceRate"];

                            $finalObj = json_encode($newObj);
                            array_push($sourceArray,$finalObj);
                        }
                }


            
                $saveAnomaliesEndPoint = 'https://ai.dataclu.com/main/save_all_anomalies';
                $saveAnomaliesParametersDaily = [
                    'json' => [
                    'user_id' => $userId->id,
                    'frequency' => "D",
                    'days' => 90,
                    'range'=> 50,
                    'sources' => $sourceArray,
                        ],
                    ];

                    // dd($optionsD14);
                    // print_r($optionsD14);
                $newAnomalyClient = new \GuzzleHttp\Client();
                $sendingRequest = $newAnomalyClient->post($saveAnomaliesEndPoint, $saveAnomaliesParametersDaily);
                $receivingResult = json_decode($sendingRequest->getBody(),true);

                $checkPreviousResults = Jobid::where('user_id',$userId->id)->where('propertyUrl','User all Analytics and Console Projects')->where('jobtype_id',1)->first();
                if($checkPreviousResults)
                {
                    Jobid::where('user_id',$userId->id)->where('propertyUrl','User all Analytics and Console Projects')->where('jobtype_id',1)->update([
                        'updateAnomaly_JobId' => $receivingResult[1],
                    ]);
                }
                else
                {
                $saveJobId = new Jobid();
                $saveJobId->user_id =  $userId->id;
                $saveJobId->jobtype_id = 1;
                $saveJobId->propertyUrl = "User all Analytics and Console Projects";
                $saveJobId->updateDb_JobId = "Not Required";
                $saveJobId->updateAnomaly_JobId = $receivingResult[1];
                $saveJobId->save();
                }

                //Daily Ends Here

           
            
                // $saveAnomaliesEndPoint = 'https://ai.dataclu.com/main/save_all_anomalies';
                $saveAnomaliesParametersWeekly = [
                    'json' => [
                    'user_id' => $userId->id,
                    'frequency' => "W-Sun",
                    'days' => 90,
                    'range'=> 50,
                    'sources' => $sourceArray,
                        ],
                    ];

                    // dd($optionsD14);
                    // print_r($optionsD14);
                $newAnomalyClientWeekly = new \GuzzleHttp\Client();
                $sendingRequestWeekly = $newAnomalyClientWeekly->post($saveAnomaliesEndPoint, $saveAnomaliesParametersWeekly);
                $receivingResultWeekly = json_decode($sendingRequestWeekly->getBody(),true);

                $checkPreviousResultsWeekly = Jobid::where('user_id',$userId->id)->where('propertyUrl','User all Analytics and Console Projects')->where('jobtype_id',2)->first();
                if($checkPreviousResultsWeekly)
                {
                    Jobid::where('user_id',$userId->id)->where('propertyUrl','User all Analytics and Console Projects')->where('jobtype_id',2)->update([
                        'updateAnomaly_JobId' => $receivingResultWeekly[1],
                    ]);
                }
                else
                {
                $saveJobIdWeekly = new Jobid();
                $saveJobIdWeekly->user_id =  $userId->id;
                $saveJobIdWeekly->jobtype_id = 2;
                $saveJobIdWeekly->propertyUrl = "User all Analytics and Console Projects";
                $saveJobIdWeekly->updateDb_JobId = "Not Required";
                $saveJobIdWeekly->updateAnomaly_JobId = $receivingResultWeekly[1];
                $saveJobIdWeekly->save();
                }

  //Save Anomalies Call Ends Here

      } 
      // if user is already existed then update token related information
      else 
      {
        $socialUser = $userFind;
        User::where('email', $userInfo->email)->update([
          'access_token' => $accessToken['access_token'],
          'expires_in' => $accessToken['expires_in'],
          'token_created' => $accessToken['created']
        ]);
      }
      auth()->login($socialUser, true);
      return redirect()->route('google/login');
    }
  }




  /**
   * Gets the social redirect.
   *
   * @param string $provider The provider
   * @param \Illuminate\Http\Request $request
   *
   * @return \Illuminate\Http\Response
   */
  public function getSocialRedirect($provider, Request $request)
  {
    $providerKey = Config::get('services.' . $provider);

    if (empty($providerKey)) {
      return view('pages.status')
        ->with('error', trans('socials.noProvider'));
    }

    return Socialite::driver($provider)->redirect();
  }

  /**
   * Gets the social handle.
   *
   * @param string $provider The provider
   * @param \Illuminate\Http\Request $request
   *
   * @return \Illuminate\Http\Response
   */
  public function getSocialHandle($provider, Request $request)
  {
    $denied = $request->denied ? $request->denied : null;
    $socialUser = null;

    if ($denied != null || $denied != '') {
      return redirect()->to('login')
        ->with('status', 'danger')
        ->with('message', trans('socials.denied'));
    }

    $socialUserObject = Socialite::driver($provider)->user();

    // Check if email is already registered
    $userCheck = User::where('email', '=', $socialUserObject->email)->first();

    $email = $socialUserObject->email;

    if (!$socialUserObject->email) {
      $email = 'missing' . str_random(10) . '@' . str_random(10) . '.example.org';
    }

    // If user is not registered
    if (empty($userCheck)) {
      $sameSocialId = Social::where('social_id', '=', $socialUserObject->id)
        ->where('provider', '=', $provider)
        ->first();

      if (empty($sameSocialId)) {
        $ipAddress = new CaptureIpTrait();
        $socialData = new Social();
        $profile = new Profile();
        $role = Role::where('slug', '=', 'user')->first();
        $fullname = explode(' ', $socialUserObject->name);
        if (count($fullname) == 1) {
          $fullname[1] = '';
        }
        $username = $socialUserObject->nickname;

        if ($username == null) {
          foreach ($fullname as $name) {
            $username .= $name;
          }
        }

        // Check to make sure username does not already exist in DB before recording
        $username = $this->checkUserName($username, $email);

        $user = User::create([
          'name'                 => $username,
          'first_name'           => $fullname[0],
          'last_name'            => $fullname[1],
          'email'                => $email,
          'password'             => bcrypt(str_random(40)),
          'token'                => str_random(64),
          'activated'            => true,
          'signup_sm_ip_address' => $ipAddress->getClientIp(),

        ]);

        $socialData->social_id = $socialUserObject->id;
        $socialData->provider = $provider;
        $user->social()->save($socialData);
        $user->attachRole($role);
        $user->activated = true;

        $user->profile()->save($profile);
        $user->save();

        if ($socialData->provider == 'github') {
          $user->profile->github_username = $socialUserObject->nickname;
        }

        // Twitter User Object details: https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/user-object
        if ($socialData->provider == 'twitter') {
          //$user->profile()->twitter_username = $socialUserObject->screen_name;
          //If the above fails try (The documentation shows screen_name however so Twitters docs may be out of date.):
          $user->profile()->twitter_username = $socialUserObject->nickname;
        }
        $user->profile->save();

        $socialUser = $user;
      } else {
        $socialUser = $sameSocialId->user;
      }

      auth()->login($socialUser, true);

      return redirect($this->redirectSuccessLogin)->with('success', trans('socials.registerSuccess'));
    }

    $socialUser = $userCheck;

    auth()->login($socialUser, true);

    return redirect($this->redirectSuccessLogin);
  }

  /**
   * Check if username against database and return valid username.
   * If username is not in the DB return the username
   * else generate, check, and return the username.
   *
   * @param string $username
   * @param string $email
   *
   * @return string
   */
  public function checkUserName($username, $email)
  {
    $userNameCheck = User::where('name', '=', $username)->first();

    if ($userNameCheck) {
      $i = 1;
      do {
        $username = $this->generateUserName($username);
        $newCheck = User::where('name', '=', $username)->first();

        if ($newCheck == null) {
          $newCheck = 0;
        } else {
          $newCheck = count($newCheck);
        }
      } while ($newCheck != 0);
    }

    return $username;
  }

  /**
   * Generate Username.
   *
   * @param string $username
   *
   * @return string
   */
  public function generateUserName($username)
  {
    return $username . '_' . str_random(10);
  }

  //Updating analytics list
  public function updateAnalyticsList(Request $request)
  {
    $customGetID;
    $user = User::where('id',Auth::user()->id)->first();
    $userPreviousAnalyticsList = AnalyticsAccount::where('user_id',Auth::user()->id)->get();
    $keepRecord = $userPreviousAnalyticsList->count();

    $client = new Google_Client();
    $client->setAuthConfig(storage_path('/analytics/client_secrets.json'));
    $accesstoken = ["access_token"=>$user->access_token,"expires_in"=>$user->expires_in,"created"=>$user->token_created];
    $client->setAccessToken($accesstoken);

    if($client->isAccessTokenExpired())
      {

        $a = $client->refreshToken($user->refresh_token);
        if(array_key_exists("error",$a))
        {
          Auth::logout();
          Session::flush();
        }
        $client->setAccessToken($a['access_token']);
        $ab =  User::where('id', $user->id)->update([
          'access_token' => $a['access_token'],
          'expires_in' => $a['expires_in'],
          'token_created' => $a['created'],
          'refresh_token' => $a['refresh_token'],
        ]);
      }

       $analyticsUser = new Google_Service_Analytics($client);
        try
        {
          $accounts = $analyticsUser->management_accountSummaries
                      ->listManagementAccountSummaries();
        }
        catch (apiServiceException $e)
        {
          print 'There was an Analytics API service error '
            . $e->getCode() . ':' . $e->getMessage();
        }
        catch (apiException $e) 
        {
          print 'There was a general API error '
            . $e->getCode() . ':' . $e->getMessage();
        }

           if(count($accounts) > 0)
        {
          // $loopCount = 0;
    foreach ($accounts->getItems() as $account) {
      $dataExist = 0;
      $sendArray = [];

      $analyticsRecord = new AnalyticsAccount();
      $analyticsRecord->user_id = Auth::user()->id;

  
        $analyticsRecord->Account_Id  = $account->getId();
        $analyticsRecord->Account_Kind = $account->getKind();
        $analyticsRecord->Account_Name = $account->getName();

            // Iterate through each Property.
            foreach ($account->getWebProperties() as $property) {
        
        $analyticsRecord->Property_Id         = $property->getId();
        $analyticsRecord->Property_Kind         = $property->getKind();
        $analyticsRecord->Property_Name       = $property->getName();
        $analyticsRecord->Internal_Property_Id = $property->getInternalWebPropertyId();
        $analyticsRecord->Property_Level       = $property->getLevel();
        $analyticsRecord->Property_Url        = $property->getWebsiteUrl();

              // Iterate through each view (profile).
              foreach ($property->getProfiles() as $profile) {
        $analyticsRecord->ViewId  = $profile->getId();
        $customGetID = $profile->getId();
        $analyticsRecord->Profile_Kind = $profile->getKind();
        $analyticsRecord->Profile_Name = $profile->getName();
        $analyticsRecord->Profile_Type = $profile->getType();


              }
            }

            foreach($userPreviousAnalyticsList as $checkList)
            {
              if($checkList->ViewId == $analyticsRecord->ViewId )
              {
                $dataExist = 1;
                break;
              }
            }
            
            if($dataExist == 0)
            {
            $analyticsRecord->save();
            // Code Adding to send call only for that viewid
              $newObj = new \stdClass();
              $newObj->siteUrl = "";
              $newObj->relative_id = $customGetID;
              $newObj->dimensions = ["pagePath"];
              $newObj->metrics = ["pageviews", "avgPageLoadTime", "bounceRate"];

              $finalObj = json_encode($newObj);
              array_push($sendArray,$finalObj);

             $saveAnomaliesEndPoint = 'https://ai.dataclu.com/main/save_all_anomalies';
                $saveAnomaliesParametersDaily = [
                    'json' => [
                    'user_id' => Auth::user()->id,
                    'frequency' => "D",
                    'days' => 90,
                    'range'=> 50,
                    'sources' => $sendArray,
                        ],
                    ];

                $newAnomalyClient = new \GuzzleHttp\Client();
                $sendingRequest = $newAnomalyClient->post($saveAnomaliesEndPoint, $saveAnomaliesParametersDaily);
                $receivingResult = json_decode($sendingRequest->getBody(),true);

                $checkPreviousResults = Jobid::where('user_id',Auth::user()->id)->where('propertyUrl','User all Analytics and Console Projects')->where('jobtype_id',1)->first();
                if($checkPreviousResults)
                {
                    Jobid::where('user_id',Auth::user()->id)->where('propertyUrl','User all Analytics and Console Projects')->where('jobtype_id',1)->update([
                        'updateAnomaly_JobId' => $receivingResult[1],
                    ]);
                }
                else
                {
                $saveJobId = new Jobid();
                $saveJobId->user_id =  Auth::user()->id;
                $saveJobId->jobtype_id = 1;
                $saveJobId->propertyUrl = "User all Analytics and Console Projects";
                $saveJobId->updateDb_JobId = "Not Required";
                $saveJobId->updateAnomaly_JobId = $receivingResult[1];
                $saveJobId->save();
                }

                //Daily Ends Here

                //Weekle Starts Here

                $saveAnomaliesParametersWeekly = [
                    'json' => [
                    'user_id' => Auth::user()->id,
                    'frequency' => "W-Sun",
                    'days' => 90,
                    'range'=> 50,
                    'sources' => $sendArray,
                        ],
                    ];

                    // dd($optionsD14);
                    // print_r($optionsD14);
                $newAnomalyClientWeekly = new \GuzzleHttp\Client();
                $sendingRequestWeekly = $newAnomalyClientWeekly->post($saveAnomaliesEndPoint, $saveAnomaliesParametersWeekly);
                $receivingResultWeekly = json_decode($sendingRequestWeekly->getBody(),true);

                $checkPreviousResultsWeekly = Jobid::where('user_id',Auth::user()->id)->where('propertyUrl','User all Analytics and Console Projects')->where('jobtype_id',2)->first();
                if($checkPreviousResultsWeekly)
                {
                    Jobid::where('user_id',Auth::user()->id)->where('propertyUrl','User all Analytics and Console Projects')->where('jobtype_id',2)->update([
                        'updateAnomaly_JobId' => $receivingResultWeekly[1],
                    ]);
                }
                else
                {
                $saveJobIdWeekly = new Jobid();
                $saveJobIdWeekly->user_id =  Auth::user()->id;
                $saveJobIdWeekly->jobtype_id = 2;
                $saveJobIdWeekly->propertyUrl = "User all Analytics and Console Projects";
                $saveJobIdWeekly->updateDb_JobId = "Not Required";
                $saveJobIdWeekly->updateAnomaly_JobId = $receivingResultWeekly[1];
                $saveJobIdWeekly->save();
                }
              
              
              //Code end
            }
            // $loopCount++;
          }
        }

        return redirect()->back()->withSuccess('Analytics List Updated Successfully');


  }


  public function updateConsoleList()
  {
    $user = User::where('id',Auth::user()->id)->first();
    $userPreviousConsoleList = ConsoleList::where('user_id',Auth::user()->id)->get();
    $keepRecord = $userPreviousConsoleList->count();

    $client = new Google_Client();
    $client->setAuthConfig(storage_path('/analytics/client_secrets.json'));
    $accesstoken = ["access_token"=>$user->access_token,"expires_in"=>$user->expires_in,"created"=>$user->token_created];
    $client->setAccessToken($accesstoken);

    if($client->isAccessTokenExpired())
      {
        $a = $client->refreshToken($user->refresh_token);
        if(array_key_exists("error",$a))
        {
          Auth::logout();
          Session::flush();
        }
        $client->setAccessToken($a['access_token']);
        $ab =  User::where('id', $user->id)->update([
          'access_token' => $a['access_token'],
          'expires_in' => $a['expires_in'],
          'token_created' => $a['created'],
          'refresh_token' => $a['refresh_token'],
        ]);
      }

      //code for getting user console urls list
    $service = new Google_Service_Webmasters($client);
    $consoleUrl = $service->sites->listSites()->getSiteEntry();

    foreach($consoleUrl as $consoleData)
    {
      $consoleDataExist = 0;
      $sendArray = [];
  
      $srConsole = new ConsoleList();
      $srConsole->user_id = Auth::user()->id;
      if(strpos($consoleData->siteUrl, 'www.') !== false )
      {
        $explSiteUrl = explode('www.',$consoleData->siteUrl);
        $finalSiteUrl = 'sc-domain:'.$explSiteUrl[1];
      }
      elseif (strpos($consoleData->siteUrl, 'https://') !== false  ) 
      {
        $explSiteUrl = explode('https://',$consoleData->siteUrl);
        $finalSiteUrl = 'sc-domain:'.$explSiteUrl[1];
      }
      elseif (strpos($consoleData->siteUrl, 'http://') !== false  ) 
      {
        $explSiteUrl = explode('http://',$consoleData->siteUrl);
        $finalSiteUrl = 'sc-domain:'.$explSiteUrl[1];
      }
      else
      {
        $finalSiteUrl = $consoleData->siteUrl;
      }
      if(strpos($finalSiteUrl, '/') !== false)
      {
        $explSiteUrlSecond = explode('/',$finalSiteUrl);
        $finalSiteUrl = $explSiteUrlSecond[0];
      }
      $srConsole->siteUrl = $finalSiteUrl;
      $srConsole->permissionLevel = $consoleData->permissionLevel;
      foreach($userPreviousConsoleList as $consoleListPrevious)
      {
        if($consoleListPrevious->siteUrl == $srConsole->siteUrl)
        {
          $consoleDataExist = 1;
          break;
        }
      }
      if($consoleDataExist == 0)
      {
      $srConsole->save();
      $newObj = new \stdClass();
      $newObj->siteUrl = $srConsole->siteUrl;
      $newObj->relative_id = "";
      $newObj->dimensions = ["query"];
      $newObj->metrics = ["clicks", "impressions"];

      $finalObj = json_encode($newObj);
      array_push($sendArray,$finalObj);

       
                $saveAnomaliesEndPoint = 'https://ai.dataclu.com/main/save_all_anomalies';
                $saveAnomaliesParametersDaily = [
                    'json' => [
                    'user_id' => Auth::user()->id,
                    'frequency' => "D",
                    'days' => 90,
                    'range'=> 50,
                    'sources' => $sendArray,
                        ],
                    ];

                $newAnomalyClient = new \GuzzleHttp\Client();
                $sendingRequest = $newAnomalyClient->post($saveAnomaliesEndPoint, $saveAnomaliesParametersDaily);
                $receivingResult = json_decode($sendingRequest->getBody(),true);

                $checkPreviousResults = Jobid::where('user_id',Auth::user()->id)->where('propertyUrl','User all Analytics and Console Projects')->where('jobtype_id',1)->first();
                if($checkPreviousResults)
                {
                    Jobid::where('user_id',Auth::user()->id)->where('propertyUrl','User all Analytics and Console Projects')->where('jobtype_id',1)->update([
                        'updateAnomaly_JobId' => $receivingResult[1],
                    ]);
                }
                else
                {
                $saveJobId = new Jobid();
                $saveJobId->user_id =  Auth::user()->id;
                $saveJobId->jobtype_id = 1;
                $saveJobId->propertyUrl = "User all Analytics and Console Projects";
                $saveJobId->updateDb_JobId = "Not Required";
                $saveJobId->updateAnomaly_JobId = $receivingResult[1];
                $saveJobId->save();
                }

                //Daily Ends Here

                //Weekle Starts Here

                $saveAnomaliesParametersWeekly = [
                    'json' => [
                    'user_id' => Auth::user()->id,
                    'frequency' => "W-Sun",
                    'days' => 90,
                    'range'=> 50,
                    'sources' => $sendArray,
                        ],
                    ];

                    // dd($optionsD14);
                    // print_r($optionsD14);
                $newAnomalyClientWeekly = new \GuzzleHttp\Client();
                $sendingRequestWeekly = $newAnomalyClientWeekly->post($saveAnomaliesEndPoint, $saveAnomaliesParametersWeekly);
                $receivingResultWeekly = json_decode($sendingRequestWeekly->getBody(),true);

                $checkPreviousResultsWeekly = Jobid::where('user_id',Auth::user()->id)->where('propertyUrl','User all Analytics and Console Projects')->where('jobtype_id',2)->first();
                if($checkPreviousResultsWeekly)
                {
                    Jobid::where('user_id',Auth::user()->id)->where('propertyUrl','User all Analytics and Console Projects')->where('jobtype_id',2)->update([
                        'updateAnomaly_JobId' => $receivingResultWeekly[1],
                    ]);
                }
                else
                {
                $saveJobIdWeekly = new Jobid();
                $saveJobIdWeekly->user_id =  Auth::user()->id;
                $saveJobIdWeekly->jobtype_id = 2;
                $saveJobIdWeekly->propertyUrl = "User all Analytics and Console Projects";
                $saveJobIdWeekly->updateDb_JobId = "Not Required";
                $saveJobIdWeekly->updateAnomaly_JobId = $receivingResultWeekly[1];
                $saveJobIdWeekly->save();
                }

      }
    }

    // dd($consoleUrl);


    return redirect()->back()->withSuccess('Console List Updated Successfully');

  }
  public function consoleDataDisplay()
  {
      //User Websites Data
      $authUserID = Auth::id();
      // $websitesList = AnalyticsAccount::where('user_id',$authUserID)->get();
//New After Console API Added
      $websitesList = ConsoleList::where('user_id',$authUserID)->get();


      //First get a single last record of user 
      $userLatestField = Jobid::where('user_id',$authUserID)->latest()->first();
      // dd($userLatestField);
      //Getting Job IDs To check the status of data before loading
      $userJobId = Jobid::where('user_id',$authUserID)->get();
      $userj_id = array();
      foreach($userJobId as $jid)
      {
        // array_push($userj_id,$jid->updateAnomaly_JobId);
        $userj_id[$jid->propertyUrl] = $jid->updateAnomaly_JobId;
      }
      
      // dd($websitesList);


      //Returning View With All The Data
      return view('admin.home')->with([
        // 'data' => $a, 'totalsession' => $totalsession,
        // 'countrydata' => $coun, 'totalview' => $totalview,
        // 'countrylabel' => $countrylabel, 'countryvalue' => $countryvalue,
        // 'countrydate' => $countrydate,
        // 'pagedata' => $pag, 'totalpage' => $totalpage,
        // 'bouncdata' => $bou,
        'webList' => $websitesList,
        'userSingleId' => $authUserID,
        'userJid' => $userj_id,
      ]);

  }

  public function guestLogin()
  {
    // dd(Auth::user()->id);
    if(Auth::user())
    {
      $fWebNewArray = [];
      $cL = 0;
      $allWebList = ShareWebsite::where('user_id',Auth::user()->id)->get();
      // dd($allWebList);
      foreach($allWebList as $awb)
      {
        $webNewArray = array ();
        $webNewArray['website'] = $awb->createdUrl;
        $exp = explode('www.',$awb->createdUrl);
        $webNewArray['display'] = $exp[1];
        if($cL == 0)
        {
          $webNewArray['default'] = 1; 
        }
        else
        {
          $webNewArray['default'] = 0;
        }
        $cL++;
        array_push($fWebNewArray,$webNewArray);
      }

      return view('admin.allAnomaliesView')->with([
        // 'webList' => $websitesList,
        'userSingleId' => Auth::user()->id,
        // 'userJid' => $userj_id,
        'consoleAnalyticsList' => $fWebNewArray,
      ]);
    }
    else
    {
    return view('auth.guestlogin');
    }
  }

  public function guestLoginPost(Request $request)
  {
    $validated = $request->validate([
        'email' => 'required',
        'password' => 'required',
    ]);
    $getEmail = User::where('email',$request->email)->first();
    if($request->email == $getEmail->email && Hash::check($request->password, $getEmail->password))
    {

      $fWebNewArray = [];
      $cL = 0;
      $allWebList = ShareWebsite::where('user_id',$getEmail->id)->get();
      // dd($allWebList);
      foreach($allWebList as $awb)
      {
        $webNewArray = array ();
        $webNewArray['website'] = $awb->createdUrl;
        $exp = explode('www.',$awb->createdUrl);
        $webNewArray['display'] = $exp[1];
        if($cL == 0)
        {
          $webNewArray['default'] = 1; 
        }
        else
        {
          $webNewArray['default'] = 0;
        }
        $cL++;
        array_push($fWebNewArray,$webNewArray);
      }
      // dd($webNewArray);
      auth()->login($getEmail, true);
      // return redirect()->route('google/login');
      return view('admin.allAnomaliesView')->with([
        // 'webList' => $websitesList,
        'userSingleId' => $getEmail->id,
        // 'userJid' => $userj_id,
        'consoleAnalyticsList' => $fWebNewArray,
      ]);
    }
    else
    {
      dd('Invalid Email or passowrd');
    }
  }
}