<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Google_Client;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectAfterLogout = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Logout, Clear Session, and Return.
     *
     * @return void
     */
    public function logout()
    {
        //if User is admin then it would simply logout else it will first revoke authenticated user acess token and then logout it,
        if(auth()->user()->isAdmin())
        {
            Auth::logout();
            return redirect('/login');
        }
        else
        {
            // Code for revoking user access token at the time of logout.

            //Commented For Short Period
            // session_start();
            // $accesstoken=$_SESSION['access_token'];
            // unset($_SESSION['access_token']);
            // unset($_SESSION['userData']);
            // $client = new Google_Client();
            // $client->revokeToken($accesstoken);
            Auth::logout();
            Session::flush();
            return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
        }
    }

    public function adminlogin()
    {
        return view ('auth.adminlogin');
    }
}
