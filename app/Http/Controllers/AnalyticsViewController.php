<?php

namespace App\Http\Controllers;
use App\Models\AnalyticsAccount;
use Auth;

use Illuminate\Http\Request;

class AnalyticsViewController extends Controller
{
    public function homePage()
    {
        # code...
        $getAuthUserId = Auth::id();
        $accountsList  = AnalyticsAccount::where('user_id',Auth::user()->id)->get();
        return \view('admin.analytics.home')->with([
            'accountList' => $accountsList,
            'getAuthUserId' => $getAuthUserId
        ]);
    }
}