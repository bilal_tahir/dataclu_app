<?php

namespace App\Http\Controllers;
use Ixudra\Curl\Facades\Curl;

class WelcomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function welcome()
    {
        return view('welcome');
    }

    public function home()
    {
       return redirect('google/login');
    }

    public function getCURL()
    {
    // $body['major_dimension'] = "browser";
    // $body['browser'] = "Chrome";
    // $body['metric'] = "pageViewsPerSession";
    // $body['start_date'] = "2020-09-01";
    // $body['end_date'] = "2020-09-25";
    // $body['window'] =  5;
    // $body['user_id'] = 3;
    // $body['View_Id'] = "147110849";
    // $body['access_token'] = "ya29.a0AfH6SMC5WpbUv3SwsZZS4T3TnfZxEia8yVpJAoEEKqgY8TLaHE54gbw7UhRPj43zuQ1_oJCjWgsElXavumFE3dYvW5TUdj3PHOt48RyZGem-4GY52PvApUAomp5GWSudbS8RHZhutB2fkgUzkoeP5Yq0Kd024f0tmL5I";
    // $body  =  json_encode($body);
    //       $response = Curl::to('https://ai.darkdata.ca/metrics/trends/')

    //             ->withData($body)

    //             ->get();

    $response = Curl::to('https://ai.darkdata.ca/metrics/trends?major_dimension=browser&browser=Chrome&metric=pageViewsPerSession&start_date=2020-09-01&end_date=2020-09-05&window=5&user_id=3&View_Id=147110849')
                            ->get();


        dd($response);
    }
}
