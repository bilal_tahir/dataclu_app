<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Feedback;
use Auth;

class FeedbackController extends Controller
{
    public function submitfeedback(Request $request)
    {
            $checkPreviousFeedback = Feedback::where(['user_id'=>Auth::user()->id,
            'anomalyId' => $request->uaid,
            ])->first();
            if($checkPreviousFeedback)
            {
            $checkPreviousFeedback->feedback = $request->userFeed;
            $checkPreviousFeedback->save();
            return response()->json(['success'=> 'Feedback updated successfully!']);
        }
        else
        {
            $serializeJSOn = serialize($request->insightData);
            $userFeedBack = new Feedback();
            $userFeedBack->user_id = Auth::user()->id;
            $userFeedBack->anomalyId = $request->uaid;
            $userFeedBack->feedback = $request->userFeed;
            $userFeedBack->insight_data = $serializeJSOn;
            $userFeedBack->save();

            return response()->json(['success'=> 'Feedback submitted successfully!']);

        }
    }

    public function submitfeedbackcomment(Request $request)
    {
           $checkPreviousFeedback = Feedback::where(['user_id'=>Auth::user()->id,
            'anomalyId' => $request->uaid,
            ])->first();
            if($checkPreviousFeedback)
            {
            $checkPreviousFeedback->feedback = $request->userFeed;
            $checkPreviousFeedback->comments = $request->userComment;
            $checkPreviousFeedback->save();
            return response()->json(['success'=> 'Feedback updated successfully!']);
        }
        else
        {
            $serializeJSOn = serialize($request->insightData);
            $userFeedBack = new Feedback();
            $userFeedBack->user_id = Auth::user()->id;
            $userFeedBack->anomalyId = $request->uaid;
            $userFeedBack->feedback = $request->userFeed;
            $userFeedBack->insight_data = $serializeJSOn;
            $userFeedBack->comments = $request->userComment;

            $userFeedBack->save();

            return response()->json(['success'=> 'Feedback submitted successfully!']);

        }
        // $checkPreviousFeedback = Feedback::where(['user_id'=>Auth::user()->id,
        //     'anomalyId' => $request->uaid,
        //     ])->first();
        //     if($checkPreviousFeedback)
        //     {
        //         if(isset($checkPreviousFeedback->comments))
        //         {
        //             return response()->json(['failed'=> 'You had submitted your comment previously!']);
        //         }
        //         $checkPreviousFeedback->comments = $request->userComment;
        //         $checkPreviousFeedback->save();
        //         return response()->json(['success'=> 'Comment submitted successfully!']);
        //     }
        //     else{
        //          return response()->json(['failed'=> 'An Error Occured!']);
        //     }
    }
}