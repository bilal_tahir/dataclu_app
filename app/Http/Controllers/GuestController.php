<?php

namespace App\Http\Controllers;

use App\Models\ShareUrl;
use Illuminate\Http\Request;

class GuestController extends Controller
{
    //
    public function guestpagereq($id)
    {
        // dd($id);
        $guestUserId = ShareUrl::where('uuid',$id)->first();
        if($guestUserId)
        {
        $id = $guestUserId->anomalyId;
        // dd($id);
        return view('admin.anomalyView')->with([
            'callId' => $id
        ]);
        }
        else
        {
            return "Your Link is Invalid or Expired. Kindly use the correct one!";
        }
        
    }
}