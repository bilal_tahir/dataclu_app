<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ShareUrl;
use App\Models\Shareanomalyresponse;
use Uuid;
use Auth;
// use Mail;

class SendEmailController extends Controller
{
    //
    public function sendemail(Request $request)
    {
        // dd($request->anomalyPageGroup);
        $checkUserExistence = ShareUrl::where('senderEmail',$request->email)->where('anomalyId',$request->anomalyId)->first();
        if($checkUserExistence)
        {
            $generateuuid = Uuid::generate(4)->string;
            $generateUrl = 'https://app.dataclu.com/guest/anomaly/detail/'.$generateuuid;
            // return redirect()->back()->withSuccess('The link has already been sent to the user!');
            $details = [
                'email' => $request->email,
                'title' => 'Mail from Darkdata.com',
                'body' => 'A Link Has Been Shared To You From ' . Auth::user()->email,
                'message' => $request->textMessage,
                'section' => $generateUrl,
            ];

            try {
                \Mail::send('emails.anomalyurl', ["details"=>$details], function($message) use ($details) {
                    // $message->from('info@darkdata.com');
                    $message->to($details['email']);
                    $message->subject($details['title']);
            
                });
                    }
            catch(\Exception $e)
            {
                return $e->getMessage();
            }
    
            ShareUrl::where('senderEmail',$request->email)->where('anomalyId',$request->anomalyId)->update([
                'uuid'=>$generateuuid,
                'comleteUrl' => $generateUrl
                ]);
            return redirect()->back()->withSuccess('The link has been sent and Updated Successfully!');
        }
        $generateuuid = Uuid::generate(4)->string;
        $generateUrl = 'https://app.dataclu.com/guest/anomaly/detail/'.$generateuuid;

        // $share = new ShareUrl();
        // $share->user_id = Auth::user()->id;
        // $share->uuid = $generateuuid;
        // $share->anomalyId = $request->anomalyId;
        // $share->senderEmail = $request->email;
        // $share->comleteUrl = $generateUrl;
        // $share->save();
        
        $details = [
            'email' => $request->email,
            'title' => 'Mail from Darkdata.com',
            'body' => 'A Link Has Been Shared To You From ' . Auth::user()->email,
            'message' => $request->textMessage,
            'section' => $generateUrl,
        ];

        try {
        \Mail::send('emails.anomalyurl', ["details"=>$details], function($message) use ($details) {
            // $message->from('info@darkdata.com');
            $message->to($details['email']);
            $message->subject($details['title']);
    
        });
            }
            catch(\Exception $e)
            {
                return $e->getMessage();
            }
        // \Mail::to($request->email)->send(new \App\Mail\AnomalyUrlEmail($details));
        $share = new ShareUrl();
        $share->user_id = Auth::user()->id;
        $share->uuid = $generateuuid;
        $share->anomalyId = $request->anomalyId;
        $share->senderEmail = $request->email;
        $share->comleteUrl = $generateUrl;
        $share->save();

        $resp = ShareUrl::where('uuid',$generateuuid)->where('anomalyId',$request->anomalyId)->first();

        if($resp)
        {
            $saveRespone = new Shareanomalyresponse();
            $saveRespone->share_anomaly_id = $resp->id;
            $saveRespone->anomaly_data = $request->anomalyDetail;
            $saveRespone->anomaly_contributor = $request->anomalyContributor;
            $saveRespone->anomaly_related_audience = $request->anomalyAudience;
            $saveRespone->anomaly_page_group = $request->anomalyPageGroup;
            $saveRespone->save();
        }


        return redirect()->back()->withSuccess('Your Email has Been Sent Successfully!');
       
        // dd("Email is Sent.");
    }
    public function createshareurl(Request $request)
    {
        $checkPreviousCreateUrl = ShareUrl::where(['user_id'=>Auth::user()->id,
        'anomalyId' => $request->aid,
        'senderEmail' => "Customly Created"
        ])->first();
        if($checkPreviousCreateUrl )
        {
            return response()->json(['success'=>$checkPreviousCreateUrl->comleteUrl]);
        }
        else
        {
        $generateuuid = Uuid::generate(4)->string;
        $generateUrl = 'https://app.dataclu.com/guest/anomaly/detail/'.$generateuuid;

        $share = new ShareUrl();
        $share->user_id = Auth::user()->id;
        $share->uuid = $generateuuid;
        $share->anomalyId = $request->aid;
        $share->senderEmail = "Customly Created";
        $share->comleteUrl = $generateUrl;
        $share->save();
        return response()->json(['success'=>$generateUrl]);
        }
    }
}