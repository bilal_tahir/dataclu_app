<?php

namespace App\Http\Controllers;
use Google_Client;
use App\Models\User;
use App\Models\AnalyticsAccount;
use App\Models\ConsoleList;
use App\Models\Jobid;
use App\Models\ApiDataResponse;
use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use App\Models\Profile;
use jeremykenedy\LaravelRoles\Models\Role;
use Illuminate\Support\Facades\Auth; 
// use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Models\AnalyticsCorelateMetrics;
use App\Models\AnomalyDetailData;
use App\Models\ShareUser;

class APIController extends Controller
{
    public $successStatus = 200;
    //
    public function validateAccessToken(Request $request)
    {
         $validator = Validator::make($request->all(), [ 
            'accesstoken' => 'required', 
            'refreshtoken' => 'required', 
        ]);
        if ($validator->fails()) 
        { 
            return response()->json(['error'=>$validator->errors()], 422);            
        }
        $matchThese = ['access_token' => $request->accesstoken, 'refresh_token' => $request->refreshtoken];
        $results = User::where($matchThese)->first();
        if($results)
        {
        $client = new Google_Client();
        $accesstoken = ["access_token"=>$results->access_token,"expires_in"=>$results->expires_in,"created"=>$results->token_created];
        $client->setAccessToken($accesstoken);
        $check  = $client->isAccessTokenExpired();
         if ($check == false)
         {
             return response()->json(['Expire'=> 'False'], 200);
         }
         elseif ($check == true )
         {
             return response()->json(['Expire'=> 'True'], 200);
         }
         else
         {
            return response()->json(['Error'=> 'Unknown Error.'], 404);
         }
        }
        else
        {
            return response()->json(['Error'=> 'User Record Not Found.'], 404);

        }
        
        
    }

    // public function getAllUserData()
    // {

        public function jsonFile()
        {
            $path = storage_path('/analytics/client_secrets.json'); // ie: /var/www/laravel/app/storage/json/filename.json

            $json = json_decode(file_get_contents($path), true); 

            dd($json);

        }

    // }

    public function refreshAccessToken(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'accesstoken' => 'required', 
            'refreshtoken' => 'required', 
        ]);
        // return response()->json(['Error'=> 'User Record Not Found'], 404);
        if ($validator->fails()) 
        { 
            return response()->json(['error'=>$validator->errors()], 422);            
        }
        $matchThese = ['access_token' => $request->accesstoken, 'refresh_token' => $request->refreshtoken];
        $results = User::where($matchThese)->first();
        if($results)
        {
            session_start();
        $client = new Google_Client();
        $client->setAuthConfig(storage_path('/analytics/client_secrets.json'));
        $a = $client->refreshToken($results->refresh_token);
        return response()->json(['results'=>$a]); 
        $ab =  User::where('id', $results->id)->update([
          "access_token" => $a['access_token'],
          "expires_in" => $a['expires_in'],
          "token_created" => $a['created'],
          "refresh_token" => $a['refresh_token'],
        ]);
        if($ab)
        {
            return response()->json(['Status'=> 'Success','Access Token'=> $a['access_token'],
                                    'Expires In'=> $a['expires_in'],'Token Created'=> $a['created'],
                                     'Refresh Token'=> $a['refresh_token']], 200);
        }
        else
        {
            return response()->json(['Status'=> 'Failed'], 400);
        }
        }
        else
        {
            return response()->json(['Error'=> 'User Record Not Found'], 404);

        }

    }

    public function getUserData($id)
        {
             $data = User::where('id',$id)->first();
             $v_id = AnalyticsAccount::where('user_id',$id)->get();
             $vid = array();
             foreach($v_id as $view) {
                $vid[] = $view->ViewId;
                }
             $getData = ['access_token' => $data->access_token, 'refresh_token' => $data->refresh_token,
                        'expires_in' => $data->expires_in, 'token_created' => $data->token_created,
                        'client_id' => '667922266448-3msvaju68vlisdahsf0nh07o7hrm2bcl.apps.googleusercontent.com', 'client_secret' => '4e54Kqda41Ua8Ksrf8ayCJQ7',
                       'token_uri' => 'https://oauth2.googleapis.com/token',
                        'token_info_uri' => 'https://oauth2.googleapis.com/tokeninfo',
                    'View_Id' => $vid];
                        
           return response()->json($getData);
            
        }

    /** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|email', 
            'password' => 'required', 
            'c_password' => 'required|same:password', 
        ]);
        if ($validator->fails()) 
        { 
            return response()->json(['error'=>$validator->errors()], 422);            
        }

        $input = $request->all(); 

            //Code To Check Email Exists Or Not
        $userFind  = User::where('email', $input['email'])->first();
        if($userFind)
        {
            return response()->json(['error'=>'User Already Exist!'], 400);       
        }
        else
        {
            $profile = new Profile();
            $role = Role::where('slug', '=', 'apirole')->first();
            $fullname = explode(' ', $input['name']);
            if (count($fullname) == 1) 
            {
                $fullname[1] = '';
            }

            // Used For Removing White Spaces From User Name
            $username = str_replace(' ', '', $input['name']);

            //Registering User
            $user = new User();
            $user->name = $username;
            $user->first_name = $fullname[0];
            $user->last_name = $fullname[1];
            $user->email = $input['email'];
            $user->password = bcrypt($input['password']); 
            $user->activated = true;
            $user->save();

            //Adding User Role
            $user->attachRole($role);

            // //Adding User Profile
            $user->profile()->save($profile);
            $user->save();

            // Returning Token
            $success['token'] =  $user->createToken('MyApp')-> accessToken; 
            $success['name'] =  $user->name;
            return response()->json(['success'=>$success], $this-> successStatus); 
        }
    }

    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(){ 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')->accessToken; 
            return response()->json(['success' => $success], $this->successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }

    public function logout (Request $request)
    {
        $token = $request->user('api')->token()->revoke();
        $response = 'You have been succesfully logged out!';
        return response($response, 200);

    }
    public function optimizedData($id,$domain)
    {
        return view('admin.optimizedData')->with([
            'userId' => $id,
            'domain' => $domain,
        ]);
    }

    public function getUserConsoleData($id)
    {

        $aAccount = AnalyticsAccount::where('user_id',$id)->get();
        $cList = ConsoleList::where('user_id',$id)->get();

        if($aAccount->isEmpty())
        {
            return "No Record Found";
        }

        $newAnalyticsRecord = array();
        $newConsoleRecord = array();
        $finalArray = array();

        foreach($aAccount as $aA)
        {
            array_push($newAnalyticsRecord, array('domain' => $aA->Property_Url,'view_id' => $aA->ViewId));
        }
        foreach($cList as $cL)
        {
            array_push($newConsoleRecord, array('domain' =>$cL->siteUrl));
        }
        $finalArray['analyticsRecord'] = $newAnalyticsRecord;
        $finalArray['consoleRecord'] = $newConsoleRecord;
        return$finalArray;
    }

    // Code Commented Start Here

    // public function updateApiDbDaily()
    // {
    //     //Update Daily
    //        $allUsers = User::whereNotBetween('id', [1, 2])->get(); //3
    //     if($allUsers->count() > 0)
    //     {
    //     foreach ($allUsers as $allUser)
    //     {
    //         $allUserAnalytics = ConsoleList::where('user_id',$allUser->id)->get(); //2
    //         if($allUserAnalytics->count() > 0)
    //         {

    //             foreach($allUserAnalytics as $uAnalytic)
    //             {
    //                 $accountURLs1 = array();
    //                 $exploded_url = $uAnalytic->siteUrl;
    //                 array_push($accountURLs1,$exploded_url);

    //                      //  Update Database With G-Analytics Database
    //                 $endpointD13 = 'https://ai.dataclu.com/update/database/';
    //                 $optionsD13 = [
    //                     'json' => [
    //                     'days' => "last_requested",
    //                     'user_id' => $allUser->id,
    //                     'siteUrls' =>  $accountURLs1,
    //                         ],
    //                     ];
    //                 $clientD13 = new \GuzzleHttp\Client();
    //                 $resD13 = $clientD13->post($endpointD13, $optionsD13);
        
    //                 $j_Did = json_decode($resD13->getBody(),true);

    //                          //  Update Anomailes in DB
    //             $endpointD14 = 'https://ai.dataclu.com/update/anomalies';
    //             $optionsD14 = [
    //                 'json' => [
    //                 'days' => 90,
    //                 'user_id' => $allUser->id,
    //                 'siteUrls' =>  $accountURLs1,
    //                 // 'metrics' =>  ["clicks", "ctr", "position", "impressions"],
    //                 // 'dimensions' =>  ["page", "query", "country"],
    //                 'dimensions' =>  ["query"],
    //                 'job_id' => $j_Did[1][0],
    //                 'frequency' => "D"
    //                     ],
    //                 ];
    //             $clientD14 = new \GuzzleHttp\Client();
    //             $resD14 = $clientD14->post($endpointD14, $optionsD14);
    //             $j_Did2 = json_decode($resD14->getBody(),true);

    //               //updating or Saving Record Seperately For Daily Call
    //               $checkPreviousJobId = Jobid::where('jobtype_id',1)
    //                                     ->where('user_id',$allUser->id)
    //                                     ->where('propertyUrl',$exploded_url)->first();
    //                 if($checkPreviousJobId)
    //                 {
    //                     Jobid::where('jobtype_id',1)
    //                             ->where('user_id',$allUser->id)
    //                             ->where('propertyUrl',$exploded_url)->update([
    //                                         'updateDb_JobId' => $j_Did[1][0],
    //                                         'updateAnomaly_JobId' => $j_Did2[1][0]
    //                                     ]);
    //                 }
    //                 else
    //                 {
    //                     $upJobId1 = new Jobid();
    //                     $upJobId1->jobtype_id = 1;
    //                     $upJobId1->user_id = $allUser->id;
    //                     $upJobId1->propertyUrl = $exploded_url;
    //                     $upJobId1->updateDb_JobId = $j_Did[1][0];
    //                     $upJobId1->updateAnomaly_JobId = $j_Did2[1][0];
    //                     $upJobId1->save();
    //                 }

    //             //   //Additional Code for calling Daily API's and saving there record
    //             //   $clickEndPoint = 'https://ai.dataclu.com/anomalies/trends?user_id='.$allUser->id.'&siteUrl='.$uAnalytic->siteUrl.'&metric=clicks&dimensions=["query"]&range=25&frequency=D';
    //             // //   $ctrEndPoint = 'https://ai.dataclu.com/anomalies/trends?user_id='.$allUser->id.'&siteUrl='.$uAnalytic->siteUrl.'&metric=ctr&dimensions=["query"]&range=25&frequency=D';
    //             //   $impressionEndPoint = 'https://ai.dataclu.com/anomalies/trends?user_id='.$allUser->id.'&siteUrl='.$uAnalytic->siteUrl.'&metric=impressions&dimensions=["query"]&range=25&frequency=D';
    //             // //   $positionEndPoint = 'https://ai.dataclu.com/anomalies/trends?user_id='.$allUser->id.'&siteUrl='.$uAnalytic->siteUrl.'&metric=position&dimensions=["query"]&range=25&frequency=D';

    //             //     // Calling and saving respone of click Api
    //             // $clickClient = new \GuzzleHttp\Client();
    //             // $clickApi = $clickClient->get($clickEndPoint);
    //             // $clickResp = json_decode($clickApi->getBody(),true);

    //             // $checkPreviousRecord = ApiDataResponse::where('user_id',$allUser->id)
    //             //                         ->where('domain_name',$uAnalytic->siteUrl)
    //             //                         ->where('data_type',1)
    //             //                         ->where('metric_name','clicks')->first();

    //             // if($checkPreviousRecord)
    //             // {
    //             //       ApiDataResponse::where('user_id',$allUser->id)
    //             //                         ->where('domain_name',$uAnalytic->siteUrl)
    //             //                         ->where('data_type',1)
    //             //                         ->where('metric_name','clicks')->update([
    //             //                                 'response' => serialize($clickResp)
    //             //                         ]);
    //             // }
    //             // else
    //             // {
    //             //     $newClick = new ApiDataResponse();
    //             //     $newClick->user_id = $allUser->id;
    //             //     $newClick->data_type = 1;
    //             //     $newClick->domain_name = $uAnalytic->siteUrl;
    //             //     $newClick->metric_name = 'clicks';
    //             //     $newClick->response = serialize($clickResp);
    //             //     $newClick->save();
    //             // }

    //             //        // Calling and saving respone of impression Api
    //             // $impressionClient = new \GuzzleHttp\Client();
    //             // $impressionApi = $impressionClient->get($impressionEndPoint);
    //             // $impressionResp = json_decode($impressionApi->getBody(),true);

    //             // $checkImpressionPreviousRecord = ApiDataResponse::where('user_id',$allUser->id)
    //             //                         ->where('domain_name',$uAnalytic->siteUrl)
    //             //                         ->where('data_type',1)
    //             //                         ->where('metric_name','impressions')->first();

    //             // if($checkImpressionPreviousRecord)
    //             // {
    //             //       ApiDataResponse::where('user_id',$allUser->id)
    //             //                         ->where('domain_name',$uAnalytic->siteUrl)
    //             //                         ->where('data_type',1)
    //             //                         ->where('metric_name','impressions')->update([
    //             //                                 'response' => serialize($impressionResp)
    //             //                         ]);
    //             // }
    //             // else
    //             // {
    //             //     $newImpression = new ApiDataResponse();
    //             //     $newImpression->user_id = $allUser->id;
    //             //     $newImpression->data_type = 1;
    //             //     $newImpression->domain_name = $uAnalytic->siteUrl;
    //             //     $newImpression->metric_name = 'impressions';
    //             //     $newImpression->response = serialize($impressionResp);
    //             //     $newImpression->save();
    //             // }


    //             }

    //         }
    //     }
    // }
    // return redirect()->back()->withSuccess('Daily Calls Anomalies Updated Successfully!');
    // }

    // Code Commented Ends Here

    
    // public function updateApiDbDataDaily()
    // {
    //     //Update Daily
    //        $allUsers = User::whereNotBetween('id', [1, 2])->get(); //3
    //     if($allUsers->count() > 0)
    //     {
    //     foreach ($allUsers as $allUser)
    //     {
    //         $allUserAnalytics = ConsoleList::where('user_id',$allUser->id)->get(); //2
    //         if($allUserAnalytics->count() > 0)
    //         {

    //             foreach($allUserAnalytics as $uAnalytic)
    //             {
                    
    //               //Additional Code for calling Daily API's and saving there record
    //               $clickEndPoint = 'https://ai.dataclu.com/anomalies/trends?user_id='.$allUser->id.'&siteUrl='.$uAnalytic->siteUrl.'&metric=clicks&dimensions=["query"]&range=25&frequency=D';
    //             //   $ctrEndPoint = 'https://ai.dataclu.com/anomalies/trends?user_id='.$allUser->id.'&siteUrl='.$uAnalytic->siteUrl.'&metric=ctr&dimensions=["query"]&range=25&frequency=D';
    //               $impressionEndPoint = 'https://ai.dataclu.com/anomalies/trends?user_id='.$allUser->id.'&siteUrl='.$uAnalytic->siteUrl.'&metric=impressions&dimensions=["query"]&range=25&frequency=D';
    //             //   $positionEndPoint = 'https://ai.dataclu.com/anomalies/trends?user_id='.$allUser->id.'&siteUrl='.$uAnalytic->siteUrl.'&metric=position&dimensions=["query"]&range=25&frequency=D';

    //                 // Calling and saving respone of click Api
    //             $clickClient = new \GuzzleHttp\Client();
    //             $clickApi = $clickClient->get($clickEndPoint);
    //             $clickResp = json_decode($clickApi->getBody(),true);

    //             $checkPreviousRecord = ApiDataResponse::where('user_id',$allUser->id)
    //                                     ->where('domain_name',$uAnalytic->siteUrl)
    //                                     ->where('data_type',1)
    //                                     ->where('metric_name','clicks')->first();

    //             if($checkPreviousRecord)
    //             {
    //                   ApiDataResponse::where('user_id',$allUser->id)
    //                                     ->where('domain_name',$uAnalytic->siteUrl)
    //                                     ->where('data_type',1)
    //                                     ->where('metric_name','clicks')->update([
    //                                             'response' => serialize($clickResp)
    //                                     ]);
    //             }
    //             else
    //             {
    //                 $newClick = new ApiDataResponse();
    //                 $newClick->user_id = $allUser->id;
    //                 $newClick->data_type = 1;
    //                 $newClick->domain_name = $uAnalytic->siteUrl;
    //                 $newClick->metric_name = 'clicks';
    //                 $newClick->response = serialize($clickResp);
    //                 $newClick->save();
    //             }

    //                    // Calling and saving respone of impression Api
    //             $impressionClient = new \GuzzleHttp\Client();
    //             $impressionApi = $impressionClient->get($impressionEndPoint);
    //             $impressionResp = json_decode($impressionApi->getBody(),true);

    //             $checkImpressionPreviousRecord = ApiDataResponse::where('user_id',$allUser->id)
    //                                     ->where('domain_name',$uAnalytic->siteUrl)
    //                                     ->where('data_type',1)
    //                                     ->where('metric_name','impressions')->first();

    //             if($checkImpressionPreviousRecord)
    //             {
    //                   ApiDataResponse::where('user_id',$allUser->id)
    //                                     ->where('domain_name',$uAnalytic->siteUrl)
    //                                     ->where('data_type',1)
    //                                     ->where('metric_name','impressions')->update([
    //                                             'response' => serialize($impressionResp)
    //                                     ]);
    //             }
    //             else
    //             {
    //                 $newImpression = new ApiDataResponse();
    //                 $newImpression->user_id = $allUser->id;
    //                 $newImpression->data_type = 1;
    //                 $newImpression->domain_name = $uAnalytic->siteUrl;
    //                 $newImpression->metric_name = 'impressions';
    //                 $newImpression->response = serialize($impressionResp);
    //                 $newImpression->save();
    //             }
    //             }

    //         }
    //     }
    // }
    // return redirect()->back()->withSuccess('Daily Calls Data Updated Successfully!');
    // }

    // Code Commented Ends Here

    // Code Commented Starts Here
    
    // public function updateApiDbWeekly()
    // {
    //     $allUsers = User::whereNotBetween('id', [1, 2])->get(); //3
    //     if($allUsers->count() > 0)
    //     {
    //     foreach ($allUsers as $allUser)
    //     {
    //         $allUserAnalytics = ConsoleList::where('user_id',$allUser->id)->get(); //2
    //         if($allUserAnalytics->count() > 0)
    //         {

    //             foreach($allUserAnalytics as $uAnalytic)
    //             {
    //                 $accountURLs1 = array();
    //                 // $complete_url = (explode("/",$uAnalytic->Property_Url));
    //                 // $exploded_url = "sc-domain:".$complete_url[2];
    //                 $exploded_url = $uAnalytic->siteUrl;
    //                 array_push($accountURLs1,$exploded_url);

    //                      //  Update Database With G-Analytics Database
    //                 $endpointD13 = 'https://ai.dataclu.com/update/database/';
    //                 $optionsD13 = [
    //                     'json' => [
    //                     'days' => "last_requested",
    //                     'user_id' => $allUser->id,
    //                     'siteUrls' =>  $accountURLs1,
    //                         ],
    //                     ];
    //                 $clientD13 = new \GuzzleHttp\Client();
    //                 $resD13 = $clientD13->post($endpointD13, $optionsD13);
        
    //                 $j_Did = json_decode($resD13->getBody(),true);

    //                          //  Update Anomailes in DB
    //             $endpointD14 = 'https://ai.dataclu.com/update/anomalies';
    //             $optionsD14 = [
    //                 'json' => [
    //                 'days' => 90,
    //                 'user_id' => $allUser->id,
    //                 'siteUrls' =>  $accountURLs1,
    //                 // 'metrics' =>  ["clicks", "ctr", "position", "impressions"],
    //                 // 'dimensions' =>  ["page", "query", "country"],
    //                 'dimensions' =>  ["query"],
    //                 'job_id' => $j_Did[1][0],
    //                 'frequency' => "W-Sun"
    //                     ],
    //                 ];
    //             $clientD14 = new \GuzzleHttp\Client();
    //             $resD14 = $clientD14->post($endpointD14, $optionsD14);
    //             $j_Did2 = json_decode($resD14->getBody(),true);

    //               //Updating or Saving Record Seperately For Daily Call
    //               $checkPreviousJobIdW = Jobid::where('jobtype_id',2)
    //                                     ->where('user_id',$allUser->id)
    //                                     ->where('propertyUrl',$exploded_url)->first();
    //                 if($checkPreviousJobIdW)
    //                 {
    //                     Jobid::where('jobtype_id',2)
    //                             ->where('user_id',$allUser->id)
    //                             ->where('propertyUrl',$exploded_url)->update([
    //                                         'updateDb_JobId' => $j_Did[1][0],
    //                                         'updateAnomaly_JobId' => $j_Did2[1][0]
    //                                     ]);
    //                 }
    //                 else
    //                 {
    //                     $upJobId2 = new Jobid();
    //                     $upJobId2->jobtype_id = 2;
    //                     $upJobId2->user_id = $allUser->id;
    //                     $upJobId2->propertyUrl = $exploded_url;
    //                     $upJobId2->updateDb_JobId = $j_Did[1][0];
    //                     $upJobId2->updateAnomaly_JobId = $j_Did2[1][0];
    //                     $upJobId2->save();
    //                 }

    //             //     //Additional Code for calling Weekly API's and saving there record
    //             //   $clickEndPoint = 'https://ai.dataclu.com/anomalies/trends?user_id='.$allUser->id.'&siteUrl='.$uAnalytic->siteUrl.'&metric=clicks&dimensions=["query"]&range=25&frequency=W-Sun';
    //             // //   $ctrEndPoint = 'https://ai.dataclu.com/anomalies/trends?user_id='.$allUser->id.'&siteUrl='.$uAnalytic->siteUrl.'&metric=ctr&dimensions=["query"]&range=25&frequency=W-Sun';
    //             //   $impressionEndPoint = 'https://ai.dataclu.com/anomalies/trends?user_id='.$allUser->id.'&siteUrl='.$uAnalytic->siteUrl.'&metric=impressions&dimensions=["query"]&range=25&frequency=W-Sun';
    //             // //   $positionEndPoint = 'https://ai.dataclu.com/anomalies/trends?user_id='.$allUser->id.'&siteUrl='.$uAnalytic->siteUrl.'&metric=position&dimensions=["query"]&range=25&frequency=W-Sun';

    //             //     // Calling and saving respone of click Api
    //             // $clickClient = new \GuzzleHttp\Client();
    //             // $clickApi = $clickClient->get($clickEndPoint);
    //             // $clickResp = json_decode($clickApi->getBody(),true);

    //             // $checkPreviousRecord = ApiDataResponse::where('user_id',$allUser->id)
    //             //                         ->where('domain_name',$uAnalytic->siteUrl)
    //             //                         ->where('data_type',2)
    //             //                         ->where('metric_name','clicks')->first();

    //             // if($checkPreviousRecord)
    //             // {
    //             //       ApiDataResponse::where('user_id',$allUser->id)
    //             //                         ->where('domain_name',$uAnalytic->siteUrl)
    //             //                         ->where('data_type',2)
    //             //                         ->where('metric_name','clicks')->update([
    //             //                                 'response' => serialize($clickResp)
    //             //                         ]);
    //             // }
    //             // else
    //             // {
    //             //     $newClick = new ApiDataResponse();
    //             //     $newClick->user_id = $allUser->id;
    //             //     $newClick->data_type = 2;
    //             //     $newClick->domain_name = $uAnalytic->siteUrl;
    //             //     $newClick->metric_name = 'clicks';
    //             //     $newClick->response = serialize($clickResp);
    //             //     $newClick->save();
    //             // }

    //             //        // Calling and saving respone of impression Api
    //             // $impressionClient = new \GuzzleHttp\Client();
    //             // $impressionApi = $impressionClient->get($impressionEndPoint);
    //             // $impressionResp = json_decode($impressionApi->getBody(),true);

    //             // $checkImpressionPreviousRecord = ApiDataResponse::where('user_id',$allUser->id)
    //             //                         ->where('domain_name',$uAnalytic->siteUrl)
    //             //                         ->where('data_type',2)
    //             //                         ->where('metric_name','impressions')->first();

    //             // if($checkImpressionPreviousRecord)
    //             // {
    //             //       ApiDataResponse::where('user_id',$allUser->id)
    //             //                         ->where('domain_name',$uAnalytic->siteUrl)
    //             //                         ->where('data_type',2)
    //             //                         ->where('metric_name','impressions')->update([
    //             //                                 'response' => serialize($impressionResp)
    //             //                         ]);
    //             // }
    //             // else
    //             // {
    //             //     $newImpression = new ApiDataResponse();
    //             //     $newImpression->user_id = $allUser->id;
    //             //     $newImpression->data_type = 2;
    //             //     $newImpression->domain_name = $uAnalytic->siteUrl;
    //             //     $newImpression->metric_name = 'impressions';
    //             //     $newImpression->response = serialize($impressionResp);
    //             //     $newImpression->save();
    //             // }

    //             //       // Calling and saving respone of ctr Api
    //             // $ctrClient = new \GuzzleHttp\Client();
    //             // $ctrApi = $ctrClient->get($ctrEndPoint);
    //             // $ctrResp = json_decode($ctrApi->getBody(),true);

    //             // $checkCtrPreviousRecord = ApiDataResponse::where('user_id',$allUser->id)
    //             //                         ->where('domain_name',$uAnalytic->siteUrl)
    //             // ->where('data_type',2)
    //             //                         ->where('metric_name','ctr')->first();

    //             // if($checkCtrPreviousRecord)
    //             // {
    //             //       ApiDataResponse::where('user_id',$allUser->id)
    //             //                         ->where('domain_name',$uAnalytic->siteUrl)
    //             // ->where('data_type',2)
    //             //                         ->where('metric_name','ctr')->update([
    //             //                                 'response' => serialize($ctrResp)
    //             //                         ]);
    //             // }
    //             // else
    //             // {
    //             //     $newCtr = new ApiDataResponse();
    //             //     $newCtr->user_id = $allUser->id;
    //             //     $newCtr->data_type = 2;
    //             //     $newCtr->domain_name = $uAnalytic->siteUrl;
    //             //     $newCtr->metric_name = 'ctr';
    //             //     $newCtr->response = serialize($ctrResp);
    //             //     $newCtr->save();
    //             // }

    //             //      // Calling and saving respone of position Api
    //             // $positionClient = new \GuzzleHttp\Client();
    //             // $positionApi = $positionClient->get($positionEndPoint);
    //             // $positionResp = json_decode($positionApi->getBody(),true);

    //             // $checkPositionPreviousRecord = ApiDataResponse::where('user_id',$allUser->id)
    //             //                         ->where('domain_name',$uAnalytic->siteUrl)
    //             // ->where('data_type',2)
    //             //                         ->where('metric_name','position')->first();

    //             // if($checkPositionPreviousRecord)
    //             // {
    //             //       ApiDataResponse::where('user_id',$allUser->id)
    //             //                         ->where('domain_name',$uAnalytic->siteUrl)
    //             // ->where('data_type',2)
    //             //                         ->where('metric_name','position')->update([
    //             //                                 'response' => serialize($positionResp)
    //             //                         ]);
    //             // }
    //             // else
    //             // {
    //             //     $newPosition = new ApiDataResponse();
    //             //     $newPosition->user_id = $allUser->id;
    //             //     $newPosition->data_type = 2;
    //             //     $newPosition->domain_name = $uAnalytic->siteUrl;
    //             //     $newPosition->metric_name = 'position';
    //             //     $newPosition->response = serialize($positionResp);
    //             //     $newPosition->save();
    //             // }

    //             }

                

    //         //      //  Update Database With G-Analytics Database
    //         //         $endpointD13 = 'https://ai.dataclu.com/update/database/';
    //         //         $optionsD13 = [
    //         //             'json' => [
    //         //             'days' => 90,
    //         //             'user_id' => $allUser->id,
    //         //             'siteUrls' =>  $accountURLs1,
    //         //                 ],
    //         //             ];
    //         //         $clientD13 = new \GuzzleHttp\Client();
    //         //         $resD13 = $clientD13->post($endpointD13, $optionsD13);
        
    //         //         $j_Did = json_decode($resD13->getBody(),true);

    //         //         \Log::info($j_Did[1][0]);

     

    //         //   //  Update Anomailes in DB
    //         //     $endpointD14 = 'https://ai.dataclu.com/update/anomalies';
    //         //     $optionsD14 = [
    //         //         'json' => [
    //         //         'days' => 90,
    //         //         'user_id' => $allUser->id,
    //         //         'siteUrls' =>  $accountURLs1,
    //         //         // 'metrics' =>  ["clicks", "ctr", "position", "impressions"],
    //         //         'dimensions' =>  ["page", "query", "country"],
    //         //         'job_id' => $j_Did[1][0],
    //         //         'frequency' => "D"
    //         //             ],
    //         //         ];
    //         //     $clientD14 = new \GuzzleHttp\Client();
    //         //     $resD14 = $clientD14->post($endpointD14, $optionsD14);

    //         }
    //     }
    // }
        
    //     return redirect()->back()->withSuccess('Weekly Calls Anomalies Updated Successfully!');
    // }

    // Code Commented Ends Here

    // Code Commented Starts Here

    // public function updateApiDbDataWeekly()
    // {
    //     $allUsers = User::whereNotBetween('id', [1, 2])->get(); //3
    //     if($allUsers->count() > 0)
    //     {
    //     foreach ($allUsers as $allUser)
    //     {
    //         $allUserAnalytics = ConsoleList::where('user_id',$allUser->id)->get(); //2
    //         if($allUserAnalytics->count() > 0)
    //         {

    //             foreach($allUserAnalytics as $uAnalytic)
    //             {
                   
    //                 //Additional Code for calling Weekly API's and saving there record
    //               $clickEndPoint = 'https://ai.dataclu.com/anomalies/trends?user_id='.$allUser->id.'&siteUrl='.$uAnalytic->siteUrl.'&metric=clicks&dimensions=["query"]&range=25&frequency=W-Sun';
    //             //   $ctrEndPoint = 'https://ai.dataclu.com/anomalies/trends?user_id='.$allUser->id.'&siteUrl='.$uAnalytic->siteUrl.'&metric=ctr&dimensions=["query"]&range=25&frequency=W-Sun';
    //               $impressionEndPoint = 'https://ai.dataclu.com/anomalies/trends?user_id='.$allUser->id.'&siteUrl='.$uAnalytic->siteUrl.'&metric=impressions&dimensions=["query"]&range=25&frequency=W-Sun';
    //             //   $positionEndPoint = 'https://ai.dataclu.com/anomalies/trends?user_id='.$allUser->id.'&siteUrl='.$uAnalytic->siteUrl.'&metric=position&dimensions=["query"]&range=25&frequency=W-Sun';

    //                 // Calling and saving respone of click Api
    //             $clickClient = new \GuzzleHttp\Client();
    //             $clickApi = $clickClient->get($clickEndPoint);
    //             $clickResp = json_decode($clickApi->getBody(),true);

    //             $checkPreviousRecord = ApiDataResponse::where('user_id',$allUser->id)
    //                                     ->where('domain_name',$uAnalytic->siteUrl)
    //                                     ->where('data_type',2)
    //                                     ->where('metric_name','clicks')->first();

    //             if($checkPreviousRecord)
    //             {
    //                   ApiDataResponse::where('user_id',$allUser->id)
    //                                     ->where('domain_name',$uAnalytic->siteUrl)
    //                                     ->where('data_type',2)
    //                                     ->where('metric_name','clicks')->update([
    //                                             'response' => serialize($clickResp)
    //                                     ]);
    //             }
    //             else
    //             {
    //                 $newClick = new ApiDataResponse();
    //                 $newClick->user_id = $allUser->id;
    //                 $newClick->data_type = 2;
    //                 $newClick->domain_name = $uAnalytic->siteUrl;
    //                 $newClick->metric_name = 'clicks';
    //                 $newClick->response = serialize($clickResp);
    //                 $newClick->save();
    //             }

    //                    // Calling and saving respone of impression Api
    //             $impressionClient = new \GuzzleHttp\Client();
    //             $impressionApi = $impressionClient->get($impressionEndPoint);
    //             $impressionResp = json_decode($impressionApi->getBody(),true);

    //             $checkImpressionPreviousRecord = ApiDataResponse::where('user_id',$allUser->id)
    //                                     ->where('domain_name',$uAnalytic->siteUrl)
    //                                     ->where('data_type',2)
    //                                     ->where('metric_name','impressions')->first();

    //             if($checkImpressionPreviousRecord)
    //             {
    //                   ApiDataResponse::where('user_id',$allUser->id)
    //                                     ->where('domain_name',$uAnalytic->siteUrl)
    //                                     ->where('data_type',2)
    //                                     ->where('metric_name','impressions')->update([
    //                                             'response' => serialize($impressionResp)
    //                                     ]);
    //             }
    //             else
    //             {
    //                 $newImpression = new ApiDataResponse();
    //                 $newImpression->user_id = $allUser->id;
    //                 $newImpression->data_type = 2;
    //                 $newImpression->domain_name = $uAnalytic->siteUrl;
    //                 $newImpression->metric_name = 'impressions';
    //                 $newImpression->response = serialize($impressionResp);
    //                 $newImpression->save();
    //             }
    //         }

    //         }
    //     }
    // }
        
    //     return redirect()->back()->withSuccess('Weekly Calls Data Updated Successfully!');
    // }

    // Code Commented Ends Here
    

    public function getApiDataDb($domain, $type, $metric)
    {
        $getApiDataFromDb = ApiDataResponse::where('user_id',Auth::id())
      ->where('data_type',(int) $type)
      ->where('metric_name',$metric)
      ->where('domain_name',$domain)->first();

      if($getApiDataFromDb)
      {
        $data = unserialize($getApiDataFromDb->response);
        $jsData = json_encode($data);

         return response()->json($jsData, 200);
      }

    }

    public function gaIndex()
    {
        $authUserID = Auth::id();
        $websitesList = AnalyticsAccount::where('user_id',$authUserID)->get();
        return view('admin.ga.index')->with([
            'webList' => $websitesList,
        'userSingleId' => $authUserID,
        // 'userJid' => $userj_id,
        ]);
    }

// Comment Starts Here

//     //Daily Anomalies Update Google Analytics
//       public function gaupdateApiDbDaily()
//     {
//         $allUsers = User::whereNotBetween('id', [1, 2])->get(); //3
//         if($allUsers->count() > 0)
//         {
//         foreach ($allUsers as $allUser)
//         {
//             $allUserAnalytics = AnalyticsAccount::where('user_id',$allUser->id)->get(); //2
//             if($allUserAnalytics->count() > 0)
//             {

//                 foreach($allUserAnalytics as $uAnalytic)
//                 {
//                     $accountURLs1 = array();
//                     // $complete_url = (explode("/",$uAnalytic->Property_Url));
//                     // $exploded_url = "sc-domain:".$complete_url[2];
//                     $exploded_url = $uAnalytic->ViewId;
//                     array_push($accountURLs1,$exploded_url);

//                          //  Update Database With G-Analytics Database
//                     $endpointD13 = 'https://ai.dataclu.com/update/database/';
//                     $optionsD13 = [
//                         'json' => [
//                         'days' => "last_requested",
//                         'user_id' => $allUser->id,
//                         'View_Ids' =>  $accountURLs1,
//                             ],
//                         ];
//                     $clientD13 = new \GuzzleHttp\Client();
//                     $resD13 = $clientD13->post($endpointD13, $optionsD13);
        
//                     $j_Did = json_decode($resD13->getBody(),true);

//                              //  Update Anomailes in DB
//                 $endpointD14 = 'https://ai.dataclu.com/ga/update/anomalies';
//                 $optionsD14 = [
//                     'json' => [
//                     'days' => 90,
//                     'user_id' => $allUser->id,
//                     'View_Ids' =>  $accountURLs1,
//                     'dimensions' =>  ["pagePath"],
//                     'job_id' => $j_Did[1][0],
//                     'frequency' => "D"
//                         ],
//                     ];
//                 $clientD14 = new \GuzzleHttp\Client();
//                 $resD14 = $clientD14->post($endpointD14, $optionsD14);
//                 $j_Did2 = json_decode($resD14->getBody(),true);

//                   //Updating or Saving Record Seperately For Daily Call
//                   $checkPreviousJobIdW = Jobid::where('jobtype_id',2)
//                                         ->where('user_id',$allUser->id)
//                                         ->where('propertyUrl',$exploded_url)->first();
//                     if($checkPreviousJobIdW)
//                     {
//                         Jobid::where('jobtype_id',2)
//                                 ->where('user_id',$allUser->id)
//                                 ->where('propertyUrl',$exploded_url)->update([
//                                             'updateDb_JobId' => $j_Did[1][0],
//                                             'updateAnomaly_JobId' => $j_Did2[1][0]
//                                         ]);
//                     }
//                     else
//                     {
//                         $upJobId2 = new Jobid();
//                         $upJobId2->jobtype_id = 1;
//                         $upJobId2->user_id = $allUser->id;
//                         $upJobId2->propertyUrl = $exploded_url;
//                         $upJobId2->updateDb_JobId = $j_Did[1][0];
//                         $upJobId2->updateAnomaly_JobId = $j_Did2[1][0];
//                         $upJobId2->account_type = 2;
//                         $upJobId2->save();
//                     }
//                 }
//             }
//         }
//     }
        
//         return redirect()->back()->withSuccess('Weekly Calls Anomalies Updated Successfully!');
//     }
// //Daily Update Google Analytics Data Ended

// Comment Ends Here

// Comment Starts Here

//     //Daily Anomalies Update Google Analytics
//       public function gaupdateApiDbWeekly()
//     {
//         $allUsers = User::whereNotBetween('id', [1, 2])->get(); //3
//         if($allUsers->count() > 0)
//         {
//         foreach ($allUsers as $allUser)
//         {
//             $allUserAnalytics = AnalyticsAccount::where('user_id',$allUser->id)->get(); //2
//             if($allUserAnalytics->count() > 0)
//             {

//                 foreach($allUserAnalytics as $uAnalytic)
//                 {
//                     $accountURLs1 = array();
//                     $exploded_url = $uAnalytic->ViewId;
//                     array_push($accountURLs1,$exploded_url);

//                          //  Update Database With G-Analytics Database
//                     $endpointD13 = 'https://ai.dataclu.com/update/database/';
//                     $optionsD13 = [
//                         'json' => [
//                         'days' => "last_requested",
//                         'user_id' => $allUser->id,
//                         'View_Ids' =>  $accountURLs1,
//                             ],
//                         ];
//                     $clientD13 = new \GuzzleHttp\Client();
//                     $resD13 = $clientD13->post($endpointD13, $optionsD13);
        
//                     $j_Did = json_decode($resD13->getBody(),true);

//                              //  Update Anomailes in DB
//                 $endpointD14 = 'https://ai.dataclu.com/ga/update/anomalies';
//                 $optionsD14 = [
//                     'json' => [
//                     'days' => 90,
//                     'user_id' => $allUser->id,
//                     'View_Ids' =>  $accountURLs1,
//                     'dimensions' =>  ["pagePath"],
//                     'job_id' => $j_Did[1][0],
//                     'frequency' => "W-Sun"
//                         ],
//                     ];
//                 $clientD14 = new \GuzzleHttp\Client();
//                 $resD14 = $clientD14->post($endpointD14, $optionsD14);
//                 $j_Did2 = json_decode($resD14->getBody(),true);

//                   //Updating or Saving Record Seperately For Daily Call
//                   $checkPreviousJobIdW = Jobid::where('jobtype_id',2)
//                                         ->where('user_id',$allUser->id)
//                                         ->where('propertyUrl',$exploded_url)->first();
//                     if($checkPreviousJobIdW)
//                     {
//                         Jobid::where('jobtype_id',2)
//                                 ->where('user_id',$allUser->id)
//                                 ->where('propertyUrl',$exploded_url)->update([
//                                             'updateDb_JobId' => $j_Did[1][0],
//                                             'updateAnomaly_JobId' => $j_Did2[1][0]
//                                         ]);
//                     }
//                     else
//                     {
//                         $upJobId2 = new Jobid();
//                         $upJobId2->jobtype_id = 2;
//                         $upJobId2->user_id = $allUser->id;
//                         $upJobId2->propertyUrl = $exploded_url;
//                         $upJobId2->updateDb_JobId = $j_Did[1][0];
//                         $upJobId2->updateAnomaly_JobId = $j_Did2[1][0];
//                         $upJobId2->account_type = 2;
//                         $upJobId2->save();
//                     }
//                 }
//             }
//         }
//     }
        
//         return redirect()->back()->withSuccess('Weekly Calls Anomalies Updated Successfully!');
//     }
// //Daily Update Google Analytics Data Ended

// Comment Ends Here

// Comment Starts Here
    // public function gaUpdateDbDaily()
    // {
    //    $allUsers = User::whereNotBetween('id', [1, 2])->get(); //3
    //     if($allUsers->count() > 0)
    //     {
    //     foreach ($allUsers as $allUser)
    //     {
    //         $allUserAnalytics = AnalyticsAccount::where('user_id',$allUser->id)->get(); //2
    //         if($allUserAnalytics->count() > 0)
    //         {

    //             foreach($allUserAnalytics as $uAnalytic)
    //             {
                    
    //               //Additional Code for calling Daily API's and saving there record
    //               $gaEndPointDaily = 'https://ai.dataclu.com/anomalies/trends?user_id='.$allUser->id.'&View_Id='.$uAnalytic->ViewId.'&metric=pageviews&dimensions=["pagePath"]&range=40&frequency=D';

    //                 // Calling and saving respone of click Api
    //             $pageViewClient = new \GuzzleHttp\Client();
    //             $pageViewApi = $pageViewClient->get($gaEndPointDaily);
    //             $pageViewResp = json_decode($pageViewApi->getBody(),true);

    //             $checkPreviousRecord = ApiDataResponse::where('user_id',$allUser->id)
    //                                     ->where('domain_name',$uAnalytic->ViewId)
    //                                     ->where('data_type',1)
    //                                     ->where('metric_name','pageviews')->first();

    //             if($checkPreviousRecord)
    //             {
    //                   ApiDataResponse::where('user_id',$allUser->id)
    //                                     ->where('domain_name',$uAnalytic->ViewId)
    //                                     ->where('data_type',1)
    //                                     ->where('metric_name','pageviews')->update([
    //                                             'response' => serialize($pageViewResp)
    //                                     ]);
    //             }
    //             else
    //             {
    //                 $newPageView = new ApiDataResponse();
    //                 $newPageView->user_id = $allUser->id;
    //                 $newPageView->data_type = 1;
    //                 $newPageView->domain_name = $uAnalytic->ViewId;
    //                 $newPageView->metric_name = 'pageviews';
    //                 $newPageView->response = serialize($pageViewResp);
    //                 $newPageView->account_type = 2;
    //                 $newPageView->save();
    //             }



              
               

    //             }

                

    //         }
    //     }
    // }     
    // }

    // Comment Ends Here

    // Comment Strats Here
    
    //  public function gaUpdateDbWeely()
    // {
    //    $allUsers = User::whereNotBetween('id', [1, 2])->get(); //3
    //     if($allUsers->count() > 0)
    //     {
    //     foreach ($allUsers as $allUser)
    //     {
    //         $allUserAnalytics = AnalyticsAccount::where('user_id',$allUser->id)->get(); //2
    //         if($allUserAnalytics->count() > 0)
    //         {

    //             foreach($allUserAnalytics as $uAnalytic)
    //             {
                    
    //               //Additional Code for calling Daily API's and saving there record
    //               $gaEndPointDaily = 'https://ai.dataclu.com/anomalies/trends?user_id='.$allUser->id.'&View_Id='.$uAnalytic->ViewId.'&metric=pageviews&dimensions=["pagePath"]&range=40&frequency=W-Sun';

    //                 // Calling and saving respone of click Api
    //             $pageViewClient = new \GuzzleHttp\Client();
    //             $pageViewApi = $pageViewClient->get($gaEndPointDaily);
    //             $pageViewResp = json_decode($pageViewApi->getBody(),true);

    //             $checkPreviousRecord = ApiDataResponse::where('user_id',$allUser->id)
    //                                     ->where('domain_name',$uAnalytic->ViewId)
    //                                     ->where('data_type',2)
    //                                     ->where('metric_name','pageviews')->first();

    //             if($checkPreviousRecord)
    //             {
    //                   ApiDataResponse::where('user_id',$allUser->id)
    //                                     ->where('domain_name',$uAnalytic->ViewId)
    //                                     ->where('data_type',2)
    //                                     ->where('metric_name','pageviews')->update([
    //                                             'response' => serialize($pageViewResp)
    //                                     ]);
    //             }
    //             else
    //             {
    //                 $newPageView = new ApiDataResponse();
    //                 $newPageView->user_id = $allUser->id;
    //                 $newPageView->data_type = 2;
    //                 $newPageView->domain_name = $uAnalytic->ViewId;
    //                 $newPageView->metric_name = 'pageviews';
    //                 $newPageView->response = serialize($pageViewResp);
    //                 $newPageView->account_type = 2;
    //                 $newPageView->save();
    //             }



              
               

    //             }

                

    //         }
    //     }
    // }     
    // }

    // Comment Ends Here

    public function corRelationValues(Request $request)
    {
        // return $request->all();
        // $validator = Validator::make($request->all(), [ 
        //     'analytics_correlate_metric' => 'required', 
        // ]);   
        // if ($validator->fails()) 
        // { 
        //     return response()->json(['error'=>$validator->errors()], 422);            
        // }
        $getData = AnalyticsCorelateMetrics::where('id',1)->first();
        if($getData)
        {
            $updateData = AnalyticsCorelateMetrics::where('id',1)->update([
           'correlation_metric_response' => serialize($request->all()),
            ]);

                if($updateData)
                {
                    $getDataUp = AnalyticsCorelateMetrics::where('id',1)->first();
                    return response()->json(['Success'=> unserialize($getDataUp->correlation_metric_response)], 200);
                }
                else
                {
                    return response()->json(['Error'=> 'Error in Updating Data'], 404);
                }
                
        }
        else
        {

            $saveRecord = new AnalyticsCorelateMetrics();
            $saveRecord->correlation_metric_response = serialize($request->all());
            $saveRecord->save();

        if($saveRecord)
        {
            return response()->json(['Success'=> 'Data Saved Successfully.'], 200);
        }
        else
        {
            return response()->json(['Error'=> 'Error in Saving Data'], 404);
        }
    }

        

    }

    public function getCorrelatedData()
    {
         $leadAndLagg = AnalyticsCorelateMetrics::where('id',1)->first();
         if($leadAndLagg)
         {
        $dataCorrelate = unserialize($leadAndLagg->correlation_metric_response);
        $dtCor = json_encode($dataCorrelate);

         return response()->json($dtCor, 200);
         }
         
    }

    public function AnalyticsConsoleData(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'data' => 'required',
            'frequency' => 'required',
            
        ]);   
        if ($validator->fails()) 
        { 
            return response()->json(['error'=>$validator->errors()], 422);            
        }
        if($request->frequency == 'D')
        {
            $getAllData = ApiDataResponse::where('user_id',$request->user_id)
                                        ->where('data_type',1)        
                                        ->where('metric_name','analytics & console data')->first();

            if($getAllData)
            {
                $updateAllData = ApiDataResponse::where('user_id',$request->user_id)
                                        ->where('data_type',1)         
                                        ->where('metric_name','analytics & console data')->update([
                                    'response' => serialize($request->data),
                        ]);
                        if($updateAllData)
                        {
                            $checkAllData = ApiDataResponse::where('user_id',$request->user_id)
                                        ->where('data_type',1)         
                                        ->where('metric_name','analytics & console data')->first();
                           return response()->json(['Success'=> unserialize($checkAllData->response)], 200);
                        }
                        else
                        {
                            return response()->json(['Error'=> 'Error in Updating Data'], 404);
                        }
            }
            else
            {
            $saveAllRecord = new ApiDataResponse();
            $saveAllRecord->user_id = $request->user_id;
            $saveAllRecord->data_type = 1;
            $saveAllRecord->domain_name = "not required";
            $saveAllRecord->metric_name = "analytics & console data";
            $saveAllRecord->response = serialize($request->data);
            $saveAllRecord->save();
            
            if($saveAllRecord)
                {
                    $checkAllData = ApiDataResponse::where('user_id',$request->user_id)
                                        ->where('data_type',1)                  
                                        ->where('metric_name','analytics & console data')->first();
                           return response()->json(['Success'=> unserialize($checkAllData->response)], 200);
                }
            else
                {
                    return response()->json(['Error'=> 'Error in Saving Data'], 404);
                }
            }
        }
        elseif ($request->frequency == 'W-Sun')
        {
             $getAllData = ApiDataResponse::where('user_id',$request->user_id)
                                        ->where('data_type',2)                       
                                        ->where('metric_name','analytics & console data')->first();

            if($getAllData)
            {
                $updateAllData = ApiDataResponse::where('user_id',$request->user_id)
                                        ->where('data_type',2)              
                                        ->where('metric_name','analytics & console data')->update([
                                    'response' => serialize($request->data),
                        ]);
                        if($updateAllData)
                        {
                            $checkAllData = ApiDataResponse::where('user_id',$request->user_id)
                                        ->where('data_type',2)      
                                        ->where('metric_name','analytics & console data')->first();
                           return response()->json(['Success'=> unserialize($checkAllData->response)], 200);
                        }
                        else
                        {
                            return response()->json(['Error'=> 'Error in Updating Data'], 404);
                        }
            }
            else
            {
            $saveAllRecord = new ApiDataResponse();
            $saveAllRecord->user_id = $request->user_id;
            $saveAllRecord->data_type = 2;
            $saveAllRecord->domain_name = "not required";
            $saveAllRecord->metric_name = "analytics & console data";
            $saveAllRecord->response = serialize($request->data);
            $saveAllRecord->save();
            
            if($saveAllRecord)
                {
                    $checkAllData = ApiDataResponse::where('user_id',$request->user_id)
                                        ->where('data_type',2)
                                        ->where('metric_name','analytics & console data')->first();
                           return response()->json(['Success'=> unserialize($checkAllData->response)], 200);
                }
            else
                {
                    return response()->json(['Error'=> 'Error in Saving Data'], 404);
                }
            }
        }
        else
        {
            return response()->json(['error'=> 'Frequency can be D or W-Sun'], 400); 
        }
        return response()->json(['Success'=> $request->data], 200);
        // $getAllData = ApiDataResponse::where('user_id',$request->user_id)
        //                                 ->where('data_type',3)
        //                                 ->where('domain_name','combined')
        //                                 ->where('metric_name','combined')->first();
            // if($getAllData)
            // {
            //     $updateAllData = ApiDataResponse::where('user_id',$request->user_id)
            //                             ->where('data_type',3)
            //                             ->where('domain_name','combined')
            //                             ->where('metric_name','combined')->update([
            //         'response' => serialize($request->data),
            //             ]);
            //             if($updateAllData)
            //             {
            //                 $checkAllData = ApiDataResponse::where('user_id',$request->user_id)
            //                             ->where('data_type',3)
            //                             ->where('domain_name','combined')
            //                             ->where('metric_name','combined')->first();
            //                return response()->json(['Success'=> unserialize($checkAllData->response)], 200);
            //             }
            //             else
            //             {
            //                 return response()->json(['Error'=> 'Error in Updating Data'], 404);
            //             }
            // }
            // else
            // {
            // $saveAllRecord = new ApiDataResponse();
            // $saveAllRecord->user_id = $request->user_id;
            // $saveAllRecord->data_type = 3;
            // $saveAllRecord->domain_name = "combined";
            // $saveAllRecord->metric_name = "combined";
            // $saveAllRecord->response = serialize($request->data);
            // $saveAllRecord->save();
            // }
            // if($saveAllRecord)
            //     {
            //         $checkAllData = ApiDataResponse::where('user_id',$request->user_id)
            //                             ->where('data_type',3)
            //                             ->where('domain_name','combined')
            //                             ->where('metric_name','combined')->first();
            //                return response()->json(['Success'=> unserialize($checkAllData->response)], 200);
            //     }
            // else
            //     {
            //         return response()->json(['Error'=> 'Error in Saving Data'], 404);
            //     }
    }

    // New Logic Of Update Api
    public function updateDataDailyNew()
    {
        $allUsers = User::whereNotBetween('id', [1, 2])->get(); //3
        if($allUsers->count() > 0)
        {
            
        foreach ($allUsers as $allUser)
        {
            $sourceArray = [];
            $allUserAnalytics = AnalyticsAccount::where('user_id',$allUser->id)->get();    
            $allUserConsole = ConsoleList::where('user_id',$allUser->id)->get(); 
            foreach ($allUserConsole as $uConsole)
            {
                $consolePresentInAnalytics = 0;
                if (strpos($uConsole->siteUrl, 'sc-domain:') !== false) {
                    $explodedConsoleUrl = explode('sc-domain:',$uConsole->siteUrl);
                    if(strpos($explodedConsoleUrl[1], 'www.') !== false )
                    {
                    $finalConsoleUrlSlash = $explodedConsoleUrl[1];
                    }
                    else
                    {
                    $finalConsoleUrlSlash = "www.".$explodedConsoleUrl[1];
                    }
                    $finalConsoleUrl = str_replace("/","",$finalConsoleUrlSlash);
                }
                if (strpos($uConsole->siteUrl, 'https://') !== false) {
                    $explodedConsoleUrl = explode('https://',$uConsole->siteUrl);
                    if(strpos($explodedConsoleUrl[1], 'www.') !== false )
                    {
                    $finalConsoleUrlSlash = $explodedConsoleUrl[1];
                    }
                    else
                    {
                    $finalConsoleUrlSlash = "www.".$explodedConsoleUrl[1];
                    }
                    $finalConsoleUrl = str_replace("/","",$finalConsoleUrlSlash);
                }
                if (strpos($uConsole->siteUrl, 'http://') !== false) {
                    $explodedConsoleUrl = explode('http://',$uConsole->siteUrl);
                    if(strpos($explodedConsoleUrl[1], 'www.') !== false )
                    {
                    $finalConsoleUrlSlash = $explodedConsoleUrl[1];
                    }
                    else
                    {
                    $finalConsoleUrlSlash = "www.".$explodedConsoleUrl[1];
                    }
                    $finalConsoleUrl = str_replace("/","",$finalConsoleUrlSlash);
                }
                foreach ($allUserAnalytics as $uAnalytic)
                {
                    if (strpos($uAnalytic->Property_Url, 'http://') !== false) {
                    $explodedAnalyticsUrl = explode('http://',$uAnalytic->Property_Url);
                    if(strpos($explodedAnalyticsUrl[1], 'www.') !== false )
                    {
                    $finalAnalyticUrlSlash = $explodedAnalyticsUrl[1];
                    }
                    else
                    {
                    $finalAnalyticUrlSlash = "www.".$explodedAnalyticsUrl[1];
                    }
                    $finalAnalyticUrl = str_replace("/","",$finalAnalyticUrlSlash);
                    }
                    if (strpos($uAnalytic->Property_Url, 'https://') !== false) {
                    $explodedAnalyticsUrl = explode('https://',$uAnalytic->Property_Url);
                    if(strpos($explodedAnalyticsUrl[1], 'www.') !== false )
                    {
                    $finalAnalyticUrlSlash = $explodedAnalyticsUrl[1];
                    }
                    else
                    {
                    $finalAnalyticUrlSlash = "www.".$explodedAnalyticsUrl[1];
                    }
                    $finalAnalyticUrl = str_replace("/","",$finalAnalyticUrlSlash);
                    }
                    if($finalAnalyticUrl == $finalConsoleUrl)
                    {
                        $againExplode = (explode("www.",$finalConsoleUrl));
                        $newObj = new \stdClass();
                        $newObj->siteUrl = "sc-domain:".$againExplode[1];
                        $newObj->relative_id = $uAnalytic->ViewId;
                        $newObj->dimensions = ["query","pagePath"];
                        $newObj->metrics = ["clicks", "impressions","pageviews", "avgPageLoadTime", "bounceRate"];

                        $finalObj = json_encode($newObj);
                        array_push($sourceArray,$finalObj);

                        $consolePresentInAnalytics = 1;
                    }
                    
                }
                if($consolePresentInAnalytics == 0)
                {
                    $againExplode = (explode("www.",$finalConsoleUrl));
                    $newObj = new \stdClass();
                    $newObj->siteUrl = "sc-domain:".$againExplode[1];
                    $newObj->relative_id = "";
                    $newObj->dimensions = ["query"];
                    $newObj->metrics = ["clicks", "impressions"];

                    $finalObj = json_encode($newObj);
                    array_push($sourceArray,$finalObj);
                }

                
            }

            //Now Reversing the loops to check analytics not present in console
            foreach ($allUserAnalytics as $uAnalytic)
                {
                    $analyticsPresentInConsole = 0;
                    if (strpos($uAnalytic->Property_Url, 'http://') !== false) {
                    $explodedAnalyticsUrl = explode('http://',$uAnalytic->Property_Url);
                    if(strpos($explodedAnalyticsUrl[1], 'www.') !== false )
                    {
                    $finalAnalyticUrlSlash = $explodedAnalyticsUrl[1];
                    }
                    else
                    {
                    $finalAnalyticUrlSlash = "www.".$explodedAnalyticsUrl[1];
                    }
                    $finalAnalyticUrl = str_replace("/","",$finalAnalyticUrlSlash);
                    }
                    if (strpos($uAnalytic->Property_Url, 'https://') !== false) {
                    $explodedAnalyticsUrl = explode('https://',$uAnalytic->Property_Url);
                    if(strpos($explodedAnalyticsUrl[1], 'www.') !== false )
                    {
                    $finalAnalyticUrlSlash = $explodedAnalyticsUrl[1];
                    }
                    else
                    {
                    $finalAnalyticUrlSlash = "www.".$explodedAnalyticsUrl[1];
                    }
                    $finalAnalyticUrl = str_replace("/","",$finalAnalyticUrlSlash);
                    }
                    foreach ($allUserConsole as $uConsole)
                        {
                            if (strpos($uConsole->siteUrl, 'sc-domain:') !== false) {
                                $explodedConsoleUrl = explode('sc-domain:',$uConsole->siteUrl);
                                if(strpos($explodedConsoleUrl[1], 'www.') !== false )
                                {
                                $finalConsoleUrlSlash = $explodedConsoleUrl[1];
                                }
                                else
                                {
                                $finalConsoleUrlSlash = "www.".$explodedConsoleUrl[1];
                                }
                                $finalConsoleUrl = str_replace("/","",$finalConsoleUrlSlash);
                            }
                            if (strpos($uConsole->siteUrl, 'https://') !== false) {
                                $explodedConsoleUrl = explode('https://',$uConsole->siteUrl);
                                if(strpos($explodedConsoleUrl[1], 'www.') !== false )
                                {
                                $finalConsoleUrlSlash = $explodedConsoleUrl[1];
                                }
                                else
                                {
                                $finalConsoleUrlSlash = "www.".$explodedConsoleUrl[1];
                                }
                                $finalConsoleUrl = str_replace("/","",$finalConsoleUrlSlash);
                            }
                            if (strpos($uConsole->siteUrl, 'http://') !== false) {
                                $explodedConsoleUrl = explode('http://',$uConsole->siteUrl);
                                if(strpos($explodedConsoleUrl[1], 'www.') !== false )
                                {
                                $finalConsoleUrlSlash = $explodedConsoleUrl[1];
                                }
                                else
                                {
                                $finalConsoleUrlSlash = "www.".$explodedConsoleUrl[1];
                                }
                                $finalConsoleUrl = str_replace("/","",$finalConsoleUrlSlash);
                            }
                            if($finalAnalyticUrl == $finalConsoleUrl)
                                {
                                    $analyticsPresentInConsole = 1;
                                }
                        }
                        if($analyticsPresentInConsole == 0)
                        {
                            $newObj = new \stdClass();
                            $newObj->siteUrl = "";
                            $newObj->relative_id = $uAnalytic->ViewId;
                            $newObj->dimensions = ["pagePath"];
                            $newObj->metrics = ["pageviews", "avgPageLoadTime", "bounceRate"];

                            $finalObj = json_encode($newObj);
                            array_push($sourceArray,$finalObj);
                        }
                }
            // $analticsArray = [];
            // $consoleArray = [];
            // foreach ($allUserConsole as $uConsole)
            // {
            //     $newObj = new \stdClass();
            //     $newObj->siteUrl = $uConsole->siteUrl;
            //     $newObj->dimensions = ["query"];
            //     $newObj->metrics = ["clicks", "impressions"];

            //     $finalObj = json_encode($newObj);
            //     array_push($consoleArray,$newObj);
            // }
            // foreach ($allUserAnalytics as $uAnalytic)
            // {
            //     $newObj = new \stdClass();
            //     $newObj->View_Id = $uAnalytic->ViewId;
            //     $newObj->dimensions = ["pagePath"];
            //     $newObj->metrics = ["pageviews", "avgPageLoadTime", "bounceRate"];

            //     $finalObj = json_encode($newObj);
            //     array_push($analticsArray,$newObj);
            // }
            // $finalArray = [];
            // $finalArray = array_merge($consoleArray,$analticsArray);

            // dd($sourceArray);

            
                $saveAnomaliesEndPoint = 'https://ai.dataclu.com/main/save_all_anomalies';
                $saveAnomaliesParametersDaily = [
                    'json' => [
                    'user_id' => $allUser->id,
                    'frequency' => "D",
                    'days' => 90,
                    'range'=> 50,
                    'sources' => $sourceArray,
                        ],
                    ];

                $newAnomalyClient = new \GuzzleHttp\Client();
                $sendingRequest = $newAnomalyClient->post($saveAnomaliesEndPoint, $saveAnomaliesParametersDaily);
                $receivingResult = json_decode($sendingRequest->getBody(),true);

                $checkPreviousResults = Jobid::where('user_id',$allUser->id)->where('propertyUrl','User all Analytics and Console Projects')->where('jobtype_id',1)->first();
                if($checkPreviousResults)
                {
                    Jobid::where('user_id',$allUser->id)->where('propertyUrl','User all Analytics and Console Projects')->where('jobtype_id',1)->update([
                        'updateAnomaly_JobId' => $receivingResult[1],
                    ]);
                }
                else
                {
                $saveJobId = new Jobid();
                $saveJobId->user_id =  $allUser->id;
                $saveJobId->jobtype_id = 1;
                $saveJobId->propertyUrl = "User all Analytics and Console Projects";
                $saveJobId->updateDb_JobId = "Not Required";
                $saveJobId->updateAnomaly_JobId = $receivingResult[1];
                $saveJobId->save();
                }

        }
        }    
        return redirect()->back()->withSuccess('Daily Calls Anomalies Updated Successfully!');
    }
    public function updateDataWeeklyNew()
    {
        $allUsers = User::whereNotBetween('id', [1, 2])->get(); //3
        if($allUsers->count() > 0)
        {
        foreach ($allUsers as $allUser)
        {
            $sourceArray = [];
            $allUserAnalytics = AnalyticsAccount::where('user_id',$allUser->id)->get();    
            $allUserConsole = ConsoleList::where('user_id',$allUser->id)->get(); 
            foreach ($allUserConsole as $uConsole)
            {
                $consolePresentInAnalytics = 0;
                if (strpos($uConsole->siteUrl, 'sc-domain:') !== false) {
                    $explodedConsoleUrl = explode('sc-domain:',$uConsole->siteUrl);
                    if(strpos($explodedConsoleUrl[1], 'www.') !== false )
                    {
                    $finalConsoleUrlSlash = $explodedConsoleUrl[1];
                    }
                    else
                    {
                    $finalConsoleUrlSlash = "www.".$explodedConsoleUrl[1];
                    }
                    $finalConsoleUrl = str_replace("/","",$finalConsoleUrlSlash);
                }
                if (strpos($uConsole->siteUrl, 'https://') !== false) {
                    $explodedConsoleUrl = explode('https://',$uConsole->siteUrl);
                    if(strpos($explodedConsoleUrl[1], 'www.') !== false )
                    {
                    $finalConsoleUrlSlash = $explodedConsoleUrl[1];
                    }
                    else
                    {
                    $finalConsoleUrlSlash = "www.".$explodedConsoleUrl[1];
                    }
                    $finalConsoleUrl = str_replace("/","",$finalConsoleUrlSlash);
                }
                if (strpos($uConsole->siteUrl, 'http://') !== false) {
                    $explodedConsoleUrl = explode('http://',$uConsole->siteUrl);
                    if(strpos($explodedConsoleUrl[1], 'www.') !== false )
                    {
                    $finalConsoleUrlSlash = $explodedConsoleUrl[1];
                    }
                    else
                    {
                    $finalConsoleUrlSlash = "www.".$explodedConsoleUrl[1];
                    }
                    $finalConsoleUrl = str_replace("/","",$finalConsoleUrlSlash);
                }
                foreach ($allUserAnalytics as $uAnalytic)
                {
                    if (strpos($uAnalytic->Property_Url, 'http://') !== false) {
                    $explodedAnalyticsUrl = explode('http://',$uAnalytic->Property_Url);
                    if(strpos($explodedAnalyticsUrl[1], 'www.') !== false )
                    {
                    $finalAnalyticUrlSlash = $explodedAnalyticsUrl[1];
                    }
                    else
                    {
                    $finalAnalyticUrlSlash = "www.".$explodedAnalyticsUrl[1];
                    }
                    $finalAnalyticUrl = str_replace("/","",$finalAnalyticUrlSlash);
                    }
                    if (strpos($uAnalytic->Property_Url, 'https://') !== false) {
                    $explodedAnalyticsUrl = explode('https://',$uAnalytic->Property_Url);
                    if(strpos($explodedAnalyticsUrl[1], 'www.') !== false )
                    {
                    $finalAnalyticUrlSlash = $explodedAnalyticsUrl[1];
                    }
                    else
                    {
                    $finalAnalyticUrlSlash = "www.".$explodedAnalyticsUrl[1];
                    }
                    $finalAnalyticUrl = str_replace("/","",$finalAnalyticUrlSlash);
                    }
                    if($finalAnalyticUrl == $finalConsoleUrl)
                    {
                        $againExplode = (explode("www.",$finalConsoleUrl));
                        $newObj = new \stdClass();
                        $newObj->siteUrl = "sc-domain:".$againExplode[1];
                        $newObj->relative_id = $uAnalytic->ViewId;
                        $newObj->dimensions = ["query","pagePath"];
                        $newObj->metrics = ["clicks", "impressions","pageviews", "avgPageLoadTime", "bounceRate"];

                        $finalObj = json_encode($newObj);
                        array_push($sourceArray,$finalObj);

                        $consolePresentInAnalytics = 1;
                    }
                    
                }
                if($consolePresentInAnalytics == 0)
                {
                    $againExplode = (explode("www.",$finalConsoleUrl));
                    $newObj = new \stdClass();
                    $newObj->siteUrl = "sc-domain:".$againExplode[1];
                    $newObj->relative_id = "";
                    $newObj->dimensions = ["query"];
                    $newObj->metrics = ["clicks", "impressions"];

                    $finalObj = json_encode($newObj);
                    array_push($sourceArray,$finalObj);
                }

                
            }

            //Now Reversing the loops to check analytics not present in console
            foreach ($allUserAnalytics as $uAnalytic)
                {
                    $analyticsPresentInConsole = 0;
                    if (strpos($uAnalytic->Property_Url, 'http://') !== false) {
                    $explodedAnalyticsUrl = explode('http://',$uAnalytic->Property_Url);
                    if(strpos($explodedAnalyticsUrl[1], 'www.') !== false )
                    {
                    $finalAnalyticUrlSlash = $explodedAnalyticsUrl[1];
                    }
                    else
                    {
                    $finalAnalyticUrlSlash = "www.".$explodedAnalyticsUrl[1];
                    }
                    $finalAnalyticUrl = str_replace("/","",$finalAnalyticUrlSlash);
                    }
                    if (strpos($uAnalytic->Property_Url, 'https://') !== false) {
                    $explodedAnalyticsUrl = explode('https://',$uAnalytic->Property_Url);
                    if(strpos($explodedAnalyticsUrl[1], 'www.') !== false )
                    {
                    $finalAnalyticUrlSlash = $explodedAnalyticsUrl[1];
                    }
                    else
                    {
                    $finalAnalyticUrlSlash = "www.".$explodedAnalyticsUrl[1];
                    }
                    $finalAnalyticUrl = str_replace("/","",$finalAnalyticUrlSlash);
                    }
                    foreach ($allUserConsole as $uConsole)
                        {
                            if (strpos($uConsole->siteUrl, 'sc-domain:') !== false) {
                                $explodedConsoleUrl = explode('sc-domain:',$uConsole->siteUrl);
                                if(strpos($explodedConsoleUrl[1], 'www.') !== false )
                                {
                                $finalConsoleUrlSlash = $explodedConsoleUrl[1];
                                }
                                else
                                {
                                $finalConsoleUrlSlash = "www.".$explodedConsoleUrl[1];
                                }
                                $finalConsoleUrl = str_replace("/","",$finalConsoleUrlSlash);
                            }
                            if (strpos($uConsole->siteUrl, 'https://') !== false) {
                                $explodedConsoleUrl = explode('https://',$uConsole->siteUrl);
                                if(strpos($explodedConsoleUrl[1], 'www.') !== false )
                                {
                                $finalConsoleUrlSlash = $explodedConsoleUrl[1];
                                }
                                else
                                {
                                $finalConsoleUrlSlash = "www.".$explodedConsoleUrl[1];
                                }
                                $finalConsoleUrl = str_replace("/","",$finalConsoleUrlSlash);
                            }
                            if (strpos($uConsole->siteUrl, 'http://') !== false) {
                                $explodedConsoleUrl = explode('http://',$uConsole->siteUrl);
                                if(strpos($explodedConsoleUrl[1], 'www.') !== false )
                                {
                                $finalConsoleUrlSlash = $explodedConsoleUrl[1];
                                }
                                else
                                {
                                $finalConsoleUrlSlash = "www.".$explodedConsoleUrl[1];
                                }
                                $finalConsoleUrl = str_replace("/","",$finalConsoleUrlSlash);
                            }
                            if($finalAnalyticUrl == $finalConsoleUrl)
                                {
                                    $analyticsPresentInConsole = 1;
                                }
                        }
                        if($analyticsPresentInConsole == 0)
                        {
                            $newObj = new \stdClass();
                            $newObj->siteUrl = "";
                            $newObj->relative_id = $uAnalytic->ViewId;
                            $newObj->dimensions = ["pagePath"];
                            $newObj->metrics = ["pageviews", "avgPageLoadTime", "bounceRate"];

                            $finalObj = json_encode($newObj);
                            array_push($sourceArray,$finalObj);
                        }
                }
            // $analticsArray = [];
            // $consoleArray = [];
            // foreach ($allUserConsole as $uConsole)
            // {
            //     $newObj = new \stdClass();
            //     $newObj->siteUrl = $uConsole->siteUrl;
            //     $newObj->dimensions = ["query"];
            //     $newObj->metrics = ["clicks", "impressions"];

            //     $finalObj = json_encode($newObj);
            //     array_push($consoleArray,$newObj);
            // }
            // foreach ($allUserAnalytics as $uAnalytic)
            // {
            //     $newObj = new \stdClass();
            //     $newObj->View_Id = $uAnalytic->ViewId;
            //     $newObj->dimensions = ["pagePath"];
            //     $newObj->metrics = ["pageviews", "avgPageLoadTime", "bounceRate"];

            //     $finalObj = json_encode($newObj);
            //     array_push($analticsArray,$newObj);
            // }
            // $finalArray = [];
            // $finalArray = array_merge($consoleArray,$analticsArray);

            
                $saveAnomaliesEndPoint = 'https://ai.dataclu.com/main/save_all_anomalies';
                $saveAnomaliesParametersWeekly = [
                    'json' => [
                    'user_id' => $allUser->id,
                    'frequency' => "W-Sun",
                    'days' => 90,
                    'range'=> 50,
                    'sources' => $sourceArray,
                        ],
                    ];

                    // dd($optionsD14);
                    // print_r($optionsD14);
                $newAnomalyClient = new \GuzzleHttp\Client();
                $sendingRequest = $newAnomalyClient->post($saveAnomaliesEndPoint, $saveAnomaliesParametersWeekly);
                $receivingResult = json_decode($sendingRequest->getBody(),true);

                $checkPreviousResults = Jobid::where('user_id',$allUser->id)->where('propertyUrl','User all Analytics and Console Projects')->where('jobtype_id',2)->first();
                if($checkPreviousResults)
                {
                    Jobid::where('user_id',$allUser->id)->where('propertyUrl','User all Analytics and Console Projects')->where('jobtype_id',2)->update([
                        'updateAnomaly_JobId' => $receivingResult[1],
                    ]);
                }
                else
                {
                $saveJobId = new Jobid();
                $saveJobId->user_id =  $allUser->id;
                $saveJobId->jobtype_id = 2;
                $saveJobId->propertyUrl = "User all Analytics and Console Projects";
                $saveJobId->updateDb_JobId = "Not Required";
                $saveJobId->updateAnomaly_JobId = $receivingResult[1];
                $saveJobId->save();
                }

        }
        }    

        return redirect()->back()->withSuccess('Weekly Calls Anomalies Updated Successfully!');
    }

    public function getSaveAnomalyData($dataResponse,$anomalyId)
    {
        // return response()->json("Ok", 200);
           $getAnomalyData = AnomalyDetailData::where('anomalyId',$anomalyId)->first();
           if($getAnomalyData)
           {
               if($dataResponse == 'anomalyDetail')
               {
               $data = unserialize($getAnomalyData->response);
                $jsData = json_encode($data);

                return response()->json($jsData, 200);
               }
               elseif ($dataResponse == 'relatedMetrics')
               {
                   $data = unserialize($getAnomalyData->related_metrics_response);
                   $jsData = json_encode($data);

                    return response()->json($jsData, 200);
               }
               elseif ($dataResponse == 'relatedAudience')
               {
                   $data = unserialize($getAnomalyData->related_audience_response);
                   $jsData = json_encode($data);

                    return response()->json($jsData, 200);
               }
               else
               {
                   $data = unserialize($getAnomalyData->page_group_anomalies_response);
                   $jsData = json_encode($data);

                    return response()->json($jsData, 200);
               }

           }
           else
           {
               $anomalyCallEndPoint = 'https://ai.dataclu.com/anomalies/trends/details?_id='.$anomalyId;
               $makeNewClient = new \GuzzleHttp\Client();
               try{
               $sendAnomalyCall = $makeNewClient->get($anomalyCallEndPoint);
               $getResultAnomaly = json_decode($sendAnomalyCall->getBody(),true);
               } catch (\Exception $e) {
                    $getResultAnomaly = "Server Error Occured";
                }

               //Related Audience Call
               $anomalyCallEndPointAudience = 'https://ai.dataclu.com/anomalies/trends/details/audience?id='.$anomalyId;
               $makeNewClientAudience = new \GuzzleHttp\Client();
               try{
               $sendAnomalyCallAudience = $makeNewClientAudience->get($anomalyCallEndPointAudience);
               $getResultAnomalyAudience = json_decode($sendAnomalyCallAudience->getBody(),true);
               } catch (\Exception $e) {
                    $getResultAnomalyAudience = "Server Error Occured";
                }

               //Relsated Metrics Calls
               $anomalyCallEndPointMerics = 'https://ai.dataclu.com/anomalies/trends/details/contributors?id='.$anomalyId;
               $makeNewClientMetrics = new \GuzzleHttp\Client();
               try{
               $sendAnomalyCallMetrics = $makeNewClientMetrics->get($anomalyCallEndPointMerics);
               $getResultAnomalyMetrics = json_decode($sendAnomalyCallMetrics->getBody(),true);
               } catch (\Exception $e) {
                    $getResultAnomalyMetrics = "Server Error Occured";
                }

               //Page Group Anomalies Call
               $anomalyCallEndPointPageGroup = 'https://ai.dataclu.com/anomalies/trends/details/page_grouped_anomalies?id='.$anomalyId;
               $makeNewClientPageGroup = new \GuzzleHttp\Client();
               try{
               $sendAnomalyCallPageGroup = $makeNewClientPageGroup->get($anomalyCallEndPointPageGroup);
               $getResultAnomalyPageGroup = json_decode($sendAnomalyCallPageGroup->getBody(),true);
               } catch (\Exception $e) {
                    $getResultAnomalyPageGroup = "Server Error Occured";
                }


            //    if($getResultAnomaly != null)
            //    {
               $saveResp = new AnomalyDetailData();
               $saveResp->anomalyId = $anomalyId;
               $saveResp->response = serialize($getResultAnomaly);
               $saveResp->related_metrics_response = serialize($getResultAnomalyMetrics) ;
               $saveResp->related_audience_response = serialize($getResultAnomalyAudience) ;
               $saveResp->page_group_anomalies_response =serialize($getResultAnomalyPageGroup)  ;
               $saveResp->save();
            //    }

               $jsData = json_encode($getResultAnomaly);
               return response()->json($jsData, 200);

           }
     
    }

    public function allAnomaliesData()
    {
        $authUserID = Auth::id();
        $websitesList = AnalyticsAccount::where('user_id',$authUserID)->get();
        return view('admin.allAnomaliesView')->with([
            'webList' => $websitesList,
        'userSingleId' => $authUserID,
        // 'userJid' => $userj_id,
        ]);
    }

    public function getApiAllAnomalies($dataType)
    {
        $userAuth = Auth::user();
        if($dataType == 1)
        {
            if($userAuth->hasRole('unverified'))
            {
                $getParent = ShareUser::where('child_user_id',$userAuth->id)->first();
                $getdata = ApiDataResponse::where('user_id',$getParent->parent_user_id)->where('data_type',1)->where('domain_name','not required')->first();
            }
            else
            {
        $getdata = ApiDataResponse::where('user_id',Auth::id())->where('data_type',1)->where('domain_name','not required')->first();
            }
        $data = unserialize($getdata->response);
                $jsData = json_encode($data);
                return response()->json($jsData, 200);
        }
        else
        {
            if($userAuth->hasRole('unverified'))
            {
                $getParent = ShareUser::where('child_user_id',$userAuth->id)->first();
                $getdata = ApiDataResponse::where('user_id',$getParent->parent_user_id)->where('data_type',2)->where('domain_name','not required')->first();
            }
        $getdata = ApiDataResponse::where('user_id',Auth::id())->where('data_type',2)->where('domain_name','not required')->first();
        $data = unserialize($getdata->response);
                $jsData = json_encode($data);
                return response()->json($jsData, 200);
        }

    }

    //Shopify Funvtions Starts
    // public function ShopifyAuthIndex()
    // {
    //     // Set variables for our request
    //     $shop = $_GET['shop'];
    //     $api_key = "03c939f30d416b83a9a048ee1d303f44";
    //     $scopes = "read_analytics,read_reports";
    //     $redirect_uri = "http://local-darkdata-admin.ca/shopify/authentication/callback";

    //     // Build install/approval URL to redirect to
    //     $install_url = "https://" . $shop . "/admin/oauth/authorize?client_id=" . $api_key . "&scope=" . $scopes . "&redirect_uri=" . urlencode($redirect_uri);

    //     // Redirect
    //     header("Location: " . $install_url);
    //     die();
    // }

    // public function ShopifyAuthRedirect()
    // {
    //     // Get our helper functions
    //     include(app_path() . '\functions\function.php');

    //     // Set variables for our request
    //     $api_key = "03c939f30d416b83a9a048ee1d303f44";
    //     $shared_secret = "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";
    //     $params = $_GET; // Retrieve all request parameters
    //     $hmac = $_GET['hmac']; // Retrieve HMAC request parameter

    //     $params = array_diff_key($params, array('hmac' => '')); // Remove hmac from params
    //     ksort($params); // Sort params lexographically

    //     $computed_hmac = hash_hmac('sha256', http_build_query($params), $shared_secret);

    //     // Use hmac data to check that the response is from Shopify or not
    //     if (hash_equals($hmac, $computed_hmac)) {

    //         // Set variables for our request
    //         $query = array(
    //             "client_id" => $api_key, // Your API key
    //             "client_secret" => $shared_secret, // Your app credentials (secret key)
    //             "code" => $params['code'] // Grab the access key from the URL
    //         );

    //         // Generate access token URL
    //         $access_token_url = "https://" . $params['shop'] . "/admin/oauth/access_token";

    //         // Configure curl client and execute request
    //         $ch = curl_init();
    //         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //         curl_setopt($ch, CURLOPT_URL, $access_token_url);
    //         curl_setopt($ch, CURLOPT_POST, count($query));
    //         curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($query));
    //         $result = curl_exec($ch);
    //         curl_close($ch);

    //         // Store the access token
    //         $result = json_decode($result, true);
    //         $access_token = $result['access_token'];

    //         // Show the access token (don't do this in production!)
    //         echo $access_token;

    //     } else {
    //         // Someone is trying to be shady!
    //         die('This request is NOT from Shopify!');
    //     }
    // }
    //Shopify Functions ENds

    //Endpoint of active users
    public function getActiveUsers()
    {
        $getActiveUsers = User::whereNotBetween('id',[1,2])->where('name' ,'!=', 'Custom User')->get();
        $resultActiveUsers = [];
            foreach ($getActiveUsers as $user) {
            $resultActiveUsers[] = [
                'id' => $user->id,
                'email' => $user->email,
            ];
            }

            return $resultActiveUsers;
    }
}