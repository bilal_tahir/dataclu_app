<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use App\Models\ShareUrl;
use App\Models\User;
use App\Models\SlackMessage;
use App\Notifications\SlackNotification;
use App\Models\ConsoleList;
use App\Models\AnalyticsAccount;
use App\Models\EnableServices;
use Illuminate\Support\Facades\Hash;
use Notification;
use DataTables;
use App\Models\Profile;
use App\Models\Social;
use jeremykenedy\LaravelRoles\Models\Role;
use App\Models\ShareWebsite;
use App\Models\ShareUser;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        if ($user->isAdmin()) {
            return view('pages.admin.home');
        }

        return view('pages.user.home');
    }

    public function anomalyId($id)
    {
        $emailSharedUser = ShareUrl::where('user_id',Auth::user()->id)->where('anomalyId',$id)->where('senderEmail','!=','Customly Created' )->get();
        $getEnableStatus = EnableServices::where('user_id',Auth::user()->id)->first();
        return view('admin.anomalyView')->with([
            'callId' => $id,
            'emailurl' => $emailSharedUser,
            'services' => $getEnableStatus
        ]);
        // dd($request->anomaly_id);
    }

    public function SlackMessage(Request $request)
    {
        // $trial = new Trial();
        // $message = Auth::user()->name."Logged In";
        // Auth::user()->notify(new SlackNotification ($message));
         $slack = new SlackMessage();

        $slack->user_id = Auth::user()->id;

        $slack->url     = $request->slackUrl;
        $slack->comment     = $request->slackComment;
        // $slack->prefix    = $request->prefix;
        // $slack->email     = $request->email;

        $slack->save();

        $slack->notify(new SlackNotification ());

        return response()->json(['success'=> 'Slack Notification Sent Successfully']);
    }

    public function userProfileIndex()
    {
        $userName = User::where('id', Auth::user()->id)->first();
        $firstName = $userName->first_name;
        $lastName = $userName->last_name;
        $getSeviceStatus = EnableServices::where('user_id', Auth::user()->id)->first();
        // dd($getSeviceStatus);
        return view('admin.userProfile')->with([
            'user' => $userName,
            'name' => $firstName,
            'lastName' => $lastName,
            'service' => $getSeviceStatus
        ]);;
    }

    public function consoleDefault(Request $request)
    {
        ConsoleList::where('user_id',Auth::user()->id)->where('default_value',1)->update([
           'default_value' => 0
        ]);
        ConsoleList::where('id', $request->id)->update([
           'default_value' => 1
        ]);
        return response()->json(['success'=> 'Default Url Changed']);
    }

    public function consoleDelete(Request $request)
    {
        $checkDefault= ConsoleList::where('id', $request->id)->first();
        if($checkDefault->default_value == 1)
        {
            $getConsole = ConsoleList::where('user_id',Auth::user()->id)->get();
            $countConsole = $getConsole->count();
            if($countConsole == 1)
            {
                ConsoleList::where('id', $request->id)->delete();
                return response()->json(['success'=> 'Console Deleted Successfully']);
            }
            else
            {
                $getRandom = ConsoleList::where('user_id',Auth::user()->id)->where('id', '!=' ,$request->id)->update([
                        'default_value' => 1
                    ]);

                ConsoleList::where('id', $request->id)->delete();
                return response()->json(['success'=> 'Console Deleted Successfully']);


            }

        }
        else
        {
            ConsoleList::where('id', $request->id)->delete();
            return response()->json(['success'=> 'Console Deleted Successfully']);
        }
    }


    public function userAllConsole(Request $request)
    {
        // $userConsole = ConsoleList::where('user_id', Auth::user()->id)->get();
        // $userName = User::where('id', Auth::user()->id)->first();
        // $firstName = $userName->first_name;

        // return response()->json(['success'=> $userConsole]);

        if ($request->ajax()) {
            $data = ConsoleList::where('user_id', Auth::user()->id)->get();
            return DataTables::of($data)
            ->editColumn('default_value', function ($inquiry) {
                if ($inquiry->default_value == 0) return 'No';
                if ($inquiry->default_value == 1) return 'Yes';
                })
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" class="edit btn btn-dark btn-sm editDefault">Select Default</a>';
                     $actionBtn = $actionBtn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteConsole">Delete</a>';
                     $actionBtn = $actionBtn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Share" class="btn btn-light btn-sm shareConsole">Share</a>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
            }
    }


    public function userAllAnalytics(Request $request)
    {
        if ($request->ajax()) {
            $data = AnalyticsAccount::where('user_id', Auth::user()->id)->get();
            return DataTables::of($data)
            ->editColumn('default_value', function ($inquiry) {
                if ($inquiry->default_value == 0) return 'No';
                if ($inquiry->default_value == 1) return 'Yes';
                })
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" class="edit btn btn-dark btn-sm editDefaultAnalytics">Select Default</a>';
                    $actionBtn = $actionBtn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'"  class="btn btn-danger btn-sm deleteAnalytics">Delete</a>';
                    $actionBtn = $actionBtn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'"  class="btn btn-light btn-sm shareAnalytics">Share</a>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
            }
    }
    public function analyticsDefault(Request $request)
    {
        AnalyticsAccount::where('user_id',Auth::user()->id)->where('default_value',1)->update([
           'default_value' => 0
        ]);
        AnalyticsAccount::where('id', $request->id)->update([
           'default_value' => 1
        ]);
        return response()->json(['success'=> 'Default Url Changed']);
    }

    public function analyticsDelete(Request $request)
    {
        $checkDefault= AnalyticsAccount::where('id', $request->id)->first();
        if($checkDefault->default_value == 1)
        {
            $getConsole = AnalyticsAccount::where('user_id',Auth::user()->id)->get();
            $countConsole = $getConsole->count();
            if($countConsole == 1)
            {
                AnalyticsAccount::where('id', $request->id)->delete();
                return response()->json(['success'=> 'Console Deleted Successfully']);
            }
            else
            {
                $getRandom = AnalyticsAccount::where('user_id',Auth::user()->id)->where('id', '!=' ,$request->id)->update([
                        'default_value' => 1
                    ]);

                AnalyticsAccount::where('id', $request->id)->delete();
                return response()->json(['success'=> 'Console Deleted Successfully']);


            }

        }
        else
        {
            AnalyticsAccount::where('id', $request->id)->delete();
            return response()->json(['success'=> 'Console Deleted Successfully']);
        }
    }

    public function enableDisable(Request $request)
    {
        $getPreviouseStatus = EnableServices::where('user_id',Auth::user()->id)->first();
        if($getPreviouseStatus)
        {
            if($request->type == "email")
            {
            EnableServices::where('user_id',Auth::user()->id)->update([
                                    'email' => $request->status,
                        ]);
            }
            else{
                 EnableServices::where('user_id',Auth::user()->id)->update([
                                    'slack' => $request->status,
                        ]);
            }
        }
        else
        {
            if($request->type == "email")
            {
                $newRec = new EnableServices ();
                $newRec->user_id = Auth::user()->id;
                $newRec->email = $request->status;
                $newRec->save();
            }
            else
            {
                $newRec = new EnableServices ();
                $newRec->user_id = Auth::user()->id;
                $newRec->slack = $request->status;
                $newRec->save();
            }
        }
    }

    public function consoleAnalyticsShareData(Request $request)
    {
        $checkExist = User::where('email', $request->email)->first();
        if(!$checkExist)
        {
            $profile = new Profile();
            $role = Role::where('slug', '=', 'unverified')->first();

            $user = new User();
            $user->name = "Custom User";
            $user->email = $request->email;
            $user->password = Hash::make($request->pass);
            $user->activated = true;
            $user->save();

            $user->attachRole($role);


            $user->profile()->save($profile);
            $user->save();

            $getSavedRecord = User::where('email', $request->email)->first();
            //Save User Record
            $newuser = new ShareUser();
            $newuser->parent_user_id = Auth::user()->id;
            $newuser->child_user_id = $getSavedRecord->id;
            $newuser->save();
        }
        // $checkShareWebsite = ShareWebsite::where('sharedUrl')->first();
        if($request->type == "console")
        {
            $getConsole = ConsoleList::where('id',$request->id)->first();
            
                if (strpos($getConsole->siteUrl, 'sc-domain:') !== false) {
                    $explodedConsoleUrl = explode('sc-domain:',$getConsole->siteUrl);
                    if(strpos($explodedConsoleUrl[1], 'www.') !== false )
                    {
                    $finalConsoleUrlSlash = $explodedConsoleUrl[1];
                    }
                    else
                    {
                    $finalConsoleUrlSlash = "www.".$explodedConsoleUrl[1];
                    }
                    $finalConsoleUrl = str_replace("/","",$finalConsoleUrlSlash);
                }
                if (strpos($getConsole->siteUrl, 'https://') !== false) {
                    $explodedConsoleUrl = explode('https://',$getConsole->siteUrl);
                    if(strpos($explodedConsoleUrl[1], 'www.') !== false )
                    {
                    $finalConsoleUrlSlash = $explodedConsoleUrl[1];
                    }
                    else
                    {
                    $finalConsoleUrlSlash = "www.".$explodedConsoleUrl[1];
                    }
                    $finalConsoleUrl = str_replace("/","",$finalConsoleUrlSlash);
                }
                if (strpos($getConsole->siteUrl, 'http://') !== false) {
                    $explodedConsoleUrl = explode('http://',$getConsole->siteUrl);
                    if(strpos($explodedConsoleUrl[1], 'www.') !== false )
                    {
                    $finalConsoleUrlSlash = $explodedConsoleUrl[1];
                    }
                    else
                    {
                    $finalConsoleUrlSlash = "www.".$explodedConsoleUrl[1];
                    }
                    $finalConsoleUrl = str_replace("/","",$finalConsoleUrlSlash);
                }

                $getUSer = User::where('email',$request->email)->first();
            $checkShareWebsite = ShareWebsite::where('createdUrl', $finalConsoleUrl)->where('user_id',$getUSer->id)->first();
            if($checkShareWebsite)
            {
                 return response()->json(['success'=> 'Website already Exist']);
            }
            else
            {
                // $getUSer = User::where('email',$request->email)->first();
                $newshare = new ShareWebsite();
                $newshare->user_id  = $getUSer->id;
                $newshare->sharedUrl = $getConsole->siteUrl;
                $newshare->createdUrl = $finalConsoleUrl;
                $newshare->save();

                 return response()->json(['success'=> 'Website Added']);
            }
        }
        if($request->type == "analytics")
        {
            $getAnalytics = AnalyticsAccount::where('id',$request->id)->first();
            if (strpos($getAnalytics->Property_Url, 'http://') !== false) {
                $explodedAnalyticsUrl = explode('http://',$getAnalytics->Property_Url);
                if(strpos($explodedAnalyticsUrl[1], 'www.') !== false )
                {
                $finalAnalyticUrlSlash = $explodedAnalyticsUrl[1];
                }
                else
                {
                $finalAnalyticUrlSlash = "www.".$explodedAnalyticsUrl[1];
                }
                $finalAnalyticUrl = str_replace("/","",$finalAnalyticUrlSlash);
             }
            if (strpos($getAnalytics->Property_Url, 'https://') !== false) {
                    $explodedAnalyticsUrl = explode('https://',$getAnalytics->Property_Url);
                    if(strpos($explodedAnalyticsUrl[1], 'www.') !== false )
                    {
                    $finalAnalyticUrlSlash = $explodedAnalyticsUrl[1];
                    }
                    else
                    {
                    $finalAnalyticUrlSlash = "www.".$explodedAnalyticsUrl[1];
                    }
                    $finalAnalyticUrl = str_replace("/","",$finalAnalyticUrlSlash);
                }
                $getUSer = User::where('email',$request->email)->first();
            $checkShareWebsite = ShareWebsite::where('createdUrl',$finalAnalyticUrl)->where('user_id',$getUSer->id)->first();
            if($checkShareWebsite)
            {
                return response()->json(['success'=> 'Website already Exist']);
            }
            else
            {
                // $getUSer = User::where('email',$request->email)->first();
                $newshare = new ShareWebsite();
                $newshare->user_id  = $getUSer->id;
                $newshare->sharedUrl = $getAnalytics->Property_Url;
                $newshare->createdUrl = $finalAnalyticUrl;
                $newshare->save();
                return response()->json(['success'=> 'Website Added']);
            }
        }
    }

    public function saveshopifyDetail(Request $request)
    {
        $newshare = new ShareWebsite();
        $newshare->user_id  = Auth::user()->id;;
        $newshare->Shop_Name = $request->shopName;
        $newshare->Api_Key = $request->apiKey;
        $newshare->save();
        return response()->json(['success'=> 'Website Added']);
    }
}
