<?php

namespace App\Models;
use App\Models\ApiDataResponse;
use Illuminate\Database\Eloquent\Model;

class ApiDataType extends Model
{
    //
    // One to Many Relation With api response
    public function apidatatyperesponse()
    {
        return $this->hasMany(ApiDataResponse::class);
    }
}