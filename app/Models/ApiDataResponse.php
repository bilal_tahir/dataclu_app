<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\ApiDataType;
use App\Models\AccountType;

class ApiDataResponse extends Model
{
    //
     // One To Many Relation with user
     public function user()
     {
         return $this->belongsTo(User::class);
     }

      // One To Many Relation with api data type
     public function apidatatype()
     {
         return $this->belongsTo(ApiDataType::class);
     }

      public function accounttype()
     {
         return $this->belongsTo(AccountType::class);
     }
}