<?php

namespace App\Models;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class EnableServices extends Model
{
    //

     // One To one Relation with user
     public function user()
     {
         return $this->belongsTo(User::class);
     }
}
