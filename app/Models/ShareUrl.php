<?php

namespace App\Models;

use App\Models\User;
use App\Models\Shareanomalyresponse;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 

class ShareUrl extends Model
{
    use SoftDeletes;
    //
     // One To Many Relation with user
     public function user()
     {
         return $this->belongsTo(User::class);
     }

     public function shareAnomalyResponse()
    {
        return $this->hasOne(Shareanomalyresponse::class);
    }
}