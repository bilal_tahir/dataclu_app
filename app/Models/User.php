<?php

namespace App\Models;

use App\Models\Jobid;
use App\Models\ShareUrl;
use App\Models\ConsoleList;
use App\Models\ApiDataResponse;
use App\Models\ShareWebsite;
use App\Models\SlackMessage;
use App\Models\EnableServices;
// use App\Models\AnalyticsCorelateMetrics;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use jeremykenedy\LaravelRoles\Traits\HasRoleAndPermission;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasRoleAndPermission;
    use Notifiable;
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    /**
     * The attributes that are hidden.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'activated',
        'token',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'first_name',
        'last_name',
        'email',
        'password',
        'activated',
        'token',
        'signup_ip_address',
        'signup_confirmation_ip_address',
        'signup_sm_ip_address',
        'admin_ip_address',
        'updated_ip_address',
        'deleted_ip_address',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                                => 'integer',
        'first_name'                        => 'string',
        'last_name'                         => 'string',
        'email'                             => 'string',
        'password'                          => 'string',
        'activated'                         => 'boolean',
        'token'                             => 'string',
        'signup_ip_address'                 => 'string',
        'signup_confirmation_ip_address'    => 'string',
        'signup_sm_ip_address'              => 'string',
        'admin_ip_address'                  => 'string',
        'updated_ip_address'                => 'string',
        'deleted_ip_address'                => 'string',
    ];

    /**
     * Get the socials for the user.
     */
    public function social()
    {
        return $this->hasMany('App\Models\Social');
    }

    /**
     * Get the profile associated with the user.
     */
    public function profile()
    {
        return $this->hasOne('App\Models\Profile');
    }

    /**
     * The profiles that belong to the user.
     */
    public function profiles()
    {
        return $this->belongsToMany('App\Models\Profile')->withTimestamps();
    }

    /**
     * Check if a user has a profile.
     *
     * @param  string  $name
     *
     * @return bool
     */
    public function hasProfile($name)
    {
        foreach ($this->profiles as $profile) {
            if ($profile->name === $name) {
                return true;
            }
        }

        return false;
    }

    /**
     * Add/Attach a profile to a user.
     *
     * @param  Profile $profile
     */
    public function assignProfile(Profile $profile)
    {
        return $this->profiles()->attach($profile);
    }

    /**
     * Remove/Detach a profile to a user.
     *
     * @param  Profile $profile
     */
    public function removeProfile(Profile $profile)
    {
        return $this->profiles()->detach($profile);
    }

    public function analyticsAccount()
    {
        return $this->hasMany('App\Models\AnalyticsAccount');
    }
    public function shopifyAccount()
    {
        return $this->hasMany('App\Models\ShopifyAccount');
    }

    // One to Many Relation With Job-Id
    public function jobids()
    {
        return $this->hasMany(Jobid::class);
    }

    // One to Many Relation With shareUrl
    public function shareurl()
    {
        return $this->hasMany(ShareUrl::class);
    }

    // One to Many Relation With ConsoleList
    public function consolelist()
    {
        return $this->hasMany(ConsoleList::class);
    }

    public function feedback()
    {
        return $this->hasMany('App\Models\Feedback');
    }

     // One to Many Relation With api response
    public function apidataresponse()
    {
        return $this->hasMany(ApiDataResponse::class);
    }

    // One to Many Relation With slack response
    public function slackNotification()
    {
        return $this->hasMany(SlackMessage::class);
    }

    // // One to Many Relation With corelation response
    // public function corelationResponse()
    // {
    //     return $this->hasMany(AnalyticsCorelateMetrics::class);
    // }

    // // for slack route
    // public function routeNotificationForSlack($notifications)
    // {
    //     return "https://hooks.slack.com/services/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";
    //     // return "https://hooks.slack.com/services/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";
    // }

     // One to One Relation With api response
    public function enableServices()
    {
        return $this->hasOne(EnableServices::class);
    }

    public function shareWebsite()
    {
        return $this->hasMany(ShareWebsite::class);
    }


}