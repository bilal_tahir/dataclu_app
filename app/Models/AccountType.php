<?php

namespace App\Models;
use App\Models\ApiDataResponse;
use App\Models\Jobid;
use Illuminate\Database\Eloquent\Model;

class AccountType extends Model
{
    //
    public function apidatatyperesponse()
    {
        return $this->hasMany(ApiDataResponse::class);
    }
    
    public function jobids()
    {
        return $this->hasMany(Jobid::class);
    }
}