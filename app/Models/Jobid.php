<?php

namespace App\Models;

use App\Models\User;
use App\Models\Jobtype;
use App\Models\AccountType;
use Illuminate\Database\Eloquent\Model;

class Jobid extends Model
{
    // One To Many Relation with user
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    // One To Many Relation with user
    public function jobtype()
    {
        return $this->belongsTo(Jobtype::class);
    }

    public function accounttype()
    {
        return $this->belongsTo(AccountType::class);
    }
    
}