<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use Illuminate\Notifications\Notifiable;
class SlackMessage extends Model
{
     use Notifiable;
    //
    public function user()
     {
         return $this->belongsTo(User::class);
     }
     // for slack route
    public function routeNotificationForSlack($notifications)
    {
        // return "https://hooks.slack.com/services/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";
        return "https://hooks.slack.com/services/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";
        // return env('SLACK_HOOK');
    }
}