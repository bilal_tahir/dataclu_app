<?php

namespace App\Models;
use App\Models\Jobid;

use Illuminate\Database\Eloquent\Model;

class Jobtype extends Model
{
    //
     // One to Many Relation With Job-Id
     public function jobids()
     {
         return $this->hasMany(Jobid::class);
     }
}