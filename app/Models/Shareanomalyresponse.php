<?php

namespace App\Models;
use App\Models\ShareUrl;
use Illuminate\Database\Eloquent\Model;

class Shareanomalyresponse extends Model
{
    //
     public function shareUrl()
     {
         return $this->belongsTo(ShareUrl::class);
     }
}