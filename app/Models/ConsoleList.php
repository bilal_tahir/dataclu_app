<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class ConsoleList extends Model
{
    //
    // One To Many Relation with user
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}