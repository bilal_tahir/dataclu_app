<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use App\Models\Jobid;
use App\Models\ApiDataResponse;
use App\Models\AnalyticsAccount;
use App\Models\ConsoleList;

class updateDbCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updatedb:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command used for updating AI DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Comment Starts Here
        $allUsers = User::whereNotBetween('id', [1, 2])->get(); //3
        if($allUsers->count() > 0)
        {
            
        foreach ($allUsers as $allUser)
        {
            $sourceArray = [];
            $allUserAnalytics = AnalyticsAccount::where('user_id',$allUser->id)->get();    
            $allUserConsole = ConsoleList::where('user_id',$allUser->id)->get(); 
            foreach ($allUserConsole as $uConsole)
            {
                $consolePresentInAnalytics = 0;
                if (strpos($uConsole->siteUrl, 'sc-domain:') !== false) {
                    $explodedConsoleUrl = explode('sc-domain:',$uConsole->siteUrl);
                    if(strpos($explodedConsoleUrl[1], 'www.') !== false )
                    {
                    $finalConsoleUrlSlash = $explodedConsoleUrl[1];
                    }
                    else
                    {
                    $finalConsoleUrlSlash = "www.".$explodedConsoleUrl[1];
                    }
                    $finalConsoleUrl = str_replace("/","",$finalConsoleUrlSlash);
                }
                if (strpos($uConsole->siteUrl, 'https://') !== false) {
                    $explodedConsoleUrl = explode('https://',$uConsole->siteUrl);
                    if(strpos($explodedConsoleUrl[1], 'www.') !== false )
                    {
                    $finalConsoleUrlSlash = $explodedConsoleUrl[1];
                    }
                    else
                    {
                    $finalConsoleUrlSlash = "www.".$explodedConsoleUrl[1];
                    }
                    $finalConsoleUrl = str_replace("/","",$finalConsoleUrlSlash);
                }
                if (strpos($uConsole->siteUrl, 'http://') !== false) {
                    $explodedConsoleUrl = explode('http://',$uConsole->siteUrl);
                    if(strpos($explodedConsoleUrl[1], 'www.') !== false )
                    {
                    $finalConsoleUrlSlash = $explodedConsoleUrl[1];
                    }
                    else
                    {
                    $finalConsoleUrlSlash = "www.".$explodedConsoleUrl[1];
                    }
                    $finalConsoleUrl = str_replace("/","",$finalConsoleUrlSlash);
                }
                foreach ($allUserAnalytics as $uAnalytic)
                {
                    if (strpos($uAnalytic->Property_Url, 'http://') !== false) {
                    $explodedAnalyticsUrl = explode('http://',$uAnalytic->Property_Url);
                    if(strpos($explodedAnalyticsUrl[1], 'www.') !== false )
                    {
                    $finalAnalyticUrlSlash = $explodedAnalyticsUrl[1];
                    }
                    else
                    {
                    $finalAnalyticUrlSlash = "www.".$explodedAnalyticsUrl[1];
                    }
                    $finalAnalyticUrl = str_replace("/","",$finalAnalyticUrlSlash);
                    }
                    if (strpos($uAnalytic->Property_Url, 'https://') !== false) {
                    $explodedAnalyticsUrl = explode('https://',$uAnalytic->Property_Url);
                    if(strpos($explodedAnalyticsUrl[1], 'www.') !== false )
                    {
                    $finalAnalyticUrlSlash = $explodedAnalyticsUrl[1];
                    }
                    else
                    {
                    $finalAnalyticUrlSlash = "www.".$explodedAnalyticsUrl[1];
                    }
                    $finalAnalyticUrl = str_replace("/","",$finalAnalyticUrlSlash);
                    }
                    if($finalAnalyticUrl == $finalConsoleUrl)
                    {
                        $againExplode = (explode("www.",$finalConsoleUrl));
                        $newObj = new \stdClass();
                        $newObj->siteUrl = "sc-domain:".$againExplode[1];
                        $newObj->relative_id = $uAnalytic->ViewId;
                        $newObj->dimensions = ["query","pagePath"];
                        $newObj->metrics = ["clicks", "impressions","pageviews", "avgPageLoadTime", "bounceRate"];

                        $finalObj = json_encode($newObj);
                        array_push($sourceArray,$finalObj);

                        $consolePresentInAnalytics = 1;
                    }
                    
                }
                if($consolePresentInAnalytics == 0)
                {
                    $againExplode = (explode("www.",$finalConsoleUrl));
                    $newObj = new \stdClass();
                    $newObj->siteUrl = "sc-domain:".$againExplode[1];
                    $newObj->relative_id = "";
                    $newObj->dimensions = ["query"];
                    $newObj->metrics = ["clicks", "impressions"];

                    $finalObj = json_encode($newObj);
                    array_push($sourceArray,$finalObj);
                }

                
            }

            //Now Reversing the loops to check analytics not present in console
            foreach ($allUserAnalytics as $uAnalytic)
                {
                    $analyticsPresentInConsole = 0;
                    if (strpos($uAnalytic->Property_Url, 'http://') !== false) {
                    $explodedAnalyticsUrl = explode('http://',$uAnalytic->Property_Url);
                    if(strpos($explodedAnalyticsUrl[1], 'www.') !== false )
                    {
                    $finalAnalyticUrlSlash = $explodedAnalyticsUrl[1];
                    }
                    else
                    {
                    $finalAnalyticUrlSlash = "www.".$explodedAnalyticsUrl[1];
                    }
                    $finalAnalyticUrl = str_replace("/","",$finalAnalyticUrlSlash);
                    }
                    if (strpos($uAnalytic->Property_Url, 'https://') !== false) {
                    $explodedAnalyticsUrl = explode('https://',$uAnalytic->Property_Url);
                    if(strpos($explodedAnalyticsUrl[1], 'www.') !== false )
                    {
                    $finalAnalyticUrlSlash = $explodedAnalyticsUrl[1];
                    }
                    else
                    {
                    $finalAnalyticUrlSlash = "www.".$explodedAnalyticsUrl[1];
                    }
                    $finalAnalyticUrl = str_replace("/","",$finalAnalyticUrlSlash);
                    }
                    foreach ($allUserConsole as $uConsole)
                        {
                            if (strpos($uConsole->siteUrl, 'sc-domain:') !== false) {
                                $explodedConsoleUrl = explode('sc-domain:',$uConsole->siteUrl);
                                if(strpos($explodedConsoleUrl[1], 'www.') !== false )
                                {
                                $finalConsoleUrlSlash = $explodedConsoleUrl[1];
                                }
                                else
                                {
                                $finalConsoleUrlSlash = "www.".$explodedConsoleUrl[1];
                                }
                                $finalConsoleUrl = str_replace("/","",$finalConsoleUrlSlash);
                            }
                            if (strpos($uConsole->siteUrl, 'https://') !== false) {
                                $explodedConsoleUrl = explode('https://',$uConsole->siteUrl);
                                if(strpos($explodedConsoleUrl[1], 'www.') !== false )
                                {
                                $finalConsoleUrlSlash = $explodedConsoleUrl[1];
                                }
                                else
                                {
                                $finalConsoleUrlSlash = "www.".$explodedConsoleUrl[1];
                                }
                                $finalConsoleUrl = str_replace("/","",$finalConsoleUrlSlash);
                            }
                            if (strpos($uConsole->siteUrl, 'http://') !== false) {
                                $explodedConsoleUrl = explode('http://',$uConsole->siteUrl);
                                if(strpos($explodedConsoleUrl[1], 'www.') !== false )
                                {
                                $finalConsoleUrlSlash = $explodedConsoleUrl[1];
                                }
                                else
                                {
                                $finalConsoleUrlSlash = "www.".$explodedConsoleUrl[1];
                                }
                                $finalConsoleUrl = str_replace("/","",$finalConsoleUrlSlash);
                            }
                            if($finalAnalyticUrl == $finalConsoleUrl)
                                {
                                    $analyticsPresentInConsole = 1;
                                }
                        }
                        if($analyticsPresentInConsole == 0)
                        {
                            $newObj = new \stdClass();
                            $newObj->siteUrl = "";
                            $newObj->relative_id = $uAnalytic->ViewId;
                            $newObj->dimensions = ["pagePath"];
                            $newObj->metrics = ["pageviews", "avgPageLoadTime", "bounceRate"];

                            $finalObj = json_encode($newObj);
                            array_push($sourceArray,$finalObj);
                        }
                }
            // $analticsArray = [];
            // $consoleArray = [];
            // foreach ($allUserConsole as $uConsole)
            // {
            //     $newObj = new \stdClass();
            //     $newObj->siteUrl = $uConsole->siteUrl;
            //     $newObj->dimensions = ["query"];
            //     $newObj->metrics = ["clicks", "impressions"];

            //     $finalObj = json_encode($newObj);
            //     array_push($consoleArray,$newObj);
            // }
            // foreach ($allUserAnalytics as $uAnalytic)
            // {
            //     $newObj = new \stdClass();
            //     $newObj->View_Id = $uAnalytic->ViewId;
            //     $newObj->dimensions = ["pagePath"];
            //     $newObj->metrics = ["pageviews", "avgPageLoadTime", "bounceRate"];

            //     $finalObj = json_encode($newObj);
            //     array_push($analticsArray,$newObj);
            // }
            // $finalArray = [];
            // $finalArray = array_merge($consoleArray,$analticsArray);

            
                $saveAnomaliesEndPoint = 'https://ai.dataclu.com/main/save_all_anomalies';
                $saveAnomaliesParametersDaily = [
                    'json' => [
                    'user_id' => $allUser->id,
                    'frequency' => "D",
                    'days' => 90,
                    'range'=> 50,
                    'sources' => $sourceArray,
                        ],
                    ];

                $newAnomalyClient = new \GuzzleHttp\Client();
                $sendingRequest = $newAnomalyClient->post($saveAnomaliesEndPoint, $saveAnomaliesParametersDaily);
                $receivingResult = json_decode($sendingRequest->getBody(),true);

                $checkPreviousResults = Jobid::where('user_id',$allUser->id)->where('propertyUrl','User all Analytics and Console Projects')->where('jobtype_id',1)->first();
                if($checkPreviousResults)
                {
                    Jobid::where('user_id',$allUser->id)->where('propertyUrl','User all Analytics and Console Projects')->where('jobtype_id',1)->update([
                        'updateAnomaly_JobId' => $receivingResult[1],
                    ]);
                }
                else
                {
                $saveJobId = new Jobid();
                $saveJobId->user_id =  $allUser->id;
                $saveJobId->jobtype_id = 1;
                $saveJobId->propertyUrl = "User all Analytics and Console Projects";
                $saveJobId->updateDb_JobId = "Not Required";
                $saveJobId->updateAnomaly_JobId = $receivingResult[1];
                $saveJobId->save();
                }

        }
        }    
        
    //     $allUsers = User::whereNotBetween('id', [1, 2])->get(); //3
    //     if($allUsers->count() > 0)
    //     {
    //     foreach ($allUsers as $allUser)
    //     {
    //         $allUserAnalytics = ConsoleList::where('user_id',$allUser->id)->get(); //2
    //         if($allUserAnalytics->count() > 0)
    //         {

    //             foreach($allUserAnalytics as $uAnalytic)
    //             {
    //                 $accountURLs1 = array();
    //                 // $complete_url = (explode("/",$uAnalytic->Property_Url));
    //                 // $exploded_url = "sc-domain:".$complete_url[2];
    //                 $exploded_url = $uAnalytic->siteUrl;
    //                 array_push($accountURLs1,$exploded_url);

    //                      //  Update Database With G-Analytics Database
    //                 $endpointD13 = 'https://ai.dataclu.com/update/database/';
    //                 $optionsD13 = [
    //                     'json' => [
    //                     'days' => "last_requested",
    //                     'user_id' => $allUser->id,
    //                     'siteUrls' =>  $accountURLs1,
    //                         ],
    //                     ];
    //                 $clientD13 = new \GuzzleHttp\Client();
    //                 $resD13 = $clientD13->post($endpointD13, $optionsD13);
        
    //                 $j_Did = json_decode($resD13->getBody(),true);

    //                          //  Update Anomailes in DB
    //             $endpointD14 = 'https://ai.dataclu.com/update/anomalies';
    //             $optionsD14 = [
    //                 'json' => [
    //                 'days' => 90,
    //                 'user_id' => $allUser->id,
    //                 'siteUrls' =>  $accountURLs1,
    //                 // 'metrics' =>  ["clicks", "ctr", "position", "impressions"],
    //                 // 'dimensions' =>  ["page", "query", "country"],
    //                 'dimensions' =>  ["query"],
    //                 'job_id' => $j_Did[1][0],
    //                 'frequency' => "D"
    //                     ],
    //                 ];
    //             $clientD14 = new \GuzzleHttp\Client();
    //             $resD14 = $clientD14->post($endpointD14, $optionsD14);
    //             $j_Did2 = json_decode($resD14->getBody(),true);

    //               //updating or Saving Record Seperately For Daily Call
    //               $checkPreviousJobId = Jobid::where('jobtype_id',1)
    //                                     ->where('user_id',$allUser->id)
    //                                     ->where('propertyUrl',$exploded_url)->first();
    //                 if($checkPreviousJobId)
    //                 {
    //                     Jobid::where('jobtype_id',1)
    //                             ->where('user_id',$allUser->id)
    //                             ->where('propertyUrl',$exploded_url)->update([
    //                                         'updateDb_JobId' => $j_Did[1][0],
    //                                         'updateAnomaly_JobId' => $j_Did2[1][0]
    //                                     ]);
    //                 }
    //                 else
    //                 {
    //                     $upJobId1 = new Jobid();
    //                     $upJobId1->jobtype_id = 1;
    //                     $upJobId1->user_id = $allUser->id;
    //                     $upJobId1->propertyUrl = $exploded_url;
    //                     $upJobId1->updateDb_JobId = $j_Did[1][0];
    //                     $upJobId1->updateAnomaly_JobId = $j_Did2[1][0];
    //                     $upJobId1->save();
    //                 }

    //             //   //Additional Code for calling Daily API's and saving there record
    //             //   $clickEndPoint = 'https://ai.dataclu.com/anomalies/trends?user_id='.$allUser->id.'&siteUrl='.$uAnalytic->siteUrl.'&metric=clicks&dimensions=["query"]&range=25&frequency=D';
    //             // //   $ctrEndPoint = 'https://ai.dataclu.com/anomalies/trends?user_id='.$allUser->id.'&siteUrl='.$uAnalytic->siteUrl.'&metric=ctr&dimensions=["query"]&range=25&frequency=D';
    //             //   $impressionEndPoint = 'https://ai.dataclu.com/anomalies/trends?user_id='.$allUser->id.'&siteUrl='.$uAnalytic->siteUrl.'&metric=impressions&dimensions=["query"]&range=25&frequency=D';
    //             // //   $positionEndPoint = 'https://ai.dataclu.com/anomalies/trends?user_id='.$allUser->id.'&siteUrl='.$uAnalytic->siteUrl.'&metric=position&dimensions=["query"]&range=25&frequency=D';

    //             //     // Calling and saving respone of click Api
    //             // $clickClient = new \GuzzleHttp\Client();
    //             // $clickApi = $clickClient->get($clickEndPoint);
    //             // $clickResp = json_decode($clickApi->getBody(),true);

    //             // $checkPreviousRecord = ApiDataResponse::where('user_id',$allUser->id)
    //             //                         ->where('domain_name',$uAnalytic->siteUrl)
    //             //                         ->where('data_type',1)
    //             //                         ->where('metric_name','clicks')->first();

    //             // if($checkPreviousRecord)
    //             // {
    //             //       ApiDataResponse::where('user_id',$allUser->id)
    //             //                         ->where('domain_name',$uAnalytic->siteUrl)
    //             //                         ->where('data_type',1)
    //             //                         ->where('metric_name','clicks')->update([
    //             //                                 'response' => serialize($clickResp)
    //             //                         ]);
    //             // }
    //             // else
    //             // {
    //             //     $newClick = new ApiDataResponse();
    //             //     $newClick->user_id = $allUser->id;
    //             //     $newClick->data_type = 1;
    //             //     $newClick->domain_name = $uAnalytic->siteUrl;
    //             //     $newClick->metric_name = 'clicks';
    //             //     $newClick->response = serialize($clickResp);
    //             //     $newClick->save();
    //             // }

    //             //        // Calling and saving respone of impression Api
    //             // $impressionClient = new \GuzzleHttp\Client();
    //             // $impressionApi = $impressionClient->get($impressionEndPoint);
    //             // $impressionResp = json_decode($impressionApi->getBody(),true);

    //             // $checkImpressionPreviousRecord = ApiDataResponse::where('user_id',$allUser->id)
    //             //                         ->where('domain_name',$uAnalytic->siteUrl)
    //             //                         ->where('data_type',1)
    //             //                         ->where('metric_name','impressions')->first();

    //             // if($checkImpressionPreviousRecord)
    //             // {
    //             //       ApiDataResponse::where('user_id',$allUser->id)
    //             //                         ->where('domain_name',$uAnalytic->siteUrl)
    //             //                         ->where('data_type',1)
    //             //                         ->where('metric_name','impressions')->update([
    //             //                                 'response' => serialize($impressionResp)
    //             //                         ]);
    //             // }
    //             // else
    //             // {
    //             //     $newImpression = new ApiDataResponse();
    //             //     $newImpression->user_id = $allUser->id;
    //             //     $newImpression->data_type = 1;
    //             //     $newImpression->domain_name = $uAnalytic->siteUrl;
    //             //     $newImpression->metric_name = 'impressions';
    //             //     $newImpression->response = serialize($impressionResp);
    //             //     $newImpression->save();
    //             // }

    //             //       // Calling and saving respone of ctr Api
    //             // $ctrClient = new \GuzzleHttp\Client();
    //             // $ctrApi = $ctrClient->get($ctrEndPoint);
    //             // $ctrResp = json_decode($ctrApi->getBody(),true);

    //             // $checkCtrPreviousRecord = ApiDataResponse::where('user_id',$allUser->id)
    //             //                         ->where('domain_name',$uAnalytic->siteUrl)
    //             // ->where('data_type',1)
    //             //                         ->where('metric_name','ctr')->first();

    //             // if($checkCtrPreviousRecord)
    //             // {
    //             //       ApiDataResponse::where('user_id',$allUser->id)
    //             //                         ->where('domain_name',$uAnalytic->siteUrl)
    //             // ->where('data_type',1)
    //             //                         ->where('metric_name','ctr')->update([
    //             //                                 'response' => serialize($ctrResp)
    //             //                         ]);
    //             // }
    //             // else
    //             // {
    //             //     $newCtr = new ApiDataResponse();
    //             //     $newCtr->user_id = $allUser->id;
    //             //     $newCtr->data_type = 1;
    //             //     $newCtr->domain_name = $uAnalytic->siteUrl;
    //             //     $newCtr->metric_name = 'ctr';
    //             //     $newCtr->response = serialize($ctrResp);
    //             //     $newCtr->save();
    //             // }

    //             //      // Calling and saving respone of position Api
    //             // $positionClient = new \GuzzleHttp\Client();
    //             // $positionApi = $positionClient->get($positionEndPoint);
    //             // $positionResp = json_decode($positionApi->getBody(),true);

    //             // $checkPositionPreviousRecord = ApiDataResponse::where('user_id',$allUser->id)
    //             //                         ->where('domain_name',$uAnalytic->siteUrl)
    //             // ->where('data_type',1)
    //             //                         ->where('metric_name','position')->first();

    //             // if($checkPositionPreviousRecord)
    //             // {
    //             //       ApiDataResponse::where('user_id',$allUser->id)
    //             //                         ->where('domain_name',$uAnalytic->siteUrl)
    //             // ->where('data_type',1)
    //             //                         ->where('metric_name','position')->update([
    //             //                                 'response' => serialize($positionResp)
    //             //                         ]);
    //             // }
    //             // else
    //             // {
    //             //     $newPosition = new ApiDataResponse();
    //             //     $newPosition->user_id = $allUser->id;
    //             //     $newPosition->data_type = 1;
    //             //     $newPosition->domain_name = $uAnalytic->siteUrl;
    //             //     $newPosition->metric_name = 'position';
    //             //     $newPosition->response = serialize($positionResp);
    //             //     $newPosition->save();
    //             // }

    //             }

                

    //         //      //  Update Database With G-Analytics Database
    //         //         $endpointD13 = 'https://ai.dataclu.com/update/database/';
    //         //         $optionsD13 = [
    //         //             'json' => [
    //         //             'days' => 90,
    //         //             'user_id' => $allUser->id,
    //         //             'siteUrls' =>  $accountURLs1,
    //         //                 ],
    //         //             ];
    //         //         $clientD13 = new \GuzzleHttp\Client();
    //         //         $resD13 = $clientD13->post($endpointD13, $optionsD13);
        
    //         //         $j_Did = json_decode($resD13->getBody(),true);

    //         //         \Log::info($j_Did[1][0]);

     

    //         //   //  Update Anomailes in DB
    //         //     $endpointD14 = 'https://ai.dataclu.com/update/anomalies';
    //         //     $optionsD14 = [
    //         //         'json' => [
    //         //         'days' => 90,
    //         //         'user_id' => $allUser->id,
    //         //         'siteUrls' =>  $accountURLs1,
    //         //         // 'metrics' =>  ["clicks", "ctr", "position", "impressions"],
    //         //         'dimensions' =>  ["page", "query", "country"],
    //         //         'job_id' => $j_Did[1][0],
    //         //         'frequency' => "D"
    //         //             ],
    //         //         ];
    //         //     $clientD14 = new \GuzzleHttp\Client();
    //         //     $resD14 = $clientD14->post($endpointD14, $optionsD14);

    //         }
    //         // Code For Google Analytics
    //         $allUserAnalyticsGA = AnalyticsAccount::where('user_id',$allUser->id)->get(); //2
    //         if($allUserAnalyticsGA->count() > 0)
    //         {

    //             foreach($allUserAnalyticsGA as $uAnalytic)
    //             {
    //                 $accountURLs1 = array();
    //                 // $complete_url = (explode("/",$uAnalytic->Property_Url));
    //                 // $exploded_url = "sc-domain:".$complete_url[2];
    //                 $exploded_url = $uAnalytic->ViewId;
    //                 array_push($accountURLs1,$exploded_url);

    //                      //  Update Database With G-Analytics Database
    //                 $endpointD13 = 'https://ai.dataclu.com/update/database/';
    //                 $optionsD13 = [
    //                     'json' => [
    //                     'days' => "last_requested",
    //                     'user_id' => $allUser->id,
    //                     'siteUrls' =>  $accountURLs1,
    //                         ],
    //                     ];
    //                 $clientD13 = new \GuzzleHttp\Client();
    //                 $resD13 = $clientD13->post($endpointD13, $optionsD13);
        
    //                 $j_Did = json_decode($resD13->getBody(),true);

    //                          //  Update Anomailes in DB
    //             $endpointD14 = 'https://ai.dataclu.com/ga/update/anomalies';
    //             $optionsD14 = [
    //                 'json' => [
    //                 'days' => 90,
    //                 'user_id' => $allUser->id,
    //                 'siteUrls' =>  $accountURLs1,
    //                 // 'metrics' =>  ["clicks", "ctr", "position", "impressions"],
    //                 // 'dimensions' =>  ["page", "query", "country"],
    //                 'dimensions' =>  ["pagePath"],
    //                 'job_id' => $j_Did[1][0],
    //                 'frequency' => "D"
    //                     ],
    //                 ];
    //             $clientD14 = new \GuzzleHttp\Client();
    //             $resD14 = $clientD14->post($endpointD14, $optionsD14);
    //             $j_Did2 = json_decode($resD14->getBody(),true);

    //               //updating or Saving Record Seperately For Daily Call
    //               $checkPreviousJobId = Jobid::where('jobtype_id',1)
    //                                     ->where('user_id',$allUser->id)
    //                                     ->where('propertyUrl',$exploded_url)->first();
    //                 if($checkPreviousJobId)
    //                 {
    //                     Jobid::where('jobtype_id',1)
    //                             ->where('user_id',$allUser->id)
    //                             ->where('propertyUrl',$exploded_url)->update([
    //                                         'updateDb_JobId' => $j_Did[1][0],
    //                                         'updateAnomaly_JobId' => $j_Did2[1][0]
    //                                     ]);
    //                 }
    //                 else
    //                 {
    //                     $upJobId1 = new Jobid();
    //                     $upJobId1->jobtype_id = 1;
    //                     $upJobId1->user_id = $allUser->id;
    //                     $upJobId1->propertyUrl = $exploded_url;
    //                     $upJobId1->updateDb_JobId = $j_Did[1][0];
    //                     $upJobId1->updateAnomaly_JobId = $j_Did2[1][0];
    //                     $upJobId1->account_type= 2;
    //                     $upJobId1->save();
    //                 }
    //             }
    //         }
    //     }
    // }

    // Comment Ends Here
        
        // \Log::info("Cron is working fine!");
     
        // $allUsers = User::whereNotBetween('id', [1, 2])->get(); //3
        // if($allUsers->count() > 0)
        // {
        // foreach ($allUsers as $allUser)
        // {
        //     $allUserAnalytics = AnalyticsAccount::where('user_id',$allUser->id)->get();    
        //     $allUserConsole = ConsoleList::where('user_id',$allUser->id)->get(); 
        //     $analticsArray = [];
        //     $consoleArray = [];
        //     foreach ($allUserConsole as $uConsole)
        //     {
        //         $newObj = new \stdClass();
        //         $newObj->siteUrl = $uConsole->siteUrl;
        //         $newObj->dimensions = ["query"];
        //         $newObj->metrics = ["clicks", "impressions"];

        //         $finalObj = json_encode($newObj);
        //         array_push($consoleArray,$newObj);
        //     }
        //     foreach ($allUserAnalytics as $uAnalytic)
        //     {
        //         $newObj = new \stdClass();
        //         $newObj->View_Id = $uAnalytic->ViewId;
        //         $newObj->dimensions = ["pagePath"];
        //         $newObj->metrics = ["pageviews", "avgPageLoadTime", "bounceRate"];

        //         $finalObj = json_encode($newObj);
        //         array_push($analticsArray,$newObj);
        //     }
        //     $finalArray = [];
        //     $finalArray = array_merge($consoleArray,$analticsArray);

            
        //         $saveAnomaliesEndPoint = 'https://ai.dataclu.com/main/save_all_anomalies';
        //         $saveAnomaliesParametersDaily = [
        //             'json' => [
        //             'user_id' => $allUser->id,
        //             'frequency' => "D",
        //             'days' => 90,
        //             'range'=> 50,
        //             'sources' => $finalArray,
        //                 ],
        //             ];

        //             // dd($optionsD14);
        //             // print_r($optionsD14);
        //         $newAnomalyClient = new \GuzzleHttp\Client();
        //         $sendingRequest = $newAnomalyClient->post($saveAnomaliesEndPoint, $saveAnomaliesParametersDaily);
        //         $receivingResult = json_decode($sendingRequest->getBody(),true);

        //         $checkPreviousResults = Jobid::where('user_id',$allUser->id)->where('propertyUrl','User all Analytics and Console Projects')->where('jobtype_id',1)->first();
        //         if($checkPreviousResults)
        //         {
        //             Jobid::where('user_id',$allUser->id)->where('propertyUrl','User all Analytics and Console Projects')->where('jobtype_id',1)->update([
        //                 'updateAnomaly_JobId' => $receivingResult[1],
        //             ]);
        //         }
        //         else
        //         {
        //         $saveJobId = new Jobid();
        //         $saveJobId->user_id =  $allUser->id;
        //         $saveJobId->jobtype_id = 1;
        //         $saveJobId->propertyUrl = "User all Analytics and Console Projects";
        //         $saveJobId->updateDb_JobId = "Not Required";
        //         $saveJobId->updateAnomaly_JobId = $receivingResult[1];
        //         $saveJobId->save();
        //         }

        // }
        // }    

    }
}