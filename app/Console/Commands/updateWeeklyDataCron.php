<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use App\Models\Jobid;
use App\Models\ApiDataResponse;
use App\Models\AnalyticsAccount;
use App\Models\ConsoleList;

class updateWeeklyDataCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateWeeklyData:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command used for updating AI data weekly';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
          $allUsers = User::whereNotBetween('id', [1, 2])->get(); //3
        if($allUsers->count() > 0)
        {
        foreach ($allUsers as $allUser)
        {
            $allUserAnalytics = ConsoleList::where('user_id',$allUser->id)->get(); //2
            if($allUserAnalytics->count() > 0)
            {

                foreach($allUserAnalytics as $uAnalytic)
                {

                    //Additional Code for calling Weekly API's and saving there record
                  $clickEndPoint = 'https://ai.dataclu.com/anomalies/trends?user_id='.$allUser->id.'&siteUrl='.$uAnalytic->siteUrl.'&metric=clicks&dimensions=["query"]&range=25&frequency=W-Sun';
                //   $ctrEndPoint = 'https://ai.dataclu.com/anomalies/trends?user_id='.$allUser->id.'&siteUrl='.$uAnalytic->siteUrl.'&metric=ctr&dimensions=["query"]&range=25&frequency=W-Sun';
                  $impressionEndPoint = 'https://ai.dataclu.com/anomalies/trends?user_id='.$allUser->id.'&siteUrl='.$uAnalytic->siteUrl.'&metric=impressions&dimensions=["query"]&range=25&frequency=W-Sun';
                //   $positionEndPoint = 'https://ai.dataclu.com/anomalies/trends?user_id='.$allUser->id.'&siteUrl='.$uAnalytic->siteUrl.'&metric=position&dimensions=["query"]&range=25&frequency=W-Sun';

                    // Calling and saving respone of click Api
                $clickClient = new \GuzzleHttp\Client();
                $clickApi = $clickClient->get($clickEndPoint);
                $clickResp = json_decode($clickApi->getBody(),true);

                $checkPreviousRecord = ApiDataResponse::where('user_id',$allUser->id)
                                        ->where('domain_name',$uAnalytic->siteUrl)
                                        ->where('data_type',2)
                                        ->where('metric_name','clicks')->first();

                if($checkPreviousRecord)
                {
                      ApiDataResponse::where('user_id',$allUser->id)
                                        ->where('domain_name',$uAnalytic->siteUrl)
                                        ->where('data_type',2)
                                        ->where('metric_name','clicks')->update([
                                                'response' => serialize($clickResp)
                                        ]);
                }
                else
                {
                    $newClick = new ApiDataResponse();
                    $newClick->user_id = $allUser->id;
                    $newClick->data_type = 2;
                    $newClick->domain_name = $uAnalytic->siteUrl;
                    $newClick->metric_name = 'clicks';
                    $newClick->response = serialize($clickResp);
                    $newClick->save();
                }

                       // Calling and saving respone of impression Api
                $impressionClient = new \GuzzleHttp\Client();
                $impressionApi = $impressionClient->get($impressionEndPoint);
                $impressionResp = json_decode($impressionApi->getBody(),true);

                $checkImpressionPreviousRecord = ApiDataResponse::where('user_id',$allUser->id)
                                        ->where('domain_name',$uAnalytic->siteUrl)
                                        ->where('data_type',2)
                                        ->where('metric_name','impressions')->first();

                if($checkImpressionPreviousRecord)
                {
                      ApiDataResponse::where('user_id',$allUser->id)
                                        ->where('domain_name',$uAnalytic->siteUrl)
                                        ->where('data_type',2)
                                        ->where('metric_name','impressions')->update([
                                                'response' => serialize($impressionResp)
                                        ]);
                }
                else
                {
                    $newImpression = new ApiDataResponse();
                    $newImpression->user_id = $allUser->id;
                    $newImpression->data_type = 2;
                    $newImpression->domain_name = $uAnalytic->siteUrl;
                    $newImpression->metric_name = 'impressions';
                    $newImpression->response = serialize($impressionResp);
                    $newImpression->save();
                }


                }

                



            }
            //Google analytics
            $allUserAnalytics = AnalyticsAccount::where('user_id',$allUser->id)->get(); //2
            if($allUserAnalytics->count() > 0)
            {

                foreach($allUserAnalytics as $uAnalytic)
                {
                    
                  //Additional Code for calling Daily API's and saving there record
                  $gaEndPointDaily = 'https://ai.dataclu.com/anomalies/trends?user_id='.$allUser->id.'&View_Id='.$uAnalytic->ViewId.'&metric=pageviews&dimensions=["pagePath"]&range=40&frequency=W-Sun';
                  $gaAvgTimeLoadEndPointDaily = 'https://ai.dataclu.com/anomalies/trends?user_id='.$allUser->id.'&View_Id='.$uAnalytic->ViewId.'&metric=avgPageLoadTime&dimensions=["pagePath"]&range=40&frequency=W-Sun';
                  $gaBounceRateEndPointDaily = 'https://ai.dataclu.com/anomalies/trends?user_id='.$allUser->id.'&View_Id='.$uAnalytic->ViewId.'&metric=bounceRate&dimensions=["pagePath"]&range=40&frequency=W-Sun';

                    // Calling and saving respone of click Api
                $pageViewClient = new \GuzzleHttp\Client();
                $pageViewApi = $pageViewClient->get($gaEndPointDaily);
                $pageViewResp = json_decode($pageViewApi->getBody(),true);

                $checkPreviousRecord = ApiDataResponse::where('user_id',$allUser->id)
                                        ->where('domain_name',$uAnalytic->ViewId)
                                        ->where('data_type',2)
                                        ->where('metric_name','pageviews')->first();

                if($checkPreviousRecord)
                {
                      ApiDataResponse::where('user_id',$allUser->id)
                                        ->where('domain_name',$uAnalytic->ViewId)
                                        ->where('data_type',2)
                                        ->where('metric_name','pageviews')->update([
                                                'response' => serialize($pageViewResp)
                                        ]);
                }
                else
                {
                    $newPageView = new ApiDataResponse();
                    $newPageView->user_id = $allUser->id;
                    $newPageView->data_type = 2;
                    $newPageView->domain_name = $uAnalytic->ViewId;
                    $newPageView->metric_name = 'pageviews';
                    $newPageView->response = serialize($pageViewResp);
                    $newPageView->account_type = 2;
                    $newPageView->save();
                }

                   // Calling and saving respone of angpageTimeLoad Api
                $avgPageLoadClient = new \GuzzleHttp\Client();
                $avgPageLoadApi = $avgPageLoadClient->get($gaAvgTimeLoadEndPointDaily);
                $avgPageLoadResp = json_decode($avgPageLoadApi->getBody(),true);

                $checkPreviousRecord = ApiDataResponse::where('user_id',$allUser->id)
                                        ->where('domain_name',$uAnalytic->ViewId)
                                        ->where('data_type',2)
                                        ->where('metric_name','avgPageLoad')->first();

                if($checkPreviousRecord)
                {
                      ApiDataResponse::where('user_id',$allUser->id)
                                        ->where('domain_name',$uAnalytic->ViewId)
                                        ->where('data_type',2)
                                        ->where('metric_name','avgPageLoad')->update([
                                                'response' => serialize($avgPageLoadResp)
                                        ]);
                }
                else
                {
                    $newAvgPageLoad = new ApiDataResponse();
                    $newAvgPageLoad->user_id = $allUser->id;
                    $newAvgPageLoad->data_type = 2;
                    $newAvgPageLoad->domain_name = $uAnalytic->ViewId;
                    $newAvgPageLoad->metric_name = 'avgPageLoad';
                    $newAvgPageLoad->response = serialize($avgPageLoadResp);
                    $newAvgPageLoad->account_type = 2;
                    $newAvgPageLoad->save();
                }

                                   // Calling and saving respone of bounceRate Api
                $bounceRateClient = new \GuzzleHttp\Client();
                $bounceRateApi = $bounceRateClient->get($gaBounceRateEndPointDaily);
                $bounceRateResp = json_decode($bounceRateApi->getBody(),true);

                $checkPreviousRecord = ApiDataResponse::where('user_id',$allUser->id)
                                        ->where('domain_name',$uAnalytic->ViewId)
                                        ->where('data_type',2)
                                        ->where('metric_name','bounceRate')->first();

                if($checkPreviousRecord)
                {
                      ApiDataResponse::where('user_id',$allUser->id)
                                        ->where('domain_name',$uAnalytic->ViewId)
                                        ->where('data_type',2)
                                        ->where('metric_name','bounceRate')->update([
                                                'response' => serialize($bounceRateResp)
                                        ]);
                }
                else
                {
                    $newBounceRate = new ApiDataResponse();
                    $newBounceRate->user_id = $allUser->id;
                    $newBounceRate->data_type = 2;
                    $newBounceRate->domain_name = $uAnalytic->ViewId;
                    $newBounceRate->metric_name = 'bounceRate';
                    $newBounceRate->response = serialize($bounceRateResp);
                    $newBounceRate->account_type = 2;
                    $newBounceRate->save();
                }


              
               

                }

                

            }
        }
    }
        
    }
}