<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\DeleteExpiredActivations::class,
        Commands\updateDbCron::class,
        Commands\updateDbWeeklyCron::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();

        // $schedule->command('activations:clean')->daily();
        // $schedule->command('updatedb:cron')->everyFiveMinutes();
        // $schedule->command('updateDbWeekly:cron')->everyMinute();

        $schedule->command('updatedb:cron')->dailyAt('00:01');
        $schedule->command('updateDbWeekly:cron')->weeklyOn(4, '00:10');
        // Comment Start Here
        // $schedule->command('updateDailyData:cron')->dailyAt('00:30');
        // $schedule->command('updateWeeklyData:cron')->weeklyOn(4, '00:40');
        // Comment ends Here

        // $schedule->command('updatedb:cron')->dailyAt('16:56')->timezone('America/Chicago');
        // $schedule->command('updateDbWeekly:cron')->dailyAt('16:59')->timezone('America/Chicago');
        //  $schedule->command('updateDailyData:cron')->dailyAt('17:04')->timezone('America/Chicago');
        // $schedule->command('updateWeeklyData:cron')->weeklyOn(4, '17:06')->timezone('America/Chicago');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');
        require base_path('routes/console.php');
    }
}