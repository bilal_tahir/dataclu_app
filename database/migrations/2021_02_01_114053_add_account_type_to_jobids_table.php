<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAccountTypeToJobidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jobids', function (Blueprint $table) {
            //
            $table->unsignedInteger('account_type')->unsigned()->nullable();

            $table->foreign('account_type')->references('id')->on('account_types')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jobids', function (Blueprint $table) {
            //
            $table->dropColumn('account_type');
        });
    }
}