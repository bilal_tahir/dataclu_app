<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOtherResponsesToAnomalyDetailDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('anomaly_detail_data', function (Blueprint $table) {
            //
            $table->longText('related_metrics_response')->nullable();
            $table->longText('related_audience_response')->nullable();
            $table->longText('page_group_anomalies_response')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('anomaly_detail_data', function (Blueprint $table) {
            //
            $table->dropColumn('related_metrics_response');
            $table->dropColumn('related_audience_response');
            $table->dropColumn('page_group_anomalies_response');
        });
    }
}
