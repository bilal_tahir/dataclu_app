<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShareanomalyresponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shareanomalyresponses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('share_anomaly_id')->unsigned();
            $table->longText('anomaly_data')->nullable();
            $table->longText('anomaly_contributor')->nullable();
            $table->longText('anomaly_related_audience')->nullable();
            $table->longText('anomaly_page_group')->nullable();
            $table->dateTime('expires_at')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('share_anomaly_id')->references('id')->on('share_urls')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shareanomalyresponses');
    }
}