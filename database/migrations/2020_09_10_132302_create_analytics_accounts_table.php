<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnalyticsAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analytics_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('user_id')->unsigned();
            $table->string("Account_Id")->default('');
            $table->string("Account_Kind")->default('');
            $table->string("Account_Name")->default('');
            $table->string("Property_Id")->default('');
            $table->string("Property_Kind")->default('');
            $table->string("Property_Name")->default('');
            $table->string("Internal_Property_Id")->default('');
            $table->string("Property_Level")->default('');
            $table->string("Property_Url")->default('');
            $table->string("Profile_Id")->default('');
            $table->string("Profile_Kind")->default('');
            $table->string("Profile_Name")->default('');
            $table->string("Profile_Type")->default('');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analytics_accounts');
    }
}
