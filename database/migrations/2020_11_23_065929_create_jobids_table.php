<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobids', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('jobtype_id')->unsigned();
            $table->unsignedBigInteger('user_id')->unsigned();
            $table->string('propertyUrl');
            $table->string('updateDb_JobId');
            $table->string('updateDb_Status')->nullable();
            $table->string('updateAnomaly_JobId');
            $table->string('updateAnomaly_Status')->nullable();
            $table->timestamps();

            $table->foreign('jobtype_id')->references('id')->on('jobtypes')
              ->onDelete('cascade');

              
            $table->foreign('user_id')->references('id')->on('users')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobids');
    }
}