<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDefaultvalueToConsoleListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('console_lists', function (Blueprint $table) {
            //
            $table->boolean('default_value')->default('0');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('console_lists', function (Blueprint $table) {
            //
            $table->dropColumn('default_value');
        });
    }
}