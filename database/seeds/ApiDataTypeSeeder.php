<?php

use Illuminate\Database\Seeder;

class ApiDataTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         DB::table('api_data_types')->insert([
            'name' => 'Daily',
        ]);
        DB::table('api_data_types')->insert([
            'name' => 'Weekly',
        ]);
    }
}