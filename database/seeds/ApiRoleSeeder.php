<?php

use Illuminate\Database\Seeder;

class ApiRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
          /*
         * 1 New Role Defined For Handling API's 
         *
         */
        $APIRoleItems = [
            [
                'name'        => 'ApiRole',
                'slug'        => 'apirole',
                'description' => 'Api User Role',
                'level'       => 6,
            ],
        ];

           /*
         * Add Role Items For Api USer Role
         *
         */
        foreach ($APIRoleItems as $APIRoleItem) {
            $newRoleItem = config('roles.models.role')::where('slug', '=', $APIRoleItem['slug'])->first();
            if ($newRoleItem === null) {
                $newRoleItem = config('roles.models.role')::create([
                    'name'          => $APIRoleItem['name'],
                    'slug'          => $APIRoleItem['slug'],
                    'description'   => $APIRoleItem['description'],
                    'level'         => $APIRoleItem['level'],
                ]);
            }
        }
    }
}
