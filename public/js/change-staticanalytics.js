
am4core.ready(function () {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create chart instance
    var chart = am4core.create("chartdiv", am4charts.XYChart);
    chart.layout = "grid";
    chart.height = 70;
    chart.paddingRight = 20;
    chart.logo.disabled = true;



    // Add data
    chart.data = [{
        "year": "1950",
        "value": 1220
    }, {
        "year": "1951",
        "value": 100
    }, {
        "year": "1952",
        "value": 600
    }, {
        "year": "1953",
        "value": 800
    }, {
        "year": "1954",
        "value": 1600
    }, {
        "year": "1955",
        "value": 1100
    }, {
        "year": "1956",
        "value": 50
    }, {
        "year": "1957",
        "value": 1500
    }, {
        "year": "1958",
        "value": 210
    }, {
        "year": "1959",
        "value": 900
    }, {
        "year": "1960",
        "value": 1900
    }, {
        "year": "1961",
        "value": 10
    }, {
        "year": "1962",
        "value": 700
    }];

    // Create axes
    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "year";
    categoryAxis.renderer.minGridDistance = 50;
    categoryAxis.renderer.grid.template.location = 0.5;
    categoryAxis.startLocation = 0.5;
    categoryAxis.endLocation = 0.5;
    categoryAxis.renderer.grid.template.disabled = true;
    categoryAxis.renderer.labels.template.disabled = true;
    categoryAxis.cursorTooltipEnabled = false;

    // Create value axis
    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.baseValue = 0;
    valueAxis.renderer.grid.template.disabled = true;
    valueAxis.renderer.baseGrid.disabled = true;
    valueAxis.renderer.labels.template.disabled = true;
    valueAxis.cursorTooltipEnabled = false;

    // Create series
    var series = chart.series.push(new am4charts.LineSeries());
    series.dataFields.valueY = "value";
    series.dataFields.categoryX = "year";
    series.strokeWidth = 3;
    series.tensionX = 0.77;
    series.stroke = am4core.color("#9795a1");




    // bullet is added because we add tooltip to a bullet for it to change color
    var bullet = series.bullets.push(new am4charts.Bullet());
    bullet.tooltipText = "{valueY}";

    bullet.adapter.add("fill", function (fill, target) {
        if (target.dataItem.valueY < 0) {
            return am4core.color("#FF0000");
        }
        return fill;
    })


    chart.cursor = new am4charts.XYCursor();

}); // end am4core.ready()



// Second Graph

am4core.ready(function () {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create chart instance
    var secondchart = am4core.create("chartdivsecond", am4charts.XYChart);
    secondchart.layout = "grid";
    secondchart.height = 70;
    secondchart.paddingRight = 20;
    secondchart.logo.disabled = true;



    // Add data
    secondchart.data = [{
        "year": "1950",
        "value": 1220
    }, {
        "year": "1951",
        "value": 100
    }, {
        "year": "1952",
        "value": 600
    }, {
        "year": "1953",
        "value": 800
    }, {
        "year": "1954",
        "value": 1600
    }, {
        "year": "1955",
        "value": 1100
    }, {
        "year": "1956",
        "value": 50
    }, {
        "year": "1957",
        "value": 1500
    }, {
        "year": "1958",
        "value": 210
    }, {
        "year": "1959",
        "value": 900
    }, {
        "year": "1960",
        "value": 1900
    }, {
        "year": "1961",
        "value": 10
    }, {
        "year": "1962",
        "value": 700
    }];

    // Create axes
    var secondcategoryAxis = secondchart.xAxes.push(new am4charts.CategoryAxis());
    secondcategoryAxis.dataFields.category = "year";
    secondcategoryAxis.renderer.minGridDistance = 50;
    secondcategoryAxis.renderer.grid.template.location = 0.5;
    secondcategoryAxis.startLocation = 0.5;
    secondcategoryAxis.endLocation = 0.5;
    secondcategoryAxis.renderer.grid.template.disabled = true;
    secondcategoryAxis.renderer.labels.template.disabled = true;
    secondcategoryAxis.cursorTooltipEnabled = false;

    // Create value axis
    var secondvalueAxis = secondchart.yAxes.push(new am4charts.ValueAxis());
    secondvalueAxis.baseValue = 0;
    secondvalueAxis.renderer.grid.template.disabled = true;
    secondvalueAxis.renderer.baseGrid.disabled = true;
    secondvalueAxis.renderer.labels.template.disabled = true;
    secondvalueAxis.cursorTooltipEnabled = false;

    // Create series
    var secondseries = secondchart.series.push(new am4charts.LineSeries());
    secondseries.dataFields.valueY = "value";
    secondseries.dataFields.categoryX = "year";
    secondseries.strokeWidth = 3;
    secondseries.tensionX = 0.77;
    secondseries.stroke = am4core.color("#9795a1");


    // bullet is added because we add tooltip to a bullet for it to change color
    var secondullet = secondseries.bullets.push(new am4charts.Bullet());
    secondbullet.tooltipText = "{valueY}";

    secondbullet.adapter.add("fill", function (fill, target) {
        if (target.dataItem.valueY < 0) {
            return am4core.color("#FF0000");
        }
        return fill;
    })


    secondchart.cursor = new am4charts.XYCursor();

}); // end am4core.ready()


// Third Graph

am4core.ready(function () {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create chart instance
    var thirdchart = am4core.create("chartdivthird", am4charts.XYChart);
    thirdchart.layout = "grid";
    thirdchart.height = 70;
    thirdchart.paddingRight = 20;
    thirdchart.logo.disabled = true;
    thirdchart.background.fill = '#c9c059'
    thirdchart.background.opacity = 0.5



    // Add data
    thirdchart.data = [{
        "year": "1950",
        "value": 1220
    }, {
        "year": "1951",
        "value": 100
    }, {
        "year": "1952",
        "value": 600
    }, {
        "year": "1953",
        "value": 800
    }, {
        "year": "1954",
        "value": 1600
    }, {
        "year": "1955",
        "value": 1100
    }, {
        "year": "1956",
        "value": 50
    }, {
        "year": "1957",
        "value": 1500
    }, {
        "year": "1958",
        "value": 210
    }, {
        "year": "1959",
        "value": 900
    }, {
        "year": "1960",
        "value": 1900
    }, {
        "year": "1961",
        "value": 10
    }, {
        "year": "1962",
        "value": 700
    }];

    // Create axes
    var thirdcategoryAxis = thirdchart.xAxes.push(new am4charts.CategoryAxis());
    thirdcategoryAxis.dataFields.category = "year";
    thirdcategoryAxis.renderer.minGridDistance = 50;
    thirdcategoryAxis.renderer.grid.template.location = 0.5;
    thirdcategoryAxis.startLocation = 0.5;
    thirdcategoryAxis.endLocation = 0.5;
    thirdcategoryAxis.renderer.grid.template.disabled = true;
    thirdcategoryAxis.renderer.labels.template.disabled = true;
    thirdcategoryAxis.cursorTooltipEnabled = false;

    // Create value axis
    var thirdvalueAxis = thirdchart.yAxes.push(new am4charts.ValueAxis());
    thirdvalueAxis.baseValue = 0;
    thirdvalueAxis.renderer.grid.template.disabled = true;
    thirdvalueAxis.renderer.baseGrid.disabled = true;
    thirdvalueAxis.renderer.labels.template.disabled = true;
    thirdvalueAxis.cursorTooltipEnabled = false;

    // Create series
    var thirdseries = thirdchart.series.push(new am4charts.LineSeries());
    thirdseries.dataFields.valueY = "value";
    thirdseries.dataFields.categoryX = "year";
    thirdseries.strokeWidth = 6;
    thirdseries.tensionX = 0.77;
    thirdseries.stroke = am4core.color("#2ECC71");



    // bullet is added because we add tooltip to a bullet for it to change color
    var thirdbullet = thirdseries.bullets.push(new am4charts.Bullet());
    thirdbullet.tooltipText = "{valueY}";
    thirdbullet.background.color = am4core.color("#2ECC71");

    thirdbullet.adapter.add("fill", function (fill, target) {
        if (target.dataItem.valueY < 0) {
            return am4core.color("#FF0000");
        }
        return fill;
    })


    thirdchart.cursor = new am4charts.XYCursor();

}); // end am4core.ready()


// Fourth Graph

am4core.ready(function () {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create chart instance
    var fourthchart = am4core.create("chartdivfourth", am4charts.XYChart);
    fourthchart.layout = "grid";
    fourthchart.height = 70;
    fourthchart.paddingRight = 20;
    fourthchart.logo.disabled = true;



    // Add data
    fourthchart.data = [{
        "year": "1950",
        "value": 1220
    }, {
        "year": "1951",
        "value": 100
    }, {
        "year": "1952",
        "value": 600
    }, {
        "year": "1953",
        "value": 800
    }, {
        "year": "1954",
        "value": 1600
    }, {
        "year": "1955",
        "value": 1100
    }, {
        "year": "1956",
        "value": 50
    }, {
        "year": "1957",
        "value": 1500
    }, {
        "year": "1958",
        "value": 210
    }, {
        "year": "1959",
        "value": 900
    }, {
        "year": "1960",
        "value": 1900
    }, {
        "year": "1961",
        "value": 10
    }, {
        "year": "1962",
        "value": 700
    }];

    // Create axes
    var fourthcategoryAxis = fourthchart.xAxes.push(new am4charts.CategoryAxis());
    fourthcategoryAxis.dataFields.category = "year";
    fourthcategoryAxis.renderer.minGridDistance = 50;
    fourthcategoryAxis.renderer.grid.template.location = 0.5;
    fourthcategoryAxis.startLocation = 0.5;
    fourthcategoryAxis.endLocation = 0.5;
    fourthcategoryAxis.renderer.grid.template.disabled = true;
    fourthcategoryAxis.renderer.labels.template.disabled = true;
    fourthcategoryAxis.cursorTooltipEnabled = false;

    // Create value axis
    var fourthvalueAxis = fourthchart.yAxes.push(new am4charts.ValueAxis());
    fourthvalueAxis.baseValue = 0;
    fourthvalueAxis.renderer.grid.template.disabled = true;
    fourthvalueAxis.renderer.baseGrid.disabled = true;
    fourthvalueAxis.renderer.labels.template.disabled = true;
    fourthvalueAxis.cursorTooltipEnabled = false;

    // Create series
    var fourthseries = fourthchart.series.push(new am4charts.LineSeries());
    fourthseries.dataFields.valueY = "value";
    fourthseries.dataFields.categoryX = "year";
    fourthseries.strokeWidth = 6;
    fourthseries.tensionX = 0.77;
    fourthseries.stroke = am4core.color("#E74C3C");


    // bullet is added because we add tooltip to a bullet for it to change color
    var fourthullet = fourthseries.bullets.push(new am4charts.Bullet());
    fourthbullet.tooltipText = "{valueY}";

    fourthbullet.adapter.add("fill", function (fill, target) {
        if (target.dataItem.valueY < 0) {
            return am4core.color("#FF0000");
        }
        return fill;
    })


    fourthchart.cursor = new am4charts.XYCursor();

}); // end am4core.ready()


// fifth Graph

am4core.ready(function () {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create chart instance
    var fifthchart = am4core.create("chartdivfifth", am4charts.XYChart);
    fifthchart.layout = "grid";
    fifthchart.height = 70;
    fifthchart.paddingRight = 20;
    fifthchart.logo.disabled = true;



    // Add data
    fifthchart.data = [{
        "year": "1950",
        "value": 1220
    }, {
        "year": "1951",
        "value": 100
    }, {
        "year": "1952",
        "value": 600
    }, {
        "year": "1953",
        "value": 800
    }, {
        "year": "1954",
        "value": 1600
    }, {
        "year": "1955",
        "value": 1100
    }, {
        "year": "1956",
        "value": 50
    }, {
        "year": "1957",
        "value": 1500
    }, {
        "year": "1958",
        "value": 210
    }, {
        "year": "1959",
        "value": 900
    }, {
        "year": "1960",
        "value": 1900
    }, {
        "year": "1961",
        "value": 10
    }, {
        "year": "1962",
        "value": 700
    }];

    // Create axes
    var fifthcategoryAxis = fifthchart.xAxes.push(new am4charts.CategoryAxis());
    fifthcategoryAxis.dataFields.category = "year";
    fifthcategoryAxis.renderer.minGridDistance = 50;
    fifthcategoryAxis.renderer.grid.template.location = 0.5;
    fifthcategoryAxis.startLocation = 0.5;
    fifthcategoryAxis.endLocation = 0.5;
    fifthcategoryAxis.renderer.grid.template.disabled = true;
    fifthcategoryAxis.renderer.labels.template.disabled = true;
    fifthcategoryAxis.cursorTooltipEnabled = false;

    // Create value axis
    var fifthvalueAxis = fifthchart.yAxes.push(new am4charts.ValueAxis());
    fifthvalueAxis.baseValue = 0;
    fifthvalueAxis.renderer.grid.template.disabled = true;
    fifthvalueAxis.renderer.baseGrid.disabled = true;
    fifthvalueAxis.renderer.labels.template.disabled = true;
    fifthvalueAxis.cursorTooltipEnabled = false;

    // Create series
    var fifthseries = fifthchart.series.push(new am4charts.LineSeries());
    fifthseries.dataFields.valueY = "value";
    fifthseries.dataFields.categoryX = "year";
    fifthseries.strokeWidth = 3;
    fifthseries.tensionX = 0.77;
    fifthseries.stroke = am4core.color("#9795a1");


    // bullet is added because we add tooltip to a bullet for it to change color
    var fifthullet = fifthseries.bullets.push(new am4charts.Bullet());
    fifthbullet.tooltipText = "{valueY}";

    fifthbullet.adapter.add("fill", function (fill, target) {
        if (target.dataItem.valueY < 0) {
            return am4core.color("#FF0000");
        }
        return fill;
    })


    fifthchart.cursor = new am4charts.XYCursor();

}); // end am4core.ready()

// Sixth Graph

am4core.ready(function () {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create chart instance
    var sixthchart = am4core.create("chartdivsixth", am4charts.XYChart);
    sixthchart.layout = "grid";
    sixthchart.height = 70;
    sixthchart.paddingRight = 20;
    sixthchart.logo.disabled = true;



    // Add data
    sixthchart.data = [{
        "year": "1950",
        "value": 1220
    }, {
        "year": "1951",
        "value": 100
    }, {
        "year": "1952",
        "value": 600
    }, {
        "year": "1953",
        "value": 800
    }, {
        "year": "1954",
        "value": 1600
    }, {
        "year": "1955",
        "value": 1100
    }, {
        "year": "1956",
        "value": 50
    }, {
        "year": "1957",
        "value": 1500
    }, {
        "year": "1958",
        "value": 210
    }, {
        "year": "1959",
        "value": 900
    }, {
        "year": "1960",
        "value": 1900
    }, {
        "year": "1961",
        "value": 10
    }, {
        "year": "1962",
        "value": 700
    }];

    // Create axes
    var sixthcategoryAxis = sixthchart.xAxes.push(new am4charts.CategoryAxis());
    sixthcategoryAxis.dataFields.category = "year";
    sixthcategoryAxis.renderer.minGridDistance = 50;
    sixthcategoryAxis.renderer.grid.template.location = 0.5;
    sixthcategoryAxis.startLocation = 0.5;
    sixthcategoryAxis.endLocation = 0.5;
    sixthcategoryAxis.renderer.grid.template.disabled = true;
    sixthcategoryAxis.renderer.labels.template.disabled = true;
    sixthcategoryAxis.cursorTooltipEnabled = false;

    // Create value axis
    var sixthvalueAxis = sixthchart.yAxes.push(new am4charts.ValueAxis());
    sixthvalueAxis.baseValue = 0;
    sixthvalueAxis.renderer.grid.template.disabled = true;
    sixthvalueAxis.renderer.baseGrid.disabled = true;
    sixthvalueAxis.renderer.labels.template.disabled = true;
    sixthvalueAxis.cursorTooltipEnabled = false;

    // Create series
    var sixthseries = sixthchart.series.push(new am4charts.LineSeries());
    sixthseries.dataFields.valueY = "value";
    sixthseries.dataFields.categoryX = "year";
    sixthseries.strokeWidth = 3;
    sixthseries.tensionX = 0.77;
    sixthseries.stroke = am4core.color("#9795a1");


    // bullet is added because we add tooltip to a bullet for it to change color
    var sixthullet = sixthseries.bullets.push(new am4charts.Bullet());
    sixthbullet.tooltipText = "{valueY}";

    sixthbullet.adapter.add("fill", function (fill, target) {
        if (target.dataItem.valueY < 0) {
            return am4core.color("#FF0000");
        }
        return fill;
    })


    sixthchart.cursor = new am4charts.XYCursor();

}); // end am4core.ready()


// Seventh Graph

am4core.ready(function () {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create chart instance
    var seventhchart = am4core.create("chartdivseventh", am4charts.XYChart);
    seventhchart.layout = "grid";
    seventhchart.height = 70;
    seventhchart.paddingRight = 20;
    seventhchart.logo.disabled = true;



    // Add data
    seventhchart.data = [{
        "year": "1950",
        "value": 1220
    }, {
        "year": "1951",
        "value": 100
    }, {
        "year": "1952",
        "value": 600
    }, {
        "year": "1953",
        "value": 800
    }, {
        "year": "1954",
        "value": 1600
    }, {
        "year": "1955",
        "value": 1100
    }, {
        "year": "1956",
        "value": 50
    }, {
        "year": "1957",
        "value": 1500
    }, {
        "year": "1958",
        "value": 210
    }, {
        "year": "1959",
        "value": 900
    }, {
        "year": "1960",
        "value": 1900
    }, {
        "year": "1961",
        "value": 10
    }, {
        "year": "1962",
        "value": 700
    }];

    // Create axes
    var seventhcategoryAxis = seventhchart.xAxes.push(new am4charts.CategoryAxis());
    seventhcategoryAxis.dataFields.category = "year";
    seventhcategoryAxis.renderer.minGridDistance = 50;
    seventhcategoryAxis.renderer.grid.template.location = 0.5;
    seventhcategoryAxis.startLocation = 0.5;
    seventhcategoryAxis.endLocation = 0.5;
    seventhcategoryAxis.renderer.grid.template.disabled = true;
    seventhcategoryAxis.renderer.labels.template.disabled = true;
    seventhcategoryAxis.cursorTooltipEnabled = false;

    // Create value axis
    var seventhvalueAxis = seventhchart.yAxes.push(new am4charts.ValueAxis());
    seventhvalueAxis.baseValue = 0;
    seventhvalueAxis.renderer.grid.template.disabled = true;
    seventhvalueAxis.renderer.baseGrid.disabled = true;
    seventhvalueAxis.renderer.labels.template.disabled = true;
    seventhvalueAxis.cursorTooltipEnabled = false;

    // Create series
    var seventhseries = seventhchart.series.push(new am4charts.LineSeries());
    seventhseries.dataFields.valueY = "value";
    seventhseries.dataFields.categoryX = "year";
    seventhseries.strokeWidth = 6;
    seventhseries.tensionX = 0.77;
    seventhseries.stroke = am4core.color("#2ECC71");


    // bullet is added because we add tooltip to a bullet for it to change color
    var seventhullet = seventhseries.bullets.push(new am4charts.Bullet());
    seventhbullet.tooltipText = "{valueY}";

    seventhbullet.adapter.add("fill", function (fill, target) {
        if (target.dataItem.valueY < 0) {
            return am4core.color("#FF0000");
        }
        return fill;
    })


    seventhchart.cursor = new am4charts.XYCursor();

}); // end am4core.ready()

// eight Graph

am4core.ready(function () {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create chart instance
    var eightchart = am4core.create("chartdiveight", am4charts.XYChart);
    eightchart.layout = "grid";
    eightchart.height = 70;
    eightchart.paddingRight = 20;
    eightchart.logo.disabled = true;



    // Add data
    eightchart.data = [{
        "year": "1950",
        "value": 1220
    }, {
        "year": "1951",
        "value": 100
    }, {
        "year": "1952",
        "value": 600
    }, {
        "year": "1953",
        "value": 800
    }, {
        "year": "1954",
        "value": 1600
    }, {
        "year": "1955",
        "value": 1100
    }, {
        "year": "1956",
        "value": 50
    }, {
        "year": "1957",
        "value": 1500
    }, {
        "year": "1958",
        "value": 210
    }, {
        "year": "1959",
        "value": 900
    }, {
        "year": "1960",
        "value": 1900
    }, {
        "year": "1961",
        "value": 10
    }, {
        "year": "1962",
        "value": 700
    }];

    // Create axes
    var eightcategoryAxis = eightchart.xAxes.push(new am4charts.CategoryAxis());
    eightcategoryAxis.dataFields.category = "year";
    eightcategoryAxis.renderer.minGridDistance = 50;
    eightcategoryAxis.renderer.grid.template.location = 0.5;
    eightcategoryAxis.startLocation = 0.5;
    eightcategoryAxis.endLocation = 0.5;
    eightcategoryAxis.renderer.grid.template.disabled = true;
    eightcategoryAxis.renderer.labels.template.disabled = true;
    eightcategoryAxis.cursorTooltipEnabled = false;

    // Create value axis
    var eightvalueAxis = eightchart.yAxes.push(new am4charts.ValueAxis());
    eightvalueAxis.baseValue = 0;
    eightvalueAxis.renderer.grid.template.disabled = true;
    eightvalueAxis.renderer.baseGrid.disabled = true;
    eightvalueAxis.renderer.labels.template.disabled = true;
    eightvalueAxis.cursorTooltipEnabled = false;

    // Create series
    var eightseries = eightchart.series.push(new am4charts.LineSeries());
    eightseries.dataFields.valueY = "value";
    eightseries.dataFields.categoryX = "year";
    eightseries.strokeWidth = 6;
    eightseries.tensionX = 0.77;
    eightseries.stroke = am4core.color("#E74C3C");


    // bullet is added because we add tooltip to a bullet for it to change color
    var eightullet = eightseries.bullets.push(new am4charts.Bullet());
    eightbullet.tooltipText = "{valueY}";

    eightbullet.adapter.add("fill", function (fill, target) {
        if (target.dataItem.valueY < 0) {
            return am4core.color("#FF0000");
        }
        return fill;
    })


    eightchart.cursor = new am4charts.XYCursor();

}); // end am4core.ready()