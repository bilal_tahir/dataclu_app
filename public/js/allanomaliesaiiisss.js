var data;
var i;
var trendarr = [];
var slopearr = [];
var slopearr2 = [];
var fields = ["date", "trends", "lower_bound", "upper_bound", "trending_slope", "disabled", "disabled2",];
var trendanalysis;
var sessid = "30TrendDays";
var arraydata = [];
var fields2 = ["date", "trends", "lower_bound", "upper_bound", "trending_slope", "disabled", "disabled2",];
var trendarr2 = [];
var fields3 = ["date", "trends", "lower_bound", "upper_bound", "trending_slope", "disabled", "disabled2",];
var trendarr3 = [];
var fields4 = ["date", "trends", "lower_bound", "upper_bound", "trending_slope", "disabled", "disabled2",];
var trendarr4 = [];
var startdate = 0;
var enddate = 0;
var bStartDate = moment().subtract(92, "days").format("MMM DD, YYYY");
var bEndDate = moment().subtract(32, "days").format("MMM DD, YYYY");
var bFinalDate = bStartDate + " - " + bEndDate;
var rangeChange = 0;
var image_path1 = "/images/greenarrowup.png";
var image_path2 = "/images/redarrowdown.png";
var rangestart = 0;
var assignResult = [];
var assignResult2;
var assignResult3;
var assignResult4;
var chartCount = 0;
const months = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC",];
var finalUrl = $("#url-change").find(":selected").val();
var userIdApi = parseInt(userIdContr);
var clickUrl;
var ctrUrl;
var impressionUrl;
var positionUrl;
var strUser = "W";
var checkDataStatus = 0;
var changeApiUrl = document.getElementById("url-change").options;
var options = { year: "numeric", month: "short", day: "numeric" };
var parts;
var mydate;
var totalArrays = 0;
var loc_arr1 = 0;

var getSelectedUrl;

var dateCheck = 0;

// // Apis for calling data from ai daily
var apiDomain = "https://ai.dataclu.com/";
var range = 25;
var dimension = '["query"]';


//calculating created divs
var calcDiv = 0;
var divReleased = 1;
var arraydataHold = [];

var gridContent;
var gridContainer;
var tableContent;
var tableContainer;
var createDiv = 0;

apiCalls();

// console.log(finalUrl);

function apiCalls() {
    arraydataHold = [];
    trendarr = [];
    totalArrays = 0;
    loc_arr1 = 0;
    trendarr2 = [];

    if (strUser == "D") {
        $.when(
            jQuery.ajax({
                url: 'getData/alluser/1',
                method: "get",
                // async: false,
                success: function (result) {
                    // console.log(JSON.parse(result));
                    assignResult = (JSON.parse(result)).filter(checkUrl);
                    totalArrays = assignResult.length;
                    for (var arrayDec = 0; arrayDec < totalArrays; arrayDec++) {
                        arraydataHold[arrayDec] = assignResult[arrayDec];
                        loc_arr1++;
                    }
                },
            })
        ).then(function () {

            $(".loader").hide();
            divReleased = 1;
            arraydata = [];
            if (assignResult.length == 0) {
                document.getElementById("data-check").style.display = "block";
                $("#loadMorebutton").hide();
            }
            else {
                // console.log(assignResult.length);
                document.getElementById("data-check").style.display = "none";
                createGridDivs();
            }
        });
    }
    if (strUser == "W") {
        $.when(
            jQuery.ajax({
                url: 'getData/alluser/2',
                method: "get",
                // async: false,
                success: function (result) {
                    // console.log(JSON.parse(result));

                    assignResult = (JSON.parse(result)).filter(checkUrl);
                    totalArrays = assignResult.length;
                    for (var arrayDec = 0; arrayDec < totalArrays; arrayDec++) {
                        arraydataHold[arrayDec] = assignResult[arrayDec];
                        loc_arr1++;
                    }
                },
            })
        ).then(function () {
            $(".loader").hide();
            divReleased = 1;
            arraydata = [];
            if (assignResult.length == 0) {
                document.getElementById("data-check").style.display = "block";
                $("#loadMorebutton").hide();
            }
            else {
                document.getElementById("data-check").style.display = "none";
                createGridDivs();
            }
        });
    }

    function checkUrl(result) {
        return result['website'] == finalUrl;
    }
}

function createGridDivs() {
    if (divReleased > 8) {
        $("#e3").DataTable().destroy();
        console.log("destroyed");
    }
    createDiv = 0;
    if (divReleased < 10) {
        calcDiv = 0;
        for (var relData = 0; relData < arraydataHold.length; relData++) {
            if (relData == 8) {
                break;
            }
            arraydata.push(arraydataHold[relData]);
            calcDiv++;
        }
        divReleased += 8;
    }

    if (divReleased <= 9) {
        $("#te").append(
            '<div id="grid-div">' +
            '<div class="row" id="createGridView">' +
            "</div></div>" +
            '<div id="table-div" class="at-chartlistview" data-label="Example" class="df-example demo-table" style="display:none;"><table class="table at-tablechartview" id="e3"><thead><tr><th><span></span></th><th><span>Metric</span></th>' +
            "<th><span>Dimension</span></th><th><span id = \"dateHeading\" style=\"text-align:center\"></span></th><th><span>Actual</span></th><th><span>Expected</span></th>" +
            '<th><span>Variance</span></th><th><span>Impact</span></th></tr></thead><tbody id="createTableView">' +
            "</tbody></table></div>"
        );
    }
    for (var gDiv = divReleased - 8; gDiv <= divReleased - 1; gDiv++) {
        if (createDiv >= calcDiv) {
            break;
        }
        $("#createGridView").append(
            '<div class="col-12 col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3" ><div class="box"><div class="row"><div class="col-12">' +
            '<div class="at-charthead "><h5 class=" headText" id="ht' +
            gDiv +
            '" ></h5><h6 class="at-chartinfor">' +
            '<span class="tooltip22" id="q' +
            gDiv +
            '"></span>' +
            // '<span id="web' + gDiv + '> Website </span>' +
            '<br><span class="dimension">Website:</span><span class="dimension" id="web' + gDiv + '"></span>' +
            // '"></span><br><span class="dimension">Country: <span id="c' + gDiv + '"></span></span>
            '<i class="icon-save"></i></h6></div></div ></div > ' +
            '<div class="row"><div class="col-12"><div class="at-chartvalue"><h5 class="mg-b- daysText"><i id="arrow' +
            gDiv +
            '"></i>' +
            '<i id="pc' +
            gDiv +
            '"></i><i id="im' +
            gDiv +
            '"></i><i id="tl' +
            gDiv +
            '"></i></h5></div></div></div>' +
            '<div class="row"><div class="col-12"><div id="chartsdivs' +
            gDiv +
            '" class="customchartthird"  style="margin-bottom:10px"></div>' +
            "</div></div></div></div>"
        );

        createDiv++;
    }

    createDiv = 0;
    for (var tDiv = divReleased - 8; tDiv <= divReleased - 1; tDiv++) {
        if (createDiv >= calcDiv) {
            break;
        }
        // tableContent +=
        $("#createTableView").append(
            '<tr><td><span id="anomaly_id' +
            tDiv +
            '"></span></td><td><h5 class="table-headText" id="tht' +
            tDiv +
            '"></h5></td><td><h5 class="mg-b-15">' +
            '<span class="table-dimension" id="htp' + tDiv + '"> </span><span class="tooltip22" style="font-size:14px !important;' +
            ' color:black !important" id="tp' +
            tDiv +
            '"></span>' +
            '<br><span class="table-dimension" >' +
            'Website: <span id="t-web' + tDiv + '"></span></span>' +
            "</h5></td><td>" +
            '<div id="tablechartsdivs' +
            tDiv +
            '" class="table-customchartthird"  style="margin-bottom:10px"></div></td>' +
            '<td><span id="tts' +
            tDiv +
            '"></span></td><td><span id="expected' +
            tDiv +
            '"></span></td><td><h5>' +
            '<i id="tim' +
            tDiv +
            '"></i><i id="ttttpc' +
            tDiv +
            '"></i> </h5></td><td> <span id="impact' +
            tDiv +
            '"></span> </td></tr>'
        );
        createDiv++;
    }
    assignValues();
}

function assignValues() {
    console.log(arraydataHold);
    createDiv = 0;
    for (
        var assignData = divReleased - 8;
        assignData <= divReleased - 1;
        assignData++
    ) {
        if (createDiv >= calcDiv) {
            break;
        }

        // Console or analytics image
        if (arraydata[assignData - 1]["dtype"] == "ga") {
            // $("#ht" + assignData).addClass("analyticsImage");
            var getElementAnalytics, setNameAnalytics, finalArrAnalytics;
            getElementAnalytics = document.getElementById("ht" + assignData);
            setNameAnalytics = "analyticsImage";
            finalArrAnalytics = getElementAnalytics.className.split(" ");
            if (finalArrAnalytics.indexOf(setNameAnalytics) == -1) {
                getElementAnalytics.className += " " + setNameAnalytics;
            }
            document.getElementById("anomaly_id" + assignData).innerHTML = '<a href="/anomaly/detail/' + arraydata[assignData - 1]["_id"] + '"' + 'target=_blank><img src="/images/ga.png" class="tabletoolimg"  /></a>';
        }
        if (arraydata[assignData - 1]["dtype"] == "gsc") {
            var getElementConsole, setNameConsole, finalArrConsole;
            getElementConsole = document.getElementById("ht" + assignData);
            setNameConsole = "consoleImage";
            finalArrConsole = getElementConsole.className.split(" ");
            if (finalArrConsole.indexOf(setNameConsole) == -1) {
                getElementConsole.className += " " + setNameConsole;
            }
            document.getElementById("anomaly_id" + assignData).innerHTML = '<a href="/anomaly/detail/' + arraydata[assignData - 1]["_id"] + '"' + 'target=_blank><img src="/images/search.png" class="tabletoolimg"  /></a>';
        }
        if (arraydata[assignData - 1]["dtype"] == "shopify") {
            var getElementConsole, setNameConsole, finalArrConsole;
            getElementConsole = document.getElementById("ht" + assignData);
            setNameConsole = "shopifyImage";
            finalArrConsole = getElementConsole.className.split(" ");
            if (finalArrConsole.indexOf(setNameConsole) == -1) {
                getElementConsole.className += " " + setNameConsole;
            }
            document.getElementById("anomaly_id" + assignData).innerHTML = '<a href="/anomaly/detail/' + arraydata[assignData - 1]["_id"] + '"' + 'target=_blank><img src="/images/shp.png" class="tabletoolimg"  /></a>';
        }
        // document.getElementById("anomaly_id" + assignData).innerHTML = '<a href="/anomaly/detail/' + arraydata[assignData - 1]["_id"] + '"' + 'target=_blank><img src="/images/ga.png" class="tabletoolimg"  /></a>';
        var cap = capital_letter(arraydata[assignData - 1]["metric"]);
        document.getElementById("ht" + assignData).innerHTML = cap;
        if ("pagePath" in arraydata[assignData - 1]) {
            document.getElementById("q" + assignData).innerHTML = "pagePath : " + stringTruncate(arraydata[assignData - 1]["pagePath"], 18) + '<span class="tooltiptext22">' + arraydata[assignData - 1]["pagePath"] + "</span>";

            document.getElementById("htp" + assignData).innerHTML = "pagePath: ";
            document.getElementById("tp" + assignData).innerHTML = stringTruncate(arraydata[assignData - 1]["pagePath"], 24) + '<span class="tooltiptext22">' + arraydata[assignData - 1]["pagePath"] + "</span>";
        }
        if ("query" in arraydata[assignData - 1]) {
            document.getElementById("q" + assignData).innerHTML = "Query : " + arraydata[assignData - 1]["query"];

            document.getElementById("htp" + assignData).innerHTML = "Query: ";
            document.getElementById("tp" + assignData).innerHTML = arraydata[assignData - 1]["query"];
        }
        if ("dimension" in arraydata[assignData - 1]) {
            document.getElementById("q" + assignData).innerHTML = "Dimension: " + arraydata[assignData - 1]["dimension"];

            document.getElementById("htp" + assignData).innerHTML = "Dimension: ";
            document.getElementById("tp" + assignData).innerHTML = arraydata[assignData - 1]["dimension"];
        }
        //removing www from website
        var website = (arraydata[assignData - 1]["website"]).split('.').slice(1);
        document.getElementById("web" + assignData).innerHTML = " " + (website).join('.');
        document.getElementById("t-web" + assignData).innerHTML = " " + (website).join('.');
        // $(".headText").after().css("background", "url(images/search.png) no-repeat");
        if (arraydata[assignData - 1]["anomaly"] == "up") {
            document.getElementById("pc" + assignData).innerHTML = '<span class="percentcss" id="ts' + assignData + '"></span>';
            document.getElementById("tl" + assignData).innerHTML = '<span class="textLightthird" id="tc' + assignData + '" > </span>';
            document.getElementById("ttttpc" + assignData).innerHTML = ' <span class="table-percentcss" id="ttc' + assignData + '"></span>';
            document.getElementById("arrow" + assignData).innerHTML = '<img src="/images/greenup.png" class="GreenUpImage"/>';
        }
        if (arraydata[assignData - 1]["anomaly"] == "down") {
            document.getElementById("pc" + assignData).innerHTML = '<span class="percentcssfourth" id="ts' + assignData + '"></span>';
            document.getElementById("tl" + assignData).innerHTML = '<span class="textLightfourth" id="tc' + assignData + '" > </span>';
            document.getElementById("ttttpc" + assignData).innerHTML = ' <span class="table-percentcssfourth" id="ttc' + assignData + '"></span>';
            document.getElementById("arrow" + assignData).innerHTML = '<img src="/images/reddown.png" class="GreenUpImage"/>';
        }
        // For Change
        if (parseInt(arraydata[assignData - 1]["stats"]["change"]) >= 0) {
            document.getElementById("im" + assignData).innerHTML = "<img src=" + image_path1 + ' class="GreenUpImage"/>';
            document.getElementById("tim" + assignData).innerHTML = "<img src=" + image_path1 + ' class="GreenUpImage"/>';
        }
        if (parseInt(arraydata[assignData - 1]["stats"]["change"]) < 0) {
            document.getElementById("im" + assignData).innerHTML = "<img src=" + image_path2 + ' class="GreenUpImage"/>';
            document.getElementById("tim" + assignData).innerHTML = "<img src=" + image_path2 + ' class="GreenUpImage"/>';
        }

        document.getElementById("ts" + assignData).innerHTML = arraydata[assignData - 1]["stats"]["agg"].toFixed(2);
        document.getElementById("tc" + assignData).innerHTML = Math.abs(parseInt(arraydata[assignData - 1]["stats"]["change"])) + "%";

        document.getElementById("tht" + assignData).innerHTML = cap;
        // document.getElementById("tp" + assignData).innerHTML = stringTruncate(arraydata[assignData - 1]["pagePath"], 24) + '<span class="tooltiptext22">' + arraydata[assignData - 1]["pagePath"] + "</span>";
        document.getElementById("tts" + assignData).innerHTML = arraydata[assignData - 1]["stats"]["actuals"][arraydata[assignData - 1]["stats"]["actuals"].length - 1].toFixed(2);
        document.getElementById("expected" + assignData).innerHTML = arraydata[assignData - 1]["stats"]["predictions"][arraydata[assignData - 1]["stats"]["predictions"].length - 1].toFixed(2);
        document.getElementById("ttc" + assignData).innerHTML = Math.abs(parseInt(arraydata[assignData - 1]["stats"]["change"])) + "%";
        document.getElementById("impact" + assignData).innerHTML = arraydata[assignData - 1]["stats"]["impact"];

        createDiv++;
    }

    // console.log(arraydata);
    // if (dateCheck == 0) {
    parts = arraydata[0]["stats"]["date"][arraydata[0]["stats"]["date"].length - 1].split("-");
    // console.log([arraydata[0]["stats"]["date"].length]);
    // console.log(parts);
    mydate = new Date(parts[0], parts[1] - 1, parts[2]);
    // console.log(moment(mydate).format("DD MMM"));
    // console.log(mydate);
    // document.getElementById("daily-date").innerHTML = "Daily";
    // document.getElementById("weekly-date").innerHTML = "Weekly";
    // }
    if (strUser == "W") {
        // document.getElementById("dateHeading").innerHTML = moment(mydate.setDate(mydate.getDate())).day(-6).format("DD MMM, YYYY") + " - " + moment(mydate.setDate(mydate.getDate())).day(0).format("DD MMM, YYYY");
        document.getElementById("dateHeading").innerHTML = moment(mydate.setDate(mydate.getDate())).day(-6).format("DD") + " - " + moment(mydate.setDate(mydate.getDate())).day(0).format("DD MMM");
    }
    else {
        // document.getElementById("dateHeading").innerHTML = mydate.toLocaleDateString("en-US", options);
        document.getElementById("dateHeading").innerHTML = moment(mydate).format("DD MMM");
    }

    function stringTruncate(str, length) {
        var dots = str.length > length ? "..." : "";
        return str.substring(0, length) + dots;
    }

    $("#e3").DataTable({
        bPaginate: false,
        bFilter: false, //hide Search bar
        bInfo: false, // hide showing entries
        language: {
        },
        aaSorting: [],
        columnDefs: [
            { orderable: true, targets: 7 },
            { orderable: false, targets: "_all" },
        ],
        order: [[7, "desc"]],
    });
    $(".loader").hide();
    display();
}

function display() {
    createDiv = 0;

    for (var extL = divReleased - 9; extL < divReleased - 1; extL++) {
        if (createDiv >= calcDiv) {
            break;
        }
        trendarr = [];
        slopearr = [];

        for (
            var i = 0,
            j =
                arraydata[extL]["stats"]["lower_bound"].length -
                arraydata[extL]["stats"]["date"].length;
            i < arraydata[extL]["stats"]["date"].length;
            i++, j++
        ) {
            parts = arraydata[extL]["stats"]["date"][i].split("-");
            mydate = new Date(parts[0], parts[1] - 1, parts[2]);
            var object = {};

            object[fields[0]] = mydate.toLocaleDateString("en-US", options);
            object[fields[1]] = arraydata[extL]["stats"]["actuals"][i].toFixed(
                2
            );
            if (j >= 0) {
                object[fields[2]] = arraydata[extL]["stats"]["lower_bound"][j];
                object[fields[3]] = arraydata[extL]["stats"]["upper_bound"][j];
            }
            object[fields[4]] = arraydata[extL]["stats"]["trending_slope"][i];
            if (
                arraydata[extL]["stats"]["anomaly_data"]["positive"][
                    "date"
                ].includes(arraydata[extL]["stats"]["date"][i]) &&
                arraydata[extL]["stats"]["anomaly_data"]["positive"][
                    "value"
                ].includes(arraydata[extL]["stats"]["actuals"][i])
            ) {
                object[fields[5]] = false;
            }
            if (
                arraydata[extL]["stats"]["anomaly_data"]["negative"][
                    "date"
                ].includes(arraydata[extL]["stats"]["date"][i]) &&
                arraydata[extL]["stats"]["anomaly_data"]["negative"][
                    "value"
                ].includes(arraydata[extL]["stats"]["actuals"][i])
            ) {
                object[fields[6]] = false;
            }
            trendarr.push(object);
        }

        trendarr2[extL] = trendarr;

        parts = arraydata[extL]["stats"]["date"][0].split("-");
        mydate = new Date(parts[0], parts[1] - 1, parts[2]);
        var object = {};

        object[fields[0]] = mydate.toLocaleDateString("en-US", options);
        object[fields[4]] = arraydata[extL]["stats"]["trending_slope"][0];
        slopearr.push(object);

        //2nd
        parts = arraydata[extL]["stats"]["date"][
            arraydata[extL]["stats"]["date"].length - 1
        ].split("-");
        mydate = new Date(parts[0], parts[1] - 1, parts[2]);
        var object = {};

        object[fields[0]] = mydate.toLocaleDateString("en-US", options);
        object[fields[4]] =
            arraydata[extL]["stats"]["trending_slope"][
            arraydata[extL]["stats"]["date"].length - 1
            ];

        slopearr.push(object);

        slopearr2[extL] = slopearr;

        createDiv++;
    }

    c();

}
function c() {
    createDiv = 0;
    for (
        var rangeChart = divReleased - 8;
        rangeChart <= divReleased - 1;
        rangeChart++
    ) {
        if (createDiv >= calcDiv) {
            break;
        }
        am4core.ready(function () {
            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            var chart = am4core.create(
                "chartsdivs" + rangeChart,
                am4charts.XYChart
            );
            // console.log("chartdivs" + rangeChart);
            chart.layout = "grid";
            // chart.height = 100;
            chart.height = am4core.percent(100);
            chart.paddingRight = 10;
            chart.paddingLeft = 10;
            chart.logo.disabled = true;

            // console.log(trendarr2[rangeChart - 1]);
            // console.log(slopearr2[rangeChart - 1]);
            chart.data = trendarr2[rangeChart - 1];
            // console.log(trendarr2[rangeChart - 1]);

            // Create axes
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "date";
            categoryAxis.renderer.minGridDistance = 50;
            categoryAxis.renderer.grid.template.location = 0.5;
            categoryAxis.startLocation = 0.5;
            categoryAxis.endLocation = 0.5;
            categoryAxis.renderer.grid.template.disabled = true;
            categoryAxis.renderer.labels.template.disabled = true;
            categoryAxis.cursorTooltipEnabled = false;

            // Create value axis
            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.baseValue = 0;
            valueAxis.renderer.grid.template.disabled = true;
            valueAxis.renderer.baseGrid.disabled = true;
            valueAxis.renderer.labels.template.disabled = true;
            valueAxis.cursorTooltipEnabled = false;

            // Create series
            var series = chart.series.push(new am4charts.LineSeries());
            series.dataFields.valueY = "trends";
            series.dataFields.categoryX = "date";
            series.strokeWidth = 2;
            series.tensionX = 0.77;
            if (arraydata[rangeChart - 1]["anomaly"] == "down") {
                series.stroke = am4core.color("red");
            }
            if (arraydata[rangeChart - 1]["anomaly"] == "up") {
                series.stroke = am4core.color("green");
            }

            // Create Upper_Bound series
            var series3 = chart.series.push(new am4charts.LineSeries());
            series3.dataFields.valueY = "upper_bound";
            series3.dataFields.openValueY = "lower_bound";
            series3.dataFields.categoryX = "date";
            series3.strokeWidth = 2;
            series3.tensionX = 0.77;
            series3.fillOpacity = 0.6;
            series3.sequencedInterpolation = true;
            series3.fill = am4core.color("lightgray");
            series3.stroke = am4core.color("lightgray");
            // series3.stacked = true;

            // Create Lower_Bound series
            var series2 = chart.series.push(new am4charts.LineSeries());
            series2.dataFields.valueY = "lower_bound";
            series2.dataFields.categoryX = "date";
            series2.strokeWidth = 2;
            series2.tensionX = 0.77;
            series2.stroke = am4core.color("lightgray");
            series2.sequencedInterpolation = true;

            // Create Trending_Slope series
            var series2 = chart.series.push(new am4charts.LineSeries());
            series2.data = slopearr2[rangeChart - 1];
            series2.dataFields.valueY = "trending_slope";
            series2.dataFields.categoryX = "date";
            series2.strokeWidth = 2;
            series2.tensionX = 0.77;

            if (arraydata[rangeChart - 1]["anomaly"] == "down") {
                series2.stroke = am4core.color("red");
            }
            if (arraydata[rangeChart - 1]["anomaly"] == "up") {
                series2.stroke = am4core.color("green");
            }


            // bullet is added because we add tooltip to a bullet for it to change color
            var bullet = series.bullets.push(new am4charts.Bullet());
            bullet.tooltipText = "{valueY} : {categoryX}";

            var bullet2 = series.bullets.push(new am4charts.CircleBullet());
            bullet2.disabled = true;
            bullet2.propertyFields.disabled = "disabled2";
            bullet2.stroke = am4core.color("red");
            bullet2.fill = am4core.color("red");

            var bullet3 = series.bullets.push(new am4charts.CircleBullet());
            bullet3.disabled = true;
            bullet3.propertyFields.disabled = "disabled";
            bullet3.stroke = am4core.color("green");
            bullet3.fill = am4core.color("green");

            chart.cursor = new am4charts.XYCursor();
        }); // end am4core.ready()

        // Chart For Grid View
        am4core.ready(function () {
            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            var tablechart = am4core.create(
                "tablechartsdivs" + rangeChart,
                am4charts.XYChart
            );
            tablechart.layout = "grid";
            tablechart.height = 100;
            // tablechart.padding = 20;
            tablechart.logo.disabled = true;

            // Add data
            // console.log(trendarr);
            tablechart.data = trendarr2[rangeChart - 1];

            // Create axes
            var tablecategoryAxis = tablechart.xAxes.push(
                new am4charts.CategoryAxis()
            );
            tablecategoryAxis.dataFields.category = "date";
            tablecategoryAxis.renderer.minGridDistance = 50;
            tablecategoryAxis.renderer.grid.template.location = 0.5;
            tablecategoryAxis.startLocation = 0.5;
            tablecategoryAxis.endLocation = 0.5;
            tablecategoryAxis.renderer.grid.template.disabled = true;
            tablecategoryAxis.renderer.labels.template.disabled = true;
            tablecategoryAxis.cursorTooltipEnabled = false;

            // Create value axis
            var tablevalueAxis = tablechart.yAxes.push(
                new am4charts.ValueAxis()
            );
            tablevalueAxis.baseValue = 0;
            tablevalueAxis.renderer.grid.template.disabled = true;
            tablevalueAxis.renderer.baseGrid.disabled = true;
            tablevalueAxis.renderer.labels.template.disabled = true;
            tablevalueAxis.cursorTooltipEnabled = false;

            // Create series
            var tableseries = tablechart.series.push(
                new am4charts.LineSeries()
            );
            tableseries.dataFields.valueY = "trends";
            tableseries.dataFields.categoryX = "date";
            tableseries.strokeWidth = 2;
            tableseries.tensionX = 0.77;
            if (arraydata[rangeChart - 1]["anomaly"] == "down") {
                tableseries.stroke = am4core.color("red");
            }
            if (arraydata[rangeChart - 1]["anomaly"] == "up") {
                tableseries.stroke = am4core.color("green");
            }


            // Create Trending_Slope series
            var tableseries4 = tablechart.series.push(
                new am4charts.LineSeries()
            );
            tableseries4.data = slopearr2[rangeChart - 1];
            tableseries4.dataFields.valueY = "trending_slope";
            tableseries4.dataFields.categoryX = "date";
            tableseries4.strokeWidth = 2;
            tableseries4.tensionX = 0.77;
            if (arraydata[rangeChart - 1]["anomaly"] == "down") {
                tableseries4.stroke = am4core.color("red");
            }
            if (arraydata[rangeChart - 1]["anomaly"] == "up") {
                tableseries4.stroke = am4core.color("green");
            }

            // bullet is added because we add tooltip to a bullet for it to change color
            var tablebullet = tableseries.bullets.push(new am4charts.Bullet());
            tablebullet.tooltipText = "{valueY} : {categoryX}";

            tablebullet.adapter.add("fill", function (fill, target) {
                if (target.dataItem.valueY < 0) {
                    return am4core.color("#FF0000");
                }
                return fill;
            });

            tablechart.cursor = new am4charts.XYCursor();
        }); // end am4core.ready()
        createDiv++;
    }
}
function capital_letter(str) {
    str = str.split(" ");

    for (let i = 0, x = str.length; i < x; i++) {
        str[i] = str[i][0].toUpperCase() + str[i].substr(1);
    }

    return str.join(" ");
}

//function when user changes the url
function urlChangeFunction() {
    var eu = document.getElementById("url-change");
    finalUrl = eu.value;
    // var changeUrl = eu.value;
    // var splitUrl = changeUrl.split("/");
    // finalUrl = "sc-domain:" + splitUrl[2];
    am4core.disposeAllCharts();
    $("#te").empty();
    // $("grid-div").remove();
    // $("table-div").remove();
    $(".loader").show();
    rangeChange = 1;
    // SessionData();
    apiCalls();
}
function rangeChangeFunction() {
    var e = document.getElementById("ddlViewBy");
    strUser = e.value;
    am4core.disposeAllCharts();
    $("#te").empty();
    $(".loader").show();
    rangeChange = 1;
    dateCheck = 1;
    apiCalls();
}

function loadMore() {
    $(".loader").show();
    calcDiv = 0;
    for (
        var againrelData = divReleased - 1;
        againrelData < arraydataHold.length;
        againrelData++
    ) {
        if (calcDiv == 8) {
            break;
        }
        arraydata.push(arraydataHold[againrelData]);
        calcDiv++;
    }
    divReleased += 8;
    if (divReleased > arraydataHold.length) {
        $("#loadMorebutton").hide();
    }
    setTimeout(function () {
        createGridDivs();
    }, 1000);
}
