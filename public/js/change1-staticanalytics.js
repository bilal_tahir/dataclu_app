
am4core.ready(function () {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create chart instance
    var tablechart = am4core.create("tablechartdiv", am4charts.XYChart);
    tablechart.layout = "grid";
    tablechart.height = 70;
    tablechart.paddingRight = 20;
    tablechart.logo.disabled = true;



    // Add data
    tablechart.data = [{
        "year": "1950",
        "value": 1220
    }, {
        "year": "1951",
        "value": 100
    }, {
        "year": "1952",
        "value": 600
    }, {
        "year": "1953",
        "value": 800
    }, {
        "year": "1954",
        "value": 1600
    }, {
        "year": "1955",
        "value": 1100
    }, {
        "year": "1956",
        "value": 50
    }, {
        "year": "1957",
        "value": 1500
    }, {
        "year": "1958",
        "value": 210
    }, {
        "year": "1959",
        "value": 900
    }, {
        "year": "1960",
        "value": 1900
    }, {
        "year": "1961",
        "value": 10
    }, {
        "year": "1962",
        "value": 700
    }];

    // Create axes
    var tablecategoryAxis = tablechart.xAxes.push(new am4charts.CategoryAxis());
    tablecategoryAxis.dataFields.category = "year";
    tablecategoryAxis.renderer.minGridDistance = 50;
    tablecategoryAxis.renderer.grid.template.location = 0.5;
    tablecategoryAxis.startLocation = 0.5;
    tablecategoryAxis.endLocation = 0.5;
    tablecategoryAxis.renderer.grid.template.disabled = true;
    tablecategoryAxis.renderer.labels.template.disabled = true;
    tablecategoryAxis.cursorTooltipEnabled = false;

    // Create value axis
    var tablevalueAxis = tablechart.yAxes.push(new am4charts.ValueAxis());
    tablevalueAxis.baseValue = 0;
    tablevalueAxis.renderer.grid.template.disabled = true;
    tablevalueAxis.renderer.baseGrid.disabled = true;
    tablevalueAxis.renderer.labels.template.disabled = true;
    tablevalueAxis.cursorTooltipEnabled = false;

    // Create series
    var tableseries = tablechart.series.push(new am4charts.LineSeries());
    tableseries.dataFields.valueY = "value";
    tableseries.dataFields.categoryX = "year";
    tableseries.strokeWidth = 3;
    tableseries.tensionX = 0.77;
    tableseries.stroke = am4core.color("#9795a1");


    // bullet is added because we add tooltip to a bullet for it to change color
    var tablebullet = tableseries.bullets.push(new am4charts.Bullet());
    tablebullet.tooltipText = "{valueY}";

    tablebullet.adapter.add("fill", function (fill, target) {
        if (target.dataItem.valueY < 0) {
            return am4core.color("#FF0000");
        }
        return fill;
    })


    tablechart.cursor = new am4charts.XYCursor();

}); // end am4core.ready()



// Second Graph

am4core.ready(function () {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create chart instance
    var tablesecondchart = am4core.create("tablechartdivsecond", am4charts.XYChart);
    tablesecondchart.layout = "grid";
    tablesecondchart.height = 70;
    tablesecondchart.paddingRight = 20;
    tablesecondchart.logo.disabled = true;



    // Add data
    tablesecondchart.data = [{
        "year": "1950",
        "value": 1220
    }, {
        "year": "1951",
        "value": 100
    }, {
        "year": "1952",
        "value": 600
    }, {
        "year": "1953",
        "value": 800
    }, {
        "year": "1954",
        "value": 1600
    }, {
        "year": "1955",
        "value": 1100
    }, {
        "year": "1956",
        "value": 50
    }, {
        "year": "1957",
        "value": 1500
    }, {
        "year": "1958",
        "value": 210
    }, {
        "year": "1959",
        "value": 900
    }, {
        "year": "1960",
        "value": 1900
    }, {
        "year": "1961",
        "value": 10
    }, {
        "year": "1962",
        "value": 700
    }];

    // Create axes
    var tablesecondcategoryAxis = tablesecondchart.xAxes.push(new am4charts.CategoryAxis());
    tablesecondcategoryAxis.dataFields.category = "year";
    tablesecondcategoryAxis.renderer.minGridDistance = 50;
    tablesecondcategoryAxis.renderer.grid.template.location = 0.5;
    tablesecondcategoryAxis.startLocation = 0.5;
    tablesecondcategoryAxis.endLocation = 0.5;
    tablesecondcategoryAxis.renderer.grid.template.disabled = true;
    tablesecondcategoryAxis.renderer.labels.template.disabled = true;
    tablesecondcategoryAxis.cursorTooltipEnabled = false;

    // Create value axis
    var tablesecondvalueAxis = tablesecondchart.yAxes.push(new am4charts.ValueAxis());
    tablesecondvalueAxis.baseValue = 0;
    tablesecondvalueAxis.renderer.grid.template.disabled = true;
    tablesecondvalueAxis.renderer.baseGrid.disabled = true;
    tablesecondvalueAxis.renderer.labels.template.disabled = true;
    tablesecondvalueAxis.cursorTooltipEnabled = false;

    // Create series
    var tablesecondseries = tablesecondchart.series.push(new am4charts.LineSeries());
    tablesecondseries.dataFields.valueY = "value";
    tablesecondseries.dataFields.categoryX = "year";
    tablesecondseries.strokeWidth = 3;
    tablesecondseries.tensionX = 0.77;
    tablesecondseries.stroke = am4core.color("#9795a1");


    // bullet is added because we add tooltip to a bullet for it to change color
    var tablesecondullet = tablesecondseries.bullets.push(new am4charts.Bullet());
    tablesecondbullet.tooltipText = "{valueY}";

    tablesecondbullet.adapter.add("fill", function (fill, target) {
        if (target.dataItem.valueY < 0) {
            return am4core.color("#FF0000");
        }
        return fill;
    })


    tablesecondchart.cursor = new am4charts.XYCursor();

}); // end am4core.ready()


// Third Graph

am4core.ready(function () {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create chart instance
    var tablethirdchart = am4core.create("tablechartdivthird", am4charts.XYChart);
    tablethirdchart.layout = "grid";
    tablethirdchart.height = 70;
    tablethirdchart.paddingRight = 20;
    tablethirdchart.logo.disabled = true;



    // Add data
    tablethirdchart.data = [{
        "year": "1950",
        "value": 1220
    }, {
        "year": "1951",
        "value": 100
    }, {
        "year": "1952",
        "value": 600
    }, {
        "year": "1953",
        "value": 800
    }, {
        "year": "1954",
        "value": 1600
    }, {
        "year": "1955",
        "value": 1100
    }, {
        "year": "1956",
        "value": 50
    }, {
        "year": "1957",
        "value": 1500
    }, {
        "year": "1958",
        "value": 210
    }, {
        "year": "1959",
        "value": 900
    }, {
        "year": "1960",
        "value": 1900
    }, {
        "year": "1961",
        "value": 10
    }, {
        "year": "1962",
        "value": 700
    }];

    // Create axes
    var tablethirdcategoryAxis = tablethirdchart.xAxes.push(new am4charts.CategoryAxis());
    tablethirdcategoryAxis.dataFields.category = "year";
    tablethirdcategoryAxis.renderer.minGridDistance = 50;
    tablethirdcategoryAxis.renderer.grid.template.location = 0.5;
    tablethirdcategoryAxis.startLocation = 0.5;
    tablethirdcategoryAxis.endLocation = 0.5;
    tablethirdcategoryAxis.renderer.grid.template.disabled = true;
    tablethirdcategoryAxis.renderer.labels.template.disabled = true;
    tablethirdcategoryAxis.cursorTooltipEnabled = false;

    // Create value axis
    var tablethirdvalueAxis = tablethirdchart.yAxes.push(new am4charts.ValueAxis());
    tablethirdvalueAxis.baseValue = 0;
    tablethirdvalueAxis.renderer.grid.template.disabled = true;
    tablethirdvalueAxis.renderer.baseGrid.disabled = true;
    tablethirdvalueAxis.renderer.labels.template.disabled = true;
    tablethirdvalueAxis.cursorTooltipEnabled = false;

    // Create series
    var tablethirdseries = tablethirdchart.series.push(new am4charts.LineSeries());
    tablethirdseries.dataFields.valueY = "value";
    tablethirdseries.dataFields.categoryX = "year";
    tablethirdseries.strokeWidth = 6;
    tablethirdseries.tensionX = 0.77;
    tablethirdseries.stroke = am4core.color("#2ECC71");


    // bullet is added because we add tooltip to a bullet for it to change color
    var tablethirdullet = tablethirdseries.bullets.push(new am4charts.Bullet());
    tablethirdbullet.tooltipText = "{valueY}";

    tablethirdbullet.adapter.add("fill", function (fill, target) {
        if (target.dataItem.valueY < 0) {
            return am4core.color("#FF0000");
        }
        return fill;
    })


    tablethirdchart.cursor = new am4charts.XYCursor();

}); // end am4core.ready()


// Fourth Graph

am4core.ready(function () {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create chart instance
    var tablefourthchart = am4core.create("tablechartdivfourth", am4charts.XYChart);
    tablefourthchart.layout = "grid";
    tablefourthchart.height = 70;
    tablefourthchart.paddingRight = 20;
    tablefourthchart.logo.disabled = true;



    // Add data
    tablefourthchart.data = [{
        "year": "1950",
        "value": 1220
    }, {
        "year": "1951",
        "value": 100
    }, {
        "year": "1952",
        "value": 600
    }, {
        "year": "1953",
        "value": 800
    }, {
        "year": "1954",
        "value": 1600
    }, {
        "year": "1955",
        "value": 1100
    }, {
        "year": "1956",
        "value": 50
    }, {
        "year": "1957",
        "value": 1500
    }, {
        "year": "1958",
        "value": 210
    }, {
        "year": "1959",
        "value": 900
    }, {
        "year": "1960",
        "value": 1900
    }, {
        "year": "1961",
        "value": 10
    }, {
        "year": "1962",
        "value": 700
    }];

    // Create axes
    var tablefourthcategoryAxis = tablefourthchart.xAxes.push(new am4charts.CategoryAxis());
    tablefourthcategoryAxis.dataFields.category = "year";
    tablefourthcategoryAxis.renderer.minGridDistance = 50;
    tablefourthcategoryAxis.renderer.grid.template.location = 0.5;
    tablefourthcategoryAxis.startLocation = 0.5;
    tablefourthcategoryAxis.endLocation = 0.5;
    tablefourthcategoryAxis.renderer.grid.template.disabled = true;
    tablefourthcategoryAxis.renderer.labels.template.disabled = true;
    tablefourthcategoryAxis.cursorTooltipEnabled = false;

    // Create value axis
    var tablefourthvalueAxis = tablefourthchart.yAxes.push(new am4charts.ValueAxis());
    tablefourthvalueAxis.baseValue = 0;
    tablefourthvalueAxis.renderer.grid.template.disabled = true;
    tablefourthvalueAxis.renderer.baseGrid.disabled = true;
    tablefourthvalueAxis.renderer.labels.template.disabled = true;
    tablefourthvalueAxis.cursorTooltipEnabled = false;

    // Create series
    var tablefourthseries = tablefourthchart.series.push(new am4charts.LineSeries());
    tablefourthseries.dataFields.valueY = "value";
    tablefourthseries.dataFields.categoryX = "year";
    tablefourthseries.strokeWidth = 6;
    tablefourthseries.tensionX = 0.77;
    tablefourthseries.stroke = am4core.color("#E74C3C");


    // bullet is added because we add tooltip to a bullet for it to change color
    var tablefourthullet = tablefourthseries.bullets.push(new am4charts.Bullet());
    tablefourthbullet.tooltipText = "{valueY}";

    tablefourthbullet.adapter.add("fill", function (fill, target) {
        if (target.dataItem.valueY < 0) {
            return am4core.color("#FF0000");
        }
        return fill;
    })


    tablefourthchart.cursor = new am4charts.XYCursor();

}); // end am4core.ready()


// fifth Graph

am4core.ready(function () {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create chart instance
    var tablefifthchart = am4core.create("tablechartdivfifth", am4charts.XYChart);
    tablefifthchart.layout = "grid";
    tablefifthchart.height = 70;
    tablefifthchart.paddingRight = 20;
    tablefifthchart.logo.disabled = true;



    // Add data
    tablefifthchart.data = [{
        "year": "1950",
        "value": 1220
    }, {
        "year": "1951",
        "value": 100
    }, {
        "year": "1952",
        "value": 600
    }, {
        "year": "1953",
        "value": 800
    }, {
        "year": "1954",
        "value": 1600
    }, {
        "year": "1955",
        "value": 1100
    }, {
        "year": "1956",
        "value": 50
    }, {
        "year": "1957",
        "value": 1500
    }, {
        "year": "1958",
        "value": 210
    }, {
        "year": "1959",
        "value": 900
    }, {
        "year": "1960",
        "value": 1900
    }, {
        "year": "1961",
        "value": 10
    }, {
        "year": "1962",
        "value": 700
    }];

    // Create axes
    var tablefifthcategoryAxis = tablefifthchart.xAxes.push(new am4charts.CategoryAxis());
    tablefifthcategoryAxis.dataFields.category = "year";
    tablefifthcategoryAxis.renderer.minGridDistance = 50;
    tablefifthcategoryAxis.renderer.grid.template.location = 0.5;
    tablefifthcategoryAxis.startLocation = 0.5;
    tablefifthcategoryAxis.endLocation = 0.5;
    tablefifthcategoryAxis.renderer.grid.template.disabled = true;
    tablefifthcategoryAxis.renderer.labels.template.disabled = true;
    tablefifthcategoryAxis.cursorTooltipEnabled = false;

    // Create value axis
    var tablefifthvalueAxis = tablefifthchart.yAxes.push(new am4charts.ValueAxis());
    tablefifthvalueAxis.baseValue = 0;
    tablefifthvalueAxis.renderer.grid.template.disabled = true;
    tablefifthvalueAxis.renderer.baseGrid.disabled = true;
    tablefifthvalueAxis.renderer.labels.template.disabled = true;
    tablefifthvalueAxis.cursorTooltipEnabled = false;

    // Create series
    var tablefifthseries = tablefifthchart.series.push(new am4charts.LineSeries());
    tablefifthseries.dataFields.valueY = "value";
    tablefifthseries.dataFields.categoryX = "year";
    tablefifthseries.strokeWidth = 3;
    tablefifthseries.tensionX = 0.77;
    tablefifthseries.stroke = am4core.color("#9795a1");


    // bullet is added because we add tooltip to a bullet for it to change color
    var tablefifthullet = tablefifthseries.bullets.push(new am4charts.Bullet());
    tablefifthbullet.tooltipText = "{valueY}";

    tablefifthbullet.adapter.add("fill", function (fill, target) {
        if (target.dataItem.valueY < 0) {
            return am4core.color("#FF0000");
        }
        return fill;
    })


    tablefifthchart.cursor = new am4charts.XYCursor();

}); // end am4core.ready()

// Sixth Graph

am4core.ready(function () {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create chart instance
    var tablesixthchart = am4core.create("tablechartdivsixth", am4charts.XYChart);
    tablesixthchart.layout = "grid";
    tablesixthchart.height = 70;
    tablesixthchart.paddingRight = 20;
    tablesixthchart.logo.disabled = true;



    // Add data
    tablesixthchart.data = [{
        "year": "1950",
        "value": 1220
    }, {
        "year": "1951",
        "value": 100
    }, {
        "year": "1952",
        "value": 600
    }, {
        "year": "1953",
        "value": 800
    }, {
        "year": "1954",
        "value": 1600
    }, {
        "year": "1955",
        "value": 1100
    }, {
        "year": "1956",
        "value": 50
    }, {
        "year": "1957",
        "value": 1500
    }, {
        "year": "1958",
        "value": 210
    }, {
        "year": "1959",
        "value": 900
    }, {
        "year": "1960",
        "value": 1900
    }, {
        "year": "1961",
        "value": 10
    }, {
        "year": "1962",
        "value": 700
    }];

    // Create axes
    var tablesixthcategoryAxis = tablesixthchart.xAxes.push(new am4charts.CategoryAxis());
    tablesixthcategoryAxis.dataFields.category = "year";
    tablesixthcategoryAxis.renderer.minGridDistance = 50;
    tablesixthcategoryAxis.renderer.grid.template.location = 0.5;
    tablesixthcategoryAxis.startLocation = 0.5;
    tablesixthcategoryAxis.endLocation = 0.5;
    tablesixthcategoryAxis.renderer.grid.template.disabled = true;
    tablesixthcategoryAxis.renderer.labels.template.disabled = true;
    tablesixthcategoryAxis.cursorTooltipEnabled = false;

    // Create value axis
    var tablesixthvalueAxis = tablesixthchart.yAxes.push(new am4charts.ValueAxis());
    tablesixthvalueAxis.baseValue = 0;
    tablesixthvalueAxis.renderer.grid.template.disabled = true;
    tablesixthvalueAxis.renderer.baseGrid.disabled = true;
    tablesixthvalueAxis.renderer.labels.template.disabled = true;
    tablesixthvalueAxis.cursorTooltipEnabled = false;

    // Create series
    var tablesixthseries = tablesixthchart.series.push(new am4charts.LineSeries());
    tablesixthseries.dataFields.valueY = "value";
    tablesixthseries.dataFields.categoryX = "year";
    tablesixthseries.strokeWidth = 3;
    tablesixthseries.tensionX = 0.77;
    tablesixthseries.stroke = am4core.color("#9795a1");


    // bullet is added because we add tooltip to a bullet for it to change color
    var tablesixthullet = tablesixthseries.bullets.push(new am4charts.Bullet());
    tablesixthbullet.tooltipText = "{valueY}";

    tablesixthbullet.adapter.add("fill", function (fill, target) {
        if (target.dataItem.valueY < 0) {
            return am4core.color("#FF0000");
        }
        return fill;
    })


    tablesixthchart.cursor = new am4charts.XYCursor();

}); // end am4core.ready()


// Seventh Graph

am4core.ready(function () {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create chart instance
    var tableseventhchart = am4core.create("tablechartdivseventh", am4charts.XYChart);
    tableseventhchart.layout = "grid";
    tableseventhchart.height = 70;
    tableseventhchart.paddingRight = 20;
    tableseventhchart.logo.disabled = true;



    // Add data
    tableseventhchart.data = [{
        "year": "1950",
        "value": 1220
    }, {
        "year": "1951",
        "value": 100
    }, {
        "year": "1952",
        "value": 600
    }, {
        "year": "1953",
        "value": 800
    }, {
        "year": "1954",
        "value": 1600
    }, {
        "year": "1955",
        "value": 1100
    }, {
        "year": "1956",
        "value": 50
    }, {
        "year": "1957",
        "value": 1500
    }, {
        "year": "1958",
        "value": 210
    }, {
        "year": "1959",
        "value": 900
    }, {
        "year": "1960",
        "value": 1900
    }, {
        "year": "1961",
        "value": 10
    }, {
        "year": "1962",
        "value": 700
    }];

    // Create axes
    var tableseventhcategoryAxis = tableseventhchart.xAxes.push(new am4charts.CategoryAxis());
    tableseventhcategoryAxis.dataFields.category = "year";
    tableseventhcategoryAxis.renderer.minGridDistance = 50;
    tableseventhcategoryAxis.renderer.grid.template.location = 0.5;
    tableseventhcategoryAxis.startLocation = 0.5;
    tableseventhcategoryAxis.endLocation = 0.5;
    tableseventhcategoryAxis.renderer.grid.template.disabled = true;
    tableseventhcategoryAxis.renderer.labels.template.disabled = true;
    tableseventhcategoryAxis.cursorTooltipEnabled = false;

    // Create value axis
    var tableseventhvalueAxis = tableseventhchart.yAxes.push(new am4charts.ValueAxis());
    tableseventhvalueAxis.baseValue = 0;
    tableseventhvalueAxis.renderer.grid.template.disabled = true;
    tableseventhvalueAxis.renderer.baseGrid.disabled = true;
    tableseventhvalueAxis.renderer.labels.template.disabled = true;
    tableseventhvalueAxis.cursorTooltipEnabled = false;

    // Create series
    var tableseventhseries = tableseventhchart.series.push(new am4charts.LineSeries());
    tableseventhseries.dataFields.valueY = "value";
    tableseventhseries.dataFields.categoryX = "year";
    tableseventhseries.strokeWidth = 6;
    tableseventhseries.tensionX = 0.77;
    tableseventhseries.stroke = am4core.color("#2ECC71");


    // bullet is added because we add tooltip to a bullet for it to change color
    var tableseventhullet = tableseventhseries.bullets.push(new am4charts.Bullet());
    tableseventhbullet.tooltipText = "{valueY}";

    tableseventhbullet.adapter.add("fill", function (fill, target) {
        if (target.dataItem.valueY < 0) {
            return am4core.color("#FF0000");
        }
        return fill;
    })


    tableseventhchart.cursor = new am4charts.XYCursor();

}); // end am4core.ready()

// eight Graph

am4core.ready(function () {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create chart instance
    var tableeightchart = am4core.create("tablechartdiveight", am4charts.XYChart);
    tableeightchart.layout = "grid";
    tableeightchart.htableeight = 70;
    tableeightchart.paddingRight = 20;
    tableeightchart.logo.disabled = true;



    // Add data
    tableeightchart.data = [{
        "year": "1950",
        "value": 1220
    }, {
        "year": "1951",
        "value": 100
    }, {
        "year": "1952",
        "value": 600
    }, {
        "year": "1953",
        "value": 800
    }, {
        "year": "1954",
        "value": 1600
    }, {
        "year": "1955",
        "value": 1100
    }, {
        "year": "1956",
        "value": 50
    }, {
        "year": "1957",
        "value": 1500
    }, {
        "year": "1958",
        "value": 210
    }, {
        "year": "1959",
        "value": 900
    }, {
        "year": "1960",
        "value": 1900
    }, {
        "year": "1961",
        "value": 10
    }, {
        "year": "1962",
        "value": 700
    }];

    // Create axes
    var tableeightcategoryAxis = tableeightchart.xAxes.push(new am4charts.CategoryAxis());
    tableeightcategoryAxis.dataFields.category = "year";
    tableeightcategoryAxis.renderer.minGridDistance = 50;
    tableeightcategoryAxis.renderer.grid.template.location = 0.5;
    tableeightcategoryAxis.startLocation = 0.5;
    tableeightcategoryAxis.endLocation = 0.5;
    tableeightcategoryAxis.renderer.grid.template.disabled = true;
    tableeightcategoryAxis.renderer.labels.template.disabled = true;
    tableeightcategoryAxis.cursorTooltipEnabled = false;

    // Create value axis
    var tableeightvalueAxis = tableeightchart.yAxes.push(new am4charts.ValueAxis());
    tableeightvalueAxis.baseValue = 0;
    tableeightvalueAxis.renderer.grid.template.disabled = true;
    tableeightvalueAxis.renderer.baseGrid.disabled = true;
    tableeightvalueAxis.renderer.labels.template.disabled = true;
    tableeightvalueAxis.cursorTooltipEnabled = false;

    // Create series
    var tableeightseries = tableeightchart.series.push(new am4charts.LineSeries());
    tableeightseries.dataFields.valueY = "value";
    tableeightseries.dataFields.categoryX = "year";
    tableeightseries.strokeWidth = 6;
    tableeightseries.tensionX = 0.77;
    tableeightseries.stroke = am4core.color("#E74C3C");


    // bullet is added because we add tooltip to a bullet for it to change color
    var tableeightullet = tableeightseries.bullets.push(new am4charts.Bullet());
    tableeightbullet.tooltipText = "{valueY}";

    tableeightbullet.adapter.add("fill", function (fill, target) {
        if (target.dataItem.valueY < 0) {
            return am4core.color("#FF0000");
        }
        return fill;
    })


    tableeightchart.cursor = new am4charts.XYCursor();

}); // end am4core.ready()