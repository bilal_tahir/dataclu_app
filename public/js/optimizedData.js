
var optData;
var metaDataData = "";
var keyDensity = "";


jQuery.ajax({
    url: "https://ai.darkdata.ca//organic_optimization/pages_optimization?user_id=" + userId + "&siteUrl=" + domain,
    method: 'get',
    async: false,
    success: function (result) {
        optData = result;
        createData();


    }
});
function createData() {
    // var metaContainer = document.getElementById('meData');
    // var keyContainer = document.getElementById('keyData');
    // metaContainer = "";
    // keyContainer = "";
    // stringAnomTruncate(concatinatingAnomBack, 25)
    for (var md = 0; md < optData['data']['metadata'].length; md++) {
        metaDataData += '<tr>' +
            '<td>' + stringAnomTruncate(optData['data']['metadata'][md]['page'], 30) + '</td>' +
            '<td>' + optData['data']['metadata'][md]['_id'] + '</td>' +
            '<td>' + optData['data']['metadata'][md]['impressions'] + '</td>' +
            '<td>' + optData['data']['metadata'][md]['ctr'] + '</td>' +
            '<td>' + optData['data']['metadata'][md]['impact'] + '</td>' +
            '</tr>';
    }
    for (var md = 0; md < optData['data']['keyword_density'].length; md++) {
        keyDensity += '<tr>' +
            '<td>' + stringAnomTruncate(optData['data']['keyword_density'][md]['page'], 30) + '</td>' +
            '<td>' + optData['data']['keyword_density'][md]['_id'] + '</td>' +
            '<td>' + optData['data']['keyword_density'][md]['impressions'] + '</td>' +
            '<td>' + optData['data']['keyword_density'][md]['ctr'] + '</td>' +
            '<td>' + optData['data']['keyword_density'][md]['impact'] + '</td>' +
            '<td>' + optData['data']['keyword_density'][md]['position'] + '</td>' +
            '</tr>';
    }
    document.getElementById('meData').innerHTML = metaDataData;
    document.getElementById('keyData').innerHTML = keyDensity;
    // console.log(metaDataData);
}
//function to truncate a string
function stringAnomTruncate(str, length) {
    var dots = str.length > length ? '...' : '';
    return str.substring(0, length) + dots;
};

function Metaview() {
    var x = document.getElementById('MetaContainer');
    var y = document.getElementById('keyContainer');

    x.style.display = "block";
    y.style.display = "none";

    // document.getElementById("table-color").style.color = "#4783ff";
    // document.getElementById("table-color").style.backgroundColor = "#fff";
    // document.getElementById("grid-color").style.color = "#fff";
    // document.getElementById("grid-color").style.backgroundColor = "#4783ff";

}

function keyview() {

    var x = document.getElementById('MetaContainer');
    var y = document.getElementById('keyContainer');



    x.style.display = "none";
    y.style.display = "block";

    // document.getElementById("grid-color").style.color = "#4783ff";
    // document.getElementById("grid-color").style.backgroundColor = "#fff";
    // document.getElementById("table-color").style.color = "#fff";
    // document.getElementById("table-color").style.backgroundColor = "#4783ff";

}