

am4core.ready(function () {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create chart instance
    var chart = am4core.create("chartdiv", am4charts.XYChart);
    chart.layout = "grid";
    chart.height = 70;
    chart.paddingRight = 20;
    chart.logo.disabled = true;

    // Add data
    chart.data = sesData;
    // Create axes
    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "date";
    categoryAxis.renderer.minGridDistance = 50;
    categoryAxis.renderer.grid.template.location = 0.5;
    categoryAxis.startLocation = 0.5;
    categoryAxis.endLocation = 0.5;
    categoryAxis.renderer.grid.template.disabled = true;
    categoryAxis.renderer.labels.template.disabled = true;
    categoryAxis.cursorTooltipEnabled = false;

    // Create value axis
    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.baseValue = 0;
    valueAxis.renderer.grid.template.disabled = true;
    valueAxis.renderer.baseGrid.disabled = true;
    valueAxis.renderer.labels.template.disabled = true;
    valueAxis.cursorTooltipEnabled = false;

    // Create series
    var series = chart.series.push(new am4charts.LineSeries());
    series.dataFields.valueY = "session";
    series.dataFields.categoryX = "date";
    series.strokeWidth = 2;
    series.tensionX = 0.77;


    // bullet is added because we add tooltip to a bullet for it to change color
    var bullet = series.bullets.push(new am4charts.Bullet());
    bullet.tooltipText = "{[date]}";

    bullet.adapter.add("fill", function (fill, target) {
        if (target.dataItem.valueY < 0) {
            return am4core.color("#FF0000");
        }
        return fill;
    })


    chart.cursor = new am4charts.XYCursor();

}); // end am4core.ready()

console.log(counData);

am4core.ready(function () {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create chart instance
    var counchart = am4core.create("countchartdiv", am4charts.XYChart);
    counchart.layout = "grid";
    counchart.height = 70;
    counchart.paddingRight = 20;
    counchart.logo.disabled = true;

    // Add data
    counchart.data = counData;
    // // Create axes
    var counAxis = counchart.xAxes.push(new am4charts.CategoryAxis());
    counAxis.dataFields.category = "country";
    counAxis.renderer.minGridDistance = 50;
    counAxis.renderer.grid.template.location = 0.5;
    counAxis.startLocation = 0.5;
    counAxis.endLocation = 0.5;
    counAxis.renderer.grid.template.disabled = true;
    counAxis.renderer.labels.template.disabled = true;
    counAxis.cursorTooltipEnabled = false;

    // // Create value axis
    var counvalueAxis = counchart.yAxes.push(new am4charts.ValueAxis());
    counvalueAxis.baseValue = 0;
    counvalueAxis.renderer.grid.template.disabled = true;
    counvalueAxis.renderer.baseGrid.disabled = true;
    counvalueAxis.renderer.labels.template.disabled = true;
    counvalueAxis.cursorTooltipEnabled = false;

    // // Create series
    var counseries = counchart.series.push(new am4charts.LineSeries());
    counseries.dataFields.valueY = "view";
    counseries.dataFields.categoryX = "country";
    counseries.strokeWidth = 2;
    counseries.tensionX = 0.77;


    // // bullet is added because we add tooltip to a bullet for it to change color
    var counbullet = counseries.bullets.push(new am4charts.Bullet());
    counbullet.tooltipText = "{date}";

    counbullet.adapter.add("fill", function (fill, target) {
        if (target.dataItem.valueY < 0) {
            return am4core.color("#FF0000");
        }
        return fill;
    })


    counchart.cursor = new am4charts.XYCursor();

}); // end am4core.ready()

// console.log(counData);

am4core.ready(function () {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create chart instance
    var pagechart = am4core.create("pagechartdiv", am4charts.XYChart);
    pagechart.layout = "grid";
    pagechart.height = 70;
    pagechart.paddingRight = 20;
    pagechart.logo.disabled = true;

    // Add data
    pagechart.data = pageData;
    // // Create axes
    var pageAxis = pagechart.xAxes.push(new am4charts.CategoryAxis());
    pageAxis.dataFields.category = "date";
    pageAxis.renderer.minGridDistance = 50;
    pageAxis.renderer.grid.template.location = 0.5;
    pageAxis.startLocation = 0.5;
    pageAxis.endLocation = 0.5;
    pageAxis.renderer.grid.template.disabled = true;
    pageAxis.renderer.labels.template.disabled = true;
    pageAxis.cursorTooltipEnabled = false;

    // // Create value axis
    var pagevAxis = pagechart.yAxes.push(new am4charts.ValueAxis());
    pagevAxis.baseValue = 0;
    pagevAxis.renderer.grid.template.disabled = true;
    pagevAxis.renderer.baseGrid.disabled = true;
    pagevAxis.renderer.labels.template.disabled = true;
    pagevAxis.cursorTooltipEnabled = false;

    // // Create series
    var pageseries = pagechart.series.push(new am4charts.LineSeries());
    pageseries.dataFields.valueY = "user";
    pageseries.dataFields.categoryX = "date";
    pageseries.strokeWidth = 2;
    pageseries.tensionX = 0.77;


    // // bullet is added because we add tooltip to a bullet for it to change color
    var pagebullet = pageseries.bullets.push(new am4charts.Bullet());
    pagebullet.tooltipText = "{date}";

    pagebullet.adapter.add("fill", function (fill, target) {
        if (target.dataItem.valueY < 0) {
            return am4core.color("#FF0000");
        }
        return fill;
    })


    pagechart.cursor = new am4charts.XYCursor();

}); // end am4core.ready()

// console.log(counData);

am4core.ready(function () {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create chart instance
    var bouncechart = am4core.create("bouncechartdiv", am4charts.XYChart);
    bouncechart.layout = "grid";
    bouncechart.height = 70;
    bouncechart.paddingRight = 20;
    bouncechart.logo.disabled = true;

    // Add data
    bouncechart.data = bouncData;
    // // Create axes
    var bounceAxis = bouncechart.xAxes.push(new am4charts.CategoryAxis());
    bounceAxis.dataFields.category = "date";
    bounceAxis.renderer.minGridDistance = 50;
    bounceAxis.renderer.grid.template.location = 0.5;
    bounceAxis.startLocation = 0.5;
    bounceAxis.endLocation = 0.5;
    bounceAxis.renderer.grid.template.disabled = true;
    bounceAxis.renderer.labels.template.disabled = true;
    bounceAxis.cursorTooltipEnabled = false;

    // // Create value axis
    var bouncevAxis = bouncechart.yAxes.push(new am4charts.ValueAxis());
    bouncevAxis.baseValue = 0;
    bouncevAxis.renderer.grid.template.disabled = true;
    bouncevAxis.renderer.baseGrid.disabled = true;
    bouncevAxis.renderer.labels.template.disabled = true;
    bouncevAxis.cursorTooltipEnabled = false;

    // // Create series
    var bounceseries = bouncechart.series.push(new am4charts.LineSeries());
    bounceseries.dataFields.valueY = "rate";
    bounceseries.dataFields.categoryX = "date";
    bounceseries.strokeWidth = 2;
    bounceseries.tensionX = 0.77;


    // // bullet is added because we add tooltip to a bullet for it to change color
    var bouncebullet = bounceseries.bullets.push(new am4charts.Bullet());
    bouncebullet.tooltipText = "{date}";

    bouncebullet.adapter.add("fill", function (fill, target) {
        if (target.dataItem.valueY < 0) {
            return am4core.color("#FF0000");
        }
        return fill;
    })


    bouncechart.cursor = new am4charts.XYCursor();

}); // end am4core.ready()




var ctx = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: clabel,
        datasets: [{
            label: 'Users Per Country',
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgb(255, 99, 132)',
            data: cvalue
        }]
    },

    // Configuration options go here
    options: {}
});
