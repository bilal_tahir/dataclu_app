var data;
var i;
var trendarr = [];
var fields = ['date', 'trends'];
var trendanalysis;
var sessid = '30TrendDays';

window.onload = SessionData();
$("select[name='trendsdropdown']").change(function () {
    var trendsdropdown = $(this).val();
    sessid = trendsdropdown;
    SessionData();
});

function SessionData() {
    am4core.ready(function () {

        if (trendarr != null) {
            trendarr = [];
        }
        if (sessid == '30TrendDays') {
            jQuery.ajax({
                url: "https://ai.darkdata.ca/metrics/trends?major_dimension=browser&browser=Chrome&metric=pageViewsPerSession&start_date=2020-09-01&end_date=2020-09-5&window=5&user_id=3&View_Id=147110849",
                method: 'get',
                success: function (result) {
                    data = result;
                    trendcheck = 1;
                    display();
                }
            });
        }
        else {
            jQuery.ajax({
                url: "https://ai.darkdata.ca/metrics/trends?major_dimension=browser&browser=Chrome&metric=pageViewsPerSession&start_date=2020-09-01&end_date=2020-09-25&window=5&user_id=3&View_Id=147110849",
                method: 'get',
                success: function (result) {
                    data = result;
                    trendcheck = 1;
                    display();
                }
            });
        }

    });
}
function c() {
    am4core.ready(function () {

        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var pagesviewpersessionchart = am4core.create("chartsdivs", am4charts.XYChart);
        pagesviewpersessionchart.layout = "grid";
        pagesviewpersessionchart.height = 70;
        pagesviewpersessionchart.paddingRight = 20;
        pagesviewpersessionchart.logo.disabled = true;

        // Add data
        // console.log(trendarr);
        pagesviewpersessionchart.data = trendarr;

        // Create axes
        var pagesviewpersessioncategoryAxis = pagesviewpersessionchart.xAxes.push(new am4charts.CategoryAxis());
        pagesviewpersessioncategoryAxis.dataFields.category = "date";
        pagesviewpersessioncategoryAxis.renderer.minGridDistance = 50;
        pagesviewpersessioncategoryAxis.renderer.grid.template.location = 0.5;
        pagesviewpersessioncategoryAxis.startLocation = 0.5;
        pagesviewpersessioncategoryAxis.endLocation = 0.5;
        pagesviewpersessioncategoryAxis.renderer.grid.template.disabled = true;
        pagesviewpersessioncategoryAxis.renderer.labels.template.disabled = true;
        pagesviewpersessioncategoryAxis.cursorTooltipEnabled = false;

        // Create value axis
        var pagesviewpersessionvalueAxis = pagesviewpersessionchart.yAxes.push(new am4charts.ValueAxis());
        pagesviewpersessionvalueAxis.baseValue = 0;
        pagesviewpersessionvalueAxis.renderer.grid.template.disabled = true;
        pagesviewpersessionvalueAxis.renderer.baseGrid.disabled = true;
        pagesviewpersessionvalueAxis.renderer.labels.template.disabled = true;
        pagesviewpersessionvalueAxis.cursorTooltipEnabled = false;

        // Create series
        var pagesviewpersessionseries = pagesviewpersessionchart.series.push(new am4charts.LineSeries());
        pagesviewpersessionseries.dataFields.valueY = "trends";
        pagesviewpersessionseries.dataFields.categoryX = "date";
        pagesviewpersessionseries.strokeWidth = 2;
        pagesviewpersessionseries.tensionX = 0.77;


        // bullet is added because we add tooltip to a bullet for it to change color
        var pagesviewpersessionbullet = pagesviewpersessionseries.bullets.push(new am4charts.Bullet());
        pagesviewpersessionbullet.tooltipText = "{valueY}";

        pagesviewpersessionbullet.adapter.add("fill", function (fill, target) {
            if (target.dataItem.valueY < 0) {
                return am4core.color("#FF0000");
            }
            return fill;
        })


        pagesviewpersessionchart.cursor = new am4charts.XYCursor();

    }); // end am4core.ready()

}
function display() {
    for (var i = 0; i < data[1]['date'].length; i++) {
        var object = {};

        object[fields[0]] = data[1]['date'][i];
        object[fields[1]] = data[1]['trends'][i];


        trendarr.push(object);
    }
    trendanalysis = JSON.stringify(trendarr);
    c();
}




am4core.ready(function () {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create chart instance
    var anomalieschart = am4core.create("chartsdivsanomalies", am4charts.XYChart);
    anomalieschart.layout = "grid";
    anomalieschart.height = 70;
    anomalieschart.paddingRight = 20;
    anomalieschart.logo.disabled = true;

    // Add data
    anomalieschart.data = [{
        "date": "2020-09-01",
        "anomalies": 1.0
    }, {
        "date": "2020-09-02",
        "anomalies": 1.0
    }, {
        "date": "2020-09-03",
        "anomalies": -1.0
    }, {
        "date": "2020-09-04",
        "anomalies": 1.0
    }, {
        "date": "2020-09-05",
        "anomalies": 1.0
    }, {
        "date": "2020-09-06",
        "anomalies": 1.0
    }, {
        "date": "2020-09-07",
        "anomalies": 1.0
    }, {
        "date": "2020-09-08",
        "anomalies": 1.0
    }, {
        "date": "2020-09-09",
        "anomalies": 1.0
    }, {
        "date": "2020-09-10",
        "anomalies": 1.0
    }, {
        "date": "2020-09-11",
        "anomalies": 1.0
    }, {
        "date": "2020-09-12",
        "anomalies": 1.0
    }, {
        "date": "2020-09-13",
        "anomalies": 1.0
    }, {
        "date": "2020-09-14",
        "anomalies": 1.0
    }, {
        "date": "2020-09-15",
        "anomalies": 1.0
    }, {
        "date": "2020-09-16",
        "anomalies": 1.0
    }, {
        "date": "2020-09-17",
        "anomalies": 1.0
    }, {
        "date": "2020-09-18",
        "anomalies": 1.0
    }, {
        "date": "2020-09-19",
        "anomalies": 1.0
    }, {
        "date": "2020-09-20",
        "anomalies": 1.0
    }, {
        "date": "2020-09-21",
        "anomalies": 1.0
    }, {
        "date": "2020-09-22",
        "anomalies": 1.0
    }, {
        "date": "2020-09-23",
        "anomalies": -1.0
    }, {
        "date": "2020-09-24",
        "anomalies": 1.0
    }, {
        "date": "2020-09-25",
        "anomalies": 1.0
    }];

    // Create axes
    var anomaliescategoryAxis = anomalieschart.xAxes.push(new am4charts.CategoryAxis());
    anomaliescategoryAxis.dataFields.category = "date";
    anomaliescategoryAxis.renderer.minGridDistance = 50;
    anomaliescategoryAxis.renderer.grid.template.location = 0.5;
    anomaliescategoryAxis.startLocation = 0.5;
    anomaliescategoryAxis.endLocation = 0.5;
    anomaliescategoryAxis.renderer.grid.template.disabled = true;
    anomaliescategoryAxis.renderer.labels.template.disabled = true;
    anomaliescategoryAxis.cursorTooltipEnabled = false;

    // Create value axis
    var anomaliesvalueAxis = anomalieschart.yAxes.push(new am4charts.ValueAxis());
    anomaliesvalueAxis.baseValue = 0;
    anomaliesvalueAxis.renderer.grid.template.disabled = true;
    anomaliesvalueAxis.renderer.baseGrid.disabled = true;
    anomaliesvalueAxis.renderer.labels.template.disabled = true;
    anomaliesvalueAxis.cursorTooltipEnabled = false;

    // Create series
    var anomaliesseries = anomalieschart.series.push(new am4charts.LineSeries());
    anomaliesseries.dataFields.valueY = "anomalies";
    anomaliesseries.dataFields.categoryX = "date";
    anomaliesseries.strokeWidth = 2;
    anomaliesseries.tensionX = 0.77;


    // bullet is added because we add tooltip to a bullet for it to change color
    var anomaliesbullet = anomaliesseries.bullets.push(new am4charts.Bullet());
    anomaliesbullet.tooltipText = "{valueY}";

    anomaliesbullet.adapter.add("fill", function (fill, target) {
        if (target.dataItem.valueY < 0) {
            return am4core.color("#FF0000");
        }
        return fill;
    })


    anomalieschart.cursor = new am4charts.XYCursor();

}); // end am4core.ready()




var sessdata;
var sessi;
var sesstrendarr = [];
var sessfields = ['date', 'trends'];
var sesstrendanalysis;
var browsersessid = '30TrendDays';
window.onload = BrowserSessData();
$("select[name='browsersessdropdown']").change(function () {
    var browsersessdropdown = $(this).val();
    browsersessid = browsersessdropdown;
    BrowserSessData();
});

function BrowserSessData() {
    am4core.ready(function () {

        if (sesstrendarr != null) {
            sesstrendarr = [];
        }
        if (browsersessid == '30TrendDays') {
            jQuery.ajax({
                url: "https://ai.darkdata.ca/metrics/trends?major_dimension=browser&browser=Chrome&metric=sessions&start_date=2020-09-01&end_date=2020-09-25&window=5&user_id=3&View_Id=147110849",
                method: 'get',
                success: function (sessresult) {
                    sessdata = sessresult;
                    sessdisplay();
                }
            });
        }
        else {
            jQuery.ajax({
                url: "https://ai.darkdata.ca/metrics/trends?major_dimension=browser&browser=Chrome&metric=sessions&start_date=2020-08-01&end_date=2020-09-25&window=5&user_id=3&View_Id=147110849",
                method: 'get',
                success: function (sessresult) {
                    sessdata = sessresult;
                    sessdisplay();
                }
            });
        }

    });
}
function sessdisplay() {
    for (var i = 0; i < sessdata[1]['date'].length; i++) {
        var sessobject = {};

        sessobject[sessfields[0]] = sessdata[1]['date'][i];
        sessobject[sessfields[1]] = sessdata[1]['trends'][i];


        sesstrendarr.push(sessobject);
    }
    sesstrendanalysis = JSON.stringify(sesstrendarr);
    sessc();
}

function sessc() {
    am4core.ready(function () {

        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var sessionbybrowserchart = am4core.create("chartsdivsessionbybrowser", am4charts.XYChart);
        sessionbybrowserchart.layout = "grid";
        sessionbybrowserchart.height = 70;
        sessionbybrowserchart.paddingRight = 20;
        sessionbybrowserchart.logo.disabled = true;

        // Add data
        // console.log(sesstrendarr);
        sessionbybrowserchart.data = sesstrendarr;

        // Create axes
        var sessionbybrowsercategoryAxis = sessionbybrowserchart.xAxes.push(new am4charts.CategoryAxis());
        sessionbybrowsercategoryAxis.dataFields.category = "date";
        sessionbybrowsercategoryAxis.renderer.minGridDistance = 50;
        sessionbybrowsercategoryAxis.renderer.grid.template.location = 0.5;
        sessionbybrowsercategoryAxis.startLocation = 0.5;
        sessionbybrowsercategoryAxis.endLocation = 0.5;
        sessionbybrowsercategoryAxis.renderer.grid.template.disabled = true;
        sessionbybrowsercategoryAxis.renderer.labels.template.disabled = true;
        sessionbybrowsercategoryAxis.cursorTooltipEnabled = false;

        // Create value axis
        var sessionbybrowservalueAxis = sessionbybrowserchart.yAxes.push(new am4charts.ValueAxis());
        sessionbybrowservalueAxis.baseValue = 0;
        sessionbybrowservalueAxis.renderer.grid.template.disabled = true;
        sessionbybrowservalueAxis.renderer.baseGrid.disabled = true;
        sessionbybrowservalueAxis.renderer.labels.template.disabled = true;
        sessionbybrowservalueAxis.cursorTooltipEnabled = false;

        // Create series
        var sessionbybrowserseries = sessionbybrowserchart.series.push(new am4charts.LineSeries());
        sessionbybrowserseries.dataFields.valueY = "trends";
        sessionbybrowserseries.dataFields.categoryX = "date";
        sessionbybrowserseries.strokeWidth = 2;
        sessionbybrowserseries.tensionX = 0.77;


        // bullet is added because we add tooltip to a bullet for it to change color
        var sessionbybrowserbullet = sessionbybrowserseries.bullets.push(new am4charts.Bullet());
        sessionbybrowserbullet.tooltipText = "{valueY}";

        sessionbybrowserbullet.adapter.add("fill", function (fill, target) {
            if (target.dataItem.valueY < 0) {
                return am4core.color("#FF0000");
            }
            return fill;
        })


        sessionbybrowserchart.cursor = new am4charts.XYCursor();

    }); // end am4core.ready()

}




var bouncedata;
var bouncei;
var bouncetrendarr = [];
var bouncefields = ['date', 'trends'];
var bouncetrendanalysis;
var Bounceid = '30TrendDays';
window.onload = BounceData();
$("select[name='Bouncedropdown']").change(function () {
    var Bouncedropdown = $(this).val();
    Bounceid = Bouncedropdown;
    BounceData();
});


function BounceData() {

    am4core.ready(function () {
        if (bouncetrendarr != null) {
            bouncetrendarr = [];
        }
        if (Bounceid == '30TrendDays') {
            jQuery.ajax({
                url: "https://ai.darkdata.ca/metrics/trends?major_dimension=deviceCategory&deviceCategory=desktop&metric=bounceRate&start_date=2020-09-01&end_date=2020-09-25&window=5&user_id=3&View_Id=147110849",
                method: 'get',
                success: function (bounceresult) {
                    bouncedata = bounceresult;
                    bouncedisplay();
                }
            });
        }
        else {
            jQuery.ajax({
                url: "https://ai.darkdata.ca/metrics/trends?major_dimension=deviceCategory&deviceCategory=desktop&metric=bounceRate&start_date=2020-08-01&end_date=2020-09-25&window=5&user_id=3&View_Id=147110849",
                method: 'get',
                success: function (bounceresult) {
                    bouncedata = bounceresult;
                    bouncedisplay();
                }
            });
        }
    });

}
function bouncedisplay() {
    for (var i = 0; i < bouncedata[1]['date'].length; i++) {
        var bounceobject = {};

        bounceobject[bouncefields[0]] = bouncedata[1]['date'][i];
        bounceobject[bouncefields[1]] = bouncedata[1]['trends'][i];


        bouncetrendarr.push(bounceobject);
    }
    bouncetrendanalysis = JSON.stringify(bouncetrendarr);
    bouncec();
}

function bouncec() {

    am4core.ready(function () {

        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var bouncechart = am4core.create("chartsdivbounce", am4charts.XYChart);
        bouncechart.layout = "grid";
        bouncechart.height = 70;
        bouncechart.paddingRight = 20;
        bouncechart.logo.disabled = true;

        // Add data
        console.log(bouncetrendarr);
        bouncechart.data = bouncetrendarr;

        // Create axes
        var bouncecategoryAxis = bouncechart.xAxes.push(new am4charts.CategoryAxis());
        bouncecategoryAxis.dataFields.category = "date";
        bouncecategoryAxis.renderer.minGridDistance = 50;
        bouncecategoryAxis.renderer.grid.template.location = 0.5;
        bouncecategoryAxis.startLocation = 0.5;
        bouncecategoryAxis.endLocation = 0.5;
        bouncecategoryAxis.renderer.grid.template.disabled = true;
        bouncecategoryAxis.renderer.labels.template.disabled = true;
        bouncecategoryAxis.cursorTooltipEnabled = false;

        // Create value axis
        var bouncevalueAxis = bouncechart.yAxes.push(new am4charts.ValueAxis());
        bouncevalueAxis.baseValue = 0;
        bouncevalueAxis.renderer.grid.template.disabled = true;
        bouncevalueAxis.renderer.baseGrid.disabled = true;
        bouncevalueAxis.renderer.labels.template.disabled = true;
        bouncevalueAxis.cursorTooltipEnabled = false;

        // Create series
        var bounceseries = bouncechart.series.push(new am4charts.LineSeries());
        bounceseries.dataFields.valueY = "trends";
        bounceseries.dataFields.categoryX = "date";
        bounceseries.strokeWidth = 2;
        bounceseries.tensionX = 0.77;


        // bullet is added because we add tooltip to a bullet for it to change color
        var bouncebullet = bounceseries.bullets.push(new am4charts.Bullet());
        bouncebullet.tooltipText = "{valueY}";

        bouncebullet.adapter.add("fill", function (fill, target) {
            if (target.dataItem.valueY < 0) {
                return am4core.color("#FF0000");
            }
            return fill;
        })


        bouncechart.cursor = new am4charts.XYCursor();

    }); // end am4core.ready()

}




var countrydata;
var countryi;
var countrytrendarr = [];
var countryfields = ['date', 'trends'];
var countrytrendanalysis;


var Clickid = '30TrendDays';
window.onload = ClickData();
$("select[name='Clickdropdown']").change(function () {
    var Clickdropdown = $(this).val();
    Clickid = Clickdropdown;
    ClickData();
});


function ClickData() {

    am4core.ready(function () {
        if (countrytrendarr != null) {
            countrytrendarr = [];
        }
        if (Clickid == '30TrendDays') {
            jQuery.ajax({
                url: "https://ai.darkdata.ca/metrics/trends?major_dimension=country&country=usa&metric=clicks&start_date=2020-09-01&end_date=2020-09-25&siteUrl=sc-domain:gloholiday.com&window=5&user_id=3",
                method: 'get',
                success: function (countryresult) {
                    countrydata = countryresult;
                    countrydisplay();
                }
            });
        }
        else {
            jQuery.ajax({
                url: "https://ai.darkdata.ca/metrics/trends?major_dimension=country&country=usa&metric=clicks&start_date=2020-08-01&end_date=2020-09-25&siteUrl=sc-domain:gloholiday.com&window=5&user_id=3",
                method: 'get',
                success: function (countryresult) {
                    countrydata = countryresult;
                    countrydisplay();
                }
            });
        }

    });
}
function countrydisplay() {
    for (var i = 0; i < countrydata[1]['date'].length; i++) {
        var countryobject = {};

        countryobject[countryfields[0]] = countrydata[1]['date'][i];
        countryobject[countryfields[1]] = countrydata[1]['trends'][i];


        countrytrendarr.push(countryobject);
    }
    countrytrendanalysis = JSON.stringify(countrytrendarr);
    countryc();
}

function countryc() {

    am4core.ready(function () {

        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var clickbycountrychart = am4core.create("chartsdivclickbycountry", am4charts.XYChart);
        clickbycountrychart.layout = "grid";
        clickbycountrychart.height = 70;
        clickbycountrychart.paddingRight = 20;
        clickbycountrychart.logo.disabled = true;

        // Add data
        // console.log(countrytrendarr);
        clickbycountrychart.data = countrytrendarr;

        // Create axes
        var clickbycountrycategoryAxis = clickbycountrychart.xAxes.push(new am4charts.CategoryAxis());
        clickbycountrycategoryAxis.dataFields.category = "date";
        clickbycountrycategoryAxis.renderer.minGridDistance = 50;
        clickbycountrycategoryAxis.renderer.grid.template.location = 0.5;
        clickbycountrycategoryAxis.startLocation = 0.5;
        clickbycountrycategoryAxis.endLocation = 0.5;
        clickbycountrycategoryAxis.renderer.grid.template.disabled = true;
        clickbycountrycategoryAxis.renderer.labels.template.disabled = true;
        clickbycountrycategoryAxis.cursorTooltipEnabled = false;

        // Create value axis
        var clickbycountryvalueAxis = clickbycountrychart.yAxes.push(new am4charts.ValueAxis());
        clickbycountryvalueAxis.baseValue = 0;
        clickbycountryvalueAxis.renderer.grid.template.disabled = true;
        clickbycountryvalueAxis.renderer.baseGrid.disabled = true;
        clickbycountryvalueAxis.renderer.labels.template.disabled = true;
        clickbycountryvalueAxis.cursorTooltipEnabled = false;

        // Create series
        var clickbycountryseries = clickbycountrychart.series.push(new am4charts.LineSeries());
        clickbycountryseries.dataFields.valueY = "trends";
        clickbycountryseries.dataFields.categoryX = "date";
        clickbycountryseries.strokeWidth = 2;
        clickbycountryseries.tensionX = 0.77;


        // bullet is added because we add tooltip to a bullet for it to change color
        var clickbycountrybullet = clickbycountryseries.bullets.push(new am4charts.Bullet());
        clickbycountrybullet.tooltipText = "{valueY}";

        clickbycountrybullet.adapter.add("fill", function (fill, target) {
            if (target.dataItem.valueY < 0) {
                return am4core.color("#FF0000");
            }
            return fill;
        })


        clickbycountrychart.cursor = new am4charts.XYCursor();

    }); // end am4core.ready()


}









am4core.ready(function () {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create chart instance
    var trafficchart = am4core.create("chartsdivtraffic", am4charts.XYChart);
    trafficchart.layout = "grid";
    trafficchart.height = 70;
    trafficchart.paddingRight = 20;
    trafficchart.logo.disabled = true;

    // Add data
    trafficchart.data = [{
        "source": "none",
        "percentage": 40.77
    }, {
        "source": "content",
        "percentage": 0.0
    }, {
        "source": "email",
        "percentage": 0.03
    }, {
        "source": "organic",
        "percentage": 59.19
    }, {
        "source": "referral",
        "percentage": 0.0
    }];

    // Create axes
    var trafficcategoryAxis = trafficchart.xAxes.push(new am4charts.CategoryAxis());
    trafficcategoryAxis.dataFields.category = "source";
    trafficcategoryAxis.renderer.minGridDistance = 50;
    trafficcategoryAxis.renderer.grid.template.location = 0.5;
    trafficcategoryAxis.startLocation = 0.5;
    trafficcategoryAxis.endLocation = 0.5;
    trafficcategoryAxis.renderer.grid.template.disabled = true;
    trafficcategoryAxis.renderer.labels.template.disabled = true;
    trafficcategoryAxis.cursorTooltipEnabled = false;

    // Create value axis
    var trafficvalueAxis = trafficchart.yAxes.push(new am4charts.ValueAxis());
    trafficvalueAxis.baseValue = 0;
    trafficvalueAxis.renderer.grid.template.disabled = true;
    trafficvalueAxis.renderer.baseGrid.disabled = true;
    trafficvalueAxis.renderer.labels.template.disabled = true;
    trafficvalueAxis.cursorTooltipEnabled = false;

    // Create series
    var trafficseries = trafficchart.series.push(new am4charts.LineSeries());
    trafficseries.dataFields.valueY = "percentage";
    trafficseries.dataFields.categoryX = "source";
    trafficseries.strokeWidth = 2;
    trafficseries.tensionX = 0.77;


    // bullet is added because we add tooltip to a bullet for it to change color
    var trafficbullet = trafficseries.bullets.push(new am4charts.Bullet());
    trafficbullet.tooltipText = "{valueY}";

    trafficbullet.adapter.add("fill", function (fill, target) {
        if (target.dataItem.valueY < 0) {
            return am4core.color("#FF0000");
        }
        return fill;
    })


    trafficchart.cursor = new am4charts.XYCursor();

}); // end am4core.ready()


