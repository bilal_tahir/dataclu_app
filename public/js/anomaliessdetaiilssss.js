// Variable that will store the data comes in the response of API Call
var apiData;
// Array that will hold data for graph
var anomalyfields = ['date', 'actuals', 'trending_slope', 'prediction', 'lower_bound', 'upper_bound', 'disabled', 'disabled2'];
// Array that will store the objects created for graph in arrangeanomalyValues function
var grapharr = [];
//rearranging grapharr for skipping lower and bund values firstly
var regrapharr = [];

//For Pie Graph
var anomalyfieldsPie = ['key', 'value'];
var grapharrPie = [];

var anomalyStartDate;
var anomalyEndDate;

var generalDateStart;
var generalDateEnd;

jQuery.ajax({
    // url: "https://ai.dataclu.com/anomalies/trends/details?_id=" + anomId,
    url: "get_save_anomaly_data/anomalyDetail/" + anomId,
    method: 'get',
    async: false,
    success: function (result) {
        apiData = JSON.parse(result);
        // console.log(apiData);
        document.getElementById('anomaly-detail').value = JSON.stringify(apiData);
        // console.log(apiData['stats']);
        // assignAnomalyValue();
        // contributorTable();
        arrangeanomalyValues();


    }
});

function assignAnomalyValue() {
    //Change Class Name
    if (apiData['anomaly'] == 'down') {
        document.getElementById("change-class").className = "at-chartheadsAnom";
    }
    //Exploding Url
    // xhecking page value
    if (apiData['page']) {
        var explodedAnomString = ('/', apiData['page']).split('/').slice(3);
        var concatinatingAnomBack = explodedAnomString.join('/');

        // console.log(concatinatingAnomBack);
        if (concatinatingAnomBack.length != 0) {
            document.getElementById('pageValue').innerHTML = "&nbsp;" + stringAnomTruncate(concatinatingAnomBack, 25) + "<span class=\"urlhovertext22\">" + apiData['page'] + "</span>"
        }
        else {
            document.getElementById('pageValue').innerHTML = "&nbsp;/" + "<span class=\"urlhovertext22\">" + apiData['page'] + "</span>"
        }
    }

    // ..MAking Console and Analytics Logo Dynamic
    if (apiData['dtype'] == "ga") {
        var getElementAnalytics, setNameAnalytics, finalArrAnalytics;
        getElementAnalytics = document.getElementById("add-class");
        setNameAnalytics = "analyticsImage";
        finalArrAnalytics = getElementAnalytics.className.split(" ");
        if (finalArrAnalytics.indexOf(setNameAnalytics) == -1) {
            getElementAnalytics.className += " " + setNameAnalytics;
        }
        document.getElementById('queryValue').innerHTML = "<b>PagePath:</b> " + apiData['pagePath'];
    }
    if (apiData['dtype'] == "gsc") {
        var getElementConsole, setNameConsole, finalArrConsole;
        getElementConsole = document.getElementById("add-class");
        setNameConsole = "consoleImage";
        finalArrConsole = getElementConsole.className.split(" ");
        if (finalArrConsole.indexOf(setNameConsole) == -1) {
            getElementConsole.className += " " + setNameConsole;
        }
        document.getElementById('queryValue').innerHTML = "<b>Query:</b> " + apiData['query'];
    }
    if (apiData['dtype'] == "shopify") {
        var getElementConsole, setNameConsole, finalArrConsole;
        getElementConsole = document.getElementById("add-class");
        setNameConsole = "shopifyImage";
        finalArrConsole = getElementConsole.className.split(" ");
        if (finalArrConsole.indexOf(setNameConsole) == -1) {
            getElementConsole.className += " " + setNameConsole;
        }
        document.getElementById('queryValue').innerHTML = "<b>Dimension:</b> " + apiData['dimension'];
    }

    var repStr = (apiData['website']).replace('www.', '');
    document.getElementById('websiteValue').innerHTML = repStr;

    // console.log(anomalyStartDate);
    // console.log(anomalyEndDate);
    var anomalyfinalDate = anomalyStartDate + ' - ' + anomalyEndDate;
    document.getElementById('titleDate').innerHTML = anomalyfinalDate;
    document.getElementById('headDate').innerHTML = anomalyfinalDate;

    //Assigning Impact Value 
    document.getElementById('impact-score').innerHTML = apiData['stats']['impact'];
    // assigning last value of actual array
    document.getElementById('actualValue').innerHTML = " " + (apiData['stats']['actuals'][(apiData['stats']['actuals']).length - 1]).toFixed(2);
    //assigning last value of prediction array
    document.getElementById('expectedValue').innerHTML = " " + (apiData['stats']['predictions'][(apiData['stats']['predictions']).length - 1]).toFixed(2);
    //asign anomaly name
    document.getElementById('anomalyName').innerHTML = "(" + capitalizeFirstAnomalyLetter(apiData['metric']) + ")";
    //asigning parameters 
    // document.getElementById('pageValue').innerHTML = stringAnomTruncate(apiData['page'], 12);
    // document.getElementById('queryValue').innerHTML = " " + apiData['query'];
    if (apiData['country']) {
        document.getElementById('countryValue').innerHTML = " " + apiData['country'];
    }
    //variance value
    document.getElementById('variance-value').innerHTML = Math.abs(parseInt(apiData['stats']['change'])) + "%";

    // Calling arrangeanomalyValues function
    // arrangeanomalyValues();
    createGraph();
}

// Function To arrange anomaly values in the form of graph accptance data.
function arrangeanomalyValues() {

    //Empty grapharray if any values added
    grapharr = [];
    grapharrPie = [];

    var anomnalyOptions = { year: 'numeric', month: 'short', day: 'numeric' };
    var anomalyParts;
    var anomalyDate;

    //For Starting Date
    anomalyParts = (apiData['stats']['date'][0]).split("-");
    anomalyDate = new Date(anomalyParts[0], anomalyParts[1] - 1, anomalyParts[2]);
    generalDateStart = anomalyDate;
    anomalyStartDate = anomalyDate.toLocaleDateString("en-US", anomnalyOptions);

    // For End Date
    anomalyParts = (apiData['stats']['date'][(apiData['stats']['date']).length - 1]).split("-");
    anomalyDate = new Date(anomalyParts[0], anomalyParts[1] - 1, anomalyParts[2]);
    generalDateEnd = anomalyDate;
    anomalyEndDate = anomalyDate.toLocaleDateString("en-US", anomnalyOptions);

    // Code for claculating anomalies half value
    var anomaliesTotal = (parseInt(apiData['stats']['date'].length)) / 2;

    //Making For loop start with 1 and make trending slope seperate

    // Upper Three Variables and following two lines are changing the format of date
    anomalyParts = (apiData['stats']['date'][0]).split("-");
    anomalyDate = new Date(anomalyParts[0], anomalyParts[1] - 1, anomalyParts[2]);

    // Data in graph format is being created in form of objext and stored in array
    var object = {};
    object[anomalyfields[0]] = anomalyDate.toString('dd MMM');
    // if (apiData['metric'] == 'ctr') {
    //     object[anomalyfields[1]] = ((apiData['stats']['actuals'][i]).toFixed(2)) + ' %';
    // }
    // else {
    object[anomalyfields[1]] = (apiData['stats']['actuals'][0]).toFixed(2);
    // }
    object[anomalyfields[2]] = apiData['stats']['trending_slope'][0];
    object[anomalyfields[4]] = apiData['stats']['upper_bound'][0];
    object[anomalyfields[5]] = apiData['stats']['lower_bound'][0];
    if (((apiData['stats']['anomaly_data']['positive']['date']).includes(apiData['stats']['date'][0])) && ((apiData['stats']['anomaly_data']['positive']['value']).includes(apiData['stats']['actuals'][0]))) {
        object[anomalyfields[6]] = false;
    }
    if (((apiData['stats']['anomaly_data']['negative']['date']).includes(apiData['stats']['date'][0])) && ((apiData['stats']['anomaly_data']['negative']['value']).includes(apiData['stats']['actuals'][0]))) {
        object[anomalyfields[7]] = false;
    }

    grapharr.push(object);
    // console.log(object);

    //End


    // console.log(apiData['stats']['date']);
    for (var i = 1; i < apiData['stats']['date'].length - anomaliesTotal; i++) {


        // Upper Three Variables and following two lines are changing the format of date
        anomalyParts = (apiData['stats']['date'][i]).split("-");
        anomalyDate = new Date(anomalyParts[0], anomalyParts[1] - 1, anomalyParts[2]);

        // Data in graph format is being created in form of objext and stored in array
        var object = {};
        object[anomalyfields[0]] = anomalyDate.toString('dd MMM');
        // if (apiData['metric'] == 'ctr') {
        //     object[anomalyfields[1]] = ((apiData['stats']['actuals'][i]).toFixed(2)) + ' %';
        // }
        // else {
        object[anomalyfields[1]] = (apiData['stats']['actuals'][i]).toFixed(2);
        // }
        // object[anomalyfields[2]] = apiData['stats']['trending_slope'][i];
        object[anomalyfields[4]] = apiData['stats']['upper_bound'][i];
        object[anomalyfields[5]] = apiData['stats']['lower_bound'][i];
        if (((apiData['stats']['anomaly_data']['positive']['date']).includes(apiData['stats']['date'][i])) && ((apiData['stats']['anomaly_data']['positive']['value']).includes(apiData['stats']['actuals'][i]))) {
            object[anomalyfields[6]] = false;
        }
        if (((apiData['stats']['anomaly_data']['negative']['date']).includes(apiData['stats']['date'][i])) && ((apiData['stats']['anomaly_data']['negative']['value']).includes(apiData['stats']['actuals'][i]))) {
            object[anomalyfields[7]] = false;
        }

        grapharr.push(object);
        // console.log(object);

    }
    for (var i = apiData['stats']['date'].length - anomaliesTotal, pred = anomaliesTotal; i < apiData['stats']['date'].length - 1; i++, pred--) {

        // Upper Three Variables and following two lines are changing the format of date
        anomalyParts = (apiData['stats']['date'][i]).split("-");
        anomalyDate = new Date(anomalyParts[0], anomalyParts[1] - 1, anomalyParts[2]);

        // Data in graph format is being created in form of objext and stored in array
        var object = {};
        object[anomalyfields[0]] = anomalyDate.toString('dd MMM');
        // if (apiData['metric'] == 'ctr') {
        //     object[anomalyfields[1]] = ((apiData['stats']['actuals'][i]).toFixed(2)) + ' %';
        // }
        // else {
        object[anomalyfields[1]] = (apiData['stats']['actuals'][i]).toFixed(2);
        // }
        // object[anomalyfields[2]] = apiData['stats']['trending_slope'][i];
        // if (apiData['metric'] == 'ctr') {
        //     object[anomalyfields[3]] = (apiData['stats']['predictions'][(apiData['stats']['predictions'].length) - pred].toFixed(2)) + ' %';;
        // }
        // else {
        object[anomalyfields[3]] = apiData['stats']['predictions'][(apiData['stats']['predictions'].length) - pred].toFixed(2);
        // }
        object[anomalyfields[4]] = apiData['stats']['upper_bound'][i];
        object[anomalyfields[5]] = apiData['stats']['lower_bound'][i];
        if (((apiData['stats']['anomaly_data']['positive']['date']).includes(apiData['stats']['date'][i])) && ((apiData['stats']['anomaly_data']['positive']['value']).includes(apiData['stats']['actuals'][i]))) {
            object[anomalyfields[6]] = false;
        }
        if (((apiData['stats']['anomaly_data']['negative']['date']).includes(apiData['stats']['date'][i])) && ((apiData['stats']['anomaly_data']['negative']['value']).includes(apiData['stats']['actuals'][i]))) {
            object[anomalyfields[7]] = false;
        }

        grapharr.push(object);
        // console.log(object);

    }

    // Making Last loop < -1 for trending_slope

    // Upper Three Variables and following two lines are changing the format of date
    anomalyParts = (apiData['stats']['date'][(apiData['stats']['date'].length) - 1]).split("-");
    anomalyDate = new Date(anomalyParts[0], anomalyParts[1] - 1, anomalyParts[2]);

    // Data in graph format is being created in form of objext and stored in array
    var object = {};
    object[anomalyfields[0]] = anomalyDate.toString('dd MMM');
    // if (apiData['metric'] == 'ctr') {
    //     object[anomalyfields[1]] = ((apiData['stats']['actuals'][i]).toFixed(2)) + ' %';
    // }
    // else {
    object[anomalyfields[1]] = (apiData['stats']['actuals'][(apiData['stats']['date'].length) - 1]).toFixed(2);
    // }
    object[anomalyfields[2]] = apiData['stats']['trending_slope'][(apiData['stats']['date'].length) - 1];
    // if (apiData['metric'] == 'ctr') {
    //     object[anomalyfields[3]] = (apiData['stats']['predictions'][(apiData['stats']['predictions'].length) - pred].toFixed(2)) + ' %';;
    // }
    // else {
    object[anomalyfields[3]] = apiData['stats']['predictions'][(apiData['stats']['predictions'].length) - (anomaliesTotal - (anomaliesTotal - 1))].toFixed(2);
    // }
    object[anomalyfields[4]] = apiData['stats']['upper_bound'][(apiData['stats']['date'].length) - 1];
    object[anomalyfields[5]] = apiData['stats']['lower_bound'][(apiData['stats']['date'].length) - 1];
    if (((apiData['stats']['anomaly_data']['positive']['date']).includes(apiData['stats']['date'][(apiData['stats']['date'].length) - 1])) && ((apiData['stats']['anomaly_data']['positive']['value']).includes(apiData['stats']['actuals'][(apiData['stats']['date'].length) - 1]))) {
        object[anomalyfields[6]] = false;
    }
    if (((apiData['stats']['anomaly_data']['negative']['date']).includes(apiData['stats']['date'][(apiData['stats']['date'].length) - 1])) && ((apiData['stats']['anomaly_data']['negative']['value']).includes(apiData['stats']['actuals'][(apiData['stats']['date'].length) - 1]))) {
        object[anomalyfields[7]] = false;
    }

    grapharr.push(object);
    // console.log(object);

    // End

    // for (var pie = 0; pie < Object.keys(apiData['stats']['percentages']).length; pie++) {
    var object = {};
    object[anomalyfieldsPie[0]] = 'Normal';
    object[anomalyfieldsPie[1]] = apiData['stats']['percentages']['normal'].toFixed(2);
    grapharrPie.push(object);

    var object = {};
    object[anomalyfieldsPie[0]] = 'Positive Anomalies';
    object[anomalyfieldsPie[1]] = apiData['stats']['percentages']['positive_anomaly'].toFixed(2);
    grapharrPie.push(object);

    var object = {};
    object[anomalyfieldsPie[0]] = 'Negative Anomalies';
    object[anomalyfieldsPie[1]] = apiData['stats']['percentages']['negative_anomaly'].toFixed(2);
    grapharrPie.push(object);

    // }
    // console.log(grapharrPie);
    // Object.keys(apiData['stats']['percentages']).length
    // createGraph();
    assignAnomalyValue();
}

// console.log(grapharr);
for (ri = 0, rj = ((apiData['stats']['lower_bound'].length) - (grapharr.length)); ri < grapharr.length; ri++, rj++) {
    var obj = {};
    if (typeof grapharr[ri]['date'] !== 'undefined') {
        obj['date'] = grapharr[ri]['date'];
    }
    if (typeof grapharr[ri]['actuals'] !== 'undefined') {
        obj['actuals'] = grapharr[ri]['actuals'];
    }
    if (typeof grapharr[ri]['trending_slope'] !== 'undefined') {
        obj['trending_slope'] = grapharr[ri]['trending_slope'];
    }
    if (typeof grapharr[ri]['prediction'] !== 'undefined') {
        obj['prediction'] = grapharr[ri]['prediction'];
    }
    if (typeof grapharr[ri]['disabled'] !== 'undefined') {
        obj['disabled'] = grapharr[ri]['disabled'];
    }
    if (typeof grapharr[ri]['disabled2'] !== 'undefined') {
        obj['disabled2'] = grapharr[ri]['disabled2'];
    }
    if (rj >= 0) {
        if (typeof grapharr[rj]['upper_bound'] !== 'undefined') {
            obj['upper_bound'] = grapharr[rj]['lower_bound'];
        }
        if (typeof grapharr[rj]['lower_bound'] !== 'undefined') {
            obj['lower_bound'] = grapharr[rj]['upper_bound'];
        }
    }
    regrapharr.push(obj);
}
// console.log(regrapharr);
// Function To Create Graph
function createGraph() {
    am4core.ready(function () {

        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var chart = am4core.create("chartdiv", am4charts.PieChart);
        chart.height = 250;
        chart.logo.disabled = true;
        // chart.dateFormatter.dateFormat = "yyyy-MM-dd";

        // Add data
        chart.data = grapharrPie;

        // Add and configure Series
        var pieSeries = chart.series.push(new am4charts.PieSeries());
        pieSeries.dataFields.value = "value";
        pieSeries.dataFields.category = "key";
        pieSeries.slices.template.stroke = am4core.color("#fff");
        pieSeries.slices.template.tooltipText = "{category}: {value} %";
        pieSeries.slices.template.strokeOpacity = 1;
        pieSeries.labels.template.disabled = true;
        pieSeries.ticks.template.disabled = true;
        pieSeries.colors.list = [
            am4core.color("#CACFD2"),
            am4core.color("green"),
            am4core.color("red")
        ];


        // This creates initial animation
        pieSeries.hiddenState.properties.opacity = 1;
        pieSeries.hiddenState.properties.endAngle = -90;
        pieSeries.hiddenState.properties.startAngle = -90;

        chart.hiddenState.properties.radius = am4core.percent(0);

        chart.legend = new am4charts.Legend();
        chart.legend.position = "top";

        chart.legend.useDefaultMarker = true;
        var marker = chart.legend.markers.template.children.getIndex(0);
        marker.cornerRadius(12, 12, 12, 12);
        marker.strokeWidth = 2;
        marker.strokeOpacity = 1;
        marker.stroke = am4core.color("#ccc");


    }); // end am4core.ready()

    //Pie Chart Ended

    //Line Chart Started
    /* Create chart instance */
    var chart = am4core.create("chartdivs", am4charts.XYChart);
    chart.logo.disabled = true;
    // chart.marginRight = 10;
    // chart.marginLeft = 50;

    /* Add data */
    // chart.data = [{
    //     "year": "01 Jun",
    //     "actual": 2000,
    //     "expected": 650
    // }, {
    //     "year": "25 Sept",
    //     "actual": 5500,
    //     "expected": 683

    // }, {
    //     "year": "16 Oct",
    //     "actual": 2200,
    //     "expected": 691

    // }, {
    //     "year": "09 Nov",
    //     "actual": 4500,
    //     "expected": 642

    // }, {
    //     "year": "16 Dec",
    //     "actual": 1200,
    //     "expected": 699

    // }, {
    //     "year": "23 Jan",
    //     "actual": 3300,
    //     "expected": 721
    // }];

    chart.data = regrapharr;
    // console.log(regrapharr);

    /* Create axes */
    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "date";
    categoryAxis.renderer.grid.template.disabled = true;
    categoryAxis.cursorTooltipEnabled = false;

    /* Create value axis */
    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.min = 0;
    valueAxis.renderer.opposite = true;
    valueAxis.renderer.grid.template.disabled = true;
    valueAxis.cursorTooltipEnabled = false;
    // valueAxis.renderer.inversed = true;
    // valueAxis.calculatePercent = true;
    // valueAxis.renderer.labels.template.adapter.add("actuals", (text, label) => { return label.dataItem.value + "%"; })

    /* Create series */
    var series1 = chart.series.push(new am4charts.LineSeries());
    series1.dataFields.valueY = "actuals";
    series1.dataFields.categoryX = "date";
    series1.name = "Observed";
    series1.strokeWidth = 3;
    series1.tensionX = 0.7;
    series1.tooltipText = "{categoryX}: [b]{valueY}[/]";
    series1.adapter.add("tooltipText", function (ev) {
        var text = "[bold]{categoryX}[/]\n"
        text += "[" + series1.stroke.hex + "]■[/] " + series1.name + ": {" + series1.dataFields.valueY + "}\n"
        text += "[" + series3.stroke.hex + "]■[/] " + series3.name + ": {" + series3.dataFields.valueY + "}\n";
        // chart.series.first(function (item) {
        //     text += "[" + item.stroke.hex + "]■[/] " + item.name + ": {" + item.dataFields.valueY + "}\n";
        // });
        return text;
    });
    series1.tooltip.getFillFromObject = false;
    series1.tooltip.background.fill = am4core.color("#fff");
    series1.tooltip.label.fill = am4core.color("#00");
    series1.tooltip.background.stroke = am4core.color("#00B2EE");
    series1.tooltip.pointerOrientation = "down";
    series1.stroke = am4core.color("#3A33FF");
    series1.fill = am4core.color("#3A33FF");
    // if (apiData['anomaly'] == 'down') {
    //     series1.stroke = am4core.color("#3A33FF");
    //     series1.fill = am4core.color("#3A33FF");
    // }
    // if (apiData['anomaly'] == 'up') {
    //     series1.stroke = am4core.color("#3A33FF");
    //     series1.fill = am4core.color("#3A33FF");
    // }

    // Prevent cross-fading of tooltips
    // series1.tooltip.defaultState.transitionDuration = 0;
    // series1.tooltip.hiddenState.transitionDuration = 0;

    // series1.strokeWidth = 2;
    // series1.dateFormatter = new am4core.DateFormatter();
    // series1.dateFormatter.dateFormat = "MM-dd";
    // series1.tooltipText = "{categoryX}: [bold]{valueY}[/]";


    var series2 = chart.series.push(new am4charts.LineSeries());
    series2.dataFields.valueY = "trending_slope";
    series2.dataFields.categoryX = "date";
    series2.name = "Trend";
    series2.strokeWidth = 3;
    series2.tensionX = 0.7;
    if (apiData['anomaly'] == 'down') {
        series2.stroke = am4core.color("red");
        series2.fill = am4core.color("red");
    }
    if (apiData['anomaly'] == 'up') {
        series2.stroke = am4core.color("green");
        series2.fill = am4core.color("green");
    }
    // series2.tooltipText = "{categoryX}: [bold]{valueY}[/]";

    var series3 = chart.series.push(new am4charts.LineSeries());
    series3.dataFields.valueY = "prediction";
    series3.dataFields.categoryX = "date";
    series3.name = "Expected";
    series3.strokeWidth = 3;
    series3.tensionX = 0.7;
    series3.stroke = am4core.color("#3A33FF");
    series3.fill = am4core.color("#3A33FF");
    series3.strokeDasharray = "1,2";
    // series2.tooltipText = "{categoryX}: [bold]{valueY}[/]";

    // Create Upper_Bound series
    var series4 = chart.series.push(new am4charts.LineSeries());
    series4.dataFields.valueY = "upper_bound";
    series4.dataFields.openValueY = "lower_bound";
    series4.dataFields.categoryX = "date";
    series4.strokeWidth = 2;
    series4.tensionX = 0.77;
    series4.fillOpacity = 0.6;
    series4.sequencedInterpolation = true;
    series4.fill = am4core.color("#ebeff2");
    series4.stroke = am4core.color("#ebeff2");
    series4.hiddenInLegend = true;
    // series3.stacked = true;

    // Create Lower_Bound series
    var series5 = chart.series.push(new am4charts.LineSeries());
    series5.dataFields.valueY = "lower_bound";
    series5.dataFields.categoryX = "date";
    series5.strokeWidth = 2;
    series5.tensionX = 0.77;
    series5.stroke = am4core.color("#ebeff2");
    series5.sequencedInterpolation = true;
    series5.hiddenInLegend = true;


    /* Add legend */
    chart.legend = new am4charts.Legend();
    chart.legend.position = "top";
    chart.legend.useDefaultMarker = true;

    var bullet2 = series1.bullets.push(new am4charts.CircleBullet());
    bullet2.disabled = true;
    bullet2.propertyFields.disabled = "disabled2";
    bullet2.stroke = am4core.color("red");
    bullet2.fill = am4core.color("red");

    var bullet3 = series1.bullets.push(new am4charts.CircleBullet());
    bullet3.disabled = true;
    bullet3.propertyFields.disabled = "disabled";
    bullet3.stroke = am4core.color("green");
    bullet3.fill = am4core.color("green");



    var marker = chart.legend.markers.template.children.getIndex(0);
    marker.cornerRadius(12, 12, 12, 12);
    marker.strokeWidth = 2;
    marker.strokeOpacity = 1;
    marker.stroke = am4core.color("#ccc");

    /* Create a cursor */
    chart.cursor = new am4charts.XYCursor();
    chart.cursor.lineY.disabled = true;
    chart.cursor.lineX.disabled = true;

    // Create axes
    // var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
    // dateAxis.renderer.grid.template.location = 0;
    // dateAxis.renderer.minGridDistance = 30;

    // var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

    // // Create series
    // function createSeries(field, name) {
    //   var series = chart.series.push(new am4charts.LineSeries());
    //   series.dataFields.valueY = field;
    //   series.dataFields.dateX = "date";
    //   series.name = name;
    //   series.tooltipText = "{dateX}: [b]{valueY}[/]";
    //   series.strokeWidth = 2;

    //   // Set up tooltip
    //   series.adapter.add("tooltipText", function(ev) {
    //     var text = "[bold]{dateX}[/]\n"
    //     chart.series.each(function(item) {
    //       text += "[" + item.stroke.hex + "]●[/] " + item.name + ": {" + item.dataFields.valueY + "}\n";
    //     });
    //     return text;
    //   });

    //   series.tooltip.getFillFromObject = false;
    //   series.tooltip.background.fill = am4core.color("#fff");
    //   series.tooltip.label.fill = am4core.color("#00");

    //   // Prevent cross-fading of tooltips
    //   series.tooltip.defaultState.transitionDuration = 0;
    //   series.tooltip.hiddenState.transitionDuration = 0;

    //   var bullet = series.bullets.push(new am4charts.CircleBullet());
    //   bullet.circle.stroke = am4core.color("#fff");
    //   bullet.circle.strokeWidth = 2;

    //   return series;
    // }

    // createSeries("value", "Series #1");
    // createSeries("value2", "Series #2");
    // createSeries("value3", "Series #3");

    // chart.legend = new am4charts.Legend();
    // chart.cursor = new am4charts.XYCursor();
    // chart.cursor.maxTooltipDistance = 0; 
}
//function to capitaliza first anoma;ly letter
function capitalizeFirstAnomalyLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
//function to truncate a string
function stringAnomTruncate(str, length) {
    var dots = str.length > length ? '...' : '';
    return str.substring(0, length) + dots;
};

//function for creating contributors div
function contributorTable() {
    // console.log(Object.keys(contData['data'][0]['dimensions']).length);
    var contContainer = document.getElementById('contributors-list');
    var contTableContent = "";
    contTableContent += '<div class="at-chartlistview" data-label="Example" class="df-example demo-table"><table  class="table at-tablechartview" id="e2"><thead><tr><th><span></span></th><th><span>Metric</span></th>' +
        '<th><span>Dimension</span></th><th><span id = \"dateHeadingContributors\" style=\"text-align:center\"></span></th><th><span>Actual</span></th><th><span>Expected</span></th>' +
        '<th><span>Variance</span></th><th><span>Impact</span></th></tr></thead><tbody>';
    for (var tDiv = 1; tDiv <= contData['data'].length; tDiv++) {
        contTableContent += '<tr><td><span id="anomaly_id' + tDiv + '"><button class="scatter-id-div"  ><img src=\"/images/search.png\" class=\"tabletoolimg\"  /></button></span></td><td><h5 class="table-headText" id="tht' + tDiv +
            '"></h5></td>' +
            '<td><h5 class="mg-b-15">';
        // for (var ii = 0; ii < Object.keys(contData['data'][0]['dimensions']).length; ii++) {
        //     contTableContent += '<span class="table-dimension">Query: <span id="tq' + tDiv + '"></span></span>';
        // }
        // for (var key in p) {
        //     if (p.hasOwnProperty(key)) {
        //         console.log(key + " -> " + p[key]);
        //     }
        for (let [key, value] of Object.entries(contData['data'][tDiv - 1]['dimensions'])) {
            // console.log(`${key}: ${value}`);
            let ad = capitalizeFirstAnomalyLetter(key);
            // let Lettervalue = stringAnomTruncate(value, 25);
            if (value.includes("/")) {
                var explodedString = ('/', value).split('/').slice(3);
                var concatinatingBack = explodedString.join('/');
                if (concatinatingBack.length != 0) {
                    let Lettervalue = stringAnomTruncate(concatinatingBack, 25);
                    contTableContent += '<span class="table-dimension">' + `${ad}: ${Lettervalue}` + '</span><br>';
                }
                else {
                    contTableContent += '<span class="table-dimension">' + `${ad}: /` + '</span><br>';
                }
            }
            else {
                contTableContent += '<span class="table-dimension">' + `${ad}: ${value}` + '</span><br>';
            }
        }
        // <br><span class="table-dimension">Page: </span><span class="tooltip22" style="font-size:14px !important;' +
        // ' color:black !important" id="tp' + tDiv + '"></span><br><span class="table-dimension" >' +
        // 'Country: <span id="tttc' + tDiv + '"></span></span>
        contTableContent += '</h5></td>' +
            '<td><div class="loader" id="loading">' +
            'Loading...</div><div id="tablechartsdivs' + tDiv + '" class="table-customchartthird"  style="margin-bottom:10px"></div></td>' +
            '<td><span id="tts' + tDiv + '"></span></td><td><span id="expected' + tDiv + '"></span></td><td><h5>' +
            '<i id="tim' + tDiv + '"></i><i id="ttttpc' + tDiv + '"></i>% </h5></td><td><span id="tImpact' + tDiv + '"></span>  </td></tr>';
        // console.log(arraydata);
    }
    contTableContent += '</tbody></table></div>';
    contContainer.innerHTML += contTableContent;
    // console.log('done');
    // createContGraph();
    assigncontTableValues();
}

// //Function for creating audience div 
function audienceTable() {
    // console.log(Object.keys(contData['data'][0]['dimensions']).length);
    var audContainer = document.getElementById('contributors-audience');
    var audTableContent = "";
    audTableContent += '<div class="at-chartlistview" data-label="Example" class="df-example demo-table"><table  class="table at-tablechartview" id="audiencetable"><thead><tr><th><span></span></th><th><span>Metric</span></th>' +
        '<th><span>Dimension</span></th><th><span id="dateHeadingAudience" style="text-align:center"></span></th><th><span>Actual</span></th><th><span>Expected</span></th>' +
        '<th><span>Variance</span></th><th><span>Correlation</span></th></tr></thead><tbody>';
    for (var tDiv = 1; tDiv <= audData['total_audience']; tDiv++) {
        audTableContent += '<tr><td><span id="anomaly_id-a' + tDiv + '"><button class="scatter-id-div"  ><img src=\"/images/search.png\" class=\"tabletoolimg\"  /></button></span></td><td><h5 class="table-headText" id="tht-audience' + tDiv +
            '"></h5></td>' +
            '<td><h5 class="mg-b-15">';
        // for (var ii = 0; ii < Object.keys(contData['data'][0]['dimensions']).length; ii++) {
        //     audTableContent += '<span class="table-dimension">Query: <span id="tq' + tDiv + '"></span></span>';
        // }
        // for (var key in p) {
        //     if (p.hasOwnProperty(key)) {
        //         console.log(key + " -> " + p[key]);
        //     }
        for (let [key, value] of Object.entries(audData['data'][tDiv - 1]['dimensions'])) {
            // console.log(`${key}: ${value}`);
            let ad = capitalizeFirstAnomalyLetter(key);
            // let Lettervalue = stringAnomTruncate(value, 25);
            if (value.includes("/")) {
                var explodedString = ('/', value).split('/').slice(3);
                var concatinatingBack = explodedString.join('/');
                if (concatinatingBack.length != 0) {
                    let Lettervalue = stringAnomTruncate(concatinatingBack, 25);
                    audTableContent += '<span class="table-dimension">' + `${ad}: ${Lettervalue}` + '</span><br>';
                }
                else {
                    audTableContent += '<span class="table-dimension">' + `${ad}: /` + '</span><br>';
                }
            }
            else {
                audTableContent += '<span class="table-dimension">' + `${ad}: ${value}` + '</span><br>';
            }
        }
        // <br><span class="table-dimension">Page: </span><span class="tooltip22" style="font-size:14px !important;' +
        // ' color:black !important" id="tp' + tDiv + '"></span><br><span class="table-dimension" >' +
        // 'Country: <span id="tttc' + tDiv + '"></span></span>
        audTableContent += '</h5></td>' +
            '<td><div class="loader" id="loading">' +
            'Loading...</div><div id="tablechartsdivs-audience' + tDiv + '" class="table-customchartthird"  style="margin-bottom:10px"></div></td>' +
            '<td><span id="tts-audience' + tDiv + '"></span></td><td><span id="expected-audience' + tDiv + '"></span></td><td><h5>' +
            '<i id="tim-a' + tDiv + '"></i><i id="ttttpc-audience' + tDiv + '"></i>% </h5></td><td><span id="tImpact-audience' + tDiv + '"> - </span>  </td></tr>';
        // console.log(arraydata);
    }
    audTableContent += '</tbody></table></div>';
    audContainer.innerHTML += audTableContent;
    // console.log('done');
    // createContGraph();
    assignaudTableValues();
}

//Function for creating audience div ended
function assigncontTableValues() {
    // console.log(generalDateEnd, generalDateStart);
    document.getElementById("dateHeadingContributors").innerHTML = moment(generalDateStart.setDate(generalDateStart.getDate())).day(0).format("DD") + '-' + moment(generalDateEnd.setDate(generalDateEnd.getDate())).day(0).format("DD MMM");;
    for (var contassignData = 1; contassignData <= contData['data'].length; contassignData++) {
        document.getElementById('tht' + contassignData).innerHTML = contData['data'][contassignData - 1]['metric'];
        // document.getElementById('tq' + contassignData).innerHTML = contData['data'][contassignData - 1]['metric'];
        if (contData['data'][contassignData - 1]['metric'] == 'ctr') {
            document.getElementById('tts' + contassignData).innerHTML = ((contData['data'][contassignData - 1]['actuals'][(contData['data'][contassignData - 1]['actuals']).length - 1])) + '%';
            document.getElementById('expected' + contassignData).innerHTML = (contData['data'][contassignData - 1]['predicted']) + '%';
        }
        else {
            document.getElementById('tts' + contassignData).innerHTML = (contData['data'][contassignData - 1]['actuals'][(contData['data'][contassignData - 1]['actuals']).length - 1]);
            document.getElementById('expected' + contassignData).innerHTML = contData['data'][contassignData - 1]['predicted'];
        }
        document.getElementById('ttttpc' + contassignData).innerHTML = contData['data'][contassignData - 1]['change'];
        document.getElementById('tImpact' + contassignData).innerHTML = contData['data'][contassignData - 1]['impact'];
    }
    // New Code For Datatable Added
    $('#e2').DataTable({
        "bPaginate": false,
        "bFilter": false, //hide Search bar
        "bInfo": false, // hide showing entries
        language: {
            // searchPlaceholder: 'Search...',
            // sSearch: '',
            // lengthMenu: '_MENU_ items/page',
        },
        "aaSorting": [],
        columnDefs: [
            { orderable: true, targets: 7 },
            { orderable: false, targets: '_all' }
        ],
        "order": [[7, "desc"]]
    });
    // New Code For Datatable Added
    createContGraph();
}

//assigning aidience table value 
function assignaudTableValues() {
    // console.log(audData);
    document.getElementById('total-audience').innerHTML = audData['total_audience'];
    // console.log(generalDateEnd, generalDateStart);
    document.getElementById("dateHeadingAudience").innerHTML = moment(generalDateStart.setDate(generalDateStart.getDate())).day(0).format("DD") + '-' + moment(generalDateEnd.setDate(generalDateEnd.getDate())).day(0).format("DD MMM");
    for (var audassignData = 1; audassignData <= audData['total_audience']; audassignData++) {
        // console.log(audData['data'][audassignData - 1]);
        document.getElementById('tht-audience' + audassignData).innerHTML = audData['data'][audassignData - 1]['metric'];
        // document.getElementById('tq' + audassignData).innerHTML = audData['data'][audassignData - 1]['metric'];
        if (audData['data'][audassignData - 1]['metric'] == 'ctr') {
            document.getElementById('tts-audience' + audassignData).innerHTML = ((audData['data'][audassignData - 1]['actuals'][(audData['data'][audassignData - 1]['actuals']).length - 1])) + '%';
            document.getElementById('expected-audience' + audassignData).innerHTML = (audData['data'][audassignData - 1]['predicted']) + '%';
        }
        else {
            document.getElementById('tts-audience' + audassignData).innerHTML = (audData['data'][audassignData - 1]['actuals'][(audData['data'][audassignData - 1]['actuals']).length - 1]);
            document.getElementById('expected-audience' + audassignData).innerHTML = audData['data'][audassignData - 1]['predicted'];
        }
        document.getElementById('ttttpc-audience' + audassignData).innerHTML = audData['data'][audassignData - 1]['change'];
        document.getElementById('tImpact-audience' + audassignData).innerHTML = audData['data'][audassignData - 1]['correlation'];
    }
    // New Code For Datatable Added
    $('#audiencetable').DataTable({
        "bPaginate": false,
        "bFilter": false, //hide Search bar
        "bInfo": false, // hide showing entries
        language: {
            // searchPlaceholder: 'Search...',
            // sSearch: '',
            // lengthMenu: '_MENU_ items/page',
        },
        "aaSorting": [],
        columnDefs: [
            { orderable: true, targets: 7 },
            { orderable: false, targets: '_all' }
        ],
        "order": [[7, "desc"]]
    });
    // New Code For Datatable Added
    createAudGraph();
    // console.log('ok');
}
// assigning aidience table value ended
function createContGraph() {
    // console.log(trendarr2);
    for (var rangeChart = 1; rangeChart <= contData['data'].length; rangeChart++) {





        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        // console.log(rangeChart);
        var tablechart = am4core.create("tablechartsdivs" + rangeChart, am4charts.XYChart);
        tablechart.layout = "grid";
        tablechart.height = 100;
        tablechart.logo.disabled = true;

        tablechart.data = contoutergrapharr[rangeChart - 1];
        // [{
        //     "date": "2020-09-16",
        //     "trends": 4
        // }, {
        //     "date": "2020-09-22",
        //     "trends": 6
        // }, {
        //     "date": "2020-09-26",
        //     "trends": 9
        // }, {
        //     "date": "2020-09-28",
        //     "trends": 1
        // }, {
        //     "date": "2020-10-08",
        //     "trends": 3
        // }, {
        //     "date": "2020-10-13",
        //     "trends": 8
        // }, {
        //     "date": "2020-10-19",
        //     "trends": 2
        // }, {
        //     "date": "2020-10-21",
        //     "trends": 6
        // }, {
        //     "date": "2020-10-24",
        //     "trends": 5
        // }, {
        //     "date": "2020-10-28",
        //     "trends": 1
        // }];


        // Create axes
        var tablecategoryAxis = tablechart.xAxes.push(new am4charts.CategoryAxis());
        tablecategoryAxis.dataFields.category = "date";
        tablecategoryAxis.renderer.minGridDistance = 50;
        tablecategoryAxis.renderer.grid.template.location = 0.5;
        tablecategoryAxis.startLocation = 0.5;
        tablecategoryAxis.endLocation = 0.5;
        tablecategoryAxis.renderer.grid.template.disabled = true;
        tablecategoryAxis.renderer.labels.template.disabled = true;
        tablecategoryAxis.cursorTooltipEnabled = false;

        // Create value axis
        var tablevalueAxis = tablechart.yAxes.push(new am4charts.ValueAxis());
        tablevalueAxis.baseValue = 0;
        tablevalueAxis.renderer.grid.template.disabled = true;
        tablevalueAxis.renderer.baseGrid.disabled = true;
        tablevalueAxis.renderer.labels.template.disabled = true;
        tablevalueAxis.cursorTooltipEnabled = false;

        // Create series
        var tableseries = tablechart.series.push(new am4charts.LineSeries());
        tableseries.dataFields.valueY = "trends";
        tableseries.dataFields.categoryX = "date";
        tableseries.strokeWidth = 2;
        tableseries.tensionX = 0.77;


        // bullet is added because we add tooltip to a bullet for it to change color
        var tablebullet = tableseries.bullets.push(new am4charts.Bullet());
        tablebullet.tooltipText = "{valueY} : {categoryX}";
        // tablebullet.tooltipText.fontSize = 6;
        // tablebullet.tooltipText.FontWeight = normal;

        tablebullet.adapter.add("fill", function (fill, target) {
            if (target.dataItem.valueY < 0) {
                return am4core.color("#FF0000");
            }
            return fill;
        })


        tablechart.cursor = new am4charts.XYCursor();


        // console.log(rangeChart);
    }

    // arrangeanomalyValues();

}
// Create audience graphs
function createAudGraph() {
    // console.log(trendarr2);
    for (var rangeChart = 1; rangeChart <= audData['total_audience']; rangeChart++) {





        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        // console.log(rangeChart);
        var tablechart = am4core.create("tablechartsdivs-audience" + rangeChart, am4charts.XYChart);
        tablechart.layout = "grid";
        tablechart.height = 100;
        tablechart.logo.disabled = true;

        tablechart.data = audoutergrapharr[rangeChart - 1];
        // [{
        //     "date": "2020-09-16",
        //     "trends": 4
        // }, {
        //     "date": "2020-09-22",
        //     "trends": 6
        // }, {
        //     "date": "2020-09-26",
        //     "trends": 9
        // }, {
        //     "date": "2020-09-28",
        //     "trends": 1
        // }, {
        //     "date": "2020-10-08",
        //     "trends": 3
        // }, {
        //     "date": "2020-10-13",
        //     "trends": 8
        // }, {
        //     "date": "2020-10-19",
        //     "trends": 2
        // }, {
        //     "date": "2020-10-21",
        //     "trends": 6
        // }, {
        //     "date": "2020-10-24",
        //     "trends": 5
        // }, {
        //     "date": "2020-10-28",
        //     "trends": 1
        // }];


        // Create axes
        var tablecategoryAxis = tablechart.xAxes.push(new am4charts.CategoryAxis());
        tablecategoryAxis.dataFields.category = "date";
        tablecategoryAxis.renderer.minGridDistance = 50;
        tablecategoryAxis.renderer.grid.template.location = 0.5;
        tablecategoryAxis.startLocation = 0.5;
        tablecategoryAxis.endLocation = 0.5;
        tablecategoryAxis.renderer.grid.template.disabled = true;
        tablecategoryAxis.renderer.labels.template.disabled = true;
        tablecategoryAxis.cursorTooltipEnabled = false;

        // Create value axis
        var tablevalueAxis = tablechart.yAxes.push(new am4charts.ValueAxis());
        tablevalueAxis.baseValue = 0;
        tablevalueAxis.renderer.grid.template.disabled = true;
        tablevalueAxis.renderer.baseGrid.disabled = true;
        tablevalueAxis.renderer.labels.template.disabled = true;
        tablevalueAxis.cursorTooltipEnabled = false;

        // Create series
        var tableseries = tablechart.series.push(new am4charts.LineSeries());
        tableseries.dataFields.valueY = "trends";
        tableseries.dataFields.categoryX = "date";
        tableseries.strokeWidth = 2;
        tableseries.tensionX = 0.77;


        // bullet is added because we add tooltip to a bullet for it to change color
        var tablebullet = tableseries.bullets.push(new am4charts.Bullet());
        tablebullet.tooltipText = "{valueY} : {categoryX}";
        // tablebullet.tooltipText.fontSize = 6;
        // tablebullet.tooltipText.FontWeight = normal;

        tablebullet.adapter.add("fill", function (fill, target) {
            if (target.dataItem.valueY < 0) {
                return am4core.color("#FF0000");
            }
            return fill;
        })


        tablechart.cursor = new am4charts.XYCursor();


        // console.log(rangeChart);
    }

    // arrangeanomalyValues();

}
//create audience graph ended
//elow is the code for contributors starts
var contData;
var contanomalyfields = ['date', 'trends'];
var contgrapharr = [];
var contoutergrapharr = [];
var contparts;
var contmydate;
var contoptions = { year: 'numeric', month: 'short', day: 'numeric' };
//Calling APi for contributors
// console.log(anomId);
jQuery.ajax({
    // url: "https://ai.dataclu.com/anomalies/trends/details/contributors?id=" + anomId,
    url: "get_save_anomaly_data/relatedMetrics/" + anomId,
    method: 'get',
    // async: false,
    success: function (result) {
        contData = JSON.parse(result);
        // console.log(contData);
        document.getElementById('anomaly-contributors').value = JSON.stringify(contData);
        // console.log(contData);

        contGraphValues();
    }
});

//  data acc to graph format
function contGraphValues() {

    contoutergrapharr = [];
    for (var cExtL = 0; cExtL < contData['data'].length; cExtL++) {
        contgrapharr = [];
        for (var i = 0; i < contData['data'][cExtL]['dates'].length; i++) {
            contparts = (contData['data'][cExtL]['dates'][i]).split("-");
            contmydate = new Date(contparts[0], contparts[1] - 1, contparts[2]);
            var contobject = {};

            contobject[contanomalyfields[0]] = contmydate.toLocaleDateString("en-US", contoptions);
            contobject[contanomalyfields[1]] = (contData['data'][cExtL]['actuals'][i]).toFixed(2);

            contgrapharr.push(contobject);

        }
        contoutergrapharr[cExtL] = contgrapharr;

    }
    // console.log('countarr');
    // console.log(contoutergrapharr);
    contributorTable();
}
//Related Audience Section
//elow is the code for contributors starts
var audData;
var audanomalyfields = ['date', 'trends'];
var audgrapharr = [];
var audoutergrapharr = [];
var audparts;
var audmydate;
var audoptions = { year: 'numeric', month: 'short', day: 'numeric' };
//Calling APi for contributors
// console.log(anomId);
jQuery.ajax({
    // url: "https://ai.dataclu.com/anomalies/trends/details/audience?id=" + anomId,
    url: "get_save_anomaly_data/relatedAudience/" + anomId,
    method: 'get',
    // async: false,
    success: function (result) {
        audData = JSON.parse(result);
        // console.log(audData);
        document.getElementById('anomaly-audience').value = JSON.stringify(audData);

        audGraphValues();
    }
});

//  data acc to graph format
function audGraphValues() {

    audoutergrapharr = [];
    for (var aExtL = 0; aExtL < audData['total_audience']; aExtL++) {
        audgrapharr = [];
        for (var i = 0; i < audData['data'][aExtL]['dates'].length; i++) {
            audparts = (audData['data'][aExtL]['dates'][i]).split("-");
            audmydate = new Date(audparts[0], audparts[1] - 1, audparts[2]);
            var audobject = {};

            audobject[audanomalyfields[0]] = audmydate.toLocaleDateString("en-US", audoptions);
            audobject[audanomalyfields[1]] = (audData['data'][aExtL]['actuals'][i]).toFixed(2);

            audgrapharr.push(audobject);

        }
        audoutergrapharr[aExtL] = audgrapharr;

    }
    // console.log('countarr');
    // console.log(contoutergrapharr);
    // console.log(audoutergrapharr);
    // console.log(audData);
    // console.log('1');
    audienceTable();
}
// Related Audience

var groupData;
var groupanomalyfields = ['date', 'trends', 'upper_bound', 'lower_bound', 'disabled'];
var groupgrapharr = [];
var groupoutergrapharr = [];
var groupparts;
var groupmydate;
var groupoptions = { year: 'numeric', month: 'short', day: 'numeric' };

//Page Group Anomalies
jQuery.ajax({
    // url: "https://ai.dataclu.com/anomalies/trends/details/page_grouped_anomalies?id=" + anomId,
    url: "get_save_anomaly_data/pageGroupAnomalies/" + anomId,
    method: 'get',
    // async: false,
    success: function (result) {
        groupData = JSON.parse(result);
        // console.log(groupData);
        document.getElementById('anomaly-group').value = JSON.stringify(groupData);

        groupGraphValues();
    }
});

//  data acc to graph format
function groupGraphValues() {

    groupoutergrapharr = [];
    for (var aExtL = 0; aExtL < groupData['data'].length; aExtL++) {
        groupgrapharr = [];
        for (var i = 0, j = (groupData['data'][aExtL]['lower_bounds'].length) - (groupData['data'][aExtL]['dates'].length); i < groupData['data'][aExtL]['dates'].length; i++, j++) {
            groupparts = (groupData['data'][aExtL]['dates'][i]).split("-");
            groupmydate = new Date(groupparts[0], groupparts[1] - 1, groupparts[2]);
            var groupobject = {};

            groupobject[groupanomalyfields[0]] = groupmydate.toLocaleDateString("en-US", groupoptions);
            groupobject[groupanomalyfields[1]] = (groupData['data'][aExtL]['actuals'][i]).toFixed(2);
            if (j >= 0) {
                groupobject[groupanomalyfields[3]] = (groupData['data'][aExtL]['lower_bounds'][j]);
                groupobject[groupanomalyfields[2]] = (groupData['data'][aExtL]['upper_bounds'][j]);
            }

            if (i == ((groupData['data'][aExtL]['dates'].length) - 1)) {
                groupobject[groupanomalyfields[4]] = false;
            }

            groupgrapharr.push(groupobject);

        }
        groupoutergrapharr[aExtL] = groupgrapharr;

    }
    // console.log(groupoutergrapharr);
    // console.log('countarr');
    // console.log(contoutergrapharr);
    // console.log(audoutergrapharr);
    // console.log(audData);
    // console.log(groupoutergrapharr);
    groupTable();
}

// //Function for creating audience div 
function groupTable() {
    // console.log(Object.keys(contData['data'][0]['dimensions']).length);
    var groupContainer = document.getElementById('page-groups');
    var groupTableContent = "";
    groupTableContent += '<div class="at-chartlistview" data-label="ExampleGroup" class="df-example demo-table"><table  class="table at-tablechartview" id="grouptable"><thead><tr><th><span></span></th><th><span>Page Group</span></th>' +
        '<th><span id="dateHeadingPageGroup" style="text-align:center"></span></th><th><span>Actual</span></th><th><span>Expected</span></th>' +
        '<th><span>Change</span></th><th><span>Impact</span></th></tr></thead><tbody>';
    for (var tDiv = 1; tDiv <= groupData['data'].length; tDiv++) {
        groupTableContent += '<tr><td><span id="anomaly_id-g' + tDiv + '"><button class="scatter-id-div"  ><img src=\"/images/search.png\" class=\"tabletoolimg\"  /></button></span></td><td><h5 class="table-headText" id="thtgroupaudience' + tDiv +
            '"></h5></td>';

        groupTableContent += '<td><div class="loader" id="loading">' +
            'Loading...</div><div id="tablechartsdivs-group' + tDiv + '" class="table-customchartthird"  style="margin-bottom:10px"></div></td>' +
            '<td><span id="tts-group' + tDiv + '"></span></td><td><span id="expected-group' + tDiv + '"></span></td><td><h5>' +
            '<i id="tim-g' + tDiv + '"></i><i id="ttttpc-group' + tDiv + '"></i>% </h5></td><td><span id="tImpact-group' + tDiv + '"> - </span>  </td></tr>';
        // console.log(arraydata);
    }
    groupTableContent += '</tbody></table></div>';
    groupContainer.innerHTML += groupTableContent;
    // console.log('done');
    // createContGraph();
    assigngroupTableValues();
}
//assigning aidience table value 
function assigngroupTableValues() {
    // console.log(audData);
    // document.getElementById('total-audience').innerHTML = audData['total_audience'];
    document.getElementById("dateHeadingPageGroup").innerHTML = moment(generalDateStart.setDate(generalDateStart.getDate())).day(0).format("DD") + '-' + moment(generalDateEnd.setDate(generalDateEnd.getDate())).day(0).format("DD MMM");
    for (var groupassignData = 1; groupassignData <= groupData['data'].length; groupassignData++) {
        // console.log(audData['data'][groupassignData - 1]);
        document.getElementById('thtgroupaudience' + groupassignData).innerHTML = groupData['data'][groupassignData - 1]['page_group'];
        // document.getElementById('tq' + groupassignData).innerHTML = audData['data'][groupassignData - 1]['metric'];
        // if(audData['data'][groupassignData - 1]['metric'] == 'ctr')
        // {
        // document.getElementById('tts-audience' + groupassignData).innerHTML = ((audData['data'][groupassignData - 1]['actuals'][(audData['data'][groupassignData - 1]['actuals']).length - 1]))+'%';
        // document.getElementById('expected-audience' + groupassignData).innerHTML = (audData['data'][groupassignData - 1]['predicted'])+'%';
        // }
        // else
        // {
        document.getElementById('tts-group' + groupassignData).innerHTML = (groupData['data'][groupassignData - 1]['actuals'][(groupData['data'][groupassignData - 1]['actuals']).length - 1]);
        document.getElementById('expected-group' + groupassignData).innerHTML = groupData['data'][groupassignData - 1]['predicted'];
        // }
        document.getElementById('ttttpc-group' + groupassignData).innerHTML = groupData['data'][groupassignData - 1]['change'];
        document.getElementById('tImpact-group' + groupassignData).innerHTML = groupData['data'][groupassignData - 1]['impact'];
    }
    // New Code For Datatable Added
    $('#grouptable').DataTable({
        "bPaginate": false,
        "bFilter": false, //hide Search bar
        "bInfo": false, // hide showing entries
        language: {
            // searchPlaceholder: 'Search...',
            // sSearch: '',
            // lengthMenu: '_MENU_ items/page',
        },
        "aaSorting": [],
        columnDefs: [
            { orderable: true, targets: 6 },
            { orderable: false, targets: '_all' }
        ],
        "order": [[6, "desc"]]
    });
    // New Code For Datatable Added
    createGroupGraph();
    // console.log('ok');
}
// Create audience graphs
function createGroupGraph() {
    // console.log(trendarr2);
    for (var rangeChart = 1; rangeChart <= groupData['data'].length; rangeChart++) {
        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        // console.log(rangeChart);
        var tablechart = am4core.create("tablechartsdivs-group" + rangeChart, am4charts.XYChart);
        tablechart.layout = "grid";
        tablechart.height = 120;
        // tablechart.padding = 100;
        tablechart.logo.disabled = true;

        tablechart.data = groupoutergrapharr[rangeChart - 1];
        // console.log(groupoutergrapharr[rangeChart - 1]);



        // Create axes
        var tablecategoryAxis = tablechart.xAxes.push(new am4charts.CategoryAxis());
        tablecategoryAxis.dataFields.category = "date";
        tablecategoryAxis.renderer.minGridDistance = 50;
        tablecategoryAxis.renderer.grid.template.location = 0.5;
        tablecategoryAxis.startLocation = 0.5;
        tablecategoryAxis.endLocation = 3;
        tablecategoryAxis.renderer.grid.template.disabled = true;
        tablecategoryAxis.renderer.labels.template.disabled = true;
        tablecategoryAxis.cursorTooltipEnabled = false;

        // Create value axis
        var tablevalueAxis = tablechart.yAxes.push(new am4charts.ValueAxis());
        tablevalueAxis.baseValue = 0;
        tablevalueAxis.renderer.grid.template.disabled = true;
        tablevalueAxis.renderer.baseGrid.disabled = true;
        tablevalueAxis.renderer.labels.template.disabled = true;
        tablevalueAxis.cursorTooltipEnabled = false;

        // Create series
        var tableseries = tablechart.series.push(new am4charts.LineSeries());
        tableseries.dataFields.valueY = "trends";
        tableseries.dataFields.categoryX = "date";
        tableseries.strokeWidth = 2;
        tableseries.tensionX = 0.77;

        // Create Upper_Bound series
        var series3 = tablechart.series.push(new am4charts.LineSeries());
        series3.dataFields.valueY = "upper_bound";
        series3.dataFields.openValueY = "lower_bound";
        series3.dataFields.categoryX = "date";
        series3.strokeWidth = 2;
        series3.tensionX = 0.77;
        series3.fillOpacity = 0.6;
        series3.sequencedInterpolation = true;
        series3.fill = am4core.color("lightgray");
        series3.stroke = am4core.color("lightgray");
        // series3.stacked = true;

        // Create Lower_Bound series
        var series2 = tablechart.series.push(new am4charts.LineSeries());
        series2.dataFields.valueY = "lower_bound";
        series2.dataFields.categoryX = "date";
        series2.strokeWidth = 2;
        series2.tensionX = 0.77;
        series2.stroke = am4core.color("lightgray");
        series2.sequencedInterpolation = true;


        // bullet is added because we add tooltip to a bullet for it to change color
        var tablebullet = tableseries.bullets.push(new am4charts.Bullet());
        tablebullet.tooltipText = "{valueY} : {categoryX}";
        // tablebullet.tooltipText.fontSize = 6;
        // tablebullet.tooltipText.FontWeight = normal;

        tablebullet.adapter.add("fill", function (fill, target) {
            if (target.dataItem.valueY < 0) {
                return am4core.color("#FF0000");
            }
            return fill;
        })

        // Add end bullet
        var bullet = tableseries.bullets.push(new am4charts.CircleBullet());
        bullet.disabled = true;
        bullet.propertyFields.disabled = "disabled";

        var secondCircle = bullet.createChild(am4core.Circle);
        secondCircle.radius = 6;
        // secondCircle.fill = tablechart.colors.getIndex(8);


        // bullet.events.on("inited", function(event){
        // animateBullet(event.target.circle);
        // })


        // function animateBullet(bullet) {
        //     var animation = bullet.animate([{ property: "scale", from: 1, to: 5 }, { property: "opacity", from: 1, to: 0 }], 1000, am4core.ease.circleOut);
        //     animation.events.on("animationended", function(event){
        //     animateBullet(event.target.object);
        //     })
        // }



        tablechart.cursor = new am4charts.XYCursor();


        // console.log(rangeChart);
    }

    // arrangeanomalyValues();

}