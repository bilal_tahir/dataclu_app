
// for (var f = 1; f <= arraydata.length; f++) {
//     document.getElementById('fd' + f).innerHTML = bFinalDate;
// }
// for (var tf = 1; tf <= arraydata.length; tf++) {
//     document.getElementById('tfd' + tf).innerHTML = bFinalDate;
// }


// console.log(userIdApi);
SessionData();

function ApiInit() {
    if (startdate == 0) {
        if (strUser == "D") {
            clickUrl = "https://ai.darkdata.ca/anomalies/trends?user_id=" + userIdApi + "&siteUrl=" + finalUrl + "&metric=clicks&dimensions=[\"country\",\"page\", \"query\"]&range=5&frequency=D";
            ctrUrl = "https://ai.darkdata.ca/anomalies/trends?user_id=" + userIdApi + "&siteUrl=" + finalUrl + "&metric=ctr&dimensions=[\"country\",\"page\", \"query\"]&range=5&frequency=D";
            impressionUrl = "https://ai.darkdata.ca/anomalies/trends?user_id=" + userIdApi + "&siteUrl=" + finalUrl + "&metric=impressions&dimensions=[\"country\",\"page\", \"query\"]&range=5&frequency=D";
            positionUrl = "https://ai.darkdata.ca/anomalies/trends?user_id=" + userIdApi + "&siteUrl=" + finalUrl + "&metric=position&dimensions=[\"country\",\"page\", \"query\"]&range=5&frequency=D";
        }
        if (strUser == "W") {
            clickUrl = "https://ai.darkdata.ca/anomalies/trends?user_id=" + userIdApi + "&siteUrl=" + finalUrl + "&metric=clicks&dimensions=[\"country\",\"page\", \"query\"]&range=5&frequency=W-Sun";
            ctrUrl = "https://ai.darkdata.ca/anomalies/trends?user_id=" + userIdApi + "&siteUrl=" + finalUrl + "&metric=ctr&dimensions=[\"country\",\"page\", \"query\"]&range=5&frequency=W-Sun";
            impressionUrl = "https://ai.darkdata.ca/anomalies/trends?user_id=" + userIdApi + "&siteUrl=" + finalUrl + "&metric=impressions&dimensions=[\"country\",\"page\", \"query\"]&range=5&frequency=W-Sun";
            positionUrl = "https://ai.darkdata.ca/anomalies/trends?user_id=" + userIdApi + "&siteUrl=" + finalUrl + "&metric=position&dimensions=[\"country\",\"page\", \"query\"]&range=5&frequency=W-Sun";
        }
    }
    else {
        clickUrl = "https://ai.darkdata.ca/anomalies/trends?user_id=" + userIdApi + "&start_date=" + startdate + "&end_date=" + enddate + "&window=5&siteUrl=" + finalUrl + "&metric=clicks&dimensions=[\"country\",\"page\", \"query\"]&range=5&frequency=D";
        ctrUrl = "https://ai.darkdata.ca/anomalies/trends?user_id=" + userIdApi + "&start_date=" + startdate + "&end_date=" + enddate + "&window=5&siteUrl=" + finalUrl + "&metric=ctr&dimensions=[\"country\",\"page\", \"query\"]&range=5&frequency=D";
        impressionUrl = "https://ai.darkdata.ca/anomalies/trends?user_id=" + userIdApi + "&start_date=" + startdate + "&end_date=" + enddate + "&window=5&siteUrl=" + finalUrl + "&metric=impressions&dimensions=[\"country\",\"page\", \"query\"]&range=5&frequency=D";
        positionUrl = "https://ai.darkdata.ca/anomalies/trends?user_id=" + userIdApi + "&start_date=" + startdate + "&end_date=" + enddate + "&window=5&siteUrl=" + finalUrl + "&metric=position&dimensions=[\"country\",\"page\", \"query\"]&range=5&frequency=D";
    }

    rangeClickCall();
}

function rangeClickCall() {
    totalArrays = 0;
    loc_arr1 = 0;
    // console.log(clickUrl);
    jQuery.ajax({
        url: clickUrl,
        method: 'get',
        async: false,
        success: function (result) {
            assignResult = result;
            totalArrays = assignResult[1].length;
            for (var arrayDec = 0; arrayDec < totalArrays; arrayDec++) {
                arraydata[arrayDec] = assignResult[1][arrayDec];
                loc_arr1++;
            }
            // console.log(arraydata);
            rangeCtrCall();
        }
    });
}
function rangeCtrCall() {
    // console.log(ctrUrl);
    jQuery.ajax({
        url: ctrUrl,
        method: 'get',
        async: false,
        success: function (result) {
            assignResult2 = result;
            totalArrays = assignResult2[1].length;

            for (var arrayDec2 = 0; arrayDec2 < totalArrays; arrayDec2++) {
                arraydata[loc_arr1] = assignResult2[1][arrayDec2];
                loc_arr1++
            }
            // console.log(arraydata);
            rangeImpressionCall();
        }
    });
}
function rangeImpressionCall() {
    // console.log(impressionUrl);
    jQuery.ajax({
        url: impressionUrl,
        method: 'get',
        async: false,
        success: function (result) {
            assignResult3 = result;
            totalArrays = assignResult3[1].length;
            for (var arrayDec3 = 0; arrayDec3 < totalArrays; arrayDec3++) {
                arraydata[loc_arr1] = assignResult3[1][arrayDec3];
                loc_arr1++
            }
            // console.log(arraydata);
            rangePositionCall();
        }
    });
}
function rangePositionCall() {
    // console.log(positionUrl);
    jQuery.ajax({
        url: positionUrl,
        method: 'get',
        async: false,
        success: function (result) {
            assignResult4 = result;
            totalArrays = assignResult4[1].length;
            for (var arrayDec4 = 0; arrayDec4 < totalArrays; arrayDec4++) {
                arraydata[loc_arr1] = assignResult4[1][arrayDec4];
                loc_arr1++
            }
            // console.log(arraydata);

            // displayDiv();
            $('.loader').hide();
            assignValues();
        }
    });
}

// function displayDiv() {
//     $('.loader').hide();
//     $("#te").load(window.location.href + "#te");
//     console.log(arraydata);
//     console.log("ok");
//     // assignValues();
// }

function SessionData() {
    am4core.ready(function () {

        if (trendarr != null) {
            trendarr = [];
        }
        if (trendarr2 != null) {
            trendarr2 = [];
        }
        if (rangeChange != 0) {
            // for (var f = 1; f <= arraydata.length; f++) {
            //     document.getElementById('fd' + f).innerHTML = bFinalDate;
            // }
            // for (var tf = 1; tf <= arraydata.length; tf++) {
            //     document.getElementById('tfd' + tf).innerHTML = bFinalDate;
            // }
            arraydata = [];
            // document.getElementById("dateValue").value = bFinalDate;
            // console.log(finalUrl);
            ajaxcalls();


        }
        if (rangeChange == 0) {
            assignValues();
        }
    });
}
// Ajax Call Functions
function capital_letter(str) {
    str = str.split(" ");

    for (let i = 0, x = str.length; i < x; i++) {
        str[i] = str[i][0].toUpperCase() + str[i].substr(1);
    }

    return str.join(" ");
}

function assignValues() {

    for (var assignData = 1; assignData <= arraydata.length; assignData++) {
        //Code For Exploding url and concatinate back after third forward slash
        // document.getElementById('anomaly_id' + assignData).value = arraydata[assignData - 1]['_id'];
        document.getElementById('anomaly_id' + assignData).innerHTML = "<a href=\"/anomaly/detail/" + arraydata[assignData - 1]['_id'] + "\"" + "target=_blank><img src=\"/images/search.png\" class=\"tabletoolimg\"  /></a>";
        var explodedString = ('/', arraydata[assignData - 1]['page']).split('/').slice(3);
        var concatinatingBack = explodedString.join('/');
        // console.log(arraydata);
        var cap = capital_letter(arraydata[assignData - 1]['metric']);
        document.getElementById('ht' + assignData).innerHTML = cap;
        document.getElementById('q' + assignData).innerHTML = arraydata[assignData - 1]['query'];
        // document.getElementById('p' + assignData).innerHTML = stringTruncate(arraydata[assignData - 1]['page'], 24) + "<span class=\"tooltiptext22\">" + arraydata[assignData - 1]['page'] + "</span>";
        if (concatinatingBack.length != 0) {
            document.getElementById('p' + assignData).innerHTML = stringTruncate(concatinatingBack, 12) + "<span class=\"tooltiptext22\">" + arraydata[assignData - 1]['page'] + "</span>";
        }
        else {
            document.getElementById('p' + assignData).innerHTML = ' /' + "<span class=\"tooltiptext22\">" + arraydata[assignData - 1]['page'] + "</span>";
        }
        // document.getElementById('url' + assignData).innerHTML = arraydata[assignData - 1]['page'];
        document.getElementById('c' + assignData).innerHTML = arraydata[assignData - 1]['country'];

        if (arraydata[assignData - 1]['anomaly'] == 'up') {
            document.getElementById('pc' + assignData).innerHTML = "<span class=\"percentcss\" id=\"ts" + assignData + "\"></span>";
            document.getElementById('tl' + assignData).innerHTML = "<span class=\"textLightthird\" id=\"tc" + assignData + "\" > </span>";
            document.getElementById('ttttpc' + assignData).innerHTML = " <span class=\"table-percentcss\" id=\"ttc" + assignData + "\"></span>";
            document.getElementById('arrow' + assignData).innerHTML = "<img src=\"/images/greenup.png\" class=\"GreenUpImage\"/>";
            // document.getElementById('im' + assignData).innerHTML = "<img src=" + image_path1 + " class=\"GreenUpImage\"/>";
            // document.getElementById('tim' + assignData).innerHTML = "<img src=" + image_path1 + " class=\"GreenUpImage\"/>";
        }
        if (arraydata[assignData - 1]['anomaly'] == 'down') {
            document.getElementById('pc' + assignData).innerHTML = "<span class=\"percentcssfourth\" id=\"ts" + assignData + "\"></span>";
            document.getElementById('tl' + assignData).innerHTML = "<span class=\"textLightfourth\" id=\"tc" + assignData + "\" > </span>";
            document.getElementById('ttttpc' + assignData).innerHTML = " <span class=\"table-percentcssfourth\" id=\"ttc" + assignData + "\"></span>";
            document.getElementById('arrow' + assignData).innerHTML = "<img src=\"/images/reddown.png\" class=\"GreenUpImage\"/>";
            // document.getElementById('im' + assignData).innerHTML = "<img src=" + image_path2 + " class=\"GreenUpImage\"/>";
            // document.getElementById('tim' + assignData).innerHTML = "<img src=" + image_path2 + " class=\"GreenUpImage\"/>";
        }
        // For Change 
        if (parseInt(arraydata[assignData - 1]['stats']['change']) >= 0) {
            document.getElementById('im' + assignData).innerHTML = "<img src=" + image_path1 + " class=\"GreenUpImage\"/>";
            document.getElementById('tim' + assignData).innerHTML = "<img src=" + image_path1 + " class=\"GreenUpImage\"/>";
        }
        if (parseInt(arraydata[assignData - 1]['stats']['change']) < 0) {
            document.getElementById('im' + assignData).innerHTML = "<img src=" + image_path2 + " class=\"GreenUpImage\"/>";
            document.getElementById('tim' + assignData).innerHTML = "<img src=" + image_path2 + " class=\"GreenUpImage\"/>";
        }
        // console.log(arraydata[assignData - 1]['stats']['agg']);

        document.getElementById('ts' + assignData).innerHTML = (arraydata[assignData - 1]['stats']['agg']).toFixed(2);
        document.getElementById('tc' + assignData).innerHTML = Math.abs(parseInt(arraydata[assignData - 1]['stats']['change'])) + '%';
        // document.getElementById('tc' + assignData).innerHTML = (arraydata[assignData - 1]['stats']['change']) + '%';

        // Table Assigning
        // Code Gor assigning dynamic values
        document.getElementById('tht' + assignData).innerHTML = cap;
        document.getElementById('tq' + assignData).innerHTML = arraydata[assignData - 1]['query'];
        // document.getElementById('tp' + assignData).innerHTML = stringTruncate(arraydata[assignData - 1]['page'], 24) + "<span class=\"tooltiptext22\">" + arraydata[assignData - 1]['page'] + "</span>";
        if (concatinatingBack.length != 0) {
            document.getElementById('tp' + assignData).innerHTML = stringTruncate(concatinatingBack, 20) + "<span class=\"tooltiptext22\">" + arraydata[assignData - 1]['page'] + "</span>";
        }
        else {
            document.getElementById('tp' + assignData).innerHTML = ' /' + "<span class=\"tooltiptext22\">" + arraydata[assignData - 1]['page'] + "</span>";
        }
        // document.getElementById('tp' + assignData).innerHTML = stringTruncate(concatinatingBack, 20) + "<span class=\"tooltiptext22\">" + arraydata[assignData - 1]['page'] + "</span>";
        document.getElementById('tttc' + assignData).innerHTML = arraydata[assignData - 1]['country'];

        // console.log()
        document.getElementById('tts' + assignData).innerHTML = (arraydata[assignData - 1]['stats']['actuals'][(arraydata[assignData - 1]['stats']['actuals']).length - 1]).toFixed(2);
        document.getElementById('expected' + assignData).innerHTML = (arraydata[assignData - 1]['stats']['predictions'][(arraydata[assignData - 1]['stats']['predictions']).length - 1]).toFixed(2);
        document.getElementById('ttc' + assignData).innerHTML = Math.abs(parseInt(arraydata[assignData - 1]['stats']['change'])) + '%';
        // document.getElementById('ttc' + assignData).innerHTML = (arraydata[assignData - 1]['stats']['change']) + '%';

    }

    parts = (arraydata[0]['stats']['date'][(arraydata[0]['stats']['date']).length - 1]).split("-");
    mydate = new Date(parts[0], parts[1] - 1, parts[2]);
    document.getElementById('daily-date').innerHTML = "Daily (" + mydate.toLocaleDateString("en-US", options) + ")";
    document.getElementById('weekly-date').innerHTML = "Weekly (" + moment().day(-6).format('DD MMM, YYYY') + " - " + moment().day(0).format('DD MMM, YYYY') + ")";

    function stringTruncate(str, length) {
        var dots = str.length > length ? '...' : '';
        return str.substring(0, length) + dots;
    };

    display();

}
function c() {
    console.log(trendarr2);
    for (var rangeChart = 1; rangeChart <= trendarr2.length; rangeChart++) {
        am4core.ready(function () {

            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            var chart = am4core.create("chartsdivs" + rangeChart, am4charts.XYChart);
            chart.layout = "grid";
            // chart.height = 100;
            chart.height = am4core.percent(100);
            chart.paddingRight = 10;
            chart.paddingLeft = 10;
            chart.logo.disabled = true;

            // console.log(trendarr2[rangeChart - 1]);
            // console.log(slopearr2[rangeChart - 1]);
            chart.data = trendarr2[rangeChart - 1];

            // Create axes
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "date";
            categoryAxis.renderer.minGridDistance = 50;
            categoryAxis.renderer.grid.template.location = 0.5;
            categoryAxis.startLocation = 0.5;
            categoryAxis.endLocation = 0.5;
            categoryAxis.renderer.grid.template.disabled = true;
            categoryAxis.renderer.labels.template.disabled = true;
            categoryAxis.cursorTooltipEnabled = false;

            // Create value axis
            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.baseValue = 0;
            valueAxis.renderer.grid.template.disabled = true;
            valueAxis.renderer.baseGrid.disabled = true;
            valueAxis.renderer.labels.template.disabled = true;
            valueAxis.cursorTooltipEnabled = false;

            // Create series
            var series = chart.series.push(new am4charts.LineSeries());
            series.dataFields.valueY = "trends";
            series.dataFields.categoryX = "date";
            series.strokeWidth = 2;
            series.tensionX = 0.77;
            if (arraydata[rangeChart - 1]['anomaly'] == 'down') {
                series.stroke = am4core.color("red");
            }
            if (arraydata[rangeChart - 1]['anomaly'] == 'up') {
                series.stroke = am4core.color("green");
            }

            // Create Upper_Bound series
            var series3 = chart.series.push(new am4charts.LineSeries());
            series3.dataFields.valueY = "upper_bound";
            series3.dataFields.openValueY = "lower_bound";
            series3.dataFields.categoryX = "date";
            series3.strokeWidth = 2;
            series3.tensionX = 0.77;
            series3.fillOpacity = 0.6;
            series3.sequencedInterpolation = true;
            series3.fill = am4core.color("lightgray");
            series3.stroke = am4core.color("lightgray");
            // series3.stacked = true;

            // Create Lower_Bound series
            var series2 = chart.series.push(new am4charts.LineSeries());
            series2.dataFields.valueY = "lower_bound";
            series2.dataFields.categoryX = "date";
            series2.strokeWidth = 2;
            series2.tensionX = 0.77;
            series2.stroke = am4core.color("lightgray");
            series2.sequencedInterpolation = true;


            // Create Trending_Slope series
            var series2 = chart.series.push(new am4charts.LineSeries());
            series2.data = slopearr2[rangeChart - 1];
            series2.dataFields.valueY = "trending_slope";
            series2.dataFields.categoryX = "date";
            series2.strokeWidth = 2;
            series2.tensionX = 0.77;

            if (arraydata[rangeChart - 1]['anomaly'] == 'down') {
                series2.stroke = am4core.color("red");
            }
            if (arraydata[rangeChart - 1]['anomaly'] == 'up') {
                series2.stroke = am4core.color("green");
            }

            // //New Trend Line
            // var trend = chart.series.push(new am4charts.LineSeries());
            // trend.data = slopearr2[rangeChart - 1];
            // trend.dataFields.valueY = "trending_slope";
            // trend.dataFields.dateX = "date";
            // trend.strokeWidth = 2;
            // trend.stroke = am4core.color("#c00");


            // bullet is added because we add tooltip to a bullet for it to change color
            var bullet = series.bullets.push(new am4charts.Bullet());
            bullet.tooltipText = "{valueY} : {categoryX}";

            // bullet.adapter.add("fill", function (fill, target) {
            //     if (target.dataItem.valueY < 0) {
            //         return am4core.color("#FF0000");
            //     }
            //     return fill;
            // })
            var bullet2 = series.bullets.push(new am4charts.CircleBullet());
            bullet2.disabled = true;
            bullet2.propertyFields.disabled = "disabled2";
            bullet2.stroke = am4core.color("red");
            bullet2.fill = am4core.color("red");

            var bullet3 = series.bullets.push(new am4charts.CircleBullet());
            bullet3.disabled = true;
            bullet3.propertyFields.disabled = "disabled";
            bullet3.stroke = am4core.color("green");
            bullet3.fill = am4core.color("green");


            chart.cursor = new am4charts.XYCursor();

        }); // end am4core.ready()


        // Chart For Grid View
        am4core.ready(function () {

            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            var tablechart = am4core.create("tablechartsdivs" + rangeChart, am4charts.XYChart);
            tablechart.layout = "grid";
            tablechart.height = 80;
            // tablechart.paddingRight = 20;
            tablechart.logo.disabled = true;

            // Add data
            // console.log(trendarr);
            tablechart.data = trendarr2[rangeChart - 1];

            // Create axes
            var tablecategoryAxis = tablechart.xAxes.push(new am4charts.CategoryAxis());
            tablecategoryAxis.dataFields.category = "date";
            tablecategoryAxis.renderer.minGridDistance = 50;
            tablecategoryAxis.renderer.grid.template.location = 0.5;
            tablecategoryAxis.startLocation = 0.5;
            tablecategoryAxis.endLocation = 0.5;
            tablecategoryAxis.renderer.grid.template.disabled = true;
            tablecategoryAxis.renderer.labels.template.disabled = true;
            tablecategoryAxis.cursorTooltipEnabled = false;

            // Create value axis
            var tablevalueAxis = tablechart.yAxes.push(new am4charts.ValueAxis());
            tablevalueAxis.baseValue = 0;
            tablevalueAxis.renderer.grid.template.disabled = true;
            tablevalueAxis.renderer.baseGrid.disabled = true;
            tablevalueAxis.renderer.labels.template.disabled = true;
            tablevalueAxis.cursorTooltipEnabled = false;

            // Create series
            var tableseries = tablechart.series.push(new am4charts.LineSeries());
            tableseries.dataFields.valueY = "trends";
            tableseries.dataFields.categoryX = "date";
            tableseries.strokeWidth = 2;
            tableseries.tensionX = 0.77;
            if (arraydata[rangeChart - 1]['anomaly'] == 'down') {
                tableseries.stroke = am4core.color("red");
            }
            if (arraydata[rangeChart - 1]['anomaly'] == 'up') {
                tableseries.stroke = am4core.color("green");
            }


            // // Create Upper_Bound series
            // var tableseries2 = tablechart.series.push(new am4charts.LineSeries());
            // tableseries2.dataFields.valueY = "upper_bound";
            // tableseries2.dataFields.openValueY = "lower_bound";
            // tableseries2.dataFields.categoryX = "date";
            // tableseries2.strokeWidth = 2;
            // tableseries2.tensionX = 0.77;
            // tableseries2.fillOpacity = 0.6;
            // tableseries2.sequencedInterpolation = true;
            // tableseries2.fill = am4core.color("lightgray");
            // tableseries2.stroke = am4core.color("lightgray");
            // // pagesviewpersessionseries3.stacked = true;

            // // Create Lower_Bound series
            // var tableseries3 = tablechart.series.push(new am4charts.LineSeries());
            // tableseries3.dataFields.valueY = "lower_bound";
            // tableseries3.dataFields.categoryX = "date";
            // tableseries3.strokeWidth = 2;
            // tableseries3.tensionX = 0.77;
            // tableseries3.stroke = am4core.color("lightgray");
            // tableseries3.sequencedInterpolation = true;


            // Create Trending_Slope series
            var tableseries4 = tablechart.series.push(new am4charts.LineSeries());
            tableseries4.data = slopearr2[rangeChart - 1];
            tableseries4.dataFields.valueY = "trending_slope";
            tableseries4.dataFields.categoryX = "date";
            tableseries4.strokeWidth = 2;
            tableseries4.tensionX = 0.77;
            if (arraydata[rangeChart - 1]['anomaly'] == 'down') {
                tableseries4.stroke = am4core.color("red");
            }
            if (arraydata[rangeChart - 1]['anomaly'] == 'up') {
                tableseries4.stroke = am4core.color("green");
            }
            // tableseries4.sequencedInterpolation = true;


            // bullet is added because we add tooltip to a bullet for it to change color
            var tablebullet = tableseries.bullets.push(new am4charts.Bullet());
            tablebullet.tooltipText = "{valueY} : {categoryX}";
            // tablebullet.tooltipText.fontSize = 6;
            // tablebullet.tooltipText.FontWeight = normal;

            tablebullet.adapter.add("fill", function (fill, target) {
                if (target.dataItem.valueY < 0) {
                    return am4core.color("#FF0000");
                }
                return fill;
            })


            tablechart.cursor = new am4charts.XYCursor();

        }); // end am4core.ready()
    }



}
function display() {

    // console.log('aya2');


    for (var extL = 0; extL < arraydata.length; extL++) {
        trendarr = [];
        slopearr = [];
        for (var i = 0; i < arraydata[extL]['stats']['date'].length; i++) {

            parts = (arraydata[extL]['stats']['date'][i]).split("-");
            mydate = new Date(parts[0], parts[1] - 1, parts[2]);
            var object = {};


            object[fields[0]] = mydate.toLocaleDateString("en-US", options);
            object[fields[1]] = (arraydata[extL]['stats']['actuals'][i]).toFixed(2);
            object[fields[2]] = arraydata[extL]['stats']['lower_bound'][i];
            object[fields[3]] = arraydata[extL]['stats']['upper_bound'][i];
            object[fields[4]] = arraydata[extL]['stats']['trending_slope'][i];
            if (((arraydata[extL]['stats']['anomaly_data']['positive']['date']).includes(arraydata[extL]['stats']['date'][i])) && ((arraydata[extL]['stats']['anomaly_data']['positive']['value']).includes(arraydata[extL]['stats']['actuals'][i]))) {
                object[fields[5]] = false;
            }
            if (((arraydata[extL]['stats']['anomaly_data']['negative']['date']).includes(arraydata[extL]['stats']['date'][i])) && ((arraydata[extL]['stats']['anomaly_data']['negative']['value']).includes(arraydata[extL]['stats']['actuals'][i]))) {
                object[fields[6]] = false;
            }
            // console.log(object);
            trendarr.push(object);
        }

        trendarr2[extL] = trendarr;

        parts = (arraydata[extL]['stats']['date'][0]).split("-");
        mydate = new Date(parts[0], parts[1] - 1, parts[2]);
        var object = {};


        object[fields[0]] = mydate.toLocaleDateString("en-US", options);
        object[fields[4]] = arraydata[extL]['stats']['trending_slope'][0];

        // console.log(object);
        slopearr.push(object);

        //2nd
        parts = (arraydata[extL]['stats']['date'][(arraydata[extL]['stats']['date'].length) - 1]).split("-");
        mydate = new Date(parts[0], parts[1] - 1, parts[2]);
        var object = {};


        object[fields[0]] = mydate.toLocaleDateString("en-US", options);
        object[fields[4]] = arraydata[extL]['stats']['trending_slope'][(arraydata[extL]['stats']['date'].length) - 1];

        // console.log(object);
        slopearr.push(object);

        slopearr2[extL] = slopearr;
    }
    c();
}

$(function () {

    $('input[name="datetimes"]').daterangepicker({
        // timePicker: true,
        singleDatePicker: true,
        autoApply: true,
        autoUpdateInput: false,
        // startDate: bFinalDate,
        // startDate: "10/23/2020",
        // startDate: bFinalDate,
        startDate: moment().subtract(92, 'days'),
        // endDate: moment(),
        maxDate: moment().subtract(3, 'days'),
        ranges: {
            'Last 30 Days': [moment().subtract(32, 'days'), moment()],
            'Last 90 Days': [moment().subtract(92, 'days'), moment()],
        },
        locale: {
            format: 'MMM DD, YYYY',
            firstDay: 1,
        },
        opens: 'left'
    }, function (start, end, label) {
        console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
        // if (label == "Last 90 Days") {
        //     console.log("90");
        // }
        if (label == "Last 30 Days" || label == "Last 90 Days") {
            am4core.disposeAllCharts();
            // document.getElementsByClassName("loader").style.display = "block";
            $('.loader').show();
            console.log('loader');
            startdate = start.format('YYYY-MM-DD');
            enddate = end.format('YYYY-MM-DD');

            bStartDate = start.format('MMM DD, YYYY');
            bEndDate = end.format('MMM DD, YYYY');
            bFinalDate = bStartDate + " - " + bEndDate;

            rangeChange = 1;

            SessionData();

        }
        if (label == "Custom Range") {
            console.log("custome range");
            var singleDate = start.format('YYYY-MM-DD');
            var splitDate = singleDate.split('-');
            var date = new Date(splitDate[0], splitDate[1] - 1, splitDate[2]);

            startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 1);
            endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 7);

            am4core.disposeAllCharts();
            //         // document.getElementsByClassName("loader").style.display = "block";
            $('.loader').show();
            //         console.log('loader');

            startdate = startDate.getFullYear() + "-" + (startDate.getMonth() + 1) + "-" + startDate.getDate();
            enddate = endDate.getFullYear() + "-" + (endDate.getMonth() + 1) + "-" + endDate.getDate();

            bStartDate = months[startDate.getMonth()] + " " + startDate.getDate() + "," + startDate.getFullYear();
            bEndDate = months[endDate.getMonth()] + " " + endDate.getDate() + "," + endDate.getFullYear();
            console.log(bStartDate);
            console.log(bEndDate);
            bFinalDate = bStartDate + " - " + bEndDate;
            console.log(bFinalDate);

            rangeChange = 1;

            SessionData();
        }

    });
});
document.getElementById("dateValue").value = bFinalDate;
// $("#web-select").change(function () {
//     var changeUrl = $(this).val();
//     var splitUrl = changeUrl.split("/");
//     finalUrl = "sc-domain:" + splitUrl[2];
//     am4core.disposeAllCharts();
//     $('.loader').show();
//     rangeChange = 1;
//     SessionData();
// });
function urlChangeFunction() {
    var eu = document.getElementById("url-change");
    var changeUrl = eu.value;
    var splitUrl = changeUrl.split("/");
    finalUrl = "sc-domain:" + splitUrl[2];
    am4core.disposeAllCharts();
    // $("#te").remove();
    // $("grid-div").remove();
    // $("table-div").remove();
    $('.loader').show();
    rangeChange = 1;
    SessionData();
}
// $("#web-select-range").change(function () {
//     var changeUrl = $(this).val();
//     // var splitUrl = changeUrl.split("/");
//     alert(changeUrl);
//     // console.log(changeUrl);
//     // am4core.disposeAllCharts();
//     // $('.loader').show();
//     // rangeChange = 1;
//     // SessionData();
// });
function rangeChangeFunction() {
    var e = document.getElementById("ddlViewBy");
    strUser = e.value;
    am4core.disposeAllCharts();
    $('.loader').show();
    rangeChange = 1;
    SessionData();
}