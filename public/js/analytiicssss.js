// const { data } = require("jquery");
//setting name label variable
var scatterXName;
var scatterYName;
var getMetricsData;
var getauthId = parseInt(getAuthUserId);
var getCoRelationData;
// Array that will hold data for graph
var datafields = ['date', 'data', 'dataX'];
var normalizedfields = ['date', 'normalized', 'normalizedY'];
// variables that will pass data to charts function
var normarr;
var dataarr;
var analyticsoptions = { month: 'short', day: 'numeric' };
//get seleted account and metrics
var selectedMetric;
var selectedAccount = $('#account-change').find(":selected").val();

var storeCorrelate;
var filterKeys;

//Getting correlations analytics metric
$.when(
    jQuery.ajax({
        url: "/getData/correlation/metric",
        method: "get",
        // async: false,
        success: function (result) {
            // console.log(JSON.parse(result));
            storeCorrelate = JSON.parse(result);
            // console.log(storeCorrelate);
            // console.log(Object.keys(storeCorrelate));
            filterKeys = Object.keys(storeCorrelate['analytics_correlate_metric']);
            console.log(filterKeys);
            // console.log(typeof (filterKeys));

        },
    })
).then(function () {
    allMetrics();
});
//Getting correlations analytics metric ended

//Get all metrices name
function allMetrics() {
    jQuery.ajax({
        url: "https://ai.dataclu.com/correlations/ga/available_metrics",
        method: 'get',
        async: false,
        success: function (result) {
            getMetricsData = result;
            metricsDropDown();
        }
    });
}

//Function to add select options acc to API Data
function metricsDropDown() {
    var metrics = document.getElementById('dynamic-metrics');
    var metricsSelectContent = "";
    metricsSelectContent += '<select class="custom-select" onchange="dataChangeFunction()" id="url-change">';
    metricsSelectContent += '<option value="' + getMetricsData['data'][0] + '" selected>' + getMetricsData['data'][0] + '</option>';
    for (var i = 1; i < (getMetricsData['data']).length; i++) {
        metricsSelectContent += '<option value="' + getMetricsData['data'][i] + '">' + getMetricsData['data'][i] + '</option>';
    }
    metricsSelectContent += '</select>';
    metrics.innerHTML += metricsSelectContent;
    selectedMetric = $('#dynamic-metrics').find(":selected").val();
    getCorelationApi()
}


//after getting all the values call get corelation data api
// console.log(getAuthUserId);
function getCorelationApi() {
    jQuery.ajax({
        url: "https://ai.dataclu.com/correlations/ga/metric_with_metrics?user_id=" + getauthId + "&View_Id=" + selectedAccount + "&metric=" + selectedMetric,
        // url: "https://ai.dataclu.com/correlations/ga/metric_with_metrics?user_id=58&View_Id=93296969&metric=avgTimeOnPage",
        method: 'get',
        async: false,
        success: function (result) {
            getCoRelationData = result;
            // console.log(result)
            $('.loader').hide();
            createTable();
            // console.log(result)
        }
    });
}
function createTable() {
    var contContainer = document.getElementById('corelations-list');
    var contTableContent = "";
    contTableContent += '<div class="at-chartlistview" data-label="Example" class="df-example demo-table"><table  class="table at-tablechartview" id="example2"><thead><tr><th><span></span></th><th><span>Name</span></th>' +
        '<th><span>Corelation Coefficent</span></th><th><span>Leading/Lagging</span></th></tr></thead><tbody>';
    for (var j = 0; j < Object.keys(getCoRelationData['data']['Y']).length; j++) {
        contTableContent += '<tr><td><button class="scatter-id-div" id="' + j + '" onclick="scatterGraphData(this)" ><img src=\"/images/ga.png\" class=\"tabletoolimg\"  /></button></td><td><h5 class="table-headText" ' +
            '">' + getCoRelationData['data']['Y'][j]['name'] + '</h5></td>' +
            '<td><h5 class="mg-b-15">' + getCoRelationData['data']['Y'][j]['correlation_coeff'] +
            '</h5></td>';
        if (filterKeys.includes(getCoRelationData['data']['Y'][j]['name'])) {
            contTableContent += '<td style="color:#2ecc71">Leading</td></tr>';
        }
        else {
            contTableContent += '<td style="color:#e74c3c">Lagging</td></tr>';
        }
    }
    contTableContent += '</tbody></table></div>';
    contContainer.innerHTML += contTableContent;

}
function scatterGraphData(obj) {
    // console.log(obj.id);
    dataarr = [];
    normarr = [];

    //This is seperated for name

    scatterYName = capitalizeFirstLetter((getCoRelationData['data']['Y'][obj.id]['name']));
    scatterXName = capitalizeFirstLetter((getCoRelationData['data']['X']['name']));


    //Seperated End

    for (var k = 0; k < (getCoRelationData['data']['X']['dates']).length; k++) {
        var analyticsparts = (getCoRelationData['data']['X']['dates'][k]).split("-");
        var analyticsmydate = new Date(analyticsparts[0], analyticsparts[1] - 1, analyticsparts[2]);

        var object = {};
        var object2 = {};


        object[datafields[0]] = analyticsmydate.toLocaleDateString("en-US", analyticsoptions);
        object[datafields[1]] = (getCoRelationData['data']['Y'][obj.id]['data'][k]);
        object[datafields[2]] = (getCoRelationData['data']['X']['data'][k]);

        object2[normalizedfields[0]] = analyticsmydate.toLocaleDateString("en-US", analyticsoptions);
        object2[normalizedfields[1]] = (getCoRelationData['data']['Y'][obj.id]['normalized'][k]);
        object2[normalizedfields[2]] = (getCoRelationData['data']['X']['normalized'][k]);

        // console.log(object);
        dataarr.push(object);
        normarr.push(object2);

    }
    // console.log('Charts Data');
    // console.log(dataarr);
    // console.log(normarr);


    var checkDisplay = document.getElementById('scatter-show-hide');
    if (checkDisplay.style.display == 'none') {
        checkDisplay.style.display = 'block';
        am4core.ready(function () {


            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            var chart = am4core.create("chartContainerScatter1", am4charts.XYChart);
            // chart.layout = "grid";
            chart.height = am4core.percent(100);
            chart.paddingRight = 10;
            chart.paddingLeft = 10;
            chart.logo.disabled = true;
            chart.data = dataarr;
            // console.log(dataarr);

            // Create axes
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "dataX";
            categoryAxis.renderer.minGridDistance = 50;
            // categoryAxis.renderer.grid.template.location = 0.5;
            // categoryAxis.startLocation = 0.5;
            // categoryAxis.endLocation = 0.5;
            // categoryAxis.renderer.grid.template.disabled = true;
            // categoryAxis.renderer.labels.template.disabled = true;
            categoryAxis.cursorTooltipEnabled = false;

            // Create value axis
            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.baseValue = 0;
            valueAxis.renderer.grid.template.disabled = true;
            valueAxis.renderer.baseGrid.disabled = true;
            // valueAxis.renderer.labels.template.disabled = true;
            valueAxis.cursorTooltipEnabled = false;

            // Create series
            var series = chart.series.push(new am4charts.LineSeries());
            series.dataFields.valueY = "data";
            series.dataFields.categoryX = "dataX";
            series.name = scatterYName;
            series.stroke = am4core.color("darkblue");
            series.fill = am4core.color("darkblue");
            series.strokeOpacity = 0;

            // // Create series2
            // var series2 = chart.series.push(new am4charts.LineSeries());
            // series2.dataFields.valueY = "dataX";
            // series2.dataFields.categoryX = "date";
            // series2.name = scatterXName;
            // series2.stroke = am4core.color("darkblue");
            // series2.fill = am4core.color("darkblue");
            // series2.strokeOpacity = 0;
            // // series2.stroke = am4core.color("#ccc");

            // bullet is added because we add tooltip to a bullet for it to change color
            var bullet = series.bullets.push(new am4charts.Bullet());
            bullet.tooltipText = scatterYName + ` : {valueY} \n \n` +
                scatterXName + ` : {dataX}`;


            // Add a bullet
            var bullet = series.bullets.push(new am4charts.Bullet());

            // Add a circle to act as am arrow
            var arrow = bullet.createChild(am4core.Circle);
            arrow.horizontalCenter = "middle";
            arrow.verticalCenter = "middle";
            arrow.strokeWidth = 0;
            arrow.fill = am4core.color("darkblue");
            arrow.direction = "top";
            arrow.width = 6;
            arrow.height = 6;


            // // Add a bullet
            // var bullet2 = series2.bullets.push(new am4charts.Bullet());

            // // Add a circle to act as am arrow
            // var arrow2 = bullet2.createChild(am4core.Circle);
            // arrow2.horizontalCenter = "middle";
            // arrow2.verticalCenter = "middle";
            // arrow2.strokeWidth = 0;
            // arrow2.fill = am4core.color("darkblue");
            // arrow2.direction = "top";
            // arrow2.width = 6;
            // arrow2.height = 6;




            /* Add legend */
            chart.legend = new am4charts.Legend();
            chart.legend.position = "top";
            chart.legend.useDefaultMarker = true;

            var marker = chart.legend.markers.template.children.getIndex(0);
            marker.cornerRadius(12, 12, 12, 12);
            marker.strokeWidth = 2;
            marker.strokeOpacity = 1;
            marker.stroke = am4core.color("#ccc");

            chart.cursor = new am4charts.XYCursor();


        });
        //Second Graph Code for Normalized data
        //second
        am4core.ready(function () {


            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            var chart = am4core.create("chartdivScatter2", am4charts.XYChart);
            chart.layout = "grid";
            chart.height = am4core.percent(100);
            chart.paddingRight = 10;
            chart.paddingLeft = 10;
            chart.logo.disabled = true;
            chart.data = normarr;

            // Create axes
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "date";
            categoryAxis.renderer.minGridDistance = 50;
            // categoryAxis.renderer.grid.template.location = 0.5;
            // categoryAxis.startLocation = 0.5;
            // categoryAxis.endLocation = 0.5;
            // categoryAxis.renderer.grid.template.disabled = true;
            // categoryAxis.renderer.labels.template.disabled = true;
            categoryAxis.renderer.labels.template.rotation = 270;
            categoryAxis.cursorTooltipEnabled = false;

            // Create value axis
            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.baseValue = 0;
            valueAxis.renderer.grid.template.disabled = true;
            valueAxis.renderer.baseGrid.disabled = true;
            valueAxis.renderer.labels.template.disabled = true;
            valueAxis.cursorTooltipEnabled = false;

            // Create series
            var series = chart.series.push(new am4charts.LineSeries());
            series.dataFields.valueY = "normalized";
            series.dataFields.categoryX = "date";
            // series.strokeOpacity = 0;
            series.name = scatterYName;
            series.stroke = am4core.color("gold");
            series.fill = am4core.color("gold");
            series.strokeWidth = 3;

            // Create series
            var series2 = chart.series.push(new am4charts.LineSeries());
            series2.dataFields.valueY = "normalizedY";
            series2.dataFields.categoryX = "date";
            // series2.strokeOpacity = 0;
            series2.name = scatterXName;
            series2.stroke = am4core.color("darkblue");
            series2.fill = am4core.color("darkblue");
            series2.strokeWidth = 3;


            // bullet is added because we add tooltip to a bullet for it to change color
            var bullet = series.bullets.push(new am4charts.Bullet());
            bullet.tooltipText = bullet.tooltipText = scatterYName + ` : {valueY} \n \n` +
                scatterXName + ` : {normalizedY}`;


            // Add a bullet
            var bullet = series.bullets.push(new am4charts.Bullet());

            // Add a circle to act as am arrow
            var arrow = bullet.createChild(am4core.Circle);
            arrow.horizontalCenter = "middle";
            arrow.verticalCenter = "middle";
            arrow.strokeWidth = 0;
            arrow.fill = am4core.color("gold");
            arrow.direction = "top";
            arrow.width = 6;
            arrow.height = 6;

            // Add a bullet
            var bullet2 = series2.bullets.push(new am4charts.Bullet());

            // Add a circle to act as am arrow
            var arrow2 = bullet2.createChild(am4core.Circle);
            arrow2.horizontalCenter = "middle";
            arrow2.verticalCenter = "middle";
            arrow2.strokeWidth = 0;
            arrow2.fill = am4core.color("darkblue");
            arrow2.direction = "top";
            arrow2.width = 6;
            arrow2.height = 6;

            /* Add legend */
            chart.legend = new am4charts.Legend();
            chart.legend.position = "top";
            chart.legend.useDefaultMarker = true;

            var marker = chart.legend.markers.template.children.getIndex(0);
            marker.cornerRadius(12, 12, 12, 12);
            marker.strokeWidth = 2;
            marker.strokeOpacity = 1;
            marker.stroke = am4core.color("#ccc");

            chart.cursor = new am4charts.XYCursor();

        });
    }
    else {
        // checkDisplay.style.display = 'none';
        am4core.disposeAllCharts();
        //second
        am4core.ready(function () {


            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            var chart = am4core.create("chartContainerScatter1", am4charts.XYChart);
            // chart.layout = "grid";
            chart.height = am4core.percent(100);
            chart.paddingRight = 10;
            chart.paddingLeft = 10;
            chart.logo.disabled = true;
            chart.data = dataarr;

            // Create axes
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "dataX";
            categoryAxis.renderer.minGridDistance = 50;
            // categoryAxis.renderer.grid.template.location = 0.5;
            // categoryAxis.startLocation = 0.5;
            // categoryAxis.endLocation = 0.5;
            // categoryAxis.renderer.grid.template.disabled = true;
            // categoryAxis.renderer.labels.template.disabled = true;
            categoryAxis.cursorTooltipEnabled = false;

            // Create value axis
            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.baseValue = 0;
            valueAxis.renderer.grid.template.disabled = true;
            valueAxis.renderer.baseGrid.disabled = true;
            // valueAxis.renderer.labels.template.disabled = true;
            valueAxis.cursorTooltipEnabled = false;

            // Create series
            var series = chart.series.push(new am4charts.LineSeries());
            series.dataFields.valueY = "data";
            series.dataFields.categoryX = "dataX";
            series.name = scatterYName;
            series.stroke = am4core.color("darkblue");
            series.fill = am4core.color("darkblue");
            series.strokeOpacity = 0;

            // // Create series
            // var series2 = chart.series.push(new am4charts.LineSeries());
            // series2.dataFields.valueY = "dataX";
            // series2.dataFields.categoryX = "date";
            // series2.name = scatterXName;
            // series2.stroke = am4core.color("darkblue");
            // series2.fill = am4core.color("darkblue");
            // series2.strokeOpacity = 0;


            // bullet is added because we add tooltip to a bullet for it to change color
            var bullet = series.bullets.push(new am4charts.Bullet());
            bullet.tooltipText = scatterYName + ` : {valueY} \n \n` +
                scatterXName + ` : {dataX}`;


            // Add a bullet
            var bullet = series.bullets.push(new am4charts.Bullet());

            // Add a circle to act as am arrow
            var arrow = bullet.createChild(am4core.Circle);
            arrow.horizontalCenter = "middle";
            arrow.verticalCenter = "middle";
            arrow.strokeWidth = 0;
            arrow.fill = am4core.color("darkblue");
            arrow.direction = "top";
            arrow.width = 6;
            arrow.height = 6;

            // // Add a bullet
            // var bullet2 = series2.bullets.push(new am4charts.Bullet());

            // // Add a circle to act as am arrow
            // var arrow2 = bullet2.createChild(am4core.Circle);
            // arrow2.horizontalCenter = "middle";
            // arrow2.verticalCenter = "middle";
            // arrow2.strokeWidth = 0;
            // arrow2.fill = am4core.color("darkblue")
            // arrow2.direction = "top";
            // arrow2.width = 6;
            // arrow2.height = 6;

            /* Add legend */
            chart.legend = new am4charts.Legend();
            chart.legend.position = "top";
            chart.legend.useDefaultMarker = true;

            var marker = chart.legend.markers.template.children.getIndex(0);
            marker.cornerRadius(12, 12, 12, 12);
            marker.strokeWidth = 2;
            marker.strokeOpacity = 1;
            marker.stroke = am4core.color("#ccc");

            chart.cursor = new am4charts.XYCursor();

        });

        //Second Graph Code for Normalized data
        //second
        am4core.ready(function () {


            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            var chart = am4core.create("chartdivScatter2", am4charts.XYChart);
            chart.layout = "grid";
            chart.height = am4core.percent(100);
            chart.paddingRight = 10;
            chart.paddingLeft = 10;
            chart.logo.disabled = true;
            chart.data = normarr;

            // Create axes
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "date";
            categoryAxis.renderer.minGridDistance = 50;
            // categoryAxis.renderer.grid.template.location = 0.5;
            // categoryAxis.startLocation = 0.5;
            // categoryAxis.endLocation = 0.5;
            // categoryAxis.renderer.grid.template.disabled = true;
            // categoryAxis.renderer.labels.template.disabled = true;
            categoryAxis.renderer.labels.template.rotation = 270;
            categoryAxis.cursorTooltipEnabled = false;

            // Create value axis
            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.baseValue = 0;
            valueAxis.renderer.grid.template.disabled = true;
            valueAxis.renderer.baseGrid.disabled = true;
            valueAxis.renderer.labels.template.disabled = true;
            valueAxis.cursorTooltipEnabled = false;

            // Create series
            var series = chart.series.push(new am4charts.LineSeries());
            series.dataFields.valueY = "normalized";
            series.dataFields.categoryX = "date";
            series.stroke = am4core.color("gold");
            series.fill = am4core.color("gold");
            series.name = scatterYName;
            // series.strokeOpacity = 0;
            series.strokeWidth = 3;

            // Create series
            var series2 = chart.series.push(new am4charts.LineSeries());
            series2.dataFields.valueY = "normalizedY";
            series2.dataFields.categoryX = "date";
            // series2.strokeOpacity = 0;
            series2.name = scatterXName;
            series2.stroke = am4core.color("darkblue");
            series2.fill = am4core.color("darkblue");
            series2.strokeWidth = 3;


            // bullet is added because we add tooltip to a bullet for it to change color
            var bullet = series.bullets.push(new am4charts.Bullet());
            bullet.tooltipText = scatterYName + ` : {valueY} \n \n` +
                scatterXName + ` : {normalizedY}`;


            // Add a bullet
            var bullet = series.bullets.push(new am4charts.Bullet());

            // Add a circle to act as am arrow
            var arrow = bullet.createChild(am4core.Circle);
            arrow.horizontalCenter = "middle";
            arrow.verticalCenter = "middle";
            arrow.strokeWidth = 0;
            arrow.fill = am4core.color("gold");
            arrow.direction = "top";
            arrow.width = 6;
            arrow.height = 6;

            // Add a bullet
            var bullet2 = series2.bullets.push(new am4charts.Bullet());

            // Add a circle to act as am arrow
            var arrow2 = bullet2.createChild(am4core.Circle);
            arrow2.horizontalCenter = "middle";
            arrow2.verticalCenter = "middle";
            arrow2.strokeWidth = 0;
            arrow2.fill = am4core.color("darkblue");
            arrow2.direction = "top";
            arrow2.width = 6;
            arrow2.height = 6;

            /* Add legend */
            chart.legend = new am4charts.Legend();
            chart.legend.position = "top";
            chart.legend.useDefaultMarker = true;

            var marker = chart.legend.markers.template.children.getIndex(0);
            marker.cornerRadius(12, 12, 12, 12);
            marker.strokeWidth = 2;
            marker.strokeOpacity = 1;
            marker.stroke = am4core.color("#ccc");


            chart.cursor = new am4charts.XYCursor();

        });
    }

}

function dataChangeFunction() {
    selectedMetric = $('#dynamic-metrics').find(":selected").val();
    selectedAccount = $('#account-change').find(":selected").val();

    $("#corelations-list").empty();
    am4core.disposeAllCharts();
    //     $("#te").empty();
    $('.loader').show();
    getCorelationApi();

    // console.log(selectedAccount + "Finally" + selectedMetric);
}
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}