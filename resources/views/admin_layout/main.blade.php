<!doctype html>
<html lang="en">

@include('admin_layout.head')

<body class="home-body">

    @if (Auth::check())
    @include('admin_layout.header')
    @endif



    @yield('content')


    <script src="{{asset('lib/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('lib/feather-icons/feather.min.js')}}"></script>
    <script src="{{asset('lib/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
    <script src="{{asset('lib/chart.js/Chart.bundle.min.js')}}"></script>
    <script src="{{asset('lib/jquery.flot/jquery.flot.js')}}"></script>
    <script src="{{asset('lib/jquery.flot/jquery.flot.stack.js')}}"></script>
    <script src="{{asset('lib/jquery.flot/jquery.flot.resize.js')}}"></script>

    <script src="{{asset('assets/js/dashforge.js')}}"></script>
    <script src="{{asset('assets/js/dashforge.sampledata.js')}}"></script>
    <script src="{{asset('assets/js/dashboard-two.js')}}"></script>

    <!-- append theme customizer -->
    <script src="{{asset('lib/js-cookie/js.cookie.js')}}"></script>
    <script src="{{asset('assets/js/dashforge.settings.js')}}"></script>
    <script>
    $(document).ready(function() {
        'use strict'

        $('[data-toggle="tooltip"]').tooltip()


    });
    </script>
    @yield('page-level-js')
</body>

</html>