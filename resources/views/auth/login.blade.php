<!DOCTYPE html>
<html lang="en">

<head>
    <title>{{env('APP_NAME') }} | Login</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="{{asset('login_assets/css/util.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('login_assets/css/main.css') }}">
    <link
        href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap"
        rel="stylesheet">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{asset('css/customs.css')}}">
    <link rel="stylesheet" href="{{asset('css/responsive.css')}}">
    <!--===============================================================================================-->
</head>

<body>
    <div class="at-loginarea">
        <div class="at-logincontentholder">
            <div class="at-logincontent">
                <strong class="at-logo">
                    <img src="{{asset('images/round-logo.png')}}" alt="Logo Image">
                </strong>
                <div class="at-description">
                    <p>Stay on top of new trends and anomalies while not getting <span>overwhelmed by the amount of new
                            analytics data.</span></p>
                </div>
                <a href="{{url('/google/login')}}" class="at-btngooglelogin">
                     <span class="at-btngoogleicon"><img src="{{asset('images/google-3.png')}}"
                            class="google-img"></span>
                    <em>Login with google</em>
                </a>
                 <a href="{{url('/guestlogin')}}" class="at-btngooglelogin" style="margin-top:10px; padding-left:0px; text-align:center; background: #1e266d">

                      
                     Login As a Guest
                 </a>
                <div class="at-createaccount">
                    <span>Are you new around here?</span>
                    <a href="javascript:void(0);">CREATE ACCOUNT</a>
                </div>
            </div>
        </div>
        <div class="at-loginimageholder">
            <figure class="at-liginmanimage">
                <img src="{{asset('images/login-img2.png')}}" alt="Login Image">
            </figure>
        </div>
    </div>
    {{--<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form">
					<span class="login100-form-title p-b-34">
						<span style="color:#031a61 !important">Dark</span><span style="color:#4287f5 !important">Data</span>
					</span>

					<span class=" p-b-34" style="text-align: center">
						Stay on top of new trends and anomalies while not getting overwhelmed by the amount of new analytics data.
					</span>

                <a href="{{url('/google/login')}}" class="loginbutton">
     <span class="loginbutton-inside"><img src="{{asset('images/google.png')}}" class="google-img" /></span>
     LOGIN WITH GOOGLE
    </a>


    <div class="w-full text-center p-t-27 p-b-239">

    </div>


    </form>

    <div class="login100-more" style="background-image: url('login_assets/images/capture.png');"></div>
    </div>
    </div>
    </div>



    <div id="dropDownSelect1"></div>--}}



</body>

</html>