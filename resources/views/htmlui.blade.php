@extends('admin_layout.main')
@section('page-title')
User Dashboard
@endsection
@section('page-level-css')
<link rel="stylesheet" href="{{asset('css/productDetail.css')}}">
<!-- <link rel="stylesheet" href="{{asset('assets/css/dashforge.demo.css')}}"> -->
<link rel="stylesheet" href="{{asset('css/custom.css')}}">
<style>
#chartdiv {
  width: 100%;
  height: 200px;
}
#chartdivs {
  width: 100%;
  height: 250px;
}
</style>
@endsection
@section('content')

 
    <div class="content content-fixed">
      <div class="container">
        <!-- <ol class="breadcrumb df-breadcrumbs mg-b-10">
          <li class="breadcrumb-item"><a href="#">Components</a></li>
          <li class="breadcrumb-item active" aria-current="page">Introduction</li>
        </ol> -->
        <div class="row">
        <div class="col-10 col-xl-11">
        <h4 class="df-title pr-text">Product Detail Views Increased last Week</h4>
        <p class="df-lead head-date">26-Oct-2020(Monday) - 01-Nov-2020(Sunday)</p>
        </div>
        <div class="col-2 col-xl-1 box-width">
        <div class="shadow bg-white bd cu-bd-5 right-head">
              <h5 class=" head-number">6</h5>
              <p class="head-text">Pages Views</p>
        </div>
        </div>
        </div>

        <div class="row tx-14">
          <div class="col-12">
            <div class=" shadow cu-bd-15 bg-white bd pd-20 pd-lg-30 d-flex flex-column justify-content-end">
            <div class="at-charthead">
            <h5 class=" headText" style="font-weight:normal !important"> Anomaly Details</h5>
            </div>
            <div class="row">
            <div class="col-3 vr-l" >


            <div class="at-chartheads">
            <h5 class=" headText mg-b-20" > 32% more than expected on <b>26-Oct-2020 - 01-Nov-2020</b></h5>
            </div>
            <div class="at-chartheadss">
            <h5 class=" headText mg-b-20" > <b>Actual Value:</b> 6,360 <br><b>Expected Value:</b>4,810</h5>
            </div>
          
            </div>
            <div class="col-3 vr-l">
            <div id="chartdiv" class="pie-chart"></div>
            <p class="pie-text">Covers <b>100%</b> of total Product Details View</p>
            </div>
            <div class="col-6" >
            <div id="chartdivs" class="pie-chart"></div>
            </div>
</div>
</div>
          </div><!-- col-12 -->
          <div class="col-12 mg-t-20 mg-sm-t-25">
          <div class="row">
            <div class="col-12">
            <h2 class="action-line"><span>Take Action</span></h2>
            <h5 class="an-text"> Share the anomalies</h5>
            </div>
</div>
<div class="row btn-sp">
            <div class="col-2">
            <button type="button" class="btn btn-primary btn-icon cu-btn"><i class='icon-link cu-btn-icon'></i><span class="cu-btn-text">Link</span></button>
            </div>
            <div class="col-2">
            <button type="button" class="btn btn-danger btn-icon cu-btn"><i data-feather="mail" class="cu-btn-icon"></i><span class="cu-btn-text">Email</span></button> 
            </div>
            <div class="col-2">
            <button type="button" class="btn btn-success btn-icon cu-btn"><i class='icon-whatsapp cu-btn-icon'></i><span class="cu-btn-text">Whatsapp</span></button>  
          </div>
            <div class="col-2">
            <button type="button" class="btn btn-skype btn-icon cu-btn"><i class='icon-skype cu-btn-icon'></i><span class="cu-btn-text">Skype</span></button>  
            </div>
            <div class="col-2">
            <button type="button" class="btn btn-stack btn-icon cu-btn"><i class='icon-stack cu-btn-icon'></i><span class="cu-btn-text">Stack</span></button>
            </div>
            <div class="col-2">
            <button type="button" class="btn btn-primary btn-icon cu-btn"><i class='icon-link cu-btn-icon'></i><span class="cu-btn-text">Trello</span></button>
            </div>
          </div>
          
          </div><!-- col-12 -->
          <div class="col-12 mg-t-20 mg-sm-t-25">
          <div class="shadow cu-bd-5 bg-white bd pd-20 pd-lg-30  d-flex flex-column justify-content-end">
              <!-- <div class="mg-b-25"><i datsa-feather="map-pin" class="wd-50 ht-50 tx-gray-500"></i></div> -->
              <h5 class="tx-inverse mg-b-20">Comments</h5>
              <div class="col-sm-12 mg-t-10">
              <textarea class="form-control" rows="3" placeholder="Add a Comment"></textarea>
              <button type="button" class="btn btn-primary btn-lg btn-send">Send</button>
            </div>
            </div>
          </div>
         
        </div><!-- row -->

        

      </div><!-- container -->
    </div><!-- content -->

@endsection
@section('page-level-js')
<!-- Resources -->
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>

<!-- Chart code -->
<script>
am4core.ready(function() {

// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
var chart = am4core.create("chartdiv", am4charts.PieChart);
chart.height = 200;
chart.logo.disabled = true;

// Add data
chart.data = [ {
  "country": "Lithuania",
  "litres": 501.9
}
];

// Add and configure Series
var pieSeries = chart.series.push(new am4charts.PieSeries());
pieSeries.dataFields.value = "litres";
pieSeries.dataFields.category = "country";
pieSeries.slices.template.stroke = am4core.color("#fff");
pieSeries.slices.template.strokeOpacity = 1;
pieSeries.labels.template.disabled = true;
pieSeries.ticks.template.disabled = true;

// This creates initial animation
pieSeries.hiddenState.properties.opacity = 1;
pieSeries.hiddenState.properties.endAngle = -90;
pieSeries.hiddenState.properties.startAngle = -90;

chart.hiddenState.properties.radius = am4core.percent(0);

chart.legend = new am4charts.Legend();
chart.legend.position = "top";

chart.legend.useDefaultMarker = true;
var marker = chart.legend.markers.template.children.getIndex(0);
marker.cornerRadius(12, 12, 12, 12);
marker.strokeWidth = 2;
marker.strokeOpacity = 1;
marker.stroke = am4core.color("#ccc");


}); // end am4core.ready()
</script>



<!-- Chart code -->
<script>
/* Create chart instance */
var chart = am4core.create("chartdivs", am4charts.XYChart);
chart.logo.disabled = true;

/* Add data */
chart.data = [{
  "year": "01 Jun",
  "actual": 2000,
  "expected": 650
}, {
  "year": "25 Sept",
  "actual": 5500,
  "expected": 683
  
}, {
  "year": "16 Oct",
  "actual": 2200,
  "expected": 691
  
}, {
  "year": "09 Nov",
  "actual": 4500,
  "expected": 642
  
}, {
  "year": "16 Dec",
  "actual": 1200,
  "expected": 699
  
}, {
  "year": "23 Jan",
  "actual": 3300,
  "expected": 721
}];

/* Create axes */
var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.dataFields.category = "year";
categoryAxis.renderer.grid.template.disabled = true;

/* Create value axis */
var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
valueAxis.renderer.opposite = true;
valueAxis.renderer.grid.template.disabled = true;

/* Create series */
var series1 = chart.series.push(new am4charts.LineSeries());
series1.dataFields.valueY = "actual";
series1.dataFields.categoryX = "year";
series1.name = "actual";
series1.strokeWidth = 3;
series1.tensionX = 0.7;
// series1.tooltipText = "{categoryX}: [bold]{valueY}[/]";


var series2 = chart.series.push(new am4charts.LineSeries());
series2.dataFields.valueY = "expected";
series2.dataFields.categoryX = "year";
series2.name = "expected";
series2.strokeWidth = 3;
series2.tensionX = 0.7;
// series2.tooltipText = "{categoryX}: [bold]{valueY}[/]";


/* Add legend */
chart.legend = new am4charts.Legend();
chart.legend.position = "top";

chart.legend.useDefaultMarker = true;
var marker = chart.legend.markers.template.children.getIndex(0);
marker.cornerRadius(12, 12, 12, 12);
marker.strokeWidth = 2;
marker.strokeOpacity = 1;
marker.stroke = am4core.color("#ccc");

/* Create a cursor */
chart.cursor = new am4charts.XYCursor();
</script>


@endsection
