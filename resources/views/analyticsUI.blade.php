@extends('admin_layout.main')
@section('page-title')
User Dashboard
@endsection
@section('page-level-css')
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    <style>
    #chartdiv {
      width: 100%;
      height: 200px;
    }
    </style>
@endsection
@section('content')


 <div class="content content-fixed">
      <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0">
      <div class="customwidth">

        <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
          <div>
          <h4 class="mg-b-0 tx-spacing--1 hc">Dark<span style="color:#4287f5 !important">Data</h4></h4>
          <h4 class="mg-b-0 tx-spacing--1 lhc">Good Morning, {{ auth()->user()->first_name }}</h4>
            <h4 class="mg-b-0 tx-spacing--1 slhc">Here what our DarkData Center has filter out the insights of your account.</h4>
           
          </div>
          <div class="d-none d-md-block">
          <input type="text" name="daterange" value="Jan 02, 2020 - Jan 04,2020" class="daterangecss" />
            <button class="btn btn-sm pd-x-15 btn-white btn-uppercase mg-l-5"><i data-feather="upload" class="wd-10 mg-r-5"></i> Export</button>
            <button class="btn btn-sm pd-x-15 btn-white btn-uppercase mg-l-5"><i data-feather="share-2" class="wd-10 mg-r-5"></i> Share</button>
            <button class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5"><i data-feather="sliders" class="wd-10 mg-r-5"></i> Settings</button>
          </div>
        </div>

         <div class="row row-xs">
            <!-- 1 -->
        <div class=" col-xs-12 col-sm-12 col-md-6 col-lg-3 col-xl-3 ">
            <div class="box2">
        <h5 class="mg-b-15 textalign headText" > Trend Analysis </h5>
        <!-- <h5 class="mg-b-30 textalign daysText"> Last 30 Days <i class="arrow down"></i></h5> -->
        <select name="trendsdropdown" id="trendsdropdown">
          <option value="30TrendDays">Last 30 Days</option>
          <option value="90TrendDays">Last 90 Days</option>
        </select>
        <h5 class="mg-b-10 textalign dynamicStats">439882</h5>
        <div id="chartsdivs" class="customchart"  ></div>
        <h5 class="mg-b- textalign daysText"><i> <img src="{{asset('images/up.png')}}" class="dropImage"/></i><span class="percentcss">-16%</span><span class="textLight"> (31350)</span> </h5>
            </div>
        </div>

            <!-- 1 -->
        <div class=" col-xs-12 col-sm-12 col-md-6 col-lg-3 col-xl-3 ">
            <div class="box">
        <h5 class="mg-b-15 textalign headText" > Anomalies </h5>
        <!-- <h5 class="mg-b-30 textalign daysText"> Last 30 Days <i class="arrow down"></i></h5> -->
        <select name="anomaliesdropdown" id="anomaliesdropdown">
          <option value="30TrendDays">Last 30 Days</option>
          <option value="90TrendDays">Last 90 Days</option>
        </select>
        <h5 class="mg-b-10 textalign dynamicStats">3230</h5>
        <div id="chartsdivsanomalies" class="customchart"  ></div>
        <h5 class="mg-b- textalign daysText"><i> </i><span class="percentcss"> - </span><span class="textLight"> (784)</span> </h5>
            </div>
        </div>

            <!-- 1 -->
        <div class=" col-xs-12 col-sm-12 col-md-6 col-lg-3 col-xl-3 ">
            <div class="box2">
        <h5 class="mg-b-15 textalign headText" > Browser (Session)  </h5>
        <!-- <h5 class="mg-b-30 textalign daysText"> Last 30 Days <i class="arrow down"></i></h5> -->
        <select name="browsersessdropdown" id="browsersessdropdown">
          <option value="30TrendDays">Last 30 Days</option>
          <option value="90TrendDays">Last 90 Days</option>
        </select>
        <h5 class="mg-b-10 textalign dynamicStats">02323</h5>
        <div id="chartsdivsessionbybrowser" class="customchart"  ></div>
        <h5 class="mg-b- textalign daysText"><i> <img src="{{asset('images/up.png')}}" class="dropImage"/></i><span class="percentcss">61%</span><span class="textLight"> (8292)</span> </h5>
            </div>
        </div>

            <!-- 1 -->
        <div class=" col-xs-12 col-sm-12 col-md-6 col-lg-3 col-xl-3 ">
            <div class="box2">
        <h5 class="mg-b-15 textalign headText" > Bounce Rate </h5>
        <!-- <h5 class="mg-b-30 textalign daysText"> Last 30 Days <i class="arrow down"></i></h5> -->
        <select name="Bouncedropdown" id="Bouncedropdown">
          <option value="30TrendDays">Last 30 Days</option>
          <option value="90TrendDays">Last 90 Days</option>
        </select>
        <h5 class="mg-b-10 textalign dynamicStats">22022</h5>
         <div id="chartsdivbounce" class="customchart"  ></div>
        <h5 class="mg-b- textalign daysText"><i> <img src="{{asset('images/up.png')}}" class="dropImage"/></i><span class="percentcss">34%</span><span class="textLight"> 3822</span> </h5>
            </div>
        </div>

            <!-- 1 -->
        <div class=" col-xs-12 col-sm-12 col-md-6 col-lg-3 col-xl-3 ">
            <div class="box2">
        <h5 class="mg-b-15 textalign headText" > Country Clicks </h5>
        <!-- <h5 class="mg-b-30 textalign daysText"> Last 30 Days <i class="arrow down"></i></h5> -->
        <select name="Clickdropdown" id="Clickdropdown">
          <option value="30TrendDays">Last 30 Days</option>
          <option value="90TrendDays">Last 90 Days</option>
        </select>
        <h5 class="mg-b-10 textalign dynamicStats">439882</h5>
        <div id="chartsdivclickbycountry" class="customchart"  ></div>
        <h5 class="mg-b- textalign daysText"><i> <img src="{{asset('images/up.png')}}" class="dropImage"/></i><span class="percentcss">9%</span><span class="textLight"> (31350)</span> </h5>
            </div>
        </div>

             <!-- 1 -->
        <div class=" col-xs-12 col-sm-12 col-md-6 col-lg-3 col-xl-3 ">
            <div class="box">
        <h5 class="mg-b-15 textalign headText" > Traffic Source </h5>
        <!-- <h5 class="mg-b-30 textalign daysText"> Last 30 Days <i class="arrow down"></i></h5> -->
        <select name="trafficdropdown" id="trafficdropdown">
          <option value="30TrendDays">Last 30 Days</option>
          <option value="90TrendDays">Last 90 Days</option>
        </select>
        <h5 class="mg-b-10 textalign dynamicStats">09342</h5>
        <div id="chartsdivtraffic" class="customchart"  ></div>
        <h5 class="mg-b- textalign daysText"><i> </i><span class="percentcss"> - </span><span class="textLight"> (3423)</span> </h5>
            </div>
        </div>


        </div>
        </div><!-- row -->
<!-- 
        <div class="row row-xs">
        <div class="col-3" style="border: 1px solid lightgrey">
        <h5 class="mg-b-5" style="text-align:center; margin-top:60px;"> Sessions (Browser Wise)</h5>
        <h5 class="mg-b-5" style="text-align:center"> Last 30 Days </h5>
        <h5 class="mg-b-5" style="text-align:center">{{$totalsession}}</h5>
      <div id="chartdiv"   ></div>
        </div>
        <div class="col-3" style="border: 1px solid lightgrey">
         <h5 class="mg-b-5" style="text-align:center; margin-top:60px;"> Page View (Country Wise)</h5>
        <h5 class="mg-b-5" style="text-align:center"> Last 30 Days </h5>
        <h5 class="mg-b-5" style="text-align:center">{{$totalview}}</h5>
      <div id="countchartdiv" ></div>
        </div>
        
        <div class="col-3" style="border: 1px solid lightgrey">
        <h5 class="mg-b-5" style="text-align:center; margin-top:60px;"> User Visits (Page Wise)</h5>
        <h5 class="mg-b-5" style="text-align:center"> Last 30 Days </h5>
        <h5 class="mg-b-5" style="text-align:center">{{$totalpage}}</h5>
      <div id="pagechartdiv" ></div>
        </div>
        <div class="col-3" style="border: 1px solid lightgrey">
        <h5 class="mg-b-5" style="text-align:center; margin-top:60px;"> Bounce Rate (Device Wise)</h5>
        <h5 class="mg-b-5" style="text-align:center"> Last 30 Days </h5>
        <!-- <h5 class="mg-b-5" style="text-align:center">{{$totalpage}}</h5> 
      <div id="bouncechartdiv" ></div>
        </div>
        <div class="col-6">
        <canvas id="myChart"></canvas>
        </div>-->
        <!-- <button class="btn btn-primary" id="ajaxSubmit">Submit</button>  -->
      
        </div><!-- row -->
        </div>
      </div><!-- container -->
    </div><!-- content -->
@endsection
@section('page-level-js')

<!-- Resources -->
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>

<script src="http://code.jquery.com/jquery-3.3.1.min.js"
      integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
      crossorigin="anonymous">
</script>

<script>
var sesData = {!! $data !!};
var counData = {!! $countrydata !!};
var pageData = {!! $pagedata !!};
var bouncData = {!! $bouncdata !!};

var clabel = {!! json_encode($countrylabel)!!};
var cvalue = {!! json_encode($countryvalue)!!};


</script>
 <script src="{{asset('js/googleanalyticsdata.js')}}"></script>
 <script>






    </script>
 
<script src="{{asset('js/staticanalytics.js')}}"></script>
<script>
$(function() {
  $('input[name="daterange"]').daterangepicker({
    locale: {
            format: 'MMM DD, YYYY'
        },
    opens: 'left'
  }, function(start, end, label) {
    console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
  });
});
</script>

@endsection