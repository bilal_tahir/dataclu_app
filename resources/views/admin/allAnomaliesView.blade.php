@extends('admin_layout.main')
@section('page-title')
    User Dashboard
@endsection
@section('page-level-css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    {{-- Further Added DT --}}
    <link href="{{ asset('lib/prismjs/themes/prism-vs.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/datatables.net-dt/css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/select2/css/select2.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/dashforge.demo.css') }}">
    {{-- Further --}}

    <link rel="stylesheet" href="{{ asset('css/consoleAnalyticsHome.css') }}">
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

    <style>
        #chartdiv {
            width: 100%;
            height: 200px;
        }

    </style>
@endsection
@section('content')

    <div class="content content-fixed" id="tee">
        <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0">


            <div class="customwidth" id="tee">


                <div class="at-dashboard">
                    <div class="at-contentholder" id="tee">
                        <div class="at-pagehead">
                            <div class="row custom-head">
                                <div class="col-xl-5 col-lg-4">
                                    <div class="at-pagetitle">
                                        <h2>Good Morning, {{ auth()->user()->first_name }}</h2>
                                        <span>Here what our DataCLU Center has filter out the insights for your
                                            accounts</span>
                                    </div>
                                </div>
                                  <div class="col-xl-2 col-lg-2">
                                      <select class="custom-select" onchange="urlChangeFunction()" id="url-change">
                                          <!-- <option value="Custom" selected>Custom</option> -->
                                          @foreach($consoleAnalyticsList as $wl)
                                          @if($wl['default'] == 1)
                                          <option value="{{$wl['website']}}" selected>
                                              {{$wl['display']}}
                                          </option>
                                          @else
                                          <option value="{{$wl['website']}}">
                                              {{$wl['display']}}
                                              @endif
                                              @endforeach
                                      </select>
                                  </div>
                                <div class="col-xl-5 col-lg-6">
                                    <ul class="at-listgridholder">
                                        <li>
                                            <select class="daterangecss" onchange="rangeChangeFunction()" id="ddlViewBy">
                                                <option value="W" id="weekly-date">Weekly</option>
                                                <option value="D" id="daily-date"> Daily</option>
                                            </select>
                                        </li>
                                        <li>
                                            <a class="at-btnlistgrid g-color" id="grid-color" href="#" onclick="gridview()">
                                                <i class="icon-menu"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="at-btnlistgrid t-color" id="table-color" href="#"
                                                onclick="tableview()">
                                                <i class="icon-list-menu"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <script>
                                var userIdContr =
                                    '<?php echo json_decode($userSingleId); ?>';

                            </script>
                            <!-- <script src="{{ asset('js/rangeapis_headersss.js') }}"></script> -->
                            <div style="width:100%; height:100px; display:none" id="data-check">
                                <h3 style="text-align:center">There is no data associated to this website
                                </h3>
                            </div>
                            <div class="at-chartgridview" id="te">


                            </div>

                            <div class="loader" id="loading">Loading...</div>

                            <div style="text-align: center">
                                <button class="btn btn-outline-dark" id="loadMorebutton" onclick="loadMore()">Load
                                    More</button>
                            </div>
                        </div>
                    </div>
                </div>



            @endsection
            @section('page-level-js')
                <!-- Resources -->
                <script src="https://cdn.amcharts.com/lib/4/core.js"></script>
                <script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
                <script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>
                <script src="//www.amcharts.com/lib/3/plugins/tools/bestFitLine/bestFitLine.min.js"></script>
                {{-- Further added --}}
                <script src="{{ asset('lib/prismjs/prism.js') }}"></script>
                <script src="{{ asset('lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
                <script src="{{ asset('lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
                <script src="{{ asset('lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
                <script src="{{ asset('lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
                <script src="{{ asset('lib/select2/js/select2.min.js') }}"></script>
                {{-- Further Added --}}
                <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js">
                </script>
                <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                <script src="{{ asset('js/allanomaliesaiiisss.js') }}"></script>


                <!-- <script>
        </script> -->
                <script>
                    function gridview() {
                        var x = document.getElementById("table-div");
                        var y = document.getElementById("grid-div");

                        x.style.display = "none";
                        y.style.display = "block";

                        document.getElementById("table-color").style.color = "#4783ff";
                        document.getElementById("table-color").style.backgroundColor = "#fff";
                        document.getElementById("grid-color").style.color = "#fff";
                        document.getElementById("grid-color").style.backgroundColor = "#4783ff";

                    }

                    function tableview() {
                        var x = document.getElementById("table-div");
                        var y = document.getElementById("grid-div");

                        x.style.display = "block";
                        y.style.display = "none";

                        document.getElementById("grid-color").style.color = "#4783ff";
                        document.getElementById("grid-color").style.backgroundColor = "#fff";
                        document.getElementById("table-color").style.color = "#fff";
                        document.getElementById("table-color").style.backgroundColor = "#4783ff";

                    }

                </script>

            @endsection
