@extends('admin_layout.main')
@section('page-title')
User Dashboard
@endsection
@section('page-level-css')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

{{-- Further Added DT --}}
<link href="{{asset('lib/prismjs/themes/prism-vs.css')}}" rel="stylesheet">
<link href="{{asset('lib/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet">
<link href="{{asset('lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet">
<link href="{{asset('lib/select2/css/select2.min.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('assets/css/dashforge.demo.css')}}">
    {{-- Further --}}


<!-- <link href="{{asset('lib/typicons.font/typicons.css')}}" />
    <link href="{{asset('lib/prismjs/themes/prism-vs.css')}}" />
    <link href="{{asset('lib/ion-rangeslider/css/ion.rangeSlider.min.css')}}" /> -->
<!-- <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" /> -->
<!-- <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script> -->
<link rel="stylesheet" href="{{asset('css/custsss.css')}}">
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<!-- <script src="{{asset('js/rangeapi_header.js')}}"></script> -->
<!-- <script src="http://malsup.github.io/jquery.blockUI.js"></script> -->

<style>
#chartdiv {
    width: 100%;
    height: 200px;
}
</style>
@endsection
@section('content')

<div class="content content-fixed" id="tee">
    <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0">


        <div class="customwidth" id="tee">

            <!-- <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
          <div>
          <h4 class="mg-b-0 tx-spacing--1 hc">Dark<span style="color:#4287f5 !important; font-weight:400">Data</h4></h4>
          <h4 class="mg-b-0 tx-spacing--1 lhc">Good Morning, {{ auth()->user()->first_name }}</h4>
          
            <h4 class="mg-b-0 tx-spacing--1 slhc">Here what our DarkData Center has filter out the insights of your account.</h4>
           
          </div>
          <div class="d-none d-md-block">
          <input type="text" name="datetimes" id="dateValue" class="daterangecss" readonly="readonly"/>
          <!-- <p>Date: <input type="text" id="datepicker1"></p> --

          <!-- <input type="text" id="date-picker" class="daterangecss"> --

          <!-- <label for="startDate">Date :</label> -->
            <!-- <input name="startDate" class="date-picker daterangecss" /> --
          <!-- <label>Week :</label> <span id="startDate"></span> - <span id="endDate"></span> --
          <a href="#" onclick="gridview()"><img src="{{asset('images/grid.png')}}"  class="grid-img"  /></a>
            <!-- <button class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5"><i data-feather="sliders" class="wd-10 mg-r-5"></i> Settings</button> --
          </div>
          
        </div> -->
            <!-- <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
          <div>
          <select class="custom-select" id="web-select" >
            {{-- @foreach($webList as $wl)
          <option value="{{$wl->Property_Url}}">{{$wl->Property_Url}}</option>
          @endforeach --}}
        </select>
          </div>
          
        </div> -->

            <!-- <div class ="row"> -->
            <!-- <div class="col-12">
        <h4 class="mg-b-0 tx-spacing--1 hc">Dark<span style="color:#4287f5 !important; font-weight:400">Data</h4></h4>
        </div> -->
            <!-- <div class="col-lg-5 col-md-5">
        <!-- <h4 class="mg-b-0 tx-spacing--1 hc">Dark<span style="color:#4287f5 !important; font-weight:400">Data</h4></h4> --
          <h4 class="mg-b-0 tx-spacing--1 lhc">Good Morning, {{ auth()->user()->first_name }}</h4>
          
            <h4 class="mg-b-0 tx-spacing--1 slhc">Here what our DarkData Center has filter out the insights of your account.</h4>
          
        </div> -->
            <!-- <div class="col-lg-3 col-md-3">
        <div>
        <select class="custom-select" id="web-select" >
          <option value="https://guitarkitworld.com" selected> https://guitarkitworld.com </option>
            {{-- @foreach($webList as $wl)
            @if($wl != 'https://guitarkitworld.com')
          <option value="{{$wl->Property_Url}}">{{$wl->Property_Url}}</option>
          @endif
          @endforeach --}}
        </select>
        </div>
        </div> -->
            <!-- <div class="col-lg-4 col-md-4">
        <input type="text" name="datetimes" id="dateValue" class="daterangecss" readonly="readonly"/>
          <a href="#" onclick="gridview()"><img src="{{asset('images/grid.png')}}"  class="grid-img"  /></a>
         
        </div> -->

            <div class="at-dashboard">
                <div class="at-contentholder" id="tee">
                    <div class="at-pagehead">
                        <div class="row custom-head">
                            <div class="col-xl-5 col-lg-4">
                                <div class="at-pagetitle">
                                    <h2>Good Morning, {{ auth()->user()->first_name }}</h2>
                                    <span>Here what our DataCLU Center has filter out the insights for your
                                        accounts</span>
                                </div>
                            </div>
                            <div class="col-xl-2 col-lg-2">
                                <select class="custom-select" onchange="urlChangeFunction()" id="url-change">
                                    <!-- <option value="Custom" selected>Custom</option> -->
                                    @foreach($webList as $wl)
                                    @if($wl->default_value == 1)
                                    <option value="{{$wl->ViewId}}" selected><?php
                                    //  $anc =  explode(":",$wl->ViewId);
                                    // echo $anc[1]; 
                                     ?>
                                     {{$wl->Property_Url}}
                                    </option>
                                    @else
                                    <option value="{{$wl->ViewId}}"><?php 
                                    // $anc =  explode(":",$wl->ViewId);
                                    // echo $anc[1]; 
                                     ?>
                                    {{$wl->Property_Url}} 
                                    </option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xl-5 col-lg-6">
                                <ul class="at-listgridholder">
                                    <!-- <li>
                                        <div class="at-inputicon">
                                            <i class="icon-down-arrow"></i>
                                            <input type="text" name="datetimes" id="dateValue" class="daterangecss"
                                                readonly="readonly">
                                        </div>
                                    </li> -->
                                    <li>
                                        <select class="daterangecss" onchange="rangeChangeFunction()" id="ddlViewBy">
                                            <option value="D" id="daily-date"> Daily</option>
                                            <option value="W" id="weekly-date">Weekly</option>
                                        </select>
                                    </li>
                                    <li>
                                        <a class="at-btnlistgrid g-color" id="grid-color" href="#" onclick="gridview()">
                                            <i class="icon-menu"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="at-btnlistgrid t-color" id="table-color" href="#"
                                            onclick="tableview()">
                                            <i class="icon-list-menu"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <script>
                        var userIdContr = '<?php echo json_decode($userSingleId); ?>';
                        
                        </script>
                        <!-- <script src="{{asset('js/rangeapis_headersss.js')}}"></script> -->
                        <div style="width:100%; height:100px; display:none" id="data-check">
                            <h3 style="text-align:center">Your Data is being loaded. Please Refresh Page After Some Time
                            </h3>
                        </div>
                        {{-- <div class="loader" id="loading">Loading...</div> --}}
                        <div class="at-chartgridview" id="te">
                            {{-- <div id="grid-div">
                                <div class="row" id="createGridView">
                                </div>
                            </div> --}}
                            {{-- //table --}}
                            {{-- <div id="table-div" class="at-chartlistview" data-label="Example" class="df-example demo-table" style="display:none;"><table class="table at-tablechartview" id="e3"><thead><tr><th><span></span></th><th><span>Metric</span></th>
        <th><span>Dimension</span></th><th></th><th><span>Actual</span></th><th><span>Expected</span></th>
        <th><span>Variance</span></th><th><span>Impact</span></th></tr></thead><tbody id="createTableView">
            </tbody></table></div> --}}
                            {{-- table ended --}}
                            <!-- <div id="grid-div"> -->
                            <!-- <div class="row">
                                    <script>
                                    if (checkDataStatus == 1) {
                                        document.write(
                                            "<div class=\"col-12\"  style=\" height:150px;\" >",
                                            "<h3 style=\"text-align:center\">Your Data is being loaded. Please Refresh Page After Some Time",
                                            "</h3></div>");
                                    } else {
                                        console.log(arraydata);
                                        for (var divCount = 1; divCount <= arraydata.length; divCount++) {
                                            document.write(
                                                "<div class=\" col-12 col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 \">",
                                                " <div class=\"box\">",
                                                "<div class=\"row\">",
                                                "<div class=\"col-12\">",
                                                "<div class=\"at-charthead\">",
                                                "<h5 class=\" headText\" id=\"ht" + divCount + "\" ></h5>",
                                                "<h6 class=\"at-chartinfor\">" +
                                                // "<span class=\"dimension\">Query: <span id=\"q"+divCount+"\"></span>" +
                                                // "</span>" +
                                                // "<span class=\"page\">Page:</span>" +
                                                // "<span class=\"tooltip22\" id=\"p"+divCount+"\"></span>" +
                                                // "<span class=\"dimension\">Country: <span id=\"c"+divCount+"\"></span>" +
                                                // "</span>" +
                                                "<span class=\"dimension\">Query: <span id=\"q" + divCount +
                                                "\"></span>" +
                                                "</span> <br>" +
                                                "<span class=\"dimension\">Page:</span>" +
                                                "<span class=\"tooltip22\" id=\"p" + divCount + "\"></span><br>" +
                                                "<span class=\"dimension\">Country: <span id=\"c" + divCount +
                                                "\"></span>" +
                                                "</span>" +
                                                "<i class='icon-save'></i>",
                                                "</h6>",
                                                "</div>",
                                                "</div>",
                                                "</div>",
                                                "<div class=\"row\">",
                                                "<div class=\"col-12\">",
                                                "<div class='at-chartvalue'>" +
                                                "<h5 class=\"mg-b- daysText\"><i id=\"arrow" + divCount +
                                                "\"> </i><i id=\"pc" + divCount + "\"></i><i id=\"im" + divCount +
                                                "\"></i><i id=\"tl" + divCount + "\"></i> </h5>",
                                                "</div>" +
                                                "</div>",
                                                "</div>",
                                                "<div class=\"loader\" id=\"loading\">Loading...</div>",
                                                "<div class=\"row\">",
                                                "<div class=\"col-12\">",
                                                " <div id=\"chartsdivs" + divCount +
                                                "\" class=\"customchartthird\"  style=\"margin-bottom:10px\" ></div>",
                                                "</div>",
                                                "</div>",
                                                // "<div class=\"row\">",
                                                // "<div class=\"col-12\">",
                                                // "<div class=\"at-chartbottom\"> ",
                                                // "<h5 id=\"fd" + divCount + "\" >",
                                                // // " <i class=\"arrow down\"></i>",
                                                // " </h5> ",
                                                // "<i class=\"icon-info\"></i>",
                                                // "</div>",
                                                // "</div>",
                                                // "</div>",
                                                "</div>",
                                                "</div>"
                                            );
                                        }
                                    }
                                    </script>
                                </div> -->
                            <!-- </div> -->
                            <!-- <script src="{{asset('js/rangeapis_headersss.js')}}"></script> -->
                            <!-- <div id="table-div" class="at-chartlistview" style="display:none;"> -->
                            <!-- <table class="table at-tablechartview">
                                    <thead>
                                        <tr>
                                            <th><span></span></th>
                                            <th><span>Metric</span></th>
                                            <th><span>Dimension</span></th>
                                            <th></th>
                                            <th><span>Actual</span></th>
                                            <th><span>Expected</span></th>
                                            <th><span>Variance</span></th>
                                            <th><span>Impact</span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <script>
                                        for (var tableDivCount = 1; tableDivCount <= arraydata
                                            .length; tableDivCount++) {
                                            document.write("<tr>",
                                                "<td>",
                                                // "<form action=\"/anomaly/detail\" method=\"POST\">",
                                                // "<input type=\"hidden\" name=\"_token\" value=\"{{ csrf_token() }}\">",
                                                // "<input type=\"hidden\" name=\"anomaly_id\" id=\"anomaly_id" +
                                                // tableDivCount +
                                                // "\" />",
                                                // "<button class=\"js-btn\" type=\"submit\">",
                                                "<span id=\"anomaly_id" + tableDivCount + "\">",
                                                // "<img src=\"{{asset('images/search.png')}}\"  class=\"tabletoolimg\"  />",
                                                "</span>",
                                                // "</button>",
                                                // "</form>",
                                                "</td>",
                                                "<td><h5 class=\" table-headText\" id=\"tht" + tableDivCount +
                                                "\">  </h5></td>",
                                                "<td>",
                                                "<h5 class=\"mg-b-15\"><span class=\"table-dimension\">Query: <span id=\"tq" +
                                                tableDivCount +
                                                "\"></span></span><br><span class=\"table-dimension\">Page: </span><span class=\"tooltip22\" style=\"font-size:14px !important; color:black !important\" id=\"tp" +
                                                tableDivCount +
                                                "\"></span><br><span class=\"table-dimension\" >Country: <span id=\"tttc" +
                                                tableDivCount + "\"></span></span></h5>",
                                                "</td>",
                                                "<td><div class=\"loader\" id=\"loading\">Loading...</div><div id=\"tablechartsdivs" +
                                                tableDivCount +
                                                "\" class=\"table-customchartthird\"  style=\"margin-bottom:10px\" ></div></td>",
                                                "<td><span id=\"tts" + tableDivCount + "\"></span></td>",
                                                "<td> <span id=\"expected" + tableDivCount + "\"></span> </td>",
                                                "<td><h5><i id=\"tim" + tableDivCount + "\"></i><i id=\"ttttpc" +
                                                tableDivCount + "\"></i> </h5></td>",
                                                "<td> - </td>",
                                                "</tr>");
                                        }
                                        </script>
                                    </tbody>
                                </table> -->
                        </div>
                    {{-- </div> --}}
                <div class="loader" id="loading">Loading...</div>
                {{-- </div> --}}
                        <div style="text-align: center">
                                <button class="btn btn-outline-dark" id="loadMorebutton" onclick="loadMore()">Load More</button>
                            </div>
                    </div>
                </div>
            </div>


            {{-- <div class="content content-fixed">
     <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0">
        <div class="customwidth" id="te">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
                <div>
                    <h4 class="mg-b-0 tx-spacing--1 hc">Dark<span style="color:#4287f5 !important; font-weight:400">Data</h4></h4>
                    <h4 class="mg-b-0 tx-spacing--1 lhc">Good Morning, {{ auth()->user()->first_name }}</h4>
            <h4 class="mg-b-0 tx-spacing--1 slhc">Here what our DarkData Center has filter out the insights of your
                account.</h4>
        </div>
        <div class="d-none d-md-block">
            <input type="text" name="datetimes" id="dateValue" class="daterangecss" readonly="readonly" />
            <a href="#" onclick="gridview()"><img src="{{asset('images/grid.png')}}" class="grid-img" /></a>
        </div>
    </div>
    <div id="grid-div" style="">
        <div class="row row-xs">
            <script>
            for (var divCount = 1; divCount <= arraydata.length; divCount++) {
                document.write("<div class=\" col-12 col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 \">",
                    " <div class=\"box\">",
                    "<div class=\"row\">",
                    "<div class=\"col-2\">",
                    "<img src=\"{{asset('images/ga.png')}}\"  class=\"toolimg\"  />",
                    "</div>",
                    "<div class=\"col-8\">",
                    " <h5 class=\" headText\" id=\"ht" + divCount + "\" >  </h5>",
                    "</div>",
                    "<div class=\"col-2\">",
                    "<img src=\"{{asset('images/msg.png')}}\"  class=\"msgimg\"  />",
                    "</div>",
                    "</div>",
                    "<div class=\"row\">",
                    "<div class=\"col-12 qpc\">",
                    "<h5 class=\"mg-b-15\"><span class=\"dimension\">Query: <span id=\"q" + divCount +
                    "\"></span></span><br><span class=\"dimension\">Page:</span> <span class=\"tooltip22\" id=\"p" +
                    divCount + "\"></span><br><span class=\"dimension\">Country: <span id=\"c" + divCount +
                    "\"></span></span></h5>",
                    "</div>",
                    "</div>",
                    "<h5 class=\"mg-b- textalign daysText\"><i id=\"arrow" + divCount + "\"> </i><i id=\"pc" +
                    divCount + "\"></i><i id=\"im" + divCount + "\"></i><i id=\"tl" + divCount +
                    "\"></i> </h5>",
                    "<div class=\"loader\" id=\"loading\">Loading...</div>",
                    " <div id=\"chartsdivs" + divCount +
                    "\" class=\"customchartthird\"  style=\"margin-bottom:10px\" ></div>",
                    "<div class=\"row\"> ",
                    "<div class=\"col-8\">",
                    "<h5 class=\"mg-b-15  bottomText\" id=\"fd" + divCount +
                    "\" >  <i class=\"arrow down\"></i> </h5> ",
                    "</div>",
                    " <div class=\"col-4\">",
                    "<img src=\"{{asset('images/information.png')}}\"  class=\"infoimg\"  />",
                    "</div>",
                    "</div>",
                    "</div>",
                    "</div>"
                );
            }
            </script>
        </div>
    </div>
    <div id="table-div" style="display:none;">
        <table class="table border">
            <thead>
                <tr>
                    <th></th>
                    <th><b>Metric</b></th>
                    <th><b>Dimension</b></th>
                    <th></th>
                    <th><b>Actual</b></th>
                    <th><b>Expected</b></th>
                    <th><b>Variance</b></th>
                    <th><b>Impact</b></th>
                </tr>
            </thead>
            <tbody>
                <script>
                for (var tableDivCount = 1; tableDivCount <= arraydata.length; tableDivCount++) {
                    document.write("<tr>",
                        "<td><img src=\"{{asset('images/search.png')}}\"  class=\"tabletoolimg\"  /></td>",
                        "<td><h5 class=\" table-headText\" id=\"tht" + tableDivCount + "\">  </h5></td>",
                        "<td>",
                        "<h5 class=\"mg-b-15\"><span class=\"table-dimension\">Query: <span id=\"tq" +
                        tableDivCount +
                        "\"></span></span><br><span class=\"table-dimension\">Page: </span><span class=\"tooltip22\" style=\"font-size:14px !important; color:black !important\" id=\"tp" +
                        tableDivCount +
                        "\"></span><br><span class=\"table-dimension\" >Country: <span id=\"tttc" +
                        tableDivCount + "\"></span></span></h5>",
                        "</td>",
                        "<td><div class=\"loader\" id=\"loading\">Loading...</div><div id=\"tablechartsdivs" +
                        tableDivCount +
                        "\" class=\"table-customchartthird\"  style=\"margin-bottom:10px\" ></div></td>",
                        "<td><span id=\"tts" + tableDivCount + "\"></span></td>",
                        "<td> - </td>",
                        "<td><h5><i id=\"tim" + tableDivCount + "\"></i><i id=\"ttttpc" + tableDivCount +
                        "\"></i> </h5></td>",
                        "<td> - </td>",
                        "</tr>");
                }
                </script>
            </tbody>
        </table>
    </div>
</div>
</div>
</div>--}}
@endsection
@section('page-level-js')
<!-- Resources -->
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>
<script src="//www.amcharts.com/lib/3/plugins/tools/bestFitLine/bestFitLine.min.js"></script>
{{-- Further added --}}
<script src="{{asset('lib/prismjs/prism.js')}}"></script>
<script src="{{asset('lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="{{asset('lib/select2/js/select2.min.js')}}"></script>
{{-- Further Added --}}

<!-- Chart code -->
<!-- <script src="{{asset('js/change-staticanalytics.js')}}"></script>
  <script src="{{asset('js/change1-staticanalytics.js')}}"></script> -->

<!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script> -->
<!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script> -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>


<!-- Single APi 4 Metrics Data -->
<!-- <script src="{{asset('lib/prismjs/prism.js')}}"></script>
    <script src="{{asset('lib/jqueryui/jquery-ui.min.js')}}"></script>
    <script>
      $('#datepicker1').datepicker();
      </script> -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- <script src="{{asset('js/rangeapis_headersss.js')}}"></script>
<script src="{{asset('js/singlescallsapisss.js')}}"></script> -->
<script src="{{asset('js/gaaaApiiCallsss.js')}}"></script>


<!-- <script>
</script> -->
<script>
function gridview() {
    var x = document.getElementById("table-div");
    var y = document.getElementById("grid-div");

    x.style.display = "none";
    y.style.display = "block";

    document.getElementById("table-color").style.color = "#4783ff";
    document.getElementById("table-color").style.backgroundColor = "#fff";
    document.getElementById("grid-color").style.color = "#fff";
    document.getElementById("grid-color").style.backgroundColor = "#4783ff";

}

function tableview() {
    var x = document.getElementById("table-div");
    var y = document.getElementById("grid-div");

    x.style.display = "block";
    y.style.display = "none";

    document.getElementById("grid-color").style.color = "#4783ff";
    document.getElementById("grid-color").style.backgroundColor = "#fff";
    document.getElementById("table-color").style.color = "#fff";
    document.getElementById("table-color").style.backgroundColor = "#4783ff";

}
</script>

@endsection