@extends('admin_layout.main')
@section('page-level-css')
<link href="{{asset('lib/typicons.font/typicons.css')}}" rel="stylesheet">
<link href="{{asset('lib/prismjs/themes/prism-vs.css')}}" rel="stylesheet">
<link href="{{asset('lib/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet">
<link href="{{asset('lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet">
<link href="{{asset('lib/select2/css/select2.min.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('assets/css/dashforge.profile.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/dashforge.demo.css')}}">
<link rel="stylesheet" href="{{asset('css/userProfile.css')}}">
@endsection
@section('content')

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Select Your Data Source</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!-- ... -->
           <a href="{{ route('updateUserAnalytics') }}" type="button" class="btn btn-dark" style="color:white; margin:1px">Add Analytics Project</a>
        <a href="{{route('updateUserConsole')}}" type="button" class="btn btn-primary" style="color:white; margin:1px">Add Console Project</a>

        <a type="button" class="btn btn-success" data-toggle="modal" data-target="#shopifyDetail" style="color:white; margin:1px">Add Shopify Project</a>



      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
  </div>
</div>

{{-- Modal For Getting Shopify Details From Users --}}
<!-- Button trigger modal -->

<!-- Modal -->
<div class="modal fade" id="shopifyDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <img src="{{asset('images/shp.png')}}" class="shopifyLogo">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5 style="text-align:center">Integrate Shopify</h5>
                <p style="text-align: justify;">Follow the instructions in the <span>Morphio Resource Section</span>to connect your shopify online store to Morphio and view active store metrics in Morphio. </p>
                <form>
                    <div class="form-group">
                        <label for="shopName">Shop Name</label>
                        <input type="text" class="form-control shopName" id="shopName" aria-describedby="emailHelp" placeholder="Enter email">
                        {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
                    </div>
                    <div class="form-group">
                        <label for="apiKey">API Key</label>
                        <input type="text" class="form-control apiKey" id="apiKey" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <label for="apiPassword">API Password</label>
                        <input type="text" class="form-control apiPassword" id="apiPassword" placeholder="Password">
                    </div>
                    {{-- <button type="submit" class="btn btn-primary">Submit</button> --}}
                </form>


            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-primary saveShopifyRecord">Save</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                
            </div>
        </div>
    </div>
</div>

{{-- Shopify Modal Ends --}}

{{-- Share Console Model  --}}
<!-- Modal -->
<div class="modal fade" id="shareConsoelModal" tabindex="-1" role="dialog" aria-labelledby="shareConsoelModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="shareConsoelModalLabel">Add New User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                 <form>
                 <input type="hidden" id="console-id" name="console-id" />
                     <div class="form-group">
                         <label for="recipient-email" class="col-form-label">Email:</label>

                         <input type="email" class="form-control" id="recipient-email" name="recipient-email">

                     </div>
                     <div class="form-group">
                         <label for="recipient-password" class="col-form-label">Password:</label>

                         <input type="password" class="form-control" id="recipient-password" name="recipient-password">
                     </div>
                 </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary shareConsoleData" data-dismiss="modal">Add</button>

            </div>
        </div>
    </div>
</div>

{{-- Share Console Model ends --}}

{{-- Share Analytics Model --}}
<!-- Modal -->
<div class="modal fade" id="shareAnalyticsModal" tabindex="-1" role="dialog" aria-labelledby="shareAnalyticsModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="shareAnalyticsModalLabel">Add New User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                 <form>
                     <input type="hidden" id="analytics-id" name="analytics-id" />
                     <div class="form-group">
                         <label for="recipient-email-analytics" class="col-form-label">Email:</label>
                         <input type="email" class="form-control" id="recipient-email-analytics" name="recipient-email-analytics">

                     </div>
                     <div class="form-group">
                         <label for="recipient-pass-analytics" class="col-form-label">Password:</label>
                         <input type="password" class="form-control" id="recipient-pass-analytics" name="recipient-pass-analytics">
                     </div>
                 </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary shareAnalyticsData" data-dismiss="modal">Add</button>

            </div>
        </div>
    </div>
</div>

{{-- Share Analytics Model Ends --}}


  <div class="content content-fixed content-profile">
      <div class="container-fluid custom-container pd-x-0 pd-lg-x-10 pd-xl-x-0">
        <div class="media d-block d-lg-flex">
          <div class="profile-sidebar pd-lg-r-25">
            <div class="row">
              <div class="col-sm-3 col-md-2 col-lg">
                <div class="avatar avatar-xxl avatar-online"><img src="{{$user->avatar}}" class="rounded-circle" alt=""></div>
              </div><!-- col -->
              <div class="col-sm-8 col-md-7 col-lg mg-t-20 mg-sm-t-0 mg-lg-t-25">
                <h5 class="mg-b-2 tx-spacing--1">{{$name}} {{$lastName}}</h5>
                <p class="tx-color-03 mg-b-25">{{$user->email}}</p>
                <div class="d-flex mg-b-25">
                  <button class="btn btn-xs btn-primary flex-fill" data-toggle="modal" data-target="#exampleModalCenter">Add Project</button>
                  <!-- <button class="btn btn-xs btn-primary flex-fill mg-l-10">Follow</button> -->
                </div>

              </div><!-- col -->



            </div><!-- row -->

          </div><!-- profile-sidebar -->
          <div class="media-body mg-t-40 mg-lg-t-0 pd-lg-x-10">

          @if(session('success'))
          <div class="alert alert-success" role="alert">...</div>
          @endif

            <!-- <h1 class="df-title">{{$name}} Profile</h1>

            <hr class="mg-t-50 mg-b-40"> -->

            <h4 id="section2" class="mg-b-10">Send Notifications To</h4>
            <div class="row">
            <div class="col-6">
                <div class="custom-control custom-checkbox">
                @if($service)
                  @if($service->email == 1)
                  <input type="checkbox" class="custom-control-input" id="emailCheck" checked>
                  @else
                    <input type="checkbox" class="custom-control-input" id="emailCheck">
                  @endif
                @else
                  <input type="checkbox" class="custom-control-input" id="emailCheck" checked>
                @endif
                  <label class="custom-control-label" for="emailCheck">Email</label>
                </div>
            </div>
            <div class="col-6">
            <div class="custom-control custom-checkbox">
            @if($service)
                  @if($service->slack == 1)
                  <input type="checkbox" class="custom-control-input" id="slackCheck" checked>
                  @else
                    <input type="checkbox" class="custom-control-input" id="slackCheck">
                  @endif
                @else
                  <input type="checkbox" class="custom-control-input" id="slackCheck" checked>
                @endif
              <label class="custom-control-label" for="slackCheck">Slack</label>
            </div>
            </div>
            
            </div>
<hr class="mg-t-50 mg-b-40">


            <h4 id="section2" class="mg-b-10">Websites Linked With Google Console</h4>
            <p class="mg-b-30">Following are the list of websites that are linked with google console.</p>

            <div data-label="Console" class="df-example demo-table">
              <table id="example2" class="table">
                <thead>
                  <tr>
                    <th class="wd-20p">WebsiteUrl</th>
                    <th class="wd-20p">PermissionLevel</th>
                    <th class="wd-20p">DefaultSelected</th>
                    <th class="wd-20p">Action</th>
                    {{-- <th class="wd-15p">Age</th>
                    <th class="wd-20p">Salary</th> --}}
                  </tr>
                </thead>
              </table>
            </div><!-- df-example -->

            {{-- Analytics Starts --}}
            <hr class="mg-t-50 mg-b-40">

            <h4 id="section2" class="mg-b-10">Websites Linked With Google Analytics</h4>
            <p class="mg-b-30">Following are the list of websites that are linked with google analytics.</p>

            <div data-label="Analytics" class="df-example demo-table">
              <table id="exampleAnalytics" class="table">
                <thead>
                  <tr>
                    <th class="wd-20p">Name</th>
                    <th class="wd-20p">Url</th>
                    <th class="wd-20p">ViewId</th>
                    <th class="wd-20p">DefaultSelected</th>
                    <th class="wd-20p">Action</th>
                    {{-- <th class="wd-15p">Age</th>
                    <th class="wd-20p">Salary</th> --}}
                  </tr>
                </thead>
              </table>
            </div><!-- df-example -->
            {{-- Analytics ends --}}

          </div><!-- media-body -->











        </div><!-- media -->
      </div><!-- container -->
    </div><!-- content -->
  @endsection


@section('page-level-js')
<script src="{{asset('lib/prismjs/prism.js')}}"></script>
  <script src="{{asset('lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
  <script src="{{asset('lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
  <script src="{{asset('lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>




<script type="text/javascript">
  $(function () {

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });

    var table = $('#example2').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        // language: {
        //   searchPlaceholder: 'Search...',
        //   sSearch: '',
        //   lengthMenu: '_MENU_ items/page',
        // },
         ajax: "{{ route('console.list') }}",
        //  ajax: {
        //   url: 'console/list',
        //   type: 'GET',
        //  },
        columns: [
            {data: 'siteUrl', name: 'WebsiteUrl'},
            {data: 'permissionLevel', name: 'PermissionLevel'},
            {data: 'default_value', name: 'DefaultSelected'},
            {
                data: 'action',
                name: 'action',
                orderable: true,
                searchable: true
            },
        ],
    //     $.ajax({
    //     type: 'GET',
    //     url: 'console/list',

    // });
     });

  $('body').on('click', '.editDefault', function () {

        var updateDataId = $(this).data("id");

        $.ajax({
            type: "POST",
            url: "{{ route('console.update') }}",
            data: {
              id : updateDataId,
            },
            success: function (data) {
                table.draw();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

    $('body').on('click', '.shareConsole', function () {

        document.getElementById("console-id").value = $(this).data("id");
        $('#shareConsoelModal').modal('show');

    });

     $('body').on('click', '.shareAnalytics', function () {

        document.getElementById("analytics-id").value = $(this).data("id");
        $('#shareAnalyticsModal').modal('show');

    });

    
    $('body').on('click', '.shareConsoleData', function () {

        var id = $("input[name=console-id]").val();
        var email = $("input[name=recipient-email]").val();
        var pass = $("input[name=recipient-password]").val();

        $.ajax({
            type: "POST",
            url: "{{ route('consoleAnalytics.shareData') }}",
            data: {
              id : id,
              email : email,
              pass : pass,
              type : "console",
            },
            success: function (data) {
                 alert(JSON.stringify(data));

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });
    
    $('body').on('click', '.shareAnalyticsData', function () {

        var id = $("input[name=analytics-id]").val();
        var email = $("input[name=recipient-email-analytics]").val();
        var pass = $("input[name=recipient-pass-analytics]").val();
        
        $.ajax({
            type: "POST",
            url: "{{ route('consoleAnalytics.shareData') }}",
            data: {
              id : id,
              email : email,
              pass : pass,
              type : "analytics",
            },
            success: function (data) {
                alert(JSON.stringify(data));

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

    $('body').on('click', '.deleteConsole', function () {

        var deleteDataId = $(this).data("id");

        $.ajax({
            type: "POST",
            url: "{{ route('console.delete') }}",
            data: {
              id : deleteDataId,
            },
            success: function (data) {
                table.draw();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });


//Analytics STarts
var tableAnalytics = $('#exampleAnalytics').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        // language: {
        //   searchPlaceholder: 'Search...',
        //   sSearch: '',
        //   lengthMenu: '_MENU_ items/page',
        // },
         ajax: "{{ route('analytics.list') }}",
        //  ajax: {
        //   url: 'console/list',
        //   type: 'GET',
        //  },
        columns: [
            {data: 'Property_Name', name: 'Name'},
            {data: 'Property_Url', name: 'Url'},
            {data: 'ViewId', name: 'ViewId'},
            {data: 'default_value', name: 'DefaultSelected'},
            {
                data: 'action',
                name: 'action',
                orderable: true,
                searchable: true
            },
        ],
    //     $.ajax({
    //     type: 'GET',
    //     url: 'console/list',

    // });
     });

     $('body').on('click', '.editDefaultAnalytics', function () {

        var updateDataId = $(this).data("id");

        $.ajax({
            type: "POST",
            url: "{{ route('analytics.update') }}",
            data: {
              id : updateDataId,
            },
            success: function (data) {
                tableAnalytics.draw();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

     $('body').on('click', '.deleteAnalytics', function () {

        var deleteDataId = $(this).data("id");

        $.ajax({
            type: "POST",
            url: "{{ route('analytics.delete') }}",
            data: {
              id : deleteDataId,
            },
            success: function (data) {
                tableAnalytics.draw();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

  });

  $("#emailCheck").on('change',function()
 {
   if(!$(this).is(':checked'))
      {
           $.ajax({
            type: "POST",
            url: "{{ route('enableServices.update') }}",
            data: {
              status : 0,
              type : "email",
            },
            success: function (data) {
                // tableAnalytics.draw();
            },
            // error: function (data) {
            //     console.log('Error:', data);
            // }
        });
      }
      else
      {
            $.ajax({
            type: "POST",
            url: "{{ route('enableServices.update') }}",
            data: {
              status : 1,
              type : "email",
            },
            success: function (data) {
                // tableAnalytics.draw();
            }
        });
      }
 }); 

 $("#slackCheck").on('change',function()
 {
   if(!$(this).is(':checked'))
      {
           $.ajax({
            type: "POST",
            url: "{{ route('enableServices.update') }}",
            data: {
              status : 0,
              type : "slack",
            },
            success: function (data) {
                // tableAnalytics.draw();
            },
            // error: function (data) {
            //     console.log('Error:', data);
            // }
        });
      }
      else
      {
            $.ajax({
            type: "POST",
            url: "{{ route('enableServices.update') }}",
            data: {
              status : 1,
              type : "slack",
            },
            success: function (data) {
                // tableAnalytics.draw();
            }
        });
      }
 }); 

 $('body').on('click', '.saveShopifyRecord', function () {

        var shopName = $("input[name=shopName]").val();
        var apiKey = $("input[name=apiKey]").val();
        var apiPassword = $("input[name=apiPassword]").val();
        
        $.ajax({
            type: "POST",
            url: "{{ route('save.shopifyDetail') }}",
            data: {
              shopName : shopName,
              apiKey : apiKey,
              apiPassword : apiPassword,
            },
            success: function (data) {
                alert(JSON.stringify(data));

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

</script>
@endsection
