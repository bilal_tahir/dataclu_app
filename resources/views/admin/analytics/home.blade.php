@extends('admin_layout.main')
@section('page-title')
User Dashboard
@endsection
@section('page-level-css')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

{{-- Further Added DT --}}
<link href="{{asset('lib/prismjs/themes/prism-vs.css')}}" rel="stylesheet">
<link href="{{asset('lib/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet">
<link href="{{asset('lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet">
<link href="{{asset('lib/select2/css/select2.min.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('assets/css/dashforge.demo.css')}}">
    {{-- Further --}}
    <link rel="stylesheet" href="{{asset('css/analyticsHome.css')}}">
    <link rel="stylesheet" href="{{asset('css/productsDetails.css')}}">
<link rel="stylesheet" href="{{asset('css/customs.css')}}">
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>


<style>
 
</style>
@endsection
@section('content')

<div class="content content-fixed" id="tee">
    <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0">
        <div class="customwidth" id="tee">

            <div class="at-dashboard">
                <div class="at-contentholder" id="tee">
                    <div class="at-pagehead">
                        <div class="row custom-head">
                            <div class="col-xl-6 col-lg-6">
                                <div class="at-pagetitle">
                                    <h2>Good Morning, {{ auth()->user()->first_name }}</h2>
                                    <span>Here what our DataCLU Center has filter out the insights for your
                                        accounts</span>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-3">
                                <select class="custom-select" onchange="dataChangeFunction()" id="account-change">
                                    {{-- <option value="Custom" selected>--Select Account--</option> --}}
                                    @if(!$accountList->isEmpty())
                                    @foreach($accountList as $acnt)
                                    @if($acnt->default_value == 1)
                                    <option value="{{$acnt->ViewId}}" selected><?php
                                    //  $anc =  explode(":",$wl->siteUrl);
                                    // echo $anc[1]; 
                                        ?>
                                        {{$acnt->Property_Url}}
                                    </option>
                                    </option>
                                    @else
                                    <option value="{{$acnt->ViewId}}"><?php
                                    //  $anc =  explode(":",$wl->siteUrl);
                                    // echo $anc[1]; 
                                     ?>
                                     {{$acnt->Property_Url}}
                                    </option>
                                    @endif
                                    @endforeach
                                    @endif
                                 
                                </select>
                            </div>
                            <div class="col-xl-3 col-lg-3" id="dynamic-metrics">
                                        {{-- <select class="daterangecss" onchange="rangeChangeFunction()" id="ddlViewBy">
                                            <option value="D" id="daily-date"> Daily</option>
                                            <option value="W" id="weekly-date">Weekly</option>
                                        </select> --}}
                            </div>
                        </div>
                    {{-- </div> --}}
                {{-- </div>
            </div> --}}


            <script>
              var getAuthUserId = '<?php echo ($getAuthUserId); ?>';
            </script>
            <div class="loader" id="loading">Loading...</div>

            <div class="row tx-14" id="dataUpdate">
                <div class="col-12 mg-t-20 mg-sm-t-25">
                    {{-- <div class="row">
                        <div class="col-12">
                            <h2 class="action-line"><span>Contributors</span></h2>
                        </div>
                    </div> --}}
                    <div id="corelations-list">

                    </div>
                </div><!-- col-12 -->
            </div>
                
            {{-- //Scatter charts --}}
            <div class="row tx-14" id="dataUpdate">
            {{-- <div id="scatter-show-hide" style="display: block"> --}}
            <div class="col-12 mg-t-20 mg-sm-t-25" id="scatter-show-hide" style="display: none">
                {{-- <div class="row">
                    <div class="col-12">
                        <h2 class="action-line"><span>Scatter Charts</span></h2>
                    </div>
                </div> --}}
                <div class="row">
                        <div class="col-12 chartDiv" >
                            <h5><span>Correlation Plot</span></h5>
                            <div class="charts" id="chartContainerScatter1">
                            </div>
                </div>
            </div>
            <div class="row">
                    <div class="col-12 chartDiv2"  >
                        <h5><span>Actual Values</span></h5>
                        <div class="charts2" id="chartdivScatter2">
                        </div>
                    </div>

                </div>




            </div><!-- col-12 -->
        {{-- </div> --}}
            {{-- Scatter Charts ends --}}
            </div>

                </div>
            </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('page-level-js')
<!-- Resources -->
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>
{{-- <script src="//www.amcharts.com/lib/3/plugins/tools/bestFitLine/bestFitLine.min.js"></script> --}}
{{-- Further added --}}
<script src="{{asset('lib/prismjs/prism.js')}}"></script>
<script src="{{asset('lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="{{asset('lib/select2/js/select2.min.js')}}"></script>
{{-- Further Added --}}

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    
{{-- Canvas  --}}
<script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

<script src="{{asset('js/analytiicssss.js')}}"></script>


<script>
    $(function () {
      'use strict'


      $('#example2').DataTable({
        responsive: true,
        "bPaginate": false,
        "bFilter": false, //hide Search bar
        "bInfo": false, // hide showing entries
        columnDefs: [
            // { orderable: true, targets: 7 },
            { orderable: false, targets: '_all' }
        ]
      });

   


   

      // Select2
      $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

    });
  </script>
  
@endsection