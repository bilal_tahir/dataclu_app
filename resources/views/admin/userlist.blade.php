@extends('admin_layout.main')
@section('page-level-css')
<link href="{{asset('lib/typicons.font/typicons.css')}}" rel="stylesheet">
<link href="{{asset('lib/prismjs/themes/prism-vs.css')}}" rel="stylesheet">
<link href="{{asset('lib/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet">
<link href="{{asset('lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet">
<link href="{{asset('lib/select2/css/select2.min.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('assets/css/dashforge.demo.css')}}">
@endsection
@section('content')




  <div class="content content-components">
    <div class="container-fluid">
      <ol class="breadcrumb df-breadcrumbs mg-b-10">
        <li class="breadcrumb-item"><a href="#">Components</a></li>
        <li class="breadcrumb-item"><a href="#">UI Elements</a></li>
        <li class="breadcrumb-item active" aria-current="page">Table Advanced</li>
      </ol>

      <h1 class="df-title">Table Advanced</h1>
      <p class="df-lead">DataTables. Add advanced interaction controls
to your HTML tables the easy way. Read the <a href="https://datatables.net/manual/" target="_blank">Official DataTable Documentation</a> for a full list of instructions and other options.</p>

      <hr class="mg-y-40">



      <h4 id="section2" class="mg-b-10">Responsive DataTable</h4>
      <p class="mg-b-30">Responsive is an extension for DataTables that resolves that problem by optimising the table's layout for different screen sizes through the dynamic insertion and removal of columns from the table.</p>

      <div data-label="Example" class="df-example demo-table">
        <table id="example2" class="table">
          <thead>
            <tr>
              <th class="wd-20p">Name</th>
              <th class="wd-25p">Google_ID</th>
              <th class="wd-20p">Email</th>

            </tr>
          </thead>
                 </table>
      </div><!-- df-example -->



    </div><!-- container -->
  </div><!-- content -->

@endsection
@section('page-level-js')
<script src="{{asset('lib/prismjs/prism.js')}}"></script>
    <script src="{{asset('lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
    <script src="{{asset('lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>

<script>

    $(function() {
          $('#example2').DataTable({
           processing: true,
          'language': {
           "processing": "<img style='width:40px; height:40px;' src='assets/images/5.gif' />",
           searchPlaceholder: "Search records",
   } ,
          serverSide: true,
          ajax: '{{ url('userrecords') }}',
          columns: [
                   { data: 'name', name: 'name' },
                   { data: 'google_id', name: 'google_id'  , orderable: false},
                   { data: 'email', name: 'email'  , orderable: false },




                ]
       });
    });
   </script>
@endsection
