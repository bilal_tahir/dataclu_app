@extends('admin_layout.main')
@section('page-title')
User Dashboard
@endsection
@section('page-level-css')
{{-- Further Added DT --}}
<link href="{{asset('lib/prismjs/themes/prism-vs.css')}}" rel="stylesheet">
<link href="{{asset('lib/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet">
<link href="{{asset('lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet">
<link href="{{asset('lib/select2/css/select2.min.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('assets/css/dashforge.demo.css')}}">
    {{-- Further --}}
<link rel="stylesheet" href="{{asset('css/productsDetails.css')}}">
<!-- <link rel="stylesheet" href="{{asset('assets/css/dashforge.demo.css')}}"> -->
<link rel="stylesheet" href="{{asset('css/consoleAnalyticsDetail.css')}}">
<style>
#chartdiv {
    width: 100%;
    height: 250px;
}

#chartdivs {
    width: 100%;
    height: 250px;
}

@media (min-width: 768px) and (max-width: 991px) {

    /* Show 4th slide on md if col-md-4*/
    .carousel-inner .active.col-md-4.carousel-item+.carousel-item+.carousel-item+.carousel-item {
        position: absolute;
        top: 0;
        right: -33.3333%;
        /*change this with javascript in the future*/
        z-index: -1;
        display: block;
        visibility: visible;
    }
}

@media (min-width: 576px) and (max-width: 768px) {

    /* Show 3rd slide on sm if col-sm-6*/
    .carousel-inner .active.col-sm-6.carousel-item+.carousel-item+.carousel-item {
        position: absolute;
        top: 0;
        right: -50%;
        /*change this with javascript in the future*/
        z-index: -1;
        display: block;
        visibility: visible;
    }
}

@media (min-width: 576px) {
    .carousel-item {
        margin-right: 0;
    }

    /* show 2 items */
    .carousel-inner .active+.carousel-item {
        display: block;
    }

    .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left),
    .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left)+.carousel-item {
        transition: none;
    }

    .carousel-inner .carousel-item-next {
        position: relative;
        transform: translate3d(0, 0, 0);
    }

    /* left or forward direction */
    .active.carousel-item-left+.carousel-item-next.carousel-item-left,
    .carousel-item-next.carousel-item-left+.carousel-item,
    .carousel-item-next.carousel-item-left+.carousel-item+.carousel-item {
        position: relative;
        transform: translate3d(-100%, 0, 0);
        visibility: visible;
    }

    /* farthest right hidden item must be also positioned for animations */
    .carousel-inner .carousel-item-prev.carousel-item-right {
        position: absolute;
        top: 0;
        left: 0;
        z-index: -1;
        display: block;
        visibility: visible;
    }

    /* right or prev direction */
    .active.carousel-item-right+.carousel-item-prev.carousel-item-right,
    .carousel-item-prev.carousel-item-right+.carousel-item,
    .carousel-item-prev.carousel-item-right+.carousel-item+.carousel-item {
        position: relative;
        transform: translate3d(100%, 0, 0);
        visibility: visible;
        display: block;
        visibility: visible;
    }
}

/* MD */
@media (min-width: 768px) {

    /* show 3rd of 3 item slide */
    .carousel-inner .active+.carousel-item+.carousel-item {
        display: block;
    }

    .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left)+.carousel-item+.carousel-item {
        transition: none;
    }

    .carousel-inner .carousel-item-next {
        position: relative;
        transform: translate3d(0, 0, 0);
    }

    /* left or forward direction */
    .carousel-item-next.carousel-item-left+.carousel-item+.carousel-item+.carousel-item {
        position: relative;
        transform: translate3d(-100%, 0, 0);
        visibility: visible;
    }

    /* right or prev direction */
    .carousel-item-prev.carousel-item-right+.carousel-item+.carousel-item+.carousel-item {
        position: relative;
        transform: translate3d(100%, 0, 0);
        visibility: visible;
        display: block;
        visibility: visible;
    }
}

/* LG */
@media (min-width: 991px) {

    /* show 4th item */
    .carousel-inner .active+.carousel-item+.carousel-item+.carousel-item {
        display: block;
    }

    .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left)+.carousel-item+.carousel-item+.carousel-item {
        transition: none;
    }

    /* Show 5th slide on lg if col-lg-3 */
    .carousel-inner .active.col-lg-3.carousel-item+.carousel-item+.carousel-item+.carousel-item+.carousel-item {
        position: absolute;
        top: 0;
        right: -25%;
        /*change this with javascript in the future*/
        z-index: -1;
        display: block;
        visibility: visible;
    }

    /* left or forward direction */
    .carousel-item-next.carousel-item-left+.carousel-item+.carousel-item+.carousel-item+.carousel-item {
        position: relative;
        transform: translate3d(-100%, 0, 0);
        visibility: visible;
    }

    /* right or prev direction //t - previous slide direction last item animation fix */
    .carousel-item-prev.carousel-item-right+.carousel-item+.carousel-item+.carousel-item+.carousel-item {
        position: relative;
        transform: translate3d(100%, 0, 0);
        visibility: visible;
        display: block;
        visibility: visible;
    }
}
</style>

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>


@endsection
@section('content')

<!-- Button trigger modal -->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
  Launch demo modal
</button> -->
{{-- Modal for feedback comment --}}

<div class="modal fade" id="exampleModalCenterFeedback" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Thanks For Your Feedback!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
            <div class="form-group">
                <label for="commentFormControlTextarea1">Please Leave A Comment!</label>
                <textarea class="form-control" name="commentFeedback" id="commentFormControlTextarea1" rows="3"></textarea>
            </div>
            
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-secondary feedback-comment" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary feedback-comment" data-dismiss="modal">Submit</button>
         </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal for Email-->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header custom-modal-header">
                <div class="row">
                    <div class="col-2">
                        <img src="{{asset('css/images/globe.png')}}" />
                    </div>
                    <div class="col-10">
                        <h5 class="modal-title custom-modal-title" id="exampleModalLongTitle">Share to web<br>Anyone
                            with the link can
                            see<br>Based
                            on darkdata.cs</h5>
                    </div>

                </div>
                <button class="close" aria-label="Close">
                    <label class="switch">
                        <input type="checkbox" onchange="switchfunc()" id="on-off" checked>
                        <span class="slider round"></span>
                    </label>
                </button>

            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="halfWidth">
                            <div class="input-wrapper">
                                <input type="text" class="input-form-link" id="input-url-gen" />
                            </div>
                            <input type="hidden" name="anomalyIdsecond" value={{$callId}} />
                            <button type="submit" class="button-link btn-submit-2" id="button-url-gen"
                                style="font-size: 13px; !important">Create
                                Link</button>

                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-12">
                        <!-- <div class="at-inputicon"> -->

                        <!-- <h5>hello<i class="icon-down-arrow"></i></h5> -->
                        <!-- </div> -->
                        <h5 class="link-opt">Show Link Options
                            <i class="icon-down-arrow cus-arr" data-toggle="collapse" data-target="#demo"></i>
                        </h5>
                        <div id="demo" class="collapse">
                            <!-- Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-12">
                        <div class="halfWidth">
                            <form action="/send-mail" method="POST">
                                @csrf
                                <input type="hidden" name="anomalyId" value={{$callId}} />
                                <div class="input-wrapper">
                                    <input type="email" class="input-form-link" name="email" id="input-url-share"
                                        placeholder="Add people, groups, or emails" />
                                        <input type="hidden" name="anomalyDetail" id="anomaly-detail" value="" />
                                        <input type="hidden" name="anomalyContributor" id="anomaly-contributors" value="" />
                                        <input type="hidden" name="anomalyAudience" id="anomaly-audience" value="" />
                                        <input type="hidden" name="anomalyPageGroup" id="anomaly-group" value="" />

                                </div>
                                <button type="submit" class="second-button-link" id="button-url-share"  disabled style="cursor:not-allowed">Add
                                    People</button>
                            {{-- </form> --}}
                        </div>
                    </div>
                </div>
                   <!-- Comment-->
                   <div class="row">

                    <div class="col-12">
                        <!-- <div class="at-inputicon"> -->

                        <!-- <h5>hello<i class="icon-down-arrow"></i></h5> -->
                        <!-- </div> -->
                        <textarea class="form-control custom-text-area" name="textMessage" id="model-comment-textarea" rows="4" cols="50" readonly="readonly">
                            
                            </textarea>
                    </div>
                </form>
                </div>
                <!-- Second -->
                <div class="row">

                    <div class="col-12">
                        <!-- <div class="at-inputicon"> -->

                        <!-- <h5>hello<i class="icon-down-arrow"></i></h5> -->
                        <!-- </div> -->
                        <h5 class="link-opt">Shared People List

                        </h5>
                    </div>
                </div>
                <div id="loadDiv">
                    @if(isset($emailurl))
                    @foreach($emailurl as $people)
                    <div class="row foot-row">
                        <!-- <div class="col-1">
                        <img src="{{asset('css/images/name.png')}}" class="avatar-image" />
                    </div> -->

                        <div class="col-9">
                            <h5 class="modal-title custom-modal-bottom" id="exampleModalLongTitle">
                                Email: {{$people->senderEmail}}
                                <!-- <br> -->
                                <!-- <span><img src="{{asset('css/images/g.globe.png')}}"
                                    class="custom-modal-bottom-image" /><span style="color:#F1C40F">
                                    Guest.</span>ahsanmemon@renesistech.com</span>
                            <br>Based
                            on Dataclu.com -->
                            </h5>
                        </div>
                        <form>
                            <!-- <input type="hidden" value="{{$people->uuid}}" name="u_id" /> -->
                            <div class="col-3">
                                <button class="btn btn-danger link-opt-2 btn-submit" id="{{$people->uuid}}">Delete
                                    <!-- <i class="icon-cross cus-arr"></i> -->
                                </button>
                            </div>
                        </form>

                    </div>
                    @endforeach
                    @endif
                </div>
               
                <div class="row foot-row-2">
                    <div class="col-9">
                        <h5 class="link-opt">
                            <i class='icon-info cu-btn-icon'></i>&nbsp;Learn about sharing
                        </h5>
                    </div>
                    <div class="col-3 c-link">
                        <button class="link-opt-copy btn-submit-2">
                            <i class='icon-link cu-btn-icon'></i>&nbsp;Copy Link
                        </button>
                    </div>

                </div>
                <!-- en -->
            </div>
        </div>
    </div>
</div>

<!-- Modal for Link-->
<div class="modal fade" id="exampleModalCenterLink" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header custom-modal-header">
                <div class="row">
                    <div class="col-2">
                        <img src="{{asset('css/images/globe.png')}}" />
                    </div>
                    <div class="col-10">
                        <h5 class="modal-title custom-modal-title" id="exampleModalLongTitle">Share to web<br>Anyone
                            with the link can
                            see<br>Based
                            on darkdata.cs</h5>
                    </div>

                </div>
                {{-- <button class="close" aria-label="Close">
                    <label class="switch">
                        <input type="checkbox" onchange="switchfunc()" id="on-off" checked>
                        <span class="slider round"></span>
                    </label>
                </button> --}}

            </div>

            <div class="modal-body">
                <div class="row">

                    <div class="col-12">
                        <!-- <div class="at-inputicon"> -->

                        <!-- <h5>hello<i class="icon-down-arrow"></i></h5> -->
                        <!-- </div> -->
                        <h3 class="link-opt-link">Generated Link
                            
                        </h3>
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        {{-- <div class="halfWidth">
                            <div class="input-wrapper"> --}}
                                <input type="text" class="input-form-link input-form-link-url" id="input-url-gen-link" />
                            </div>
                            {{-- <input type="hidden" name="anomalyIdsecond" value={{$callId}} /> --}}
                            {{-- <button type="submit" class="button-link btn-submit-2" id="button-url-gen"
                                style="font-size: 13px; !important">Create
                                Link</button> --}}

                        {{-- </div>
                    </div> --}}
                </div>
             
            </div>
            
        </div>
    </div>
</div>
{{-- Model End --}}

{{-- //Slack Model Started --}}
<div class="modal fade" id="exampleModalCenterSlack" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header custom-modal-header">
                <div class="row">
                    <div class="col-2">
                        <img src="{{asset('css/images/globe.png')}}" />
                    </div>
                    <div class="col-10">
                        <h5 class="modal-title custom-modal-title" id="exampleModalLongTitle">Share to web<br>Anyone
                            with the link can
                            see<br>Based
                            on darkdata.cs</h5>
                    </div>

                </div>
                {{-- <button class="close" aria-label="Close">
                    <label class="switch">
                        <input type="checkbox" onchange="switchfunc()" id="on-off" checked>
                        <span class="slider round"></span>
                    </label>
                </button> --}}

            </div>

            <div class="modal-body">
                <div class="row">

                    <div class="col-12">
                        <!-- <div class="at-inputicon"> -->

                        <!-- <h5>hello<i class="icon-down-arrow"></i></h5> -->
                        <!-- </div> -->
                        <h3 class="link-opt-link">Generated Link
                            
                        </h3>
                        
                    </div>
                </div>
                {{-- <form action="/send/slack/notification" method="POST">
                    @csrf --}}
                <div class="row">
                    <div class="col-12">
                                <input type="text" class="input-form-link input-form-link-url" name="slackUrl" id="input-url-gen-slack" disabled />
                            </div>
                </div>
                <div class="row">

                    <div class="col-12">
                        <h3 class="link-opt-link">Would you Like To Comment Anything!   
                        </h3>
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <textarea class="form-control custom-text-area" id="SlackComment" name="textSlackComment"  rows="4" cols="50">
                            
                            </textarea>
                            </div>
                     
                </div>
             
            </div>
            <div class="modal-footer">
        <button type="button" class="btn btn-secondary " data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary  send-slack-notf" data-dismiss="modal">Submit</button>
      </div>
      {{-- </form> --}}
            
        </div>
    </div>
</div>
{{-- Slack Model Ended --}}


<div class="content content-fixed">
    <div class="container">
        <!-- <ol class="breadcrumb df-breadcrumbs mg-b-10">
          <li class="breadcrumb-item"><a href="#">Components</a></li>
          <li class="breadcrumb-item active" aria-current="page">Introduction</li>
        </ol> -->
        <div class="row">
            <div class="col-10 col-xl-11">
                <!-- <h4 class="df-title pr-text">Product Detail Views Increased last Week</h4> -->
                <p class="df-lead head-date" id="titleDate"></p>
            </div>
            <div class="col-2 col-xl-1 box-width">
                <div class="shadow bg-white bd cu-bd-5 right-head">
                    <h5 class=" head-number" id="impact-score"></h5>
                    <p class="head-text">Impact</p>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        var anomId = '<?php echo $callId ;?>';
        </script>
        <div class="row tx-14">
            <div class="col-12">
                <div class=" shadow cu-bd-15 bg-white bd pd-20 pd-lg-30 d-flex flex-column justify-content-end">
                    <div class="at-charthead at-charthead-anomaly">
                        <h5 class=" headText" style="font-weight:normal !important" id="add-class"> Anomaly Details <span

                                id="anomalyName"></span></h5>
                    </div>
                    <div class="row">
                        <div class="col-3 vr-l">


                            <div class="at-chartheads" id="change-class">
                                <h5 class=" headText mg-b-20"> <span id="variance-value"></span> more than expected
                                    on <b id="headDate"></b></h5>
                            </div>
                            <div class="at-chartheadss">
                                <h5 class=" headText mg-b-20">
                                    <h6><b class="anomaly-values">Actual Value:</b><span id="actualValue">
                                        </span><br></h6>
                                    <h6><b>Expected
                                            Value:</b><span id="expectedValue"></span></h6>
                                    <!-- <h6><b>
                                            Page:</b><span id="pageValue" class="urlhover22"> </span> </h6> -->
                                    <h6 id="queryValue">
                                    {{-- <b>Query:</b><span id="queryValue">
                                        </span> --}}
                                        <!-- <br> -->
                                    </h6>
                                    <h6><b>Website: </b><span id="websiteValue"></span></h6>

                                    <!-- <h6><b>
                                            Country:</b><span id="countryValue"></span></h6> -->
                                </h5>
                                <!-- <h5 class=" headText mg-b-20"></h5> -->
                            </div>

                        </div>
                        <div class="col-3 vr-l">
                            <div id="chartdiv" class="pie-chart"></div>
                            <p class="pie-text">Covers <b>100%</b> of total Product Details View</p>
                        </div>
                        <div class="col-6">
                            <div id="chartdivs" class="pie-chart"></div>
                            <div class="float-right">
                                <a href="https://search.google.com/search-console/performance/search-analytics?resource_id=sc-domain%3Aguitarkitworld.com&query=!diy%20guitar%20kits&breakdown=page&page=!https%3A%2F%2Fguitarkitworld.com%2F" style="color: black"><p class="float-left">See On Search Console &nbsp;</p></a>
                                <a target="_blank" href="https://search.google.com/search-console/performance/search-analytics?resource_id=sc-domain%3Aguitarkitworld.com&query=!diy%20guitar%20kits&breakdown=page&page=!https%3A%2F%2Fguitarkitworld.com%2F"><img src="{{asset('images/external-link.png')}}" class="external-link-css float-left" /></a>
                                <p class="float-left">&nbsp; Is it useful? &nbsp;</p>
                                <a class="thumbs-up" id="thumb-up"  ><img src="{{asset('css/images/thumbs-up-black.png')}}" class="external-link-css float-left" data-toggle="modal" data-target="#exampleModalCenterFeedback" /></a>
                                <a class="thumbs-down" id="thumb-down"><img src="{{asset('css/images/thumbs-down-black.png')}}" class="external-link-css float-left" data-toggle="modal" data-target="#exampleModalCenterFeedback"  /></a>

                            
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- col-12 -->

            <div class="col-12 mg-t-20 mg-sm-t-25">
                <div class="row">
                    <div class="col-12">
                        <h2 class="action-line"><span>Related Metrices</span></h2>
                    </div>
                </div>
                <div id="contributors-list">

                </div>



            </div><!-- col-12 -->

            {{-- Code for related audience --}}
            <div class="col-12 mg-t-20 mg-sm-t-25">
                <div class="row">
                    <div class="col-6">
                        <h5 style="font-size: 20px; margin-top:20px; margin-bottom:20px"><span>Related Audience</span></h5>
                    </div>
                    <div class="col-6">
                        <h5 style="float: right; font-size:20px;  margin-top:20px; margin-bottom:20px">Total Audience: <span id="total-audience"></span></h5>
                    </div>
                </div>
                <div id="contributors-audience">

                </div>



            </div><!-- col-12 -->

             {{-- Code for page group--}}
              <div class="col-12 mg-t-20 mg-sm-t-25">
                <div class="row">
                    <div class="col-12">
                        <h2 class="action-line"><span>Page Group Anomalies</span></h2>
                    </div>
                </div>
                <div id="page-groups">

                </div>



            </div><!-- col-12 -->

            {{-- //Scatter charts --}}
            {{-- <div id="scatter-show-hide" style="display: block"> --}}
            <div class="col-12 mg-t-20 mg-sm-t-25" id="scatter-show-hide" style="display: none">
                <div class="row">
                    <div class="col-12">
                        <h2 class="action-line"><span>Scatter Charts</span></h2>
                    </div>
                </div>
                <div class="row">
                        <div class="col-6" id="chartContainerScatter1" style="height: 300px; width: 90%;">
                </div>
                    <div class="col-6" id="chartdivScatter2">
                      
                    </div>

                </div>




            </div><!-- col-12 -->
        {{-- </div> --}}
            {{-- Scatter Charts ends --}}

            <div class="col-12 mg-t-20 mg-sm-t-25">
                <div class="row">
                    <div class="col-12">
                        <h2 class="action-line"><span>Recommended Actions</span></h2>
                    </div>
                </div>
                <!-- <div id="contributors-list">

                </div> -->
                <!-- <div data-label="Example" class="df-example">
                    <div id="carouselExample2" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner row">
                            <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3  active">

                            </div>
                            <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">

                            </div>
                            <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">

                            </div>
                              <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3  active">

                            </div>
                            <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">

                            </div>
                            <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">

                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExample2" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"><i
                                    data-feather="chevron-left"></i></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExample2" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"><i
                                    data-feather="chevron-right"></i></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div> -->
                <!-- df-example -->
                <div class="top-content">
                    <div class="container">
                        <div id="carousel-example" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner row w-100 mx-auto" role="listbox">
                                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 active">
                                    <!-- <img src="{{asset('assets/img/adminlogin.jpg')}}" class="img-fluid mx-auto d-block"
                                        alt="img1"> -->
                                    {{-- <div data-label="Example" class="df-example"> --}}
                                        <div class="card ">
                                            <img src="{{asset('assets/img/a1.jpg')}}" class="card-img-top"
                                                style="height:80px;" alt="...">
                                            <img src="{{asset('assets/img/digi.jpg')}}" class="card-img-top above-img"
                                                alt="...">
                                            <div class="card-body aligning-center">
                                                <h5 class="card-title">Marketing & Content</h5>
                                                <p class="card-text">All about marketing , lead generation, content
                                                    marketing and growth hacks.</p>
                                                <a href="#" class="btn btn-primary btn-custom-blue btn-custom-blue"><img
                                                        src="{{asset('images/list.png')}}"
                                                        class="custom-favicon-list">Follow 358.2k</a>
                                            </div>
                                        </div>
                                    {{-- </div><!-- df-example --> --}}
                                </div>
                                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
                                    <!-- <img src="{{asset('assets/img/adminlogin.jpg')}}" class="img-fluid mx-auto d-block"
                                        alt="img2"> -->
                                    {{-- <div data-label="Example" class="df-example"> --}}
                                        <div class="card ">
                                            <img src="{{asset('assets/img/a2.jpg')}}" class="card-img-top"
                                                style="height:80px;" alt="...">
                                            <img src="{{asset('assets/img/tech.jpg')}}" class="card-img-top above-img"
                                                alt="...">
                                            <div class="card-body aligning-center">
                                                <h5 class="card-title">Marketing & Content</h5>
                                                <p class="card-text">All about marketing , lead generation, content
                                                    marketing and growth hacks.</p>
                                                <a href="#" class="btn btn-primary btn-custom-blue btn-custom-blue"><img
                                                        src="{{asset('images/list.png')}}"
                                                        class="custom-favicon-list">Follow 358.2k</a>
                                            </div>
                                        </div>
                                    {{-- </div><!-- df-example --> --}}
                                </div>
                                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
                                    <!-- <img src="{{asset('assets/img/adminlogin.jpg')}}" class="img-fluid mx-auto d-block"
                                        alt="img3"> -->
                                    {{-- <div data-label="Example" class="df-example"> --}}
                                        <div class="card ">
                                            <img src="{{asset('assets/img/a3.jpg')}}" class="card-img-top"
                                                style="height:80px;" alt="...">
                                            <img src="{{asset('assets/img/digi.jpg')}}" class="card-img-top above-img"
                                                alt="...">
                                            <div class="card-body aligning-center">
                                                <h5 class="card-title">Marketing & Content</h5>
                                                <p class="card-text">All about marketing , lead generation, content
                                                    marketing and growth hacks.</p>
                                                <a href="#" class="btn btn-primary btn-custom-blue btn-custom-blue"><img
                                                        src="{{asset('images/list.png')}}"
                                                        class="custom-favicon-list">Follow 358.2k</a>
                                            </div>
                                        </div>
                                    {{-- </div><!-- df-example --> --}}

                                </div>
                                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
                                    <!-- <img src="{{asset('assets/img/adminlogin.jpg')}}" class="img-fluid mx-auto d-block"
                                        alt="img4"> -->
                                    {{-- <div data-label="Example" class="df-example"> --}}
                                        <div class="card ">
                                            <img src="{{asset('assets/img/a4.jpg')}}" class="card-img-top"
                                                style="height:80px;" alt="...">
                                            <img src="{{asset('assets/img/tech.jpg')}}" class="card-img-top above-img"
                                                alt="...">
                                            <div class="card-body aligning-center">
                                                <h5 class="card-title">Marketing & Content</h5>
                                                <p class="card-text">All about marketing , lead generation, content
                                                    marketing and growth hacks.</p>
                                                <a href="#" class="btn btn-primary btn-custom-blue btn-custom-blue"><img
                                                        src="{{asset('images/list.png')}}"
                                                        class="custom-favicon-list">Follow 358.2k</a>
                                            </div>
                                        </div>
                                    {{-- </div><!-- df-example --> --}}
                                </div>
                                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
                                    <!-- <img src="{{asset('assets/img/adminlogin.jpg')}}" class="img-fluid mx-auto d-block"
                                        alt="img5"> -->
                                    {{-- <div data-label="Example" class="df-example"> --}}
                                        <div class="card ">
                                            <img src="{{asset('assets/img/a1.jpg')}}" class="card-img-top"
                                                style="height:80px;" alt="...">
                                            <img src="{{asset('assets/img/digi.jpg')}}" class="card-img-top above-img"
                                                alt="...">
                                            <div class="card-body aligning-center">
                                                <h5 class="card-title">Marketing & Content</h5>
                                                <p class="card-text">All about marketing , lead generation, content
                                                    marketing and growth hacks.</p>
                                                <a href="#" class="btn btn-primary btn-custom-blue btn-custom-blue"><img
                                                        src="{{asset('images/list.png')}}"
                                                        class="custom-favicon-list">Follow 358.2k</a>
                                            </div>
                                        </div>
                                    {{-- </div><!-- df-example --> --}}
                                </div>
                                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
                                    <!-- <img src="{{asset('assets/img/adminlogin.jpg')}}" class="img-fluid mx-auto d-block"
                                        alt="img6"> -->
                                    {{-- <div data-label="Example" class="df-example"> --}}
                                        <div class="card ">
                                            <img src="{{asset('assets/img/a2.jpg')}}" class="card-img-top"
                                                style="height:80px;" alt="...">
                                            <img src="{{asset('assets/img/tech.jpg')}}" class="card-img-top above-img"
                                                alt="...">
                                            <div class="card-body aligning-center">
                                                <h5 class="card-title">Marketing & Content</h5>
                                                <p class="card-text">All about marketing , lead generation, content
                                                    marketing and growth hacks.</p>
                                                <a href="#" class="btn btn-primary btn-custom-blue btn-custom-blue"><img
                                                        src="{{asset('images/list.png')}}"
                                                        class="custom-favicon-list">Follow 358.2k</a>
                                            </div>
                                        </div>
                                    {{-- </div><!-- df-example --> --}}
                                </div>
                                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
                                    <!-- <img src="{{asset('assets/img/adminlogin.jpg')}}" class="img-fluid mx-auto d-block"
                                        alt="img7"> -->
                                    {{-- <div data-label="Example" class="df-example"> --}}
                                        <div class="card ">
                                            <img src="{{asset('assets/img/a3.jpg')}}" class="card-img-top"
                                                style="height:80px;" alt="...">
                                            <img src="{{asset('assets/img/digi.jpg')}}" class="card-img-top above-img"
                                                alt="...">
                                            <div class="card-body aligning-center">
                                                <h5 class="card-title">Marketing & Content</h5>
                                                <p class="card-text">All about marketing , lead generation, content
                                                    marketing and growth hacks.</p>
                                                <a href="#" class="btn btn-primary btn-custom-blue btn-custom-blue"><img
                                                        src="{{asset('images/list.png')}}"
                                                        class="custom-favicon-list">Follow 358.2k</a>
                                            </div>
                                        </div>
                                    {{-- </div><!-- df-example --> --}}
                                </div>
                                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
                                    <!-- <img src="{{asset('assets/img/adminlogin.jpg')}}" class="img-fluid mx-auto d-block"
                                        alt="img8"> -->
                                    {{-- <div data-label="Example" class="df-example"> --}}
                                        <div class="card ">
                                            <img src="{{asset('assets/img/a4.jpg')}}" class="card-img-top"
                                                style="height:80px;" alt="...">
                                            <img src="{{asset('assets/img/tech.jpg')}}" class="card-img-top above-img"
                                                alt="...">
                                            <div class="card-body aligning-center">
                                                <h5 class="card-title">Marketing & Content</h5>
                                                <p class="card-text">All about marketing , lead generation, content
                                                    marketing and growth hacks.</p>
                                                <a href="#" class="btn btn-primary btn-custom-blue btn-custom-blue"><img
                                                        src="{{asset('images/list.png')}}"
                                                        class="custom-favicon-list">Follow 358.2k</a>
                                            </div>
                                        </div>
                                    {{-- </div><!-- df-example --> --}}
                                </div>
                            </div>
                            <!-- <a class="carousel-control-prev" href="#carousel-example" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a> -->
                            <a class="carousel-control-prev" href="#carousel-example" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon slider-custom-arrow" aria-hidden="true"><i
                                        data-feather="chevron-left"></i></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <!-- <a class="carousel-control-next" href="#carousel-example" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a> -->
                            <a class="carousel-control-next" href="#carousel-example" role="button" data-slide="next">
                                <span class="carousel-control-next-icon slider-custom-arrow" aria-hidden="true"><i
                                        data-feather="chevron-right"></i></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>



            </div><!-- col-12 -->
            @if (Auth::check())
            @role('user')
            <div class="col-12 mg-t-20 mg-sm-t-25">
                <div class="row">
                    <div class="col-12">
                        <h2 class="action-line"><span>Take Action</span></h2>
                        <h5 class="an-text"> Share the anomalies</h5>
                        @if(session('success'))
                        <h4>{{session('success')}}</h4>
                        @endif
                        @if(session('errors'))
                        <h4 style="color: red">{{session('errors')}}</h4>
                        @endif
                    </div>
                </div>
                <div class="row btn-sp">
                    <div class="col-2">
                        <button type="button" class="btn btn-primary btn-icon cu-btn btn-submit-3"><i
                                class='icon-link cu-btn-icon'></i><span class="cu-btn-text">Link</span></button>
                    </div>
                    @if($services)
                    @if($services->email == 1)
                    <div class="col-2">
                        <button type="button" class="btn btn-danger btn-icon cu-btn mail-model" 
                         {{-- data-toggle="modal"
                            data-target="#exampleModalCenter" --}}
                            ><i data-feather="mail" class="cu-btn-icon"></i><span
                                class="cu-btn-text">Email</span></button>
                    </div>
                    @endif
                    @else
                    <div class="col-2">
                        <button type="button" class="btn btn-danger btn-icon cu-btn mail-model" 
                         {{-- data-toggle="modal"
                            data-target="#exampleModalCenter" --}}
                            ><i data-feather="mail" class="cu-btn-icon"></i><span
                                class="cu-btn-text">Email</span></button>
                    </div>
                    @endif

                    {{-- <div class="col-2">
                        <button type="button" class="btn btn-success btn-icon cu-btn"><i
                                class='icon-whatsapp cu-btn-icon'></i><span class="cu-btn-text">Whatsapp</span></button>
                    </div>
                    <div class="col-2">
                        <button type="button" class="btn btn-skype btn-icon cu-btn"><i
                                class='icon-skype cu-btn-icon'></i><span class="cu-btn-text">Skype</span></button>
                    </div> --}}
                    @if($services)
                    @if($services->slack == 1)
                    <div class="col-2">
                        <button type="button" class="btn btn1 btn-stack btn-icon cu-btn btn-submit-slack">
                            {{-- <i
                                class='icon-stack cu-btn-icon'></i> --}}
                                <img src="{{asset('images/slack.png')}}" style="float: left;
    height: 18px;
    margin-top: 2px;" />
                                <span class="cu-btn-text">Slack</span></button>
                    </div>
                    @endif
                    @else
                    <div class="col-2">
                        <button type="button" class="btn btn1 btn-stack btn-icon cu-btn btn-submit-slack">
                            {{-- <i
                                class='icon-stack cu-btn-icon'></i> --}}
                                <img src="{{asset('images/slack.png')}}" style="float: left;
    height: 18px;
    margin-top: 2px;" />
                                <span class="cu-btn-text">Slack</span></button>
                    </div>
                    @endif
                    {{-- <div class="col-2">
                        <button type="button" class="btn btn-primary btn-icon cu-btn"><i
                                class='icon-link cu-btn-icon'></i><span class="cu-btn-text">Trello</span></button>
                    </div> --}}
                </div>

            </div><!-- col-12 -->
            <div class="col-12 mg-t-20 mg-sm-t-25">
                <div class="shadow cu-bd-5 bg-white bd pd-20 pd-lg-30  d-flex flex-column justify-content-end">
                    <!-- <div class="mg-b-25"><i datsa-feather="map-pin" class="wd-50 ht-50 tx-gray-500"></i></div> -->
                    <h5 class="tx-inverse mg-b-20">Comments</h5>
                    <div class="col-sm-12 mg-t-10">
                        <textarea class="form-control" id="mail-comment" rows="3" placeholder="Add a Comment"></textarea>
                        <button type="button" class="btn btn-primary btn-lg btn-send mail-model">Send</button>
                    </div>
                </div>
            </div>
            @endrole
            @endif

        </div><!-- row -->



    </div><!-- container -->
</div><!-- content -->

@endsection
@section('page-level-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/datejs/1.0/date.min.js"></script>
<!-- Resources -->
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>
{{-- Further added --}}
<script src="{{asset('lib/prismjs/prism.js')}}"></script>
<script src="{{asset('lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="{{asset('lib/select2/js/select2.min.js')}}"></script>
{{-- Further Added --}}

<script src="{{asset('js/anomaliessdetaiilssss.js')}}"></script>


<Script>
    var thumbsUpSelected = 0;
    var thumbsDownSelected = 0;
function switchfunc() {
    // Get the checkbox
    var checkBox = document.getElementById("on-off");
    // If the checkbox is checked, display the output text
    if (checkBox.checked == true) {
        // console.log('check');
        document.getElementById("input-url-gen").disabled = false;
        document.getElementById("button-url-gen").disabled = false;
        document.getElementById("input-url-share").disabled = false;
        document.getElementById("button-url-share").disabled = false;
        $('#loadDiv').find('button').attr('disabled', false);
        document.getElementById("input-url-gen").style.cursor = "text";
        document.getElementById("button-url-gen").style.cursor = "pointer";
        document.getElementById("input-url-share").style.cursor = "text";
        document.getElementById("button-url-share").style.cursor = "pointer";
        $('#loadDiv').find('button').css('cursor', "pointer");
    } else {
        // console.log('notcheck');
        document.getElementById("input-url-gen").disabled = true;
        document.getElementById("button-url-gen").disabled = true;
        document.getElementById("input-url-share").disabled = true;
        document.getElementById("button-url-share").disabled = true;
        $('#loadDiv').find('button').attr('disabled', true);
        document.getElementById("input-url-gen").style.cursor = "not-allowed";
        document.getElementById("button-url-gen").style.cursor = "not-allowed";
        document.getElementById("input-url-share").style.cursor = "not-allowed";
        document.getElementById("button-url-share").style.cursor = "not-allowed";
        // document.getElementById("loadDiv").style.cursor = "not-allowed";
        $('#loadDiv').find('button').css('cursor', "not-allowed");
    }
}
</script>
<script type="text/javascript">
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(".btn-submit").click(function(e) {

    e.preventDefault();

    var uid = $(this).attr("id");
    console.log(uid);
    $(this).closest('.row-foot').remove();
    // $("#loadDiv").load(" #loadDiv");

    $.ajax({
        type: 'POST',
        url: 'delete/share/url',
        data: {
            uid: uid,
        },
        success: function(data) {
            $("#loadDiv").load(" #loadDiv");
            console.log(data);
        }
    });


});
//create url

$(".btn-submit-2").click(function(e) {

    e.preventDefault();

    var aid = $("input[name=anomalyIdsecond]").val();
    console.log('aya');

    $.ajax({
        type: 'POST',
        url: 'create/share/url',
        data: {
            aid: aid,
        },
        success: function(data) {
            document.getElementById('input-url-gen').value = data['success'];
            // console.log(data);
        }
    });
});

    //create url link ajax call function

    $(".btn-submit-3").click(function(e) {

e.preventDefault();

var aid = $("input[name=anomalyIdsecond]").val();
// console.log('aya');

$.ajax({
    type: 'POST',
    url: 'create/share/url',
    // changes
    data: {
        aid: aid,
    },
    success: function(data) {
        // document.getElementById('input-url-gen').value = data['success'];
        // input-url-gen-link
        document.getElementById('input-url-gen-link').value = data['success'];
        $("#exampleModalCenterLink").modal('show');
    }
});


});

//Url generated for slack
 $(".btn-submit-slack").click(function(e) {

e.preventDefault();

var aid = $("input[name=anomalyIdsecond]").val();
// console.log('aya');

$.ajax({
    type: 'POST',
    url: 'create/share/url',
    // changes
    data: {
        aid: aid,
    },
    success: function(data) {
        // document.getElementById('input-url-gen').value = data['success'];
        // input-url-gen-link
        document.getElementById('input-url-gen-slack').value = data['success'];
        $("#exampleModalCenterSlack").modal('show');
    }
});
});
//Url generated for slack ended



//Send Slack
 $(".send-slack-notf").click(function(e) {

e.preventDefault();

var slackUrl = $("input[name=slackUrl]").val();
var slackComment = $('textarea#SlackComment').val();
// console.log('aya');

$.ajax({
    type: 'POST',
    url: 'send/slack/notification',
    // changes
    data: {
        slackUrl: slackUrl,
        slackComment: slackComment
    },
    success: function(data) {
        // document.getElementById('input-url-gen').value = data['success'];
        // input-url-gen-link
        // document.getElementById('input-url-gen-slack').value = data['success'];
        // $("#exampleModalCenterSlack").modal('show');
        console.log('data');
    }
});
});
//Send slack ended
//thumbs-up

$('.thumbs-up').on('click', function() {
    event.preventDefault();
    // console.log('click');
    $(".thumbs-up").empty();
    $(".thumbs-down").empty();
    document.getElementById('thumb-up').innerHTML = "<img src=\"{{asset('css/images/thumbs-up-blue.png')}}\" class=\"external-link-css float-left\" data-toggle=\"modal\" data-target=\"#exampleModalCenterFeedback\" />";
    document.getElementById('thumb-down').innerHTML = "<img src=\"{{asset('css/images/thumbs-down-black.png')}}\" class=\"external-link-css float-left\"  data-toggle=\"modal\" data-target=\"#exampleModalCenterFeedback\" />";

    //submitting response
  

    // $.ajax({
    //     type: 'POST',
    //     url: 'feedback',
    //     data: {
    //         uaid: anomId,
    //         userFeed: 1,
    //         insightData: apiData,
    //     },
    //     success: function(data) {
    //         // document.getElementById('input-url-gen').value = data['success'];
    //         // alert(data);
    //     }
    // });
    thumbsUpSelected = 1;
    thumbsDownSelected = 0;
});
// On button click mail fetch the textarea and value and open model
$(".mail-model").click(function(e) {

e.preventDefault();

var textValue = ($("#mail-comment").val());

$("#model-comment-textarea").val(textValue);
// document.getElementById('input-url-gen-link').value = data['success'];
// console.log(textValue);
$("#exampleModalCenter").modal('show');



});

</script>

<script>
$('#carousel-example').on('slide.bs.carousel', function(e) {
    /*
        CC 2.0 License Iatek LLC 2018 - Attribution required
    */
    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 5;
    var totalItems = $('.carousel-item').length;

    if (idx >= totalItems - (itemsPerSlide - 1)) {
        var it = itemsPerSlide - (totalItems - idx);
        for (var i = 0; i < it; i++) {
            // append slides to end
            if (e.direction == "left") {
                $('.carousel-item').eq(i).appendTo('.carousel-inner');
            } else {
                $('.carousel-item').eq(0).appendTo('.carousel-inner');
            }
        }
    }
});



</script>
{{-- // Disabled Button false --}}
<script type="text/javascript">
   $(document).ready(function () {
   
       $('#input-url-share').keyup(function () { 

        if ( (document.getElementById('input-url-share').value).indexOf("@") > -1 )
        {
        document.getElementById("button-url-share").disabled = false;
        document.getElementById("button-url-share").style.cursor = "pointer";
        // console.log('ok');
        }
        
        });
   });

   $(document).ready(function () {
   
   $('#input-url-share').mouseover(function () { 

    if ( (document.getElementById('input-url-share').value).indexOf("@") > -1 )
    {
    document.getElementById("button-url-share").disabled = false;
    document.getElementById("button-url-share").style.cursor = "pointer";
    // console.log('ok');
    }
    
    });
});



$('.thumbs-down').on('click', function() {
    event.preventDefault();
    // console.log('click');
    $(".thumbs-up").empty();
    $(".thumbs-down").empty();
    document.getElementById('thumb-down').innerHTML = "<img src=\"{{asset('css/images/thumbs-down-blue.png')}}\" class=\"external-link-css float-left\" data-toggle=\"modal\" data-target=\"#exampleModalCenterFeedback\" />";
    document.getElementById('thumb-up').innerHTML = "<img src=\"{{asset('css/images/thumbs-up-black.png')}}\" class=\"external-link-css float-left\" data-toggle=\"modal\" data-target=\"#exampleModalCenterFeedback\" />";

  
    // $.ajax({
    //     type: 'POST',
    //     url: 'feedback',
    //     data: {
    //         uaid: anomId,
    //         userFeed: 0,
    //         insightData: apiData,
    //     },
    //     success: function(data) {
    //         // document.getElementById('input-url-gen').value = data['success'];
    //         // alert(data);
    //     }
    // });
    thumbsUpSelected = 0;
    thumbsDownSelected = 1;
    
});


$('.feedback-comment').on('click', function() {
    event.preventDefault();
  
    var userComment = $('textarea#commentFormControlTextarea1').val();
//     if(userComment === undefined || userComment === null)
//     {
//  alert('Please insert comment');
// }
// else
// {
    // if(userComment.length != 0)
    // {
        // console.log(userComment);
        // console.log(thumbsDownSelected);
        // console.log(thumbsUpSelected);
        // console.log(apiData);
        if(thumbsUpSelected === 1)
        {
        $.ajax({
        type: 'POST',
        url: 'feedback/comment',
        data: {
            uaid: anomId,
            userComment: userComment,
            userFeed: 1,
            insightData: apiData,
        },
        success: function(data) {
            // document.getElementById('input-url-gen').value = data['success'];
            // alert(JSON.stringify(data));
        }
    });
    }
    if(thumbsDownSelected === 1)
    {
        $.ajax({
            type: 'POST',
            url: 'feedback/comment',
            data: {
                uaid: anomId,
                userComment: userComment,
                userFeed: 0,
                insightData: apiData,
            },
            success: function(data) {
                // document.getElementById('input-url-gen').value = data['success'];
                // alert(JSON.stringify(data));
            }
        });
    }
// }
// else
// {
    // alert('Please insert comment');
// }
// }
    
});
</script>

   

@endsection