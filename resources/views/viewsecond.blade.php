@extends('admin_layout.main')
@section('page-title')
User Dashboard
@endsection
@section('page-level-css')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    <style>
    #chartdiv 
    {
      width: 100%;
      height: 200px;
    }
    </style>
@endsection
@section('content')
 <div class="content content-fixed">
      <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0">

    
        
<div class="customwidth">

<div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
          <div>
          <h4 class="mg-b-0 tx-spacing--1 hc">Dark<span style="color:#4287f5 !important">Data</h4></h4>
          <h4 class="mg-b-0 tx-spacing--1 lhc">Good Morning, {{ auth()->user()->first_name }}</h4>
            <h4 class="mg-b-0 tx-spacing--1 slhc">Here what our DarkData Center has filter out the insights of your account.</h4>
           
          </div>
          <div class="d-none d-md-block">
          <input type="text" name="daterange" value="Jan 02, 2020 - Jan 04,2020" class="daterangecss" />
            
          <a href="#" onclick="gridview()"><img src="{{asset('images/grid.png')}}"  class="grid-img"  /></a>
            <!-- <button class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5"><i data-feather="sliders" class="wd-10 mg-r-5"></i> Settings</button> -->
          </div>
        </div>




  <div id="grid-div" style="">
        <div class="row row-xs">
         
            
          


             <!-- 1 -->
             <div class=" col-4 col-xs-4 col-sm-4 col-md-4 col-lg-3 col-xl-3 ">
            <div class="box">
            <!-- New Row For Image and Heading  -->
            <div class="row">
              <div class="col-2">
                  <img src="{{asset('images/ga.png')}}"  class="toolimg"  />
              </div>
              <div class="col-8">
                  <h5 class=" headText" > Clicks </h5>
              </div>
              <div class="col-2">
                  <img src="{{asset('images/msg.png')}}"  class="msgimg"  />
              </div>
            </div>

        <!-- New Row  -->
        <div class="row">
            <div class="col-2">
            </div>
            <div class="col-8">
                <h5 class="mg-b-15"><span class="dimension">Query: GuitarKits<br>Page: /blogs/resources/guitar-finish ...<br>Country: United States</span></h5>
            </div>
            <div class="col-2">
            </div>
        </div>
        
        <h5 class="mg-b- textalign daysText"><i> <img src="{{asset('images/greenup.png')}}" class="GreenUpImage"/></i><span class="percentcss">673K</span><img src="{{asset('images/greenarrowup.png')}}" class="GreenUpImage"/><span class="textLightthird"> 1875%</span> </h5>
        <div id="chartdivthird" class="customchartthird"  style="margin-bottom:10px" ></div>
          <!-- New Row -->
        <div class="row"> 
            <div class="col-8">
                <h5 class="mg-b-15  bottomText" > Sep 29, 2020 - Oct 5, 2020 <i class="arrow down"></i> </h5> 
            </div>
            <div class="col-4">
                <img src="{{asset('images/information.png')}}"  class="infoimg"  />
            </div>
        </div>

      </div>
      </div>

        </div><!-- row -->
  </div>


        <!-- Table View (Second View) -->

<div id="table-div" style="display:none;">
<table class="table border">
  <thead>
    <tr>
      <th>Value</th>
      <th>Metric</th>
      <th></th>
      <th>Date Range</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>3.28<br><h5 class=""><i> <img src="{{asset('images/drop.png')}}" class="table-dropImage"/></i><span class="table-percentcss">1%</span><span class="table-textLight"> (31350)</span> </h5></th>
      <td>     <!-- New Row For Image and Heading  -->
            <div class="row custom-row">
              <div class="col-1">
                  <img src="{{asset('images/ga.png')}}"  class="table-toolimg"  />
              </div>
              <div class="col-11">
                  <h5 class=" table-headText" > Avg Session Duration </h5>
              </div>
            </div>

        <!-- New Row  -->
        <div class="row custom-row">
            <div class="col-1">
            </div>
            <div class="col-11">
                <h5 class="mg-b-15"><span class="dimension">Query: GuitarKits<br>Page: /blogs/resources/guitar-finish ...<br>Country: United States</span></h5>
            </div>
        </div></td>
      <td><div id="tablechartdiv" class="customchartthird"  style="margin-bottom:10px" ></div></td>
      <td><h5 class="mg-b-30  daysText"> Last 30 Days <i class="arrow down"></i></h5></td>
      <td><img src="{{asset('images/information.png')}}"  class="infoimg"  /></td>
    </tr>
 
    
   
  </tbody>
</table>
  </div>




        </div>
      </div><!-- container -->
    </div><!-- content -->
@endsection
@section('page-level-js')
<!-- Resources -->
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>


  <!-- Single APi 4 Metrics Data -->
  <script src="{{asset('js/singlecallapi.js')}}"></script>

  




<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
  <script>
$(function() {
  $('input[name="daterange"]').daterangepicker({
    locale: {
            format: 'MMM DD, YYYY'
        },
    opens: 'left'
  }, function(start, end, label) {
    console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
  });
});
</script>
<script>
  function gridview()
  {
    var x = document.getElementById("table-div");
    var y = document.getElementById("grid-div");
  if (x.style.display === "none") {
    x.style.display = "block";
    y.style.display = "none";
  } else {
    x.style.display = "none";
    y.style.display = "block";
  }
  }
  </script>

@endsection